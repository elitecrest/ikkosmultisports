using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using System.Net.Http;
using System.Net.Http.Headers;

namespace WorkerMails
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("WorkerMails is running");

            try
            {
                SendMails();
                Thread.Sleep(TimeSpan.FromDays(1)); 
                Trace.TraceInformation("Working", "Information");

            }
            catch (Exception ex)
            { 
                throw;
            }

            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        private void SendMails()
        {
            HttpClient client = new HttpClient();

            string baseURL = "http://ikkosmultisportsapitest1.azurewebsites.net/";// CloudConfigurationManager.GetSetting("BaseURL").ToString();
            client.BaseAddress = new Uri(baseURL);
            string Api = "/Videos/GetChannelStatusForWorkerProcess";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response1 = client.GetAsync(Api).Result;
            if (response1.IsSuccessStatusCode)
            {
            }

        }
        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("WorkerMails has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("WorkerMails is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("WorkerMails has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }
    }
}
