﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="IKKOS.Worker" generation="1" functional="0" release="0" Id="da5beca9-1e69-400c-9cdd-1bf6c54dcdbc" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="IKKOS.WorkerGroup" generation="1" functional="0" release="0">
      <settings>
        <aCS name="WorkerMails:APPINSIGHTS_INSTRUMENTATIONKEY" defaultValue="">
          <maps>
            <mapMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/MapWorkerMails:APPINSIGHTS_INSTRUMENTATIONKEY" />
          </maps>
        </aCS>
        <aCS name="WorkerMails:BaseURL" defaultValue="">
          <maps>
            <mapMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/MapWorkerMails:BaseURL" />
          </maps>
        </aCS>
        <aCS name="WorkerMails:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/MapWorkerMails:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="WorkerMailsInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/MapWorkerMailsInstances" />
          </maps>
        </aCS>
      </settings>
      <maps>
        <map name="MapWorkerMails:APPINSIGHTS_INSTRUMENTATIONKEY" kind="Identity">
          <setting>
            <aCSMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/WorkerMails/APPINSIGHTS_INSTRUMENTATIONKEY" />
          </setting>
        </map>
        <map name="MapWorkerMails:BaseURL" kind="Identity">
          <setting>
            <aCSMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/WorkerMails/BaseURL" />
          </setting>
        </map>
        <map name="MapWorkerMails:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/WorkerMails/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapWorkerMailsInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/WorkerMailsInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="WorkerMails" generation="1" functional="0" release="0" software="E:\WorkingCopies\ikkosmultisports\IKKOS.Worker\csx\Release\roles\WorkerMails" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="APPINSIGHTS_INSTRUMENTATIONKEY" defaultValue="" />
              <aCS name="BaseURL" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;WorkerMails&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;WorkerMails&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/WorkerMailsInstances" />
            <sCSPolicyUpdateDomainMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/WorkerMailsUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/IKKOS.Worker/IKKOS.WorkerGroup/WorkerMailsFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="WorkerMailsUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="WorkerMailsFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="WorkerMailsInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
</serviceModel>