﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Net.Http;
using CopyMeSportsWebAPI.Models;
using CopyMeSportsWebAPI.ShardUtils;

namespace CopyMeSportsWebAPI.Controllers
{
    public class SportsController : ApiController
    {
        CustomResponse customResponse = new CustomResponse();
        // GET: Sports
        [System.Web.Http.HttpGet]
        public CustomResponse GetSports()
        {

            int uId = 10;
            try
            {
                //if (BaseUtils.didUniqueKeyExists(Request.Headers, out uId))
                //{
                var response = SportsModel.GetSports(uId);
                if (response != null && response.Count > 0)
                {
                    customResponse.Response = response;
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Message = Constants.SPORTS_RETRIEVED;
                }
                else
                {
                    customResponse.Response = null;
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                }
                //}
                //else
                //{
                //    customResponse.Status = CustomResponseStatus.UnSuccessful;
                //    customResponse.Message = Constants.GENERIC_NO_HEADER_MSG;
                //}
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = e.Message;
                customResponse.Response = null;
            }
            return customResponse;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetLibraryBySportId(int sportId)
        {
            int uId = 0;
            //
            try
            {
                //if (BaseUtils.didUniqueKeyExists(Request.Headers, out uId))
                //{
                var response = SportsModel.GetLibraryById(sportId);
                if (response != null && response.Count > 0)
                {
                    customResponse.Response = response;
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Message = Constants.LIBRARIES_RETRIEVED;
                }
                else
                {
                    customResponse.Response = null;
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                }
                //}
                //else
                //{
                //    customResponse.Status = CustomResponseStatus.UnSuccessful;
                //    customResponse.Message = Constants.GENERIC_NO_HEADER_MSG;
                //}
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = e.Message;
                customResponse.Response = null;
            }
            return customResponse;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetChannelsBySportId(int sportId)
        {
            int userId = 0;
            RatingsController rating = new RatingsController();
            try
            {
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    List<Libraries> libraries = new List<Libraries>();
                    libraries = SportsModel.GetChannelById(sportId);
                    foreach (var i in libraries)
                    {
                        var ratings = RatingsModel.GetRatingsForLibrary(i.LibraryId, userId);
                        if (ratings.ToList().Count > 0)
                        {
                            double avgRate = rating.CalculateAvgRating(ratings);
                            i.AverageRating = avgRate;
                        }
                    }

                    if (libraries != null && libraries.Count > 0)
                    {
                        customResponse.Response = libraries;
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Message = Constants.LIBRARIES_RETRIEVED;
                    }
                    else
                    {
                        customResponse.Response = null;
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Message = Constants.NO_RECORDS;
                    }
                }
                //}
                //else
                //{
                //    customResponse.Status = CustomResponseStatus.UnSuccessful;
                //    customResponse.Message = Constants.GENERIC_NO_HEADER_MSG;
                //}
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = e.Message;
                customResponse.Response = null;
            }
            return customResponse;
        }


        [System.Web.Http.HttpGet]
        public CustomResponse GetChannelStatus(int channelId,int sportId)
        {
            int channelStatus = 1;
            //

            try
            {

                channelStatus = SportsModel.GetChannelStatus(channelId, sportId);

                if (channelStatus > 0)
                {
                    customResponse.Response = channelStatus;
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Message = Constants.GENERIC_SUCCESS_MSG;
                }
                else
                {
                    customResponse.Response = 1;
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.GENERIC_FAILURE_MSG;
                }

            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = e.Message;
                customResponse.Response = null;
            }
            return customResponse;
        }

        public List<Sports> GetSports1()
        {

            int uId = 10;
            List<Sports> response = new List<Sports>();
            try
            {
                //if (BaseUtils.didUniqueKeyExists(Request.Headers, out uId))
                //{
                response = SportsModel.GetSports(uId);
                if (response != null && response.Count > 0)
                {
                    return response;
                }
                //}
                //else
                //{
                //    customResponse.Status = CustomResponseStatus.UnSuccessful;
                //    customResponse.Message = Constants.GENERIC_NO_HEADER_MSG;
                //}
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return response;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetChannelDetailsByChannelId(int channelId)
        {
            int uId = 0;
            //
            try
            {

                var response = SportsModel.GetChannelDetailsByChannelId(channelId);
                if (response != null)
                {
                    response.SportsInfo = SportsModel.GetSportsListByChannelId(channelId);
                    customResponse.Response = response;
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Message = Constants.CHANNELS_RETRIEVED;
                }
                else
                {
                    customResponse.Response = null;
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                }

            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = e.Message;
                customResponse.Response = null;
            }
            return customResponse;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse AddSportToChannel(int channelId, int sportId)
        {
            try
            {
                var response = SportsModel.AddSportsToChannel(channelId, sportId);
                if (response != null && response > 0)
                {
                    customResponse.Response = response;
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Message = Constants.SPORT_ADDED;
                }
                else
                {
                    customResponse.Response = null;
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.SPORT_NOT_ADDED;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = e.Message;
                customResponse.Response = null;
            }
            return customResponse;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetSportsByChannel(int channelId)
        {
            try
            {
                var response = SportsModel.GetSportsListByChannelId(channelId);
                if (response != null)
                {
                    customResponse.Response = response;
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Message = Constants.SPORT_INFO_RETRIEVED;
                }
                else
                {
                    customResponse.Response = null;
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.SPORT_NOT_FOUND;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = e.Message;
                customResponse.Response = null;
            }
            return customResponse;
        }

    }
}