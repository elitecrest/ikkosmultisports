﻿using CopyMeSportsWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CopyMeSportsWebAPI.ShardUtils;

namespace CopyMeSportsWebAPI.Controllers
{
    public class PaymentController : ApiController
    {
        CustomResponse Result = new CustomResponse();
        // GET: Payment
        [System.Web.Http.HttpPost]
        public CustomResponse AddSubscription(PaymentDTO payment)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var packages = PaymentsModel.AddSubscription(payment, userId);
                        if (packages > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = packages;
                            Result.Message = Constants.SUBSCRIBE_SUCCESS;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.SUBSCRIBE_UNSUCCESS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse IsPaidChannel(int channelId, int sportId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var result = PaymentsModel.IsPaidChannel(channelId, userId, sportId);
                        if (result)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = result;
                            Result.Message = Constants.CHANNEL_PAID;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Response = result;
                            Result.Message = Constants.CHANNEL_NOT_PAID;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse IsPaidPackage(int packageId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var result = PaymentsModel.IsPaidPackage(packageId, userId);
                        if (result)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = result;
                            Result.Message = Constants.PACKAGE_PAID;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Response = result;
                            Result.Message = Constants.PACKAGE_NOT_PAID;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
    }
}