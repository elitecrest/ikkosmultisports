﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Net.Http;
using CopyMeSportsWebAPI.Models;
using CopyMeSportsWebAPI.ShardUtils;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System.Net;

namespace CopyMeSportsWebAPI.Controllers
{
    public class VideosController : ApiController
    {
        CustomResponse Result = new CustomResponse();
        // GET: Videos
        // #region [HttpGet] Methods
        [System.Web.Http.HttpGet]
        public CustomResponse AllVideos()
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.AllVideos(userId);


                        if (videos.Count > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetVideo(int videoId, string userName)
        {
            try
            {
                //if (UsersDAL.CheckAdminByEmail(userName))
                VideosInfoDTO video = VideosModel.GetVideo(videoId);

                if (video.ID > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = video;
                    Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse VideoOfTheDay(int categoryId)
        {
            CustomResponse res = new CustomResponse();
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    int DeviceId = 0;

                    //var headers = System.Web.HttpContext.Current.Request.Headers;

                    //if (headers.AllKeys.Contains("APPID"))
                    //{

                    //    string AppId = headers.GetValues("APPID").First();
                    //    string[] arryAppId = AppId.Split('_');
                    //    DeviceId = Convert.ToInt32(arryAppId[1]);
                    //}
                    //else if (headers.AllKeys.Contains("AppID"))
                    //{

                    //    string AppId = headers.GetValues("AppID").First();
                    //    string[] arryAppId = AppId.Split('_');
                    //    DeviceId = Convert.ToInt32(arryAppId[1]);
                    //}
                    var videooftheday = VideosModel.GetVideoOfTheDay(userId, categoryId, DeviceId);
                    if (videooftheday != null)
                    {
                        Result.Status = CustomResponseStatus.Successful;
                        Result.Response = videooftheday;
                        Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                    }
                    else
                    {
                        Result.Status = CustomResponseStatus.UnSuccessful;
                        Result.Message = Constants.NO_RECORDS;
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; // javaScriptSerializer.Serialize(Result);
            }
        }
        // #endregion

        [System.Web.Http.HttpGet] // This api was not using now.
        public CustomResponse LibraryIntroVideo(int libraryId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.GetLibraryIntroVideos(libraryId, userId);
                        if (videos != null)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }

        [System.Web.Http.HttpGet]
        public CustomResponse ChannelIntroVideo(int libraryId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.GetChannelIntroVideos(libraryId, userId);
                        if (videos != null)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }

        [System.Web.Http.HttpGet]
        public CustomResponse PackageIntroVideos(int libraryId, bool isPaidChannel, int sportId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.GetPackageIntroVideos(libraryId, userId, isPaidChannel, sportId);
                        if (videos.Count > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetAllPackageVideos(int packageId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.GetAllPackagesVideos(packageId, userId);
                        if (videos.Count > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpPost]
        public CustomResponse AddOrUpdateAdminVideo(VideoInfoDTO videoInfo)
        {
            try
            {
                int count = 0;
                if (videoInfo != null && videoInfo.Id > 0)
                {
                    count = VideosModel.EditAdminVideo(videoInfo);
                    if (count > 0)
                    {
                        Result.Status = CustomResponseStatus.Successful;
                        Result.Response = count;
                        Result.Message = Constants.VIDEO_UPDATED;
                    }
                    else
                    {
                        Result.Status = CustomResponseStatus.UnSuccessful;
                        Result.Message = Constants.VIDEO_NOT_UPDATED;
                    }
                }
                else
                {
                    count = VideosModel.AddAdminVideo(videoInfo);
                    if (count > 0)
                    {
                        Result.Status = CustomResponseStatus.Successful;
                        Result.Response = count;
                        Result.Message = Constants.VIDEO_ADDED;
                    }
                    else
                    {
                        Result.Status = CustomResponseStatus.UnSuccessful;
                        Result.Message = Constants.VIDEO_NOT_ADDED;
                    }
                }

            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetAdminVideos(int adminUserId, int sportId)
        {
            try
            {
                var videos = VideosModel.GetVideos(adminUserId, sportId);
                if (videos.Count > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = videos;
                    Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.NO_RECORDS;
                }

            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpDelete]
        public CustomResponse DeleteVideo(int videoId)
        {
            try
            {
                var video = VideosModel.DeleteVideo(videoId);
                if (video > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = video;
                    Result.Message = Constants.DELETE_VIDEO_SUCCESS;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.DELETE_VIDEO_UNSUCCESS;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpPost]
        public CustomResponse UploadVideo(RecordVideoDTO videos)
        {
            try
            {
                var res = VideosModel.UploadVideo(videos);
                if (res > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = res;
                    Result.Message = Constants.VIDEO_ADDED;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.VIDEO_NOT_ADDED;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet] // This api was not using now.
        public CustomResponse GetLibraryVideos()
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var videos = VideosModel.GetLibraryVideos(userId);
                        if (videos.Count > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = videos;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }

        public CustomResponse createSpamVideo(SpamVideoDto spamVideoDto)
        {
            try
            {
                var res = VideosModel.createSpamVideo(spamVideoDto);
                if (res > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = res;
                    Result.Message = Constants.VIDEO_ADDED;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.VIDEO_ALREADY_SPAMED;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse DeleteLibraryVideo(int LibraryVideoId)
        {
            try
            {
                var video = VideosModel.DeleteLibraryVideo(LibraryVideoId);
                if (video > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = video;
                    Result.Message = Constants.DELETE_VIDEO_SUCCESS;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.DELETE_VIDEO_UNSUCCESS;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
        [System.Web.Http.HttpGet]
        public CustomResponse GetChannelStatusForWorkerProcess()
        {
            try
            {
                List<ChannelDTO> Channels = SubAdminModel.GetAllChannels();
                if (Channels.Count > 0)
                {
                    foreach (ChannelDTO i in Channels)
                    {
                        //Get All Sports for a channel
                        //For each Sport Check the condition and mail and get the sport name 
                        List<MultiSports> TotalSportsInChannel = SportsModel.GetSportsListByChannelId(i.ChannelId);
                        if (TotalSportsInChannel.Count > 0)
                        {
                            foreach (MultiSports j in TotalSportsInChannel)
                            {
                                var workerdto = VideosModel.GetChannelStatus(i.ChannelId, j.SportId);
                                if (workerdto.VideoStatus == 1)
                                {
                                    SendMailVideos(i.Email, j.SportName,i.SubAdmins);
                                }
                                if (workerdto.ModuleStatus == 1)
                                {
                                    SendMailModules(i.Email, j.SportName,i.SubAdmins);
                                }
                            }
                        }
                    }
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Message = Constants.GENERIC_SUCCESS_MSG;
                }
                else
                {
                    Result.Response = null;
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_FAILURE_MSG;
                }
            }
            catch (Exception e)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = e.Message;
                Result.Response = null;
            }
            return Result;
        }
        public static int SendMailVideos(string email, string SportName,List<SubAdminDTO> subadmins)
        {

            string Message = string.Empty;
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/VerifyEmailForVideos.html"));
                var readFile = reader.ReadToEnd();
                string myString = readFile;
                myString = myString.Replace("$$Name$$", email);
                myString = myString.Replace("$$SportName$$", SportName);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + email + ""));
                Msg.CC.Add(new MailAddress("rlam@ikkos.com"));
                foreach(var i in subadmins)
                {
                    Msg.CC.Add(new MailAddress(i.EmailAddress));
                }
                // Msg.CC.Add(new MailAddress("lohitm@elitecrest.com"));
                Msg.Subject = " Notification From IKKOS";
                Msg.Body = myString;
                Msg.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"],
                            ConfigurationManager.AppSettings["Password1"]),
                    Timeout = 1000000
                };
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                smtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                UsersModel.AddException(Message);
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }

        }


        public static int SendMailModules(string email, string SportName,List<SubAdminDTO> subadmins)
        {
            string Message = string.Empty;
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/VerifyEmailForModules.html"));
                var readFile = reader.ReadToEnd();
                string myString = readFile;
                myString = myString.Replace("$$Name$$", email);
                myString = myString.Replace("$$SportName$$", SportName);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + email + ""));
                Msg.CC.Add(new MailAddress("rlam@ikkos.com"));
                //Msg.CC.Add(new MailAddress("lohitm@elitecrest.com"));
                foreach (var i in subadmins)
                {
                    Msg.CC.Add(new MailAddress(i.EmailAddress));
                }
                Msg.Subject = " Notification From IKKOS";
                Msg.Body = myString;
                Msg.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"],
                            ConfigurationManager.AppSettings["Password1"]),
                    Timeout = 1000000
                };
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                smtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                UsersModel.AddException(Message);
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }

        }
    }
}