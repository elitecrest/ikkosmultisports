﻿using CopyMeSportsWebAPI.Models;
using CopyMeSportsWebAPI.ShardUtils;
using System;
using System.Web.Http;
using System.IO;
using System.Net.Mail;
using System.Configuration;
using System.Web;
using System.Net;

namespace CopyMeSportsWebAPI.Controllers
{
    public class UsersController : ApiController
    {
        CustomResponse customResponse = new CustomResponse();

        [HttpPost]
        public CustomResponse login(UserInfoDto userInfoDto)
        {
            try
            {
                if (userInfoDto.LoginType == 1)
                {
                    int flag;
                    var response = UsersModel.CheckUserExists(userInfoDto, out flag);
                    if (flag == 1)
                    {
                        var result = UsersModel.GetUserInformation(userInfoDto.UserId);
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = result;
                        customResponse.Message = Constants.USER_RETRIEVED;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.Checked;
                        customResponse.Response = response;
                        customResponse.Message = Constants.USER_CHECKED;
                    }
                }
                else if(userInfoDto.LoginType == 2)
                {
                    int result;
                    int flag;
                    var response = UsersModel.CheckUserExists(userInfoDto, out flag);
                    if(flag > 0)
                    {
                        userInfoDto.UserId = UsersModel.MobileUserExits(userInfoDto.email, userInfoDto.Password, response.UserId, response.LoginType, out result);
                        if (result > 0)
                        {
                            customResponse.Status = CustomResponseStatus.Successful;
                            customResponse.Response = UsersModel.GetUserInformation(userInfoDto.UserId);
                            customResponse.Message = Constants.VALIDATED;
                        }
                        else if (result == -2)
                        {
                            customResponse.Status = CustomResponseStatus.UnSuccessful;
                            customResponse.Response = "";
                            customResponse.Message = Constants.PASSWORD_MISMATCH;
                        }
                        else if (result == -3)
                        {
                            customResponse.Status = CustomResponseStatus.UnSuccessful;
                            customResponse.Response = "";
                            customResponse.Message = "Looks like you used this Email address to login using 'Facebook' already, please use same login or use a different ID";
                        }
                        else
                        {
                            customResponse.Status = CustomResponseStatus.UnSuccessful;
                            customResponse.Response = "";
                            customResponse.Message = Constants.USER_NOT_EXISTS;
                        }
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Response = "";
                        customResponse.Message = Constants.USER_NOT_EXISTS;
                    }
                }

            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }


        [HttpPost]
        public CustomResponse CreateUser(UserInfoDto userInfoDto)
        {
            try
            {
                if (userInfoDto.LoginType == 1)
                {
                    userInfoDto.UserId = UsersModel.AddUserIntoShardManager(userInfoDto.email, userInfoDto.DeviceId);
                    var response = UsersModel.checkForUserAndInsert(userInfoDto);
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.USER_CREATION_SUCCESS_MSG;
                }
                else if (userInfoDto.LoginType == 2)
                {
                    int alreadyExists;
                    userInfoDto.ProfilePicUrl = userInfoDto.ProfilePicUrl != null ? UsersModel.UploadImageFromBase64(userInfoDto.ProfilePicUrl, "png") : string.Empty;
                    userInfoDto.UserId = UsersModel.CheckUserInShardManager(userInfoDto.email, userInfoDto.DeviceId, out alreadyExists);
                    if(alreadyExists > 0)
                    {
                        var response = UsersModel.checkForUserAndInsert(userInfoDto);
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = response;
                        customResponse.Message = Constants.USER_CREATION_SUCCESS_MSG;
                    }
                    else
                    {
                        var res = UsersModel.CheckEmailExists(userInfoDto.email, userInfoDto.LoginType, userInfoDto.UserId);
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Response = null;
                        customResponse.Message = res == 0 ? Constants.USER_EXISTS : Constants.EMAIL_EXISTS;
                    }
                }
                else if (userInfoDto.LoginType == 5)
                {
                    //userInfoDto.ProfilePicUrl = userInfoDto.ProfilePicUrl != null ? UsersModel.UploadImageFromBase64(userInfoDto.ProfilePicUrl, "png") : string.Empty;
                    userInfoDto.UserId = UsersModel.CheckDeviceInShardManager(userInfoDto.DeviceId);
                    if (userInfoDto.UserId > 0)
                    {
                        var response = UsersModel.checkForUserAndInsert(userInfoDto);
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = response;
                        customResponse.Message = Constants.USER_CREATION_SUCCESS_MSG;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Response = null;
                        customResponse.Message = Constants.USER_EXISTS;
                    }
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
        /// <summary>
        /// Getting the User Information based on the User ID
        /// </summary>
        /// <param name="id"> Unique user id</param>
        /// <returns> Success with the required JSON or Unsuccess </returns>
        [HttpGet]
        public CustomResponse GetUserInfo(int id)
        {
            try
            {
                int uId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out uId))
                {

                    customResponse.Response = UsersModel.GetUserInformation(uId);
                    if (customResponse.Response != null)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Message = Constants.USER_RETRIEVED;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Message = Constants.NO_RECORDS;
                    }
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                customResponse.Response = null;
            }
            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetAllUsers()
        {

            var response = UsersModel.GetAllUsers();
            if (response != null && response.Count > 0)
            {
                customResponse.Status = CustomResponseStatus.Successful;
                customResponse.Response = response;
                customResponse.Message = Constants.GENERIC_SUCCESS_MSG;
            }
            else
            {
                customResponse.Status = CustomResponseStatus.UnSuccessful;
                customResponse.Response = null;
                customResponse.Message = Constants.GENERIC_FAILURE_MSG;
            }
            return customResponse;
        }

        /// <summary>
        /// Moved this code to model file
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //public UserInfoDto createUserInfoCommandAndExecute(int id)
        //{
        //    UserInfoDto userInfoDto = null;
        //    SqlCommand sqlCommand = new SqlCommand("[spGetUser]");
        //    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

        //    sqlCommand.Parameters.AddWithValue("@Id", id);
        //    SqlDataReader reader;

        //    BaseUtils.executeReaderCommandOnSingleShard(id, sqlCommand, out reader);
        //    while (reader != null && reader.Read())
        //    {
        //        userInfoDto = new UserInfoDto();
        //        userInfoDto.UserId = reader["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserId"]);
        //        userInfoDto.Name = (reader["Name"] == DBNull.Value || reader["Name"] == string.Empty)
        //                    ? string.Empty
        //                    : Convert.ToString(reader["Name"]);
        //        userInfoDto.LibraryId = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]);
        //        userInfoDto.email = (reader["email"] == DBNull.Value || reader["email"] == string.Empty)
        //                    ? string.Empty
        //                    : Convert.ToString(reader["email"]);
        //        userInfoDto.ProfilePicUrl = (reader["ProfilePicUrl"] == DBNull.Value || reader["ProfilePicUrl"] == string.Empty)
        //                    ? string.Empty
        //                    : Convert.ToString(reader["ProfilePicUrl"]);
        //        userInfoDto.FacebookId = (reader["FacebookId"] == DBNull.Value || reader["FacebookId"] == string.Empty)
        //                    ? string.Empty
        //                    : Convert.ToString(reader["FacebookId"]);
        //    }
        //    return userInfoDto;
        //}

        /// <summary>
        /// Moved this code to Model file
        /// </summary>
        /// <param name="userInfoDto"></param>
        //public void checkForUserAndInsert(UserInfoDto userInfoDto)
        //{
        //    SqlConnection sqlConnection = SqlDatabaseUtils.connectToMapManagerDB();
        //    SqlCommand sqlCommand = new SqlCommand("[spCheckForUserAndInsert]", sqlConnection);
        //    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //    SqlParameter usrname = sqlCommand.Parameters.Add("@emailId", System.Data.SqlDbType.NVarChar, 255);
        //    usrname.Direction = System.Data.ParameterDirection.Input;
        //    usrname.Value = userInfoDto.email;
        //    sqlConnection.Open();
        //    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //    while (sqlDataReader != null && sqlDataReader.Read())
        //    {
        //        userInfoDto.UserId = sqlDataReader["Id"] == DBNull.Value ? 0 : sqlDataReader["Id"];
        //    }
        //    BaseUtils.AddUserInShard(userInfoDto);
        //}

        [HttpPost]
        public CustomResponse RegisterDevice(DeviceInfoDTO deviceInfoDto)
        {

            try
            {
                int uId = 0;
              
                    var currentDevice = UsersModel.RegisterDevice(deviceInfoDto);

                    if (currentDevice != null)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Message = Constants.DEVICE_REGISTERED;
                        customResponse.Response = currentDevice;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Message = Constants.NO_RECORDS;
                    customResponse.Message = Constants.DEVICE_NOT_REGISTERED;
                }
                  
                return customResponse;
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                return customResponse;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public CustomResponse GetSkipLoginInfo(int deviceId)
        {
            try
            {
                int flag; 

                 var currentDevice = UsersModel.CheckDeviceRegistered(deviceId, out flag);

                if (flag == 1)
                {
                    var res = UsersModel.GetUserInformation(currentDevice);
                    if(res != null)                   
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Message = Constants.DEVICE_INFO;
                        customResponse.Response = res;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Response = res;
                        customResponse.Message = Constants.NO_RECORDS;
                    }
                   
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.Checked;
                    customResponse.Response = "";
                    customResponse.Message = Constants.USER_CHECKED;
                }

                return customResponse;
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                return customResponse;// javaScriptSerializer.Serialize(Result);
            }
        }
        [HttpPost]
        public CustomResponse UserFeedback(FeedbackDTO fb)
        {
            //algoritham 1. we need to search for corresponding email id and prepare message and send it to related email.
            //  CustomResponse res = new Utility.CustomResponse();

            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    var result = UsersModel.AddFeedback(fb, userId);
                    if (result > 0)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Message = Constants.FEEDBACK;
                        customResponse.Response = null;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Message = Constants.GENERIC_FAILURE_MSG;
                        customResponse.Response = null;
                    }

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
                return customResponse;
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                return customResponse;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public CustomResponse UpdateSportId(int sportId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    var result = UsersModel.UpdateLibraryInfo(userId, sportId);

                    if (result != null)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = result;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Message = Constants.NO_RECORDS;
                    }
                    // todo: add other conditions
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
                return customResponse;
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                return customResponse;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpPost]
        public CustomResponse UserRegistration(AdminUsersDto UsersDto)
        {
            try
            {
                int flag;
                var response = UsersModel.UserRegistration(UsersDto, out flag);
                if (flag > 0)
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = null;
                    customResponse.Message = Constants.USER_EXISTS;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.USER_CREATION_SUCCESS_MSG;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse UserLogin(string emailAddress, string password, int ChannelId)
        {
            try
            {
                var usersCount = UsersModel.UserExits(emailAddress, password, ChannelId);
                if (usersCount > 0)
                {
                    var res = UsersModel.GetAdminUserInformation(emailAddress, password);
                    res.SportsInfo = SportsModel.GetSportsListByChannelId(res.Id);
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = res;
                    customResponse.Message = Constants.VALIDATED;
                }
                else if (usersCount == -2)
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = "";
                    customResponse.Message = Constants.PASSWORD_MISMATCH;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = "";
                    customResponse.Message = Constants.USER_NOT_EXISTS;
                }

                return customResponse;
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                return customResponse;
            }
        }

        [HttpGet]
        public CustomResponse ForgotPassword(string emailAddress)
        {

            try
            {
                string code = UsersModel.GetUserExists(emailAddress);
                if (code != string.Empty)
                {
                    string token = code;

                    //need to send a mail to user

                    if (SendMailForForgotPassword(emailAddress, token) == 0)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Message = Constants.FORGOT_PASSWORD_SUCCESSFULLY;
                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Message = string.Empty;

                        //"Failed to Send Email, Please try again later";
                    }
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.USER_DOES_NOT_EXIST;
                }
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                return customResponse;// javaScriptSerializer.Serialize(_result);
            }

            return customResponse;
        }

        public static int SendMailForForgotPassword(string username, string token)
        {
            string Message = string.Empty;
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ResetPassword.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                myString = myString.Replace("$$Token$$", token);
                //   myString = myString.Replace("$$Password$$", password);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + username + ""));
                Msg.Subject = " Password Reset Requested";
                Msg.Body = myString;
                Msg.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"],
                            ConfigurationManager.AppSettings["Password1"]),
                    Timeout = 1000000
                };
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                smtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                UsersModel.AddException(Message);
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }

        }

        [HttpGet]
        public CustomResponse ResetPassword(string userName, string token, string confirmPassword)
        {
            CustomResponse res = new CustomResponse();
            try
            {
                //isReset = WebSecurity.ResetPassword(token, confirmPassword);
                string email = UsersModel.UpdateAdminPassword(userName, token, confirmPassword);
                if (email != string.Empty)
                {
                    res.Status = CustomResponseStatus.Successful;
                    res.Message = Constants.RESET_PWD_SUCCESS;

                }
                else
                {
                    res.Status = CustomResponseStatus.UnSuccessful;
                    res.Message = Constants.RESET_PWD_FAILED;
                }
            }
            catch (Exception ex)
            {
                res.Status = CustomResponseStatus.Exception;
                res.Message = ex.Message;
                return res;// javaScriptSerializer.Serialize(_result);
            }
            return res;
        }

        [HttpPost]
        public CustomResponse UpdateChannelInfo(AdminUsersDto UsersDto)
        {
            try
            {
                //int flag;
                var response = UsersModel.UpdateChannelInfo(UsersDto);
                if (response != null)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.CHANNEL_UPDATED_SUCCESS;
                    
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = null;
                    customResponse.Message = Constants.CHANNEL_NOT_UPDATED;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse UsageTracking(int channelId)
        {
            try
            {
                var response = UsersModel.UsageTracking(channelId);
                if (response != null)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.USAGE_INFO;
                    
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = null;
                    customResponse.Message = Constants.USAGE_INFO_NO;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpPost]
        public CustomResponse FeatureTracking(FeatureTrackingDto featuresDto)
        {
            try
            {
                var response = UsersModel.FeatureTracking(featuresDto);
                if (response > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FEATURE_SAVED;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = null;
                    customResponse.Message = Constants.FEATURE_NOT_SAVED;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }


        [HttpGet]
        public CustomResponse GetVideographersList()
        { 
            var response = UsersModel.GetVideographersList();

            if (response != null && response.Count > 0)
            {
                customResponse.Status = CustomResponseStatus.Successful;
                customResponse.Response = response;
                customResponse.Message = Constants.GENERIC_SUCCESS_MSG;
            }
            else
            {
                customResponse.Status = CustomResponseStatus.UnSuccessful;
                customResponse.Response = null;
                customResponse.Message = Constants.GENERIC_FAILURE_MSG;
            }
            return customResponse;
        }



        [HttpPost]
        public CustomResponse AddVideographer(VideographersDTO videographer)
        {
            try
            { 
                var response = UsersModel.AddVideographer(videographer);
                if (response <= 0)
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = null;
                    customResponse.Message = Constants.VIDEOGRAPHER_EXISTS;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.VIDEOGRAPHER_CREATION_SUCCESS_MSG;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
    }
}
