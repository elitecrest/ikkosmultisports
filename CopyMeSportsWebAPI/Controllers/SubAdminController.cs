﻿using CopyMeSportsWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Configuration;

namespace CopyMeSportsWebAPI.Controllers
{
    public class SubAdminController : ApiController
    {
        CustomResponse Result = new CustomResponse();

        [HttpPost]
        public CustomResponse AddSubAdmin(SubAdminDTO adminDto)
        {
            try
            {
                    var admins = SubAdminModel.AddSubAdmin(adminDto);
                    if (admins > 0)
                    { 
                        Result.Status = CustomResponseStatus.Successful;
                        Result.Response = SubAdminModel.GetSubAdminDetails(adminDto.AdminId); 
                        Result.Message = Constants.SUB_ADMIN_ADDED;
                        SendMailLoginDetails(adminDto);                }
                    else
                    {
                        Result.Status = CustomResponseStatus.UnSuccessful;
                        Result.Message = Constants.SUB_ADMIN_NOT_ADDED;
                    }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        private int SendMailLoginDetails(SubAdminDTO adminDto)
        {
            string Message = string.Empty;
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/SubadminLoginDetails.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$UserName$$", adminDto.EmailAddress);
                myString = myString.Replace("$$Password$$", adminDto.Password);
                myString = myString.Replace("$$ChannelName$$", adminDto.ChannelName);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + adminDto.EmailAddress + ""));
                Msg.Subject = " Login Details For SubAdmin";
                Msg.Body = myString;
                Msg.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"],
                            ConfigurationManager.AppSettings["Password1"]),
                    Timeout = 1000000
                };
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                smtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                UsersModel.AddException(Message);
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }

        [HttpGet]
        public CustomResponse GetAllChannelDetails()
        {
            try
            {
                var channels = SubAdminModel.GetAllChannels();
                if (channels.Count > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = channels;
                    Result.Message = Constants.CHANNELS_GET;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Response = channels;
                    Result.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public CustomResponse SubAdminLogin(string EmailAddress, string Password, int ChannelId)
        {
            try
            {
                int result;
                var subAdminId = SubAdminModel.AdminUserExists(EmailAddress, Password, ChannelId, out result);
                if (result > 0)
                {
                    var res = SubAdminModel.GetChannelInformation(EmailAddress, Password, ChannelId);
                    res.SportsInfo = SportsModel.GetSportsListByChannelId(res.Id);
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = res;
                    Result.Message = Constants.CHANNEL_INFO;
                }
                else if(result == -1)
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Response = null;
                    Result.Message = Constants.EMAIL_NOT_REGISTERED_CHANNEL;
                }
                else if(result == -2)
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Response = null;
                    Result.Message = Constants.PASSWORD_MISMATCH;
                }
                else if(result == -3)
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Response = null;
                    Result.Message = Constants.EMAIL_NOT_REGISTERED;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public CustomResponse GetSubAdminsBySuperAdmin(int ChannelId)
        {
            try
            {
                var channels = SubAdminModel.GetSubAdminDetails(ChannelId);
                if (channels.Count > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = channels;
                    Result.Message = Constants.SUB_ADMIN_GET;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Response = channels;
                    Result.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public CustomResponse ForgotPassword(string emailAddress)
        {

            try
            {
                string code = UsersModel.GetSubAdminUserExists(emailAddress);
                if (code != string.Empty)
                {
                    string token = code;

                    //need to send a mail to user 
                    if (SendMailForForgotPassword(emailAddress, token) == 0)
                    {
                        Result.Status = CustomResponseStatus.Successful;
                        Result.Message = Constants.FORGOT_PASSWORD_SUCCESSFULLY;
                    }
                    else
                    {
                        Result.Status = CustomResponseStatus.UnSuccessful;
                        Result.Message = string.Empty;

                        //"Failed to Send Email, Please try again later";
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.USER_DOES_NOT_EXIST;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(_result);
            }

            return Result;
        }

        public static int SendMailForForgotPassword(string username, string token)
        {
            string Message = string.Empty;
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ForgotPassword.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                myString = myString.Replace("$$Token$$", token);
                //   myString = myString.Replace("$$Password$$", password);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + username + ""));
                Msg.Subject = " Password Reset Requested";
                Msg.Body = myString;
                Msg.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"],
                            ConfigurationManager.AppSettings["Password1"]),
                    Timeout = 1000000
                };
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                smtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                UsersModel.AddException(Message);
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }

        }

        [HttpGet]
        public CustomResponse ResetPassword(string userName, string token, string confirmPassword)
        {
            CustomResponse res = new CustomResponse();
            try
            {
              
                string email = UsersModel.UpdateSubAdminPassword(userName, token, confirmPassword);
                if (email != string.Empty)
                {
                    res.Status = CustomResponseStatus.Successful;
                    res.Message = Constants.RESET_PWD_SUCCESS;

                }
                else
                {
                    res.Status = CustomResponseStatus.UnSuccessful;
                    res.Message = Constants.RESET_PWD_FAILED;
                }
            }
            catch (Exception ex)
            {
                res.Status = CustomResponseStatus.Exception;
                res.Message = ex.Message;
                return res;// javaScriptSerializer.Serialize(_result);
            }
            return res;
        }
    }
}
