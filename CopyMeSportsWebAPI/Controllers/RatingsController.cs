﻿using CopyMeSportsWebAPI.Models;
using CopyMeSportsWebAPI.ShardUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CopyMeSportsWebAPI.Controllers
{
    public class RatingsController : ApiController
    {
        CustomResponse Result = new CustomResponse();
        [System.Web.Http.HttpGet]
        public CustomResponse ChannelOrPackageRating(int rating, int channelId, int type)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        if (type == 2)
                        {
                            var ratings = RatingsModel.PackageRating(channelId, rating, userId);
                            if (ratings.Count > 0)
                            {
                                var avgCount = CalculateAvgRating(ratings);
                                Result.Status = CustomResponseStatus.Successful;
                                Result.Response = PackageModel.AddRatingToPackage(channelId, avgCount, userId);
                                Result.Message = Constants.RATING_SUCCESS;
                            }
                            else
                            {
                                Result.Status = CustomResponseStatus.UnSuccessful;
                                Result.Message = Constants.RATING_UNSUCCESS;
                            }
                        }
                        else
                        {
                            var ratings = RatingsModel.ChannelRating(channelId, rating, userId);
                            if (ratings.Count > 0)
                            {
                                var avgCount = CalculateAvgRating(ratings);
                                Result.Status = CustomResponseStatus.Successful;
                                Result.Response = RatingsModel.AddRatingToChannel(channelId, avgCount, userId);
                                Result.Message = Constants.RATING_SUCCESS;
                            }
                            else
                            {
                                Result.Status = CustomResponseStatus.UnSuccessful;
                                Result.Message = Constants.RATING_UNSUCCESS;
                            }
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        public double CalculateAvgRating(List<Ratings> ratings)
        {
            int ratingcount = 0;
            double avgrating = 0.0;
            try
            {
                foreach (var rating in ratings)
                {
                    ratingcount = ratingcount + rating.Rating;
                }
                
                    avgrating = ratingcount / ratings.Count;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return avgrating;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse ChannelRating(int rating, int channelId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var ratings = RatingsModel.PackageRating(channelId, rating, userId);
                        if (ratings.Count > 0)
                        {
                            var avgCount = CalculateAvgRating(ratings);
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = PackageModel.AddRatingToPackage(channelId, avgCount, userId);
                            Result.Message = Constants.RATING_SUCCESS;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.RATING_UNSUCCESS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
    }
}
