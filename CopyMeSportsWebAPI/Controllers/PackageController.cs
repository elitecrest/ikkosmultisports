﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Net.Http;
using CopyMeSportsWebAPI.Models;
using CopyMeSportsWebAPI.ShardUtils;

namespace CopyMeSportsWebAPI.Controllers
{
    public class PackageController : ApiController
    {
        CustomResponse Result = new CustomResponse();
        // GET: Package
        #region Mobile App Methods

        [System.Web.Http.HttpGet]
        public CustomResponse GetFavouritePackages()
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var packages = PackageModel.GetFavouritePackages(userId);
                        if (packages.Count > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = packages;
                            Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.NO_RECORDS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse AddPackageToFavourites(int packageId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var packages = PackageModel.AddPackageToFavourites(packageId, userId);
                        if (packages > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = packages;
                            Result.Message = Constants.FAVOURITE_SUCCESS;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.FAVOURITE_UNSUCCESS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse PackageRating(int rating, int packageId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var ratings = PackageModel.AddRatingToPackage(packageId, rating, userId);
                        if (ratings > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = ratings;
                            Result.Message = Constants.RATING_SUCCESS;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.RATING_UNSUCCESS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpDelete]
        public CustomResponse DeleteFavouritePackage(int packageId)
        {
            try
            {
                int userId = 0;
                if (BaseUtils.didUniqueKeyExists(Request.Headers, out userId))
                {
                    if (userId > 0)
                    {
                        var ratings = PackageModel.DeleteFavouritePack(packageId, userId);
                        if (ratings > 0)
                        {
                            Result.Status = CustomResponseStatus.Successful;
                            Result.Response = ratings;
                            Result.Message = Constants.DELETE_SUCCESS;
                        }
                        else
                        {
                            Result.Status = CustomResponseStatus.UnSuccessful;
                            Result.Message = Constants.DELETE_UNSUCCESS;
                        }
                    }
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.GENERIC_NO_HEADER_MSG;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        #endregion

        #region Admin Portal Methods

        [System.Web.Http.HttpGet]
        public CustomResponse GetPricingCatalog()
        {
            try
            {
                var pricing = PackageModel.GetPricingCatalog();
                if (pricing.Count > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = pricing;
                    Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpPost]
        public CustomResponse InsertOrUpdatePackage(PackagesInfoDTO packages)
        {
            try
            {
                int packageId = 0;
                if (packages != null && packages.PackageId > 0)
                {
                    packageId = PackageModel.EditPackageInfo(packages);
                    if (packageId > 0)
                    {
                        for (int i = 0; i < packages.PackageVideoUrlDetails.Count; i++)
                        {
                            if (packages.PackageVideoUrlDetails[i].VideoId > 0)
                            {
                                int res = PackageModel.UpdatePackageVideoInfo(packages.PackageVideoUrlDetails[i].ActualVideoId, packages.PackageVideoUrlDetails[i].PreIntroVideoId, packages.PackageVideoUrlDetails[i].PostIntroVideoId, packageId, packages.CreatedBy, packages.PackageVideoUrlDetails[i].VideoId);
                            }
                            else
                            {
                                int res = PackageModel.AddPackageVideoInfo(packages.PackageVideoUrlDetails[i].ActualVideoId, packages.PackageVideoUrlDetails[i].PreIntroVideoId, packages.PackageVideoUrlDetails[i].PostIntroVideoId, packageId, packages.CreatedBy);
                            }
                        }
                        Result.Status = CustomResponseStatus.Successful;
                        Result.Response = packageId;
                        Result.Message = Constants.PACKAGE_UPDATED;
                    }
                    else
                    {
                        Result.Status = CustomResponseStatus.UnSuccessful;
                        Result.Message = Constants.PACKAGE_NOT_UPDATED;
                    }
                }
                else
                {
                    packageId = PackageModel.AddPackageInfo(packages);
                    if (packageId > 0)
                    {
                        for (int i = 0; i < packages.PackageVideoUrlDetails.Count; i++)
                        {
                            int res = PackageModel.AddPackageVideoInfo(packages.PackageVideoUrlDetails[i].ActualVideoId, packages.PackageVideoUrlDetails[i].PreIntroVideoId, packages.PackageVideoUrlDetails[i].PostIntroVideoId, packageId, packages.CreatedBy);
                        }
                        Result.Status = CustomResponseStatus.Successful;
                        Result.Response = packageId;
                        Result.Message = Constants.PACKAGE_ADDED;
                    }
                    else
                    {
                        Result.Status = CustomResponseStatus.UnSuccessful;
                        Result.Message = Constants.PACKAGE_NOT_ADDED;
                    }
                }

            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetPackagesByChannelId(int channelId, int sportId)
        {
            try
            {
                var packages = PackageModel.GetPackagesByID(channelId, sportId);
                if (packages.Count > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = packages;
                    Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public CustomResponse GetPackagesByPackageId(int packageId)
        {
            try
            {
                var package = PackageModel.GetPackage(packageId);
                package.PackageVideoUrlDetails = PackageModel.GetVideoUrlDetails(packageId);
                if (package != null)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = package;
                    Result.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpDelete]
        public CustomResponse DeletePackage(int packageId)
        {
            try
            {
                var result = PackageModel.DeletePackage(packageId);
                if (result > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = result;
                    Result.Message = Constants.PACKAGE_DELETE;
                }
                else
                {
                    Result.Status = CustomResponseStatus.UnSuccessful;
                    Result.Message = Constants.PACKAGE_NOT_DELETED;
                }

            }
            catch (Exception ex)
            {
                Result.Status = CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        #endregion
    }
}