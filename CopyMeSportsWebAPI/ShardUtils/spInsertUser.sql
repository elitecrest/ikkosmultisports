﻿
create proc spInsertUser
@userId int,
@name nvarchar(100),
@libraryId int
as
begin
 IF EXISTS (SELECT 1 FROM Users WHERE UserId = @userId)
                        UPDATE Users
                            SET Name = @name, LibraryId = @LibraryId
                            WHERE UserId = @userId
                    ELSE
                        INSERT INTO Users (UserId, Name, LibraryId)
                        VALUES (@userId, @name, @libraryId)


end




