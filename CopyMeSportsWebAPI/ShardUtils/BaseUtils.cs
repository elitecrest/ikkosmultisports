﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using CopyMeSportsWebAPI.Models;
using System.Data.SqlClient;
using System.Net.Http.Headers;
using Microsoft.Azure.SqlDatabase.ElasticScale.Query;


namespace CopyMeSportsWebAPI.ShardUtils
{
    public class BaseUtils
    {
        /// <summary>
        /// The shard map manager, or null if it does not exist. 
        /// It is recommended that you keep only one shard map manager instance in
        /// memory per AppDomain so that the mapping cache is not duplicated.
        /// </summary>
        private static ShardMapManager s_shardMapManager;


        /// <summary>
        /// Gets the shard map, if it exists. If it doesn't exist, writes out the reason and returns null.
        /// </summary>
        public static RangeShardMap<int> TryGetShardMap()
        {

            if (s_shardMapManager == null)
            {
                s_shardMapManager = ShardManagementUtils.TryGetShardMapManager(ShardsConfiguration.ShardMapManagerServerName, ShardsConfiguration.ShardMapManagerDatabaseName);
            }


            RangeShardMap<int> shardMap;
            bool mapExists = s_shardMapManager.TryGetRangeShardMap(ShardsConfiguration.ShardMapName, out shardMap);

            if (!mapExists)
            {
                ConsoleUtils.WriteWarning("Shard Map Manager has been created, but the Shard Map has not been created");
                return null;
            }

            return shardMap;
        }



        public static void executeReaderCommandOnSingleShard(int userId, SqlCommand sqlCommand, out SqlDataReader sqlDataReader)
        {
            sqlDataReader = null;

            RangeShardMap<int> shardMap = TryGetShardMap();
            if (shardMap != null)
            {

                // Looks up the key in the shard map and opens a connection to the shard
               SqlConnection conn = shardMap.OpenConnectionForKey(userId, ShardsConfiguration.GetCredentialsConnectionString());

                sqlCommand.Connection = conn;
                sqlCommand.CommandTimeout = 180;

                // Execute the command
                sqlDataReader = sqlCommand.ExecuteReader();
                //conn.Close();
            }



        }

        public static MultiShardConnection getMultiShardConnection()
        {
            RangeShardMap<int> shardMap = TryGetShardMap();

            IEnumerable<Shard> shards = shardMap.GetShards();
            //foreach(Shard shard in shards)
            //{
            //    SqlCommand sqlCommand = new SqlCommand("[spGetUser]");
            //    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            //    sqlCommand.Parameters.AddWithValue("@Id",)
            //    sqlCommand.Connection = shard.OpenConnection(ShardsConfiguration.GetCredentialsConnectionString());
            //}
            MultiShardConnection conn = new MultiShardConnection(shards, ShardsConfiguration.GetCredentialsConnectionString());
            return conn;
        }


        public static MultiShardDataReader ExecuteReaderCommandOnMultiShard(MultiShardCommand multiShardCommand)
        {


            // Get the shards to connect to
            RangeShardMap<int> shardMap = TryGetShardMap();
            IEnumerable<Shard> shards = shardMap.GetShards();
            multiShardCommand.ExecutionOptions = MultiShardExecutionOptions.IncludeShardNameColumn;
            multiShardCommand.ExecutionPolicy = MultiShardExecutionPolicy.PartialResults;
            multiShardCommand.CommandTimeout = 180;
            return multiShardCommand.ExecuteReader();
        }

        


        public static bool didUniqueKeyExists(HttpHeaders requestHeaders, out int userId)
        {
            bool exists = false;
            userId = 0;


            IEnumerable<string> headerValues = null;
            if (requestHeaders.TryGetValues(Constants.UNIQUE_HEADER_KEY, out headerValues))
            {
                userId = Convert.ToInt32(headerValues.FirstOrDefault());
                exists = true;
            }
            return exists;

        }









        //public static UserInfoDto executeReaderCommandOnShard(int userId, SqlCommand sqlCommand, out SqlDataReader sqlDataReader)
        //{
        //    sqlDataReader = null;
        //    UserInfoDto userInfoDto = null;
        //    RangeShardMap<int> shardMap = TryGetShardMap();
        //    if (shardMap != null)
        //    {

        //        SqlDatabaseUtils.SqlRetryPolicy.ExecuteAction(() =>
        //        {
        //            // Looks up the key in the shard map and opens a connection to the shard
        //            using (SqlConnection conn = shardMap.OpenConnectionForKey(userId, ShardsConfiguration.GetCredentialsConnectionString()))
        //            {
        //                sqlCommand.Connection = conn;


        //                sqlCommand.CommandTimeout = 60;

        //                // Execute the command
        //                SqlDataReader reader = sqlCommand.ExecuteReader();
        //                while (reader.Read())
        //                {
        //                    userInfoDto = new UserInfoDto();
        //                    userInfoDto.UserId = (int)reader["UserId"];
        //                    userInfoDto.Name = (reader["Name"] == DBNull.Value || reader["Name"] == string.Empty)
        //                                ? string.Empty
        //                                : reader["Name"].ToString();
        //                    userInfoDto.LibraryId = (int)reader["LibraryId"];
        //                    userInfoDto.email = (reader["email"] == DBNull.Value || reader["email"] == string.Empty)
        //                                ? string.Empty
        //                                : reader["email"].ToString();


        //                    userInfoDto.ProfilePicUrl = (reader["ProfilePicUrl"] == DBNull.Value || reader["ProfilePicUrl"] == string.Empty)
        //                                ? string.Empty
        //                                : reader["ProfilePicUrl"].ToString();


        //                    userInfoDto.FacebookId = (reader["FacebookId"] == DBNull.Value || reader["FacebookId"] == string.Empty)
        //                                ? string.Empty
        //                                : reader["FacebookId"].ToString();


        //                }

        //                return userInfoDto;

        //            }


        //        });

        //    }

        //    return userInfoDto;
        //}

        /// <summary>
        /// Executes the Data-Dependent Routing sample.
        /// </summary>
        public static void AddUserInShard(UserInfoDto userinfoDto)
        {
            RangeShardMap<int> shardMap = TryGetShardMap();
            if (shardMap != null)
            {
                ExecuteAddUserQuery(
                    shardMap,
                    ShardsConfiguration.GetCredentialsConnectionString(), userinfoDto);
            }
        }




        private static void ExecuteAddUserQuery(ShardMap shardMap,
           string credentialsConnectionString,
           UserInfoDto userInfodto)
        {

            SqlDatabaseUtils.SqlRetryPolicy.ExecuteAction(() =>
            {
                // Looks up the key in the shard map and opens a connection to the shard
                using (SqlConnection conn = shardMap.OpenConnectionForKey(userInfodto.UserId, credentialsConnectionString))
                {
                    // Create a simple command that will insert or update the customer information
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = @"
                    IF EXISTS (SELECT 1 FROM Users WHERE UserId = @userId)
                        UPDATE Users
                            SET Name = @name, LibraryId = @LibraryId, email = @email, FacebookId = @facebookId, ProfilePicUrl = @profilepicurl, Gender = @Gender,
                            WHERE UserId = @userId
                    ELSE
                        INSERT INTO Users (UserId, Name, LibraryId,email,ProfilePicUrl,FacebookId,Gender,LoginType)
                        VALUES (@userId, @name, @libraryId,@email,@facebookId,@profilepicurl,@Gender, @LoginType)";
                    cmd.Parameters.AddWithValue("@userId", userInfodto.UserId);
                    cmd.Parameters.AddWithValue("@name", userInfodto.Name);
                    cmd.Parameters.AddWithValue("@libraryId", userInfodto.LibraryId);
                    cmd.Parameters.AddWithValue("@email", userInfodto.email);
                    cmd.Parameters.AddWithValue("@facebookId", userInfodto.FacebookId);
                    cmd.Parameters.AddWithValue("@profilepicurl", userInfodto.ProfilePicUrl);
                    cmd.Parameters.AddWithValue("@Gender", userInfodto.Gender);
                    cmd.Parameters.AddWithValue("@LoginType", userInfodto.LoginType);

                    cmd.CommandTimeout = 60;

                    // Execute the command
                    cmd.ExecuteNonQuery();
                }
            });

        }
    }

}