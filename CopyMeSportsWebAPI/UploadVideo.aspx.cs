﻿using CopyMeSportsWebAPI.Models;
using CopyMeSportsWebAPI.ShardUtils;
using Microsoft.WindowsAzure.MediaServices.Client;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CopyMeSportsWebAPI
{
    public partial class UploadVideo : System.Web.UI.Page
    {
        CustomResponse response = new CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                response = Handlefile();
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string jsonString = javaScriptSerializer.Serialize(response);
                Response.Write(jsonString);
            }
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    response = Handlefile();
        //    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //    string jsonString = javaScriptSerializer.Serialize(response);
        //    Response.Write(jsonString);

        //}
        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        private CustomResponse Handlefile()
        {
            try
            {
                string baseUrl = ConfigurationManager.AppSettings["BaseURL"];
                //string baseUrl = "http://localhost:53175/";
                Stream stream = Request.InputStream;
                string videoFormat = Convert.ToString(Request.QueryString["VideoFormat"]).Trim();
                string packageId = Convert.ToString(Request.QueryString["PackageId"]).Trim();
                string duration = Convert.ToString(Request.QueryString["Duration"]).Trim();
                //string

                RecordVideoDTO video = new RecordVideoDTO();
                string videoUrl = Streamaudio(stream, videoFormat);
                //string videoUrl = "https://newchurchblob.blob.core.windows.net:443/asset-3712445d-1500-80c5-588f-f1e5d5446923/fn1487471924.mp4?sv=2012-02-12&sr=c&si=099211b3-0f49-4dba-badf-7a9681a4d438&sig=ceYsoC%2FhRvqze5n3PIy6VNNSKLrAn5Sk9y9nu3pl6QE%3D&se=2017-02-16T07%3A02%3A39Z";
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                video.ServerVideoId = 0;
                video.ServerVideoUrl = videoUrl;
                video.UserId = Convert.ToInt32(Request.QueryString["UserId"].Trim());
                video.VideoFormat = videoFormat;
                video.PackageId = Convert.ToInt32(packageId);
                video.Duration = Convert.ToInt32(duration);
                video.RecordVideoId = Convert.ToString(Request.QueryString["RecordId"].Trim());//no need
                video.UserName = Convert.ToString(Request.QueryString["UserName"].Trim());
                video.IdealVideoId = Convert.ToInt32(Request.QueryString["IdealVideoId"].Trim());
                video.Status = 1;
                video.ThumbNailUrl = GetVideoThumbnails(video); 

                var res = UploadVideoToDB(video);
                /*if (res.IsSuccessStatusCode)*/
                if (res > 0)
                {
                    response.Status = CustomResponseStatus.Successful;
                    response.Response = res;
                    response.Message = Constants.VIDEO_UPLOADED;
                }
            }
            catch (Exception ex)
            {
                var Message = ex.Message;
                UsersModel.AddException(Message);
                return null;
            }
            return response;
        }

        private string GetVideoThumbnails(RecordVideoDTO videoResp)
        {
            try
            {
                RecordVideoDTO video = new RecordVideoDTO();

                var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                int totDuration = videoResp.Duration;
                float startPoint = 0;
                float dur = (float)totDuration / 6;

                var frame = startPoint + (float)Math.Round(2.0, 2);
                string thumbnailJpeGpath = HttpContext.Current.Server.MapPath("~/videoThumb" + 1 + ".jpg");
                ffMpeg.GetVideoThumbnail(videoResp.ServerVideoUrl, thumbnailJpeGpath, frame);
                byte[] bytes = File.ReadAllBytes(thumbnailJpeGpath);
                string base64String = Convert.ToBase64String(bytes);
                string thumbnailUrl = GetImageURL(base64String, ".jpg", "");
                startPoint = startPoint + (float)Math.Round(dur, 2);
                return thumbnailUrl;
            }
            catch (Exception ex)
            {
                var Message = ex.Message;
                UsersModel.AddException(Message);
                return null;
            }
        }
        public string Streamaudio(Stream stream, string format)
        {
            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);
            var fileStream = File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();
            var ext = format;
            Stream fileStreambanners = stream;
            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string videoUrl = UploadImage1(fileNamebanners, path, ext, fileStreambanners);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            return videoUrl;
        }
        public string UploadImage1(string fileName, string path, string ext, Stream inputStream)
        {
            try
            {
                string account = ConfigurationManager.AppSettings["MediaServicesAccountName"];
                string key = ConfigurationManager.AppSettings["MediaServicesAccountKey"];
                CloudMediaContext context = new CloudMediaContext(account, key);
                var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);
                var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));
                assetFile.Upload(path);
                var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
                var daysForWhichStreamingUrlIsActive = 10000;
                var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
                var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive), AccessPermissions.Read);
                string streamingUrl = string.Empty;
                var assetFiles = streamingAsset.AssetFiles.ToList();
                var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
                if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
                {
                    var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                    var mp4Uri = new UriBuilder(locator.Path);
                    mp4Uri.Path += "/" + streamingAssetFile.Name;
                    streamingUrl = mp4Uri.ToString();
                }
                return streamingUrl;
            }
            catch(Exception ex)
            {
                var Message = ex.Message;
                UsersModel.AddException(Message);
                throw;
            }
           
        }

        public string GetImageURL(string binary, string format, string contentType)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();
                string filename = "" + r.Next(999999) + DateTime.Now.Millisecond + r.Next(99999) + format;
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();
                container.SetPermissions(new BlobContainerPermissions
                {
                    PublicAccess =
                        BlobContainerPublicAccessType.Blob
                });
                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
                // Create or overwrite the "myblob" blob with contents from a local file.
                byte[] binarydata = Convert.FromBase64String(binary);
                blockBlob.Properties.ContentType = contentType;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);
                //UploadFromStream(fileStream);
                //}
                // Retrieve reference to a blob named "myblob".
                CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);
                imageurl = blockBlob1.Uri.ToString();
            }
            catch (Exception ex)
            {

            }
            return imageurl;
        }

        internal static int UploadVideoToDB(RecordVideoDTO videos)
        {
            int result = 0;
            try
            {
                videos.UserId = videos.UserId == null ? 0 : videos.UserId;
                videos.ServerVideoId = 0;
                videos.RecordVideoId = videos.RecordVideoId == null ? string.Empty : videos.RecordVideoId;
                videos.PackageId = videos.PackageId == null ? 0 : videos.PackageId;
                videos.ThumbNailUrl = videos.ThumbNailUrl == null ? string.Empty : videos.ThumbNailUrl;
                videos.ServerVideoUrl = videos.ServerVideoUrl == null ? string.Empty : videos.ServerVideoUrl;
                videos.UserName = videos.UserName == null ? string.Empty : videos.UserName;
                videos.Duration = videos.Duration == null ? 0 : videos.Duration;
                videos.VideoFormat = videos.VideoFormat == null ? string.Empty : videos.VideoFormat;
                videos.IdealVideoId = videos.IdealVideoId == null ? 0 : videos.IdealVideoId;
                SqlCommand sqlCommand = new SqlCommand("[UploadVideoToCmp]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", videos.UserId);
                sqlCommand.Parameters.AddWithValue("@VideoId", videos.ServerVideoId);
                sqlCommand.Parameters.AddWithValue("@RecordVideoId", videos.RecordVideoId);
                sqlCommand.Parameters.AddWithValue("@PackageId", videos.PackageId);
                sqlCommand.Parameters.AddWithValue("@ThumbNailUrl", videos.ThumbNailUrl);
                sqlCommand.Parameters.AddWithValue("@VideoUrl", videos.ServerVideoUrl);
                sqlCommand.Parameters.AddWithValue("@UserName", videos.UserName);
                sqlCommand.Parameters.AddWithValue("@Duration", videos.Duration);
                sqlCommand.Parameters.AddWithValue("@Format", videos.VideoFormat);
                sqlCommand.Parameters.AddWithValue("@IdealVideoId", videos.IdealVideoId);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(videos.UserId, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                var Message = e.Message;
                UsersModel.AddException(Message);
                throw;
            }
            return result;
        }
    }
}