﻿using CopyMeSportsWebAPI.ShardUtils;
using Microsoft.Azure.SqlDatabase.ElasticScale.Query;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CopyMeSportsWebAPI.Models
{
    internal static class SportsModel
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CopyMeGolf"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        internal static List<Sports> GetSports(int userId)
        {
            List<Sports> sportsList = new List<Sports>();
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetSportsDetails]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    Sports sports = new Sports();
                    sports.SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]);
                    sports.SportName = (reader["SportName"] == DBNull.Value || reader["SportName"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["SportName"]);
                    sports.IsDataAvailable = reader["IsGreyOut"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IsGreyOut"]);
                    sports.SportsImageUrl = (sports.IsDataAvailable == 1 ? ((reader["SportsImageUrl"] == DBNull.Value)
                                ? string.Empty
                                : Convert.ToString(reader["SportsImageUrl"])) : ((reader["SportsImageUrl1"] == DBNull.Value)
                                ? string.Empty
                                : Convert.ToString(reader["SportsImageUrl1"])));
                    sports.SportsBannerImageUrl = (reader["BannerImageUrl"] == DBNull.Value || reader["BannerImageUrl"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["BannerImageUrl"]);
                    sportsList.Add(sports);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return sportsList;
        }

        internal static List<Libraries> GetLibraryById(int sportId)
        {
            List<Libraries> libraries = new List<Libraries>();
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetLibrariesBySportId]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Id", sportId);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(sportId, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    Libraries library = new Libraries();
                    library.LibraryId = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]);
                    library.LibraryName = (reader["LibraryName"] == DBNull.Value || reader["LibraryName"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["LibraryName"]);
                    library.SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]);
                    library.LibraryImageUrl = (reader["LibraryImageUrl"] == DBNull.Value || reader["LibraryImageUrl"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["LibraryImageUrl"]);
                    library.IsPaid = reader["IsPaid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IsPaid"]);
                    libraries.Add(library);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return libraries;
        }

        internal static List<Libraries> GetChannelById(int sportId)
        {
            List<Libraries> libraries = new List<Libraries>();
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetChannelsBySportId]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Id", sportId);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(sportId, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    Libraries library = new Libraries();
                    library.LibraryId = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]);
                    library.LibraryName = (reader["LibraryName"] == DBNull.Value || reader["LibraryName"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["LibraryName"]);
                    library.SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]);
                    library.LibraryImageUrl = (reader["LibraryImageUrl"] == DBNull.Value || reader["LibraryImageUrl"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["LibraryImageUrl"]);
                    library.IsPaid = reader["IsPaid"] == DBNull.Value ? 0 : Convert.ToInt32(reader["IsPaid"]);
                    library.Price = reader["Price"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Price"]);
                    library.SkuId = reader["SKUID"] == DBNull.Value ? string.Empty : Convert.ToString(reader["SKUID"]);
                    library.PricingCatalogId = reader["PricingCatalogId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["PricingCatalogId"]);
                    library.Description = (reader["Description"] == DBNull.Value || reader["Description"] == string.Empty)
                               ? string.Empty
                               : Convert.ToString(reader["Description"]);
                    libraries.Add(library);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return libraries;
        }

        internal static AdminUsersDto GetChannelDetailsByChannelId(int channelId)
        {
            AdminUsersDto channels = null;
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetChannelsByChannelId]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ChannelId", channelId);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(10, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    channels = new AdminUsersDto();
                    channels.Id = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]);
                    channels.ChannelName = (reader["LibraryName"] == DBNull.Value || reader["LibraryName"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["LibraryName"]);
                    channels.SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]);
                    channels.ProfilePicURL = (reader["LibraryImageUrl"] == DBNull.Value || reader["LibraryImageUrl"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["LibraryImageUrl"]);
                    channels.EmailAddress = (reader["EmailAddress"] == DBNull.Value || reader["EmailAddress"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["EmailAddress"]);
                    channels.Password = (reader["Password"] == DBNull.Value || reader["Password"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["Password"]);
                    channels.FirstName = (reader["FirstName"] == DBNull.Value || reader["FirstName"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["FirstName"]);
                    channels.LastName = (reader["LastName"] == DBNull.Value || reader["LastName"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["LastName"]);
                    channels.Description = (reader["Description"] == DBNull.Value || reader["Description"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["Description"]);
                    channels.VideoURL = (reader["VideoURL"] == DBNull.Value || reader["VideoURL"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["VideoURL"]);
                    channels.ThumbNailURL = (reader["ThumbNailURL"] == DBNull.Value || reader["ThumbNailURL"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["ThumbNailURL"]);
                    channels.PricingCatalogId = reader["PricingCatalogId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["PricingCatalogId"]);
                    channels.SportName = (reader["SportName"] == DBNull.Value || reader["SportName"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["SportName"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return channels;
        }


        internal static int GetChannelStatus(int channelId,int sportId)
        {
            int result = 1;

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetChannelStatus]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ChannelId", channelId);
                sqlCommand.Parameters.AddWithValue("@SportId", sportId);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(10, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                throw;
            }



            return result;
        }

        internal static List<MultiSports> GetSportsListByChannelId(int channelId)
        {
            List<MultiSports> sportsList = new List<MultiSports>();
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetSportsByChannelId]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ChannelId", channelId);
                SqlDataReader reader;
                BaseUtils.executeReaderCommandOnSingleShard(10, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    MultiSports sports = new MultiSports();
                    sports.SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]);
                    sports.SportName = (reader["SportName"] == DBNull.Value || reader["SportName"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["SportName"]);
                    sportsList.Add(sports);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return sportsList;
        }

        internal static int AddSportsToChannel(int channelId, int sportId)
        {
            int result = 0;
            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[AddSportsToChannel]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@ChannelId", channelId);
                command.Parameters.AddWithValue("@SportId", sportId);

                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }
    }
}