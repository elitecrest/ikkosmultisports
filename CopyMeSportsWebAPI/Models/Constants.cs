﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CopyMeSportsWebAPI.Models
{
    public class Constants
    {
        public static string UNIQUE_HEADER_KEY = "uniqueId";
        public static string GENERIC_FAILURE_MSG = "Some thing terrible happend.";
        public static string GENERIC_SUCCESS_MSG = "Success.";
        public static string GENERIC_NO_HEADER_MSG = "Please include unique header value.";

        #region User Controller Constants
        public static string USER_CREATION_SUCCESS_MSG = "User created successfully";
        public static string USER_RETRIEVED = "User retrieved successfully";
        public static string USER_CHECKED = "User Not exists";
        public static string FEEDBACK = "Your Feedback has been submitted successfully. Thanks for your feedback !";
        #endregion
       
        #region Sports Controller Constants
        public static string SPORTS_RETRIEVED = "Sports Retrieved Successfully";
        public static string LIBRARIES_RETRIEVED = "Channels Retrieved Successfully";
        public static string CHANNELS_RETRIEVED = "Channels Retrieved Successfully";
        #endregion

        #region Common Constants
        public static string NO_RECORDS = "No Records Found";
        public static string DETAILS_GET_SUCCESSFULLY = "Data retrieved successfully";
        #endregion


        public static string FAVOURITE_SUCCESS = "Added Package to Favourites list.";
        public static string FAVOURITE_UNSUCCESS = "Package not added to Favourites List"; 
        public static string USER_EXISTS = "User already exists with this Email Address"; 
        public static string RATING_SUCCESS = "Successfully rated the package";
        public static string RATING_UNSUCCESS = "Could not be able to rate the package";
        public static string VALIDATED = "User Validated Successfully";
        public static string PASSWORD_MISMATCH = "Password mismatch with the EmailAddress";
        public static string USER_NOT_EXISTS = "User not exists with the EmailAddress provided.";
        public static string EMAIL_ALREADY_EXISTS = "";
        public static string VIDEO_UPDATED = "Video info updated successfully";
        public static string VIDEO_NOT_UPDATED = "Video info not updated";
        public static string VIDEO_ADDED = "Video added successfully";
        public static string VIDEO_NOT_ADDED = "Video not added";
        public static string PACKAGE_ADDED = "Package Created successfully";
        public static string PACKAGE_NOT_ADDED = "Package not created";
        public static string DELETE_SUCCESS = "Successfully deleted favourite package";
        public static string DELETE_UNSUCCESS = "Could not be able to delete the favourite package";
        public static string DELETE_VIDEO_SUCCESS = "Successfully deleted Video";
        public static string DELETE_VIDEO_UNSUCCESS = "Could not be able to delete the Video";
        public static string SUBSCRIBE_SUCCESS = "Successfully subscribed the package";
        public static string SUBSCRIBE_UNSUCCESS = "Couldn't able to subscribe the package";
        public static string PACKAGE_DELETE = "Successfully deleted package";
        public static string PACKAGE_NOT_DELETED = "Could not be able to delete the package";
        public static string PACKAGE_UPDATED = "Package updated successfully";
        public static string PACKAGE_NOT_UPDATED = "Couldn't be able to update package";
        public static string CHANNEL_PAID = "This Channel is subscribed successfully";
        public static string CHANNEL_NOT_PAID = "This Channel is not subscribed";
        public static string PACKAGE_PAID = "This Package is subscribed successfully";
        public static string PACKAGE_NOT_PAID = "This Package is not subscribed";
        public static string FORGOT_PASSWORD_SUCCESSFULLY = "Mail sent successfully to registered email address";
        public static string USER_DOES_NOT_EXIST = "User does not exist";
        public static string RESET_PWD_SUCCESS = "Password has been reset successfully";

        public static string RESET_PWD_FAILED = "Resetting password has been failed.Try again!";
        public static string SPORT_ADDED = "Sport added successfully to the channel";
        public static string SPORT_NOT_ADDED = "Couldn't be able to add the sport to the channel, Please try again later.";
        public static string CHANNEL_NOT_UPDATED = "Couldn't be able to update the channel information, please try later after sometime.";
        public static string CHANNEL_UPDATED_SUCCESS = "Channel information updated successfully.";
        public static string SPORT_INFO_RETRIEVED = "Sports Info retrieved successfully";
        public static string SPORT_NOT_FOUND = "Sports not found for this channel";
        public static string USAGE_INFO = "Usage Information successfully fetched";
        public static string USAGE_INFO_NO = "Usage Information not fetched";
        public static string DEVICE_REGISTERED = "Device Registered Successfully";
        public static string DEVICE_NOT_REGISTERED = "Device Not Registered Successfully";
        public static string DEVICE_INFO = "Device information retrieved Successfully";
        public static string FEATURE_SAVED = "Feature usage Tracked Successfully";
        public static string FEATURE_NOT_SAVED = "Couldn't be able to track the feature usage, Please try after sometime.";
        public static string EMAIL_EXISTS = "This email has already used to login using 'Facebook'. Please choose someother email address";
        public static string SUB_ADMIN_ADDED = "Sub Admin has created successfully";
        public static string SUB_ADMIN_NOT_ADDED = "This email address is already assigned to this channel, please use different email address";
        public static string CHANNELS_GET = "Channels retrieved successfully";
        public static string EMAIL_NOT_REGISTERED_CHANNEL = "Looks this email id is not registered with this channel, Please select the proper channel and log in";
        public static string EMAIL_NOT_REGISTERED = "This Email is not registered under this channel.";
        public static string CHANNEL_INFO = "Channel Info retrieved successfully";
        public static string SUB_ADMIN_GET = "Sub Admins information retrieved successfully";
        public static string VIDEO_UPLOADED = "Video Uploaded successfully";
        public static string VIDEO_ALREADY_SPAMED = "Video already spamed";
        internal static string VIDEOGRAPHER_EXISTS = "Videographer already exists"; 
        public static string VIDEOGRAPHER_CREATION_SUCCESS_MSG = "Videographer created successfully";
    }
}