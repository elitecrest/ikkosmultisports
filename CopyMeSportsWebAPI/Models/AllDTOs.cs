﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace CopyMeSportsWebAPI.Models
{

    public partial class UserInfoDto
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public int LibraryId { get; set; }
        public string email { get; set; }
        public string ProfilePicUrl { get; set; }
        public string FacebookId { get; set; }
        public int Gender { get; set; }
        public int LoginType { get; set; }
        public int DeviceId { get; set; }
        public string SportName { get; set; }
        public string LibraryName { get; set; }
        public int DeviceType { get; set; }
        public int SportId { get; set; }
        public string SportsBannerImageUrl { get; set; }
        public string Password { get; set; }
    }

    public partial class CustomResponse
    {
        public CustomResponseStatus Status;
        public Object Response;
        public string Message;

    }

    public partial class Sports
    {
        public int SportId { get; set; }
        public string SportName { get; set; }
        public string SportsImageUrl { get; set; }
        public string SportsBannerImageUrl { get; set; }
        public int IsDataAvailable { get; set; }
    }

    public partial class Libraries
    {
        public int LibraryId { get; set; }
        public string LibraryName { get; set; }
        public int SportId { get; set; }
        public string LibraryImageUrl { get; set; }
        public int IsPaid { get; set; }
        public int PricingCatalogId { get; set; }
        public string Price { get; set; }
        public string SkuId { get; set; }
        public string Description { get; set; }
        public double AverageRating { get; set; }
    }


    public partial class SpamVideoDto
    {
        public int ID;
        public int VideoId;
        public int SpamBy;
        public DateTime CreatedDate;


    }
    public enum CustomResponseStatus
    {
        Successful,
        UnSuccessful,
        Exception,
        Checked,
        PASSWORD_MISMATCH
    }

    public partial class VideosInfoDTO
    {
        //public VideosInfoDTO()
        //{
        //    this.ShardID = 0;
        //    this.SubCategoryID = 0;
        //    this.MovementName = "";
        //    this.URL = "";
        //    this.ThumbNailURL = "";
        //    this.Type = 1;
        //    this.Status = 1;
        //    this.Format = "";
        //    this.CreatedDate = DateTime.Now;
        //    this.CreatedBy = 0;
        //    this.OwnerID = 0;
        //    this.Orientation = 0;
        //    this.Paid = 0;
        //}
        public int ID { get; set; }
        public int SubCategoryID { get; set; }
        public string Segment { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string MovementName { get; set; }
        public string ActorName { get; set; }
        public int OwnerID { get; set; }
        public string Format { get; set; }
        public string Speed { get; set; }
        public string Quality { get; set; }
        public int? Size { get; set; }
        public int? Duration { get; set; }
        public string URL { get; set; }
        public string ThumbNailURL { get; set; }
        public string AccessKey { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public string ThumbnailString { get; set; }
        public string Base64String { get; set; }
        public string ThumbnailFormat { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public double Rating { get; set; }
        public int Orientation { get; set; }
        public int ShardID { get; set; }
        public int Facing { get; set; }
        public int AverageRating { get; set; }
        public int Paid { get; set; }
        public int PlayCount { get; set; }
        public bool isPurchased { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public string SKUID { get; set; }
    }

    public partial class AppInfoDTO
    {
        public AppInfoDTO()
        {
            this.AppID = "";
        }

        public string AppID { get; set; }
    }

    public partial class DeviceInfoDTO
    {
        public int Id { get; set; }
        public string DeviceId { get; set; }
        public int DeviceType { get; set; }  // Iphone:1,Android:2
        public DateTime CreatedDate { get; set; }
    }
    public partial class EventTypeDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Subcategory { get; set; }
    }

    public partial class BestTimesValuesDTO
    {
        public int BestTimeId { get; set; }
        public string Stroke { get; set; }
        public string BestTimeValue { get; set; }
    }
    public partial class PlanDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public List<int> VideoIDs { get; set; }
        public int Isdefault { get; set; }
    }
    public partial class PromotionalContentDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ThumbNailURL { get; set; }
        public string ThumbNailURLFormat { get; set; }
        public string URL { get; set; }
        public string URLFormat { get; set; }
        public string ActionType { get; set; }
        public int ActionTypeID { get; set; }
        public int Status { get; set; }
        public DateTime? EventStartDate { get; set; }
        public DateTime? EventEndDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int UserID { get; set; }

    }
    public partial class FeedbackDTO
    {
        public string Name { get; set; }
        public string DeviceId { get; set; }
        public string Comment { get; set; }
        public string EmailAddress { get; set; }
        public string DeviceType { get; set; }
        public int UserRating { get; set; }
    }
    public partial class UserTrainingPlanDTO : TrainingPlanDTO
    {
        public int UTPId { get; set; }
        public int TrainingPlanId { get; set; }
        public DateTime StartDate { get; set; }
        public TimeSpan BestCurrentTime1 { get; set; }
        public TimeSpan BestCurrentTime2 { get; set; }
        public bool ShowQuestion { get; set; }
        public string Question { get; set; }
        public bool ShowRecord { get; set; }
        public int RecordedVideoId { get; set; }
        public bool ShowResults { get; set; }
        public string QuestionAns { get; set; }
        public bool started { get; set; }
        public int IdealVideoId { get; set; }
    }
    public partial class TrainingPlanDTO
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int BestTime1 { get; set; }
        public int BestTime2 { get; set; }
        public int Gender { get; set; }
        public int Age { get; set; }
        public int EventId { get; set; }
        public int UserId { get; set; }
        public int Default { get; set; }
        public int Day { get; set; }
        public List<int> VideoId { get; set; }
    }
    public partial class PackagesInfoDTO
    {
        public int PackageId { get; set; }
        public string PackageName { get; set; }
        public string Description { get; set; }
        public int LibraryId { get; set; }
        public string Format { get; set; }
        public string VideoURL { get; set; }
        public string ThumbNailURL { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int PricingCatalogId { get; set; }
        public string Price { get; set; }
        public string SkuId { get; set; }
        public int Rating { get; set; }
        public int IsFavourite { get; set; }
        public int IsPaid { get; set; }
        public List<PackageVideoInfo> PackageVideoDetails { get; set; }
        public List<PackageVideoURLInfo> PackageVideoUrlDetails { get; set; }
        public int PackageIntroVideoId { get; set; }
        public int SportId { get; set; }
        public int LoginType { get; set; }
    }

    public class PackageVideoURLInfo
    {
        public string ActualVideoUrl { get; set; }
        public string PreIntroVideoUrl { get; set; }
        public string PostIntroVideoUrl { get; set; }
        public int ActualVideoId { get; set; }
        public int PreIntroVideoId { get; set; }
        public int PostIntroVideoId { get; set; }
        public int VideoId { get; set; }
    }
    public partial class PackageVideoInfo
    {
        public int ActualVideoId { get; set; }
        public int PreIntroVideoId { get; set; }
        public int PostIntroVideoId { get; set; }

    }
    public partial class PackageVideosDTO
    {
        public int ID { get; set; }
        public int SubCategoryID { get; set; }
        public string Segment { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string MovementName { get; set; }
        public string ActorName { get; set; }
        public string Format { get; set; }
        public string Speed { get; set; }
        public string Quality { get; set; }
        public string URL { get; set; }
        public string ThumbNailURL { get; set; }
        public string AccessKey { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public float Rating { get; set; }
        public int AverageRating { get; set; }
        public int PlayCount { get; set; }
        public string InstructionsVideoURL { get; set; }
        public string InstructionsVideoURL2 { get; set; }
        public int PackageId { get; set; }
        public int VideoMode { get; set; }
    }

    public partial class VideoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string VideoURL { get; set; }
        public string ThumbnailURL { get; set; }
        public int Status { get; set; }
        public string VideoFormat { get; set; }
        public int Count { get; set; }
        public int Duration { get; set; }
        // public List<string> ExtractedThumbnailURLs { get; set; }

        public int UserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string MovementName { get; set; }
        public string ActorName { get; set; }

    }
    public partial class PaymentDTO
    {
        public string PaymentToken { get; set; }
        public int DeviceType { get; set; } //Android=2 or Iphone=1
        public string PaymentType { get; set; } // Package or Channel
        public int IsSubscription { get; set; }  //Monthly = 1, Yearly = 2
        public int PackageId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int PaymentId { get; set; }
        public int UserId { get; set; }
        public string SKUID { get; set; }
        public int SportId { get; set; }
        public int DeviceId { get; set; }
    }

    public partial class FeatureTrackingDto
    {
        public int FeatureId { get; set; }
        public int DeviceId { get; set; }
        public int DeviceType { get; set; }
        public int Value { get; set; }
        public int UserId { get; set; }
    }

    public partial class RecordVideoDTO
    {
        public string RecordVideoId { get; set; }
        public int UserId { get; set; }
        public int PackageId { get; set; }
        public int IdealVideoId { get; set; }
        public string UserName { get; set; }
        public int ServerVideoId { get; set; }
        public string ThumbNailUrl { get; set; }
        public string ServerVideoUrl { get; set; }
        public string VideoFormat { get; set; }
        public int Duration { get; set; }
        public int Status { get; set; }
        public string IdealVideoUrl { get; set; }
        public string IdealVideoThumbNail { get; set; }
        public string IdealVideoName { get; set; }
        public string ActorName { get; set; }
    }

    public partial class Ratings
    {
        public int Id { get; set; }
        public int ChannelId { get; set; }
        public int Type { get; set; } //ChannelType = 1 and PackageType = 2
        public int Rating { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
    #region Portal Stuff
    public partial class AdminUsersDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public int SportId { get; set; }
        public string ChannelName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ProfilePicURL { get; set; }
        public string VideoURL { get; set; }
        public string ThumbNailURL { get; set; }
        public string Description { get; set; }
        public int PricingCatalogId { get; set; }
        public string SportName { get; set; }
        public List<MultiSports> SportsInfo { get; set; }
        public int LoginType { get; set; }
    }

    public partial class MultiSports
    {
        public int SportId { get; set; }
        public string SportName { get; set; }
    }

    public partial class VideoInfoDTO
    {
        public int Id { get; set; }
        public string VideoURL { get; set; }
        public string ActorName { get; set; }
        public string MomentName { get; set; }
        public string IntroVideoURL { get; set; }
        public int Status { get; set; }
        public int CreatedBy { get; set; }
        public int Duration { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ThumbNailURL { get; set; }
        public int VideoMode { get; set; }
        public int SportId { get; set; }
        public int LoginType { get; set; }
    }

    public partial class PricingCatalogDTO
    {
        public int Id { get; set; }
        public string Price { get; set; }
        public string SKUID { get; set; }
        public DateTime CreatedDate { get; set; }
        public int PricingType { get; set; }
    }

    public partial class PackageInfoDTO
    {
        public int Id { get; set; }
        public string PackageName { get; set; }
        public string PackageDescription { get; set; }
        public int LibraryId { get; set; }
        public int PricingCatalogId { get; set; }

    }

    public partial class UsageTrackingDTO
    {
        public int ChannelSubscribers { get; set; }
        public int PackageSubscribers { get; set; }
    }

    public partial class SubAdminDTO
    {
        public int SubAdminId { get; set; }
        public int AdminId { get; set; }
        public string ChannelName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }

    }

    public partial class ChannelDTO
    { 
        public int ChannelId { get; set; }
        public string ChannelName { get; set; } 
        public string Email { get; set; }
        public List<SubAdminDTO> SubAdmins { get; set; }
    }
    public partial class VideographersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public string Phonenumber { get; set; } 
        public string Address { get; set; }  
        public string Emailaddress  { get; set; } 
        public int CreatedBy { get; set; }
    }

    public partial class WorkerDTO
    {
        public int VideoStatus { get; set; }
        public int ModuleStatus { get; set; }
    }

    public partial class SupportMailDTO
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string SSL { get; set; }

    }
    #endregion
}