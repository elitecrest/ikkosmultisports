﻿using CopyMeSportsWebAPI.ShardUtils;
using Microsoft.Azure.SqlDatabase.ElasticScale.Query;
using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using Microsoft.WindowsAzure.Storage;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CopyMeSportsWebAPI.Models
{
    internal static class UsersModel
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CopyMeGolf"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        internal static UserInfoDto GetUserInformation(int id)
        {
            UserInfoDto userInfoDto = null;
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[spGetUser]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Id", id);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(id, sqlCommand, out reader);
                while (reader != null && reader.Read())
                {
                    userInfoDto = new UserInfoDto();
                    userInfoDto.UserId = reader["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserId"]);
                    userInfoDto.Name = (reader["Name"] == DBNull.Value || reader["Name"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["Name"]);
                   // userInfoDto.LibraryId = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]);
                    userInfoDto.email = (reader["email"] == DBNull.Value || reader["email"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["email"]);
                    userInfoDto.ProfilePicUrl = (reader["ProfilePicUrl"] == DBNull.Value || reader["ProfilePicUrl"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["ProfilePicUrl"]);
                    userInfoDto.FacebookId = (reader["FacebookId"] == DBNull.Value || reader["FacebookId"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["FacebookId"]);
                    userInfoDto.Gender = reader["Gender"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Gender"]);
                    userInfoDto.LoginType = reader["LoginType"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LoginType"]);
                    userInfoDto.DeviceId = reader["DeviceId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DeviceId"]);
                    //userInfoDto.LibraryName = (reader["LibraryName"] == DBNull.Value || reader["LibraryName"] == string.Empty)
                    //            ? string.Empty
                    //            : Convert.ToString(reader["LibraryName"]);
                    userInfoDto.SportName = (reader["SportName"] == DBNull.Value || reader["SportName"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["SportName"]);
                    userInfoDto.DeviceType = reader["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DeviceType"]);
                    userInfoDto.SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]);
                    userInfoDto.SportsBannerImageUrl = (reader["BannerImageUrl"] == DBNull.Value || reader["BannerImageUrl"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["BannerImageUrl"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return userInfoDto;
        }

        internal static List<UserInfoDto> GetAllUsers()
        {
            List<UserInfoDto> usersList = new List<UserInfoDto>();
            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[spGetAllUsers]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    UserInfoDto userInfoDto = new UserInfoDto();
                    userInfoDto.UserId = reader["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserId"]);
                    userInfoDto.Name = (reader["Name"] == DBNull.Value || reader["Name"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["Name"]);
                    userInfoDto.LibraryId = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]);
                    userInfoDto.email = (reader["email"] == DBNull.Value || reader["email"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["email"]);
                    userInfoDto.ProfilePicUrl = (reader["ProfilePicUrl"] == DBNull.Value || reader["ProfilePicUrl"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["ProfilePicUrl"]);
                    userInfoDto.FacebookId = (reader["FacebookId"] == DBNull.Value || reader["FacebookId"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["FacebookId"]);
                    usersList.Add(userInfoDto);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return usersList;
        }

        internal static int CheckEmailExists(string emailAddress, int loginType, int userId)
        {
            int result = 0;
            //int userId = 0;
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[MobileEmailExists]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmailAddress", emailAddress);
                //sqlCommand.Parameters.AddWithValue("@Password", password);
                sqlCommand.Parameters.AddWithValue("@LoginType", loginType);

                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                    //userId = reader["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserId"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static int CheckDeviceInShardManager(int deviceId)
        {
            int result = 0;
            try
            {
                SqlConnection sqlConnection = SqlDatabaseUtils.connectToMapManagerDB();
                SqlCommand sqlCommand = new SqlCommand("[CheckForDeviceTokenANDInsert]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                
                SqlParameter device = sqlCommand.Parameters.Add("@DeviceId", System.Data.SqlDbType.Int);
                device.Direction = System.Data.ParameterDirection.Input;
                device.Value = deviceId;
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    result = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static UserInfoDto checkForUserAndInsert(UserInfoDto userInfoDto)
        {
            UserInfoDto userInfo = null;
            try
            {
                //SqlConnection myConn = ConnectTODB();
                userInfoDto.FacebookId = userInfoDto.FacebookId != null ? userInfoDto.FacebookId : string.Empty;
                userInfoDto.ProfilePicUrl = userInfoDto.ProfilePicUrl != null ? userInfoDto.ProfilePicUrl : string.Empty;
                //check admin user ...
                SqlCommand cmd = new SqlCommand("[AddUserInfo]");
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@userId", userInfoDto.UserId);
                cmd.Parameters.AddWithValue("@name", userInfoDto.Name);
                //cmd.Parameters.AddWithValue("@libraryId", userInfoDto.LibraryId);
                cmd.Parameters.AddWithValue("@email", userInfoDto.email);
                cmd.Parameters.AddWithValue("@facebookId", userInfoDto.FacebookId);
                cmd.Parameters.AddWithValue("@profilepicurl", userInfoDto.ProfilePicUrl);
                cmd.Parameters.AddWithValue("@Gender", userInfoDto.Gender);
                cmd.Parameters.AddWithValue("@LoginType", userInfoDto.LoginType);
                cmd.Parameters.AddWithValue("@DeviceId", userInfoDto.DeviceId);
                cmd.Parameters.AddWithValue("@DeviceType", userInfoDto.DeviceType);
                cmd.Parameters.AddWithValue("@SportId", userInfoDto.SportId);
                cmd.Parameters.AddWithValue("@Password", userInfoDto.Password);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(userInfoDto.UserId, cmd, out reader);
                // reader.Close();
                while (reader != null && reader.Read())
                {
                    userInfo = new UserInfoDto()
                    {
                        UserId = reader["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserId"]),
                        Name = (reader["Name"] == DBNull.Value || reader["Name"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["Name"]),
                        //LibraryId = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]),
                        email = (reader["email"] == DBNull.Value || reader["email"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["email"]),
                        ProfilePicUrl = (reader["ProfilePicUrl"] == DBNull.Value || reader["ProfilePicUrl"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["ProfilePicUrl"]),
                        FacebookId = (reader["FacebookId"] == DBNull.Value || reader["FacebookId"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["FacebookId"]),
                        Gender = reader["Gender"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Gender"]),
                        LoginType = reader["LoginType"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LoginType"]),
                        DeviceId = (reader["DeviceId"] == DBNull.Value || reader["DeviceId"] == string.Empty)
                                    ? 0
                                    : Convert.ToInt32(reader["DeviceId"]),
                        //LibraryName = (reader["LibraryName"] == DBNull.Value || reader["LibraryName"] == string.Empty)
                        //? string.Empty
                        //: Convert.ToString(reader["LibraryName"]),
                        SportName = (reader["SportName"] == DBNull.Value || reader["SportName"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["SportName"]),
                        DeviceType = reader["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DeviceType"]),
                        SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]),
                        SportsBannerImageUrl = (reader["BannerImageUrl"] == DBNull.Value || reader["BannerImageUrl"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["BannerImageUrl"]),
                        Password = reader["Password"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Password"])
                    };
                }
                reader.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return userInfo;
        }

        /// <summary>
        /// To check whether the user exists in the shard manager DB or not.
        /// </summary>
        /// <param name="userInfoDto"> User information without UserId and Library Id </param>
        /// <returns>User Information with UserId and without Library Id</returns>
        internal static UserInfoDto CheckUserExists(UserInfoDto userInfoDto, out int flag)
        {
            try
            {
                SqlConnection sqlConnection = SqlDatabaseUtils.connectToMapManagerDB();
                SqlCommand sqlCommand = new SqlCommand("[spCheckForUser]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter usrname = sqlCommand.Parameters.Add("@emailId", System.Data.SqlDbType.NVarChar, 255);
                usrname.Direction = System.Data.ParameterDirection.Input;
                usrname.Value = userInfoDto.email;
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                flag = 0;
                while (sqlDataReader.Read())
                {
                    userInfoDto.UserId = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    flag = sqlDataReader["userexists"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["userexists"]);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return userInfoDto;
        }

        internal static int CheckUserInShardManager(string emailId, int deviceId, out int Exists)
        {
            int result = 0;
            try
            {
                SqlConnection sqlConnection = SqlDatabaseUtils.connectToMapManagerDB();
                SqlCommand sqlCommand = new SqlCommand("[CheckForUserAndInsert]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter usrname = sqlCommand.Parameters.Add("@emailId", System.Data.SqlDbType.NVarChar, 255);
                usrname.Direction = System.Data.ParameterDirection.Input;
                usrname.Value = emailId.Trim().ToLower();
                SqlParameter device = sqlCommand.Parameters.Add("@DeviceId", System.Data.SqlDbType.Int);
                device.Direction = System.Data.ParameterDirection.Input;
                device.Value = deviceId;
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                Exists = 0;
                while (sqlDataReader.Read())
                {
                    result = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    Exists = sqlDataReader["AlreadyExists"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AlreadyExists"]);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        /// <summary>
        /// Checking whether the user exists in the db or not when they logged in from the mobile app
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        internal static int MobileUserExits(string emailAddress, string password, int userId, int loginType, out int result)
        {
             result = 0;
            //int userId = 0;
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[MobileUserExists]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmailAddress", emailAddress);
                sqlCommand.Parameters.AddWithValue("@Password", password);
                sqlCommand.Parameters.AddWithValue("@LoginType", loginType);

                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                    userId = reader["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserId"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return userId;
        }
        internal static DeviceInfoDTO RegisterDevice(DeviceInfoDTO deviceInfo)
        {
            DeviceInfoDTO deviceInfoDto = null;
            try
            {
                SqlConnection sqlConnection = SqlDatabaseUtils.connectToMapManagerDB();
                SqlCommand sqlCommand = new SqlCommand("[CheckForDeviceAndInsert]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter deviceId = sqlCommand.Parameters.Add("@DeviceId", System.Data.SqlDbType.NVarChar, 1024);
                deviceId.Direction = System.Data.ParameterDirection.Input;
                deviceId.Value = deviceInfo.DeviceId.Trim();
                SqlParameter deviceType = sqlCommand.Parameters.Add("@DeviceType", System.Data.SqlDbType.Int);
                deviceType.Direction = System.Data.ParameterDirection.Input;
                deviceType.Value = deviceInfo.DeviceType;
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    deviceInfoDto = new DeviceInfoDTO();
                    deviceInfoDto.Id = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    deviceInfoDto.DeviceId = sqlDataReader["DeviceToken"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["DeviceToken"]);
                    deviceInfoDto.DeviceType = sqlDataReader["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeviceType"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return deviceInfoDto;
        }

        internal static UserInfoDto GetDeviceInformation(int userId)
        {
            UserInfoDto userInfoDto = null;
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetDeviceUserInfo]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@userId", userId);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out reader);
                while (reader != null && reader.Read())
                {
                    userInfoDto = new UserInfoDto();
                    userInfoDto.UserId = reader["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserId"]);
                    userInfoDto.Name = (reader["Name"] == DBNull.Value || reader["Name"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["Name"]);
                    // userInfoDto.LibraryId = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]);
                    userInfoDto.email = (reader["email"] == DBNull.Value || reader["email"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["email"]);
                    userInfoDto.ProfilePicUrl = (reader["ProfilePicUrl"] == DBNull.Value || reader["ProfilePicUrl"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["ProfilePicUrl"]);
                    userInfoDto.FacebookId = (reader["FacebookId"] == DBNull.Value || reader["FacebookId"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["FacebookId"]);
                    userInfoDto.Gender = reader["Gender"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Gender"]);
                    userInfoDto.LoginType = reader["LoginType"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LoginType"]);
                    //userInfoDto.DeviceId = (reader["DeviceId"] == DBNull.Value || reader["DeviceId"] == string.Empty)
                    //            ? string.Empty
                    //            : Convert.ToString(reader["DeviceId"]);
                    //userInfoDto.LibraryName = (reader["LibraryName"] == DBNull.Value || reader["LibraryName"] == string.Empty)
                    //            ? string.Empty
                    //            : Convert.ToString(reader["LibraryName"]);
                    userInfoDto.SportName = (reader["SportName"] == DBNull.Value || reader["SportName"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["SportName"]);
                    userInfoDto.DeviceType = reader["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DeviceType"]);
                    userInfoDto.SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]);
                    userInfoDto.SportsBannerImageUrl = (reader["BannerImageUrl"] == DBNull.Value || reader["BannerImageUrl"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["BannerImageUrl"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return userInfoDto;
        }
      
        internal static int CheckDeviceRegistered(int deviceId, out int flag)
        {
            int result = 0;
            try
            {
                SqlConnection sqlConnection = SqlDatabaseUtils.connectToMapManagerDB();
                SqlCommand sqlCommand = new SqlCommand("[spCheckForDevice]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter usrname = sqlCommand.Parameters.Add("@DeviceId", System.Data.SqlDbType.Int);
                usrname.Direction = System.Data.ParameterDirection.Input;
                usrname.Value = deviceId;
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                flag = 0;
                while (sqlDataReader.Read())
                {
                    result = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    flag = sqlDataReader["userexists"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["userexists"]);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }
        internal static int UserExits(string emailAddress, string password,int ChannelId)
        {
           int  result = 0;
            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[UserExists]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@EmailAddress", emailAddress);
                command.Parameters.AddWithValue("@Password", password);
                command.Parameters.AddWithValue("@ChannelId", ChannelId);

                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static string GetUserExists(string emailAddress)
        {
            string code = string.Empty;

            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[GetLoginUserExists]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@EmailAddress", emailAddress);
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    code = reader["Code"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Code"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return code;
        }

        private static void ExecuteAddUserQuery(RangeShardMap<int> shardMap, string p, int UserId, string deviceId, int deviceType)
        {
            SqlDatabaseUtils.SqlRetryPolicy.ExecuteAction(() =>
            {
                // Looks up the key in the shard map and opens a connection to the shard
                using (SqlConnection conn = shardMap.OpenConnectionForKey(UserId, p))
                {
                    // Create a simple command that will insert or update the customer information
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = @"
                    IF NOT EXISTS (SELECT 1 FROM Device WHERE PhoneID = @DeviceId)
                        INSERT INTO Device (PhoneID, Type, Status,CreatedBy,CreatedDate,IsFirst)
                        VALUES (@DeviceId, @DeviceType, 1,0,GETUTCDATE(),1)";
                    cmd.Parameters.AddWithValue("@DeviceId", deviceId);
                    cmd.Parameters.AddWithValue("@DeviceType", deviceType);

                    cmd.CommandTimeout = 60;

                    // Execute the command
                    cmd.ExecuteNonQuery();
                }
            });
        }

        internal static int FeatureTracking(FeatureTrackingDto featuresDto)
        {
            int result = 0;
            try
            {
                //SqlConnection myConn = ConnectTODB();

                //check admin user ...
                SqlCommand sqlCommand = new SqlCommand("[FeatureViewTracking]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FeatureId", featuresDto.FeatureId);
                sqlCommand.Parameters.AddWithValue("@DeviceId", featuresDto.DeviceId);
                sqlCommand.Parameters.AddWithValue("@DeviceType", featuresDto.DeviceType);
                sqlCommand.Parameters.AddWithValue("@Value", featuresDto.Value);
                sqlCommand.Parameters.AddWithValue("@CreatedBy", featuresDto.UserId);
            
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(featuresDto.UserId, sqlCommand, out reader);
                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static void AddException(string message)
        {
           
            try
            {
                //SqlConnection myConn = ConnectTODB();

                //check admin user ...
                SqlCommand sqlCommand = new SqlCommand("[CreateException]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@Name", message);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(10, sqlCommand, out reader);
                while (reader != null && reader.Read())
                {
                  var result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            
        }

        internal static string UploadImageFromBase64(string binary, string format)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999).ToString() + DateTime.Now.Millisecond + "." + format;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(binary);


                blockBlob.Properties.ContentType = "image/jpeg";
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                imageurl = blockBlob1.Uri.ToString();


            }
            catch (Exception ex)
            {
                string s = ex.Message;
                throw;
            }
            return imageurl;

        }

        internal static UsageTrackingDTO UsageTracking(int channelId)
        {
            UsageTrackingDTO usage = new UsageTrackingDTO();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetUsageCount]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@ChannelId", channelId);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(10, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    usage.ChannelSubscribers = reader["ChannelCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["ChannelCount"]);
                    usage.PackageSubscribers = reader["PackageCount"] == DBNull.Value ? 0 : Convert.ToInt32(reader["PackageCount"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return usage;
        }

        internal static int UpdateChannelInfo(AdminUsersDto UsersDto)
        {
            int result = 0;
            try
            {
                UsersDto.VideoURL = string.IsNullOrWhiteSpace(UsersDto.VideoURL) ? string.Empty : UsersDto.VideoURL;
                UsersDto.ThumbNailURL = string.IsNullOrWhiteSpace(UsersDto.ThumbNailURL) ? string.Empty : UsersDto.ThumbNailURL;
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[UpdateChannelInfo]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@FirstName", UsersDto.FirstName);
                command.Parameters.AddWithValue("@LastName", UsersDto.LastName);
                command.Parameters.AddWithValue("@ProfilePicUrl", UsersDto.ProfilePicURL);
                command.Parameters.AddWithValue("@VideoUrl", UsersDto.VideoURL);
                command.Parameters.AddWithValue("@ThumbNailUrl", UsersDto.ThumbNailURL);
                command.Parameters.AddWithValue("@description", UsersDto.Description);
                command.Parameters.AddWithValue("@Id", UsersDto.Id);
                
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);
                while (reader != null && reader.Read())
                {
                    result = reader["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserId"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static string UpdateAdminPassword(string userName, string token, string confirmPassword)
        {
            try
            {
               
                    MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                    MultiShardCommand command = multiShardConnection.CreateCommand();
                    command.CommandText = "[UpdateParentPassword]";
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Token", token);
                    command.Parameters.AddWithValue("@Emailaddress", userName);
                    command.Parameters.AddWithValue("@Password", confirmPassword);
                    MultiShardDataReader myData = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                    while (myData.Read())
                    {

                        userName = myData["Email"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Email"]);

                    }
                    myData.Close();
                    //myConn.Close();
               
            }
            catch (Exception e)
            {

                throw;
            }

            return userName;
        }

        internal static int AddFeedback(FeedbackDTO fb, int userId)
        {
            int result = 0;
            int phoneid = 1;
            try
            {
                //SqlConnection myConn = ConnectTODB();

                //check admin user ...
                SqlCommand sqlCommand = new SqlCommand("[AddFeedback]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@PhoneID", fb.DeviceId);
                sqlCommand.Parameters.AddWithValue("@Name", fb.Name);
                sqlCommand.Parameters.AddWithValue("@Comment", fb.Comment);
                sqlCommand.Parameters.AddWithValue("@emailaddress", fb.EmailAddress);
                sqlCommand.Parameters.AddWithValue("@DeviceType", fb.DeviceType);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@UserRating", fb.UserRating);
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out reader);
                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static int AddUserIntoShardManager(string emailId, int deviceId)
        {
            int result = 0;
            try
            {
                SqlConnection sqlConnection = SqlDatabaseUtils.connectToMapManagerDB();
                SqlCommand sqlCommand = new SqlCommand("[spCheckForUserAndInsert]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter usrname = sqlCommand.Parameters.Add("@emailId", System.Data.SqlDbType.NVarChar, 255);
                usrname.Direction = System.Data.ParameterDirection.Input;
                usrname.Value = emailId.Trim();
                SqlParameter DeviceId = sqlCommand.Parameters.Add("@DeviceId", System.Data.SqlDbType.Int);
                DeviceId.Direction = System.Data.ParameterDirection.Input;
                DeviceId.Value = deviceId;
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    result = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static UserInfoDto UpdateLibraryInfo(int userId, int sportId)
        {
            UserInfoDto userInfo = null;
            try
            {
                //SqlConnection myConn = ConnectTODB();
                
                SqlCommand sqlCommand = new SqlCommand("[UpdateSportId]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@SportId", sportId);
                
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out reader);
                while (reader != null && reader.Read())
                {
                    userInfo = new UserInfoDto()
                    {
                        UserId = reader["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserId"]),
                        Name = (reader["Name"] == DBNull.Value || reader["Name"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["Name"]),
                        //LibraryId = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]),
                        email = (reader["email"] == DBNull.Value || reader["email"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["email"]),
                        ProfilePicUrl = (reader["ProfilePicUrl"] == DBNull.Value || reader["ProfilePicUrl"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["ProfilePicUrl"]),
                        FacebookId = (reader["FacebookId"] == DBNull.Value || reader["FacebookId"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["FacebookId"]),
                        Gender = reader["Gender"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Gender"]),
                        LoginType = reader["LoginType"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LoginType"]),
                        //DeviceId = (reader["DeviceId"] == DBNull.Value || reader["DeviceId"] == string.Empty)
                        //            ? string.Empty
                        //            : Convert.ToString(reader["DeviceId"]),
                        //LibraryName = (reader["LibraryName"] == DBNull.Value || reader["LibraryName"] == string.Empty)
                        //? string.Empty
                        //: Convert.ToString(reader["LibraryName"]),
                        SportName = (reader["SportName"] == DBNull.Value || reader["SportName"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["SportName"]),
                        DeviceType = reader["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(reader["DeviceType"]),
                        SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]),
                        SportsBannerImageUrl = (reader["BannerImageUrl"] == DBNull.Value || reader["BannerImageUrl"] == string.Empty)
                                    ? string.Empty
                                    : Convert.ToString(reader["BannerImageUrl"])
                    };
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return userInfo;
        }

        internal static int UserRegistration(AdminUsersDto UsersDto, out int flag)
        {
            int result = 0;
            Guid obj = Guid.NewGuid();
            try
            {
                UsersDto.VideoURL = string.IsNullOrWhiteSpace(UsersDto.VideoURL) ? string.Empty : UsersDto.VideoURL;
                UsersDto.ThumbNailURL = string.IsNullOrWhiteSpace(UsersDto.ThumbNailURL) ? string.Empty : UsersDto.ThumbNailURL;
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[CreateAdminUser]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@FirstName", UsersDto.FirstName);
                command.Parameters.AddWithValue("@LastName", UsersDto.LastName);
                command.Parameters.AddWithValue("@ChannelName", UsersDto.ChannelName);
                command.Parameters.AddWithValue("@EmailAddress", UsersDto.EmailAddress);
                command.Parameters.AddWithValue("@Password", UsersDto.Password);
                command.Parameters.AddWithValue("@SportId", UsersDto.SportId);
                command.Parameters.AddWithValue("@ProfilePicUrl", UsersDto.ProfilePicURL);
                command.Parameters.AddWithValue("@VideoUrl", UsersDto.VideoURL);
                command.Parameters.AddWithValue("@ThumbNailUrl", UsersDto.ThumbNailURL);
                command.Parameters.AddWithValue("@description", UsersDto.Description);
                command.Parameters.AddWithValue("@Id", UsersDto.Id);
                command.Parameters.AddWithValue("@PricingCatalogId", UsersDto.PricingCatalogId);
                command.Parameters.AddWithValue("@AuthCode", Convert.ToString(obj).ToUpper());
                command.Parameters.AddWithValue("@LoginType", Convert.ToInt32(UsersDto.LoginType));
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);
                flag = 0;
                while (reader != null && reader.Read())
                {
                    flag = reader["UserExists"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserExists"]);
                    result = reader["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["UserId"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static AdminUsersDto GetAdminUserInformation(string emailAddress, string password)
        {
            AdminUsersDto adminUsersDTO = null;
            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[GetAdminUserInfo]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@EmailAddress", emailAddress);
                command.Parameters.AddWithValue("@Password", password);
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    adminUsersDTO = new AdminUsersDto();
                    adminUsersDTO.Id = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]);
                    adminUsersDTO.ChannelName = (reader["ChannelName"] == DBNull.Value || reader["ChannelName"] == string.Empty)
                             ? string.Empty
                             : Convert.ToString(reader["ChannelName"]);
                    adminUsersDTO.EmailAddress = reader["EmailAddress"] == DBNull.Value ? string.Empty : Convert.ToString(reader["EmailAddress"]);
                    adminUsersDTO.FirstName = reader["FirstName"] == DBNull.Value ? string.Empty : Convert.ToString(reader["FirstName"]);
                    adminUsersDTO.LastName = reader["LastName"] == DBNull.Value ? string.Empty : Convert.ToString(reader["LastName"]);
                    adminUsersDTO.Password = reader["Password"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Password"]);
                    adminUsersDTO.SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]);
                    adminUsersDTO.SportName = reader["SportName"] == DBNull.Value ? string.Empty : Convert.ToString(reader["SportName"]);
                    adminUsersDTO.Description = reader["Description"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Description"]);
                    adminUsersDTO.ProfilePicURL = reader["ProfilePicURL"] == DBNull.Value ? string.Empty : Convert.ToString(reader["ProfilePicURL"]);
                    adminUsersDTO.PricingCatalogId = reader["PricingCatalogId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["PricingCatalogId"]);
                    adminUsersDTO.LoginType = reader["LoginType"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LoginType"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return adminUsersDTO;
        }


        internal static string GetSubAdminUserExists(string emailAddress)
        {
            string code = string.Empty;

            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[GetSubAdminExists]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@EmailAddress", emailAddress);
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    code = reader["Code"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Code"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return code;
        }


        internal static string UpdateSubAdminPassword(string userName, string token, string confirmPassword)
        {
            try
            {

                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[UpdateSubAdminPassword]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Token", token);
                command.Parameters.AddWithValue("@Emailaddress", userName);
                command.Parameters.AddWithValue("@Password", confirmPassword);
                MultiShardDataReader myData = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (myData.Read())
                {

                    userName = myData["Email"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Email"]);

                }
                myData.Close();
                //myConn.Close();

            }
            catch (Exception e)
            {

                throw;
            }

            return userName;
        }

        internal static List<VideographersDTO> GetVideographersList()
        {
            List<VideographersDTO> Videographers = new List<VideographersDTO>();
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetVideographersList]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure; 
                SqlDataReader reader;
                BaseUtils.executeReaderCommandOnSingleShard(33, sqlCommand, out reader); 
      

                while (reader != null && reader.Read())
                {
                    VideographersDTO Videographer = new VideographersDTO();
                    Videographer.Id = reader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Id"]);
                    Videographer.Name = (reader["Name"] == DBNull.Value || reader["Name"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["Name"]);
                    Videographer.Address = reader["Address"] == DBNull.Value ? string.Empty : reader["Address"].ToString();
                    Videographer.Phonenumber = (reader["Phonenumber"] == DBNull.Value || reader["Phonenumber"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["Phonenumber"]);
                    Videographer.Emailaddress = (reader["Emailaddress"] == DBNull.Value || reader["Emailaddress"] == string.Empty)
                                ? string.Empty
                                : Convert.ToString(reader["Emailaddress"]);
                    Videographer.CreatedBy = (reader["CreatedBy"] == DBNull.Value || reader["CreatedBy"] == string.Empty)
                                ? 0
                                : Convert.ToInt32(reader["CreatedBy"]);
                    Videographers.Add(Videographer);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return Videographers;
        }


        internal static int AddVideographer(VideographersDTO Videographer)
        {
            int result = 0;
            Guid obj = Guid.NewGuid();
            try
            { 
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[AddVideographer]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Name", Videographer.Name);
                command.Parameters.AddWithValue("@Phonenumber", Videographer.Phonenumber);
                command.Parameters.AddWithValue("@Address", Videographer.Address);
                command.Parameters.AddWithValue("@EmailAddress", Videographer.Emailaddress);
                command.Parameters.AddWithValue("@CreatedBy", Videographer.CreatedBy); 
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command); 
                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]); 
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }
        internal static SupportMailDTO GetSupportMails(string name)
        {
            SupportMailDTO SupportInfo = new SupportMailDTO();
            try
            {
                //SqlConnection myConn = ConnectTODB();

                SqlCommand sqlCommand = new SqlCommand("[GetSupportMails]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Name", name);

                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(10, sqlCommand, out reader);
                while (reader != null && reader.Read())
                {
                    SupportInfo.EmailAddress = reader["Emailaddress"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Emailaddress"]);
                    SupportInfo.Password = reader["Password"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Password"]);
                    SupportInfo.Host = reader["Host"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Host"]);
                    SupportInfo.SSL = reader["SSL"] == DBNull.Value ? string.Empty : Convert.ToString(reader["SSL"]);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return SupportInfo;
        }
    }
}