﻿using CopyMeSportsWebAPI.ShardUtils;
using Microsoft.Azure.SqlDatabase.ElasticScale.Query;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CopyMeSportsWebAPI.Models
{
    public class SubAdminModel
    {
        internal static int AddSubAdmin(SubAdminDTO adminDto)
        {
            int result = 0;
            Guid obj = Guid.NewGuid();
            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[CreateSubAdmin]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@AdminId", adminDto.AdminId);
                command.Parameters.AddWithValue("@EmailAddress", adminDto.EmailAddress);
                command.Parameters.AddWithValue("@Password", adminDto.Password);
                command.Parameters.AddWithValue("@AuthCode", Convert.ToString(obj).ToUpper());
                MultiShardDataReader myData = BaseUtils.ExecuteReaderCommandOnMultiShard(command);
               
                while (myData != null && myData.Read())
                {
                    result = myData["result"] == DBNull.Value ? 0 : Convert.ToInt32(myData["result"]);
                    //SubAdminDTO admin = new SubAdminDTO();
                    //admin.SubAdminId = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                    //admin.AdminId = myData["AdminId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["AdminId"]);
                    //admin.EmailAddress = myData["EmailAddress"] == DBNull.Value ? string.Empty : Convert.ToString(myData["EmailAddress"]);
                    //admin.Password = myData["Password"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Password"]);
                    //admin.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    //subAdmin.Add(admin);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<ChannelDTO> GetAllChannels()
        {
            List<ChannelDTO> ChannelDTO = new List<ChannelDTO>();
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetAllChannels]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(20, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    //result = myData["result"] == DBNull.Value ? 0 : Convert.ToInt32(myData["result"]);
                    ChannelDTO channel = new ChannelDTO();
                    channel.ChannelId = myData["ChannelId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ChannelId"]);
                    channel.ChannelName = myData["Channelname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Channelname"]);
                    channel.Email = myData["EmailAddress"] == DBNull.Value ? string.Empty : Convert.ToString(myData["EmailAddress"]);
                    channel.SubAdmins = GetSubAdminDetails(channel.ChannelId);
                    ChannelDTO.Add(channel);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return ChannelDTO;
        }

        internal static AdminUsersDto GetChannelInformation(string emailAddress, string password, int channelId)
        {
            AdminUsersDto adminUsersDTO = null;
            try
            {
                SqlCommand command = new SqlCommand("[GetChannelBySubAdmin]");
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader reader;

                command.Parameters.AddWithValue("@EmailAddress", emailAddress);
                command.Parameters.AddWithValue("@Password", password);
                command.Parameters.AddWithValue("@ChannelId", channelId);
               // MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                BaseUtils.executeReaderCommandOnSingleShard(20, command, out reader);

                while (reader != null && reader.Read())
                {
                    adminUsersDTO = new AdminUsersDto();
                    adminUsersDTO.Id = reader["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["LibraryId"]);
                    adminUsersDTO.ChannelName = (reader["ChannelName"] == DBNull.Value || reader["ChannelName"] == string.Empty)
                             ? string.Empty
                             : Convert.ToString(reader["ChannelName"]);
                    adminUsersDTO.EmailAddress = reader["EmailAddress"] == DBNull.Value ? string.Empty : Convert.ToString(reader["EmailAddress"]);
                    adminUsersDTO.FirstName = reader["FirstName"] == DBNull.Value ? string.Empty : Convert.ToString(reader["FirstName"]);
                    adminUsersDTO.LastName = reader["LastName"] == DBNull.Value ? string.Empty : Convert.ToString(reader["LastName"]);
                    adminUsersDTO.Password = reader["Password"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Password"]);
                    adminUsersDTO.SportId = reader["SportId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SportId"]);
                    adminUsersDTO.SportName = reader["SportName"] == DBNull.Value ? string.Empty : Convert.ToString(reader["SportName"]);
                    adminUsersDTO.Description = reader["Description"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Description"]);
                    adminUsersDTO.ProfilePicURL = reader["ProfilePicURL"] == DBNull.Value ? string.Empty : Convert.ToString(reader["ProfilePicURL"]);
                    adminUsersDTO.PricingCatalogId = reader["PricingCatalogId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["PricingCatalogId"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return adminUsersDTO;
        }

        internal static int AdminUserExists(string emailAddress, string password, int channelId, out int result)
        {
            result = 0;
            int userId = 0;
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[LoginSubAdmin]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@EmailAddress", emailAddress);
                sqlCommand.Parameters.AddWithValue("@Password", password);
                sqlCommand.Parameters.AddWithValue("@ChannelId", channelId);

                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    result = reader["AdminExists"] == DBNull.Value ? 0 : Convert.ToInt32(reader["AdminExists"]);
                    userId = reader["SubAdminId"] == DBNull.Value ? 0 : Convert.ToInt32(reader["SubAdminId"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return userId;
        }

        internal static List<SubAdminDTO> GetSubAdminDetails(int adminId)
        {
            List<SubAdminDTO> subAdmin = new List<SubAdminDTO>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetSubAdminsByAdmin]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AdminId", adminId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(20, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    //result = myData["result"] == DBNull.Value ? 0 : Convert.ToInt32(myData["result"]);
                    SubAdminDTO admin = new SubAdminDTO();
                    admin.SubAdminId = myData["SubAdminID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SubAdminID"]);
                    admin.AdminId = myData["AdminId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["AdminId"]);
                    admin.ChannelName = myData["ChannelName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ChannelName"]);
                    admin.EmailAddress = myData["EmailAddress"] == DBNull.Value ? string.Empty : Convert.ToString(myData["EmailAddress"]);
                    admin.Password = myData["Password"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Password"]);
                    admin.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    subAdmin.Add(admin);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return subAdmin;
        }
    }
}