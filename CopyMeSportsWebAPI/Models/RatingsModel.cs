﻿using CopyMeSportsWebAPI.ShardUtils;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CopyMeSportsWebAPI.Models
{
    public class RatingsModel
    {
        internal static List<Ratings> PackageRating(int packageId, int rating, int userId)
        {
            List<Ratings> ratings = new List<Ratings>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[PackagesRating]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PackageId", packageId);
                sqlCommand.Parameters.AddWithValue("@Rating", rating);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    Ratings rate = new Ratings();
                    rate.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                    rate.ChannelId = myData["PackageId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PackageId"]);
                    rate.Type = 2;
                    rate.Rating = myData["Rating"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Rating"]);
                    rate.CreatedBy = myData["CreatedBy"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CreatedBy"]);
                    rate.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    ratings.Add(rate);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return ratings;
        }

        internal static List<Ratings> ChannelRating(int channelId, int rating, int userId)
        {
            List<Ratings> ratings = new List<Ratings>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[ChannelsRating]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ChannelId", channelId);
                sqlCommand.Parameters.AddWithValue("@Rating", rating);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    Ratings rate = new Ratings();
                    rate.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                    rate.ChannelId = myData["ChannelId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ChannelId"]);
                    rate.Type = 1;
                    rate.Rating = myData["Rating"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Rating"]);
                    rate.CreatedBy = myData["CreatedBy"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CreatedBy"]);
                    rate.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    ratings.Add(rate);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return ratings;
        }

        internal static double AddRatingToChannel(int channelId, double rating, int userId)
        {
            double result = 0;

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[ChannelRating]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PackageId", channelId);
                sqlCommand.Parameters.AddWithValue("@Rating", Convert.ToString(rating));
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    result = myData["result"] == DBNull.Value ? 0 : Convert.ToDouble(myData["result"]);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<Ratings> GetRatingsForLibrary(int channelId, int userId)
        {
            List<Ratings> ratings = new List<Ratings>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetChannelsRating]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@ChannelId", channelId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    Ratings rate = new Ratings();
                    rate.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                    rate.ChannelId = myData["ChannelId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ChannelId"]);
                    rate.Type = 1;
                    rate.Rating = myData["Rating"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Rating"]);
                    rate.CreatedBy = myData["CreatedBy"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CreatedBy"]);
                    rate.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    ratings.Add(rate);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return ratings;
        }
    }
}