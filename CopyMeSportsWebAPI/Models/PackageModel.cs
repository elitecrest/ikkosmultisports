﻿using CopyMeSportsWebAPI.ShardUtils;
using Microsoft.Azure.SqlDatabase.ElasticScale.Query;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
namespace CopyMeSportsWebAPI.Models
{
    internal static class PackageModel
    {
        #region Mobile App methods
        internal static List<PackagesInfoDTO> GetFavouritePackages(int userId)
        {
            List<PackagesInfoDTO> videos = new List<PackagesInfoDTO>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetFavouritePackages]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    PackagesInfoDTO v = new PackagesInfoDTO();
                    v.PackageId = myData["PackageID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PackageID"]);
                    v.PackageName = myData["PackageName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["PackageName"]);
                    v.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                    v.Format = myData["Format"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Format"]);
                    v.LibraryId = myData["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["LibraryId"]);
                    v.VideoURL = myData["VideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["VideoURL"]);
                    v.ThumbNailURL = myData["thumbnailurl"] == DBNull.Value ? Convert.ToString(myData["LibraryImageUrl"]) : Convert.ToString(myData["thumbnailurl"]);
                    v.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    v.Price = myData["price"] == DBNull.Value ? string.Empty : Convert.ToString(myData["price"]);
                    v.PricingCatalogId = myData["PricingCatalogId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PricingCatalogId"]);
                    v.SkuId = myData["SKUID"] == DBNull.Value ? string.Empty : Convert.ToString(myData["SKUID"]);
                    v.Rating = myData["Rating"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Rating"]);
                    v.IsFavourite = myData["IsFavourite"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsFavourite"]);
                    v.IsPaid = myData["IsPaid"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsPaid"]);
                    videos.Add(v);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return videos;
        }

        internal static int AddPackageToFavourites(int packageId, int userId)
        {
            int result = 0;

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[AddPackagesToFavourites]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PackageId", packageId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);
                //while (myData != null && myData.Read())
                //{
                //    v = new PackagesInfoDTO();
                //    v.PackageId = myData["PackageID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PackageID"]);
                //    v.PackageName = myData["PackageName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["PackageName"]);
                //    v.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                //    v.Format = myData["Format"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Format"]);
                //    v.LibraryId = myData["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["LibraryId"]);
                //    v.VideoURL = myData["VideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["VideoURL"]);
                //    v.ThumbNailURL = myData["thumbnailurl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["thumbnailurl"]);
                //    v.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                //}
                while (myData != null && myData.Read())
                {
                    result = myData["result"] == DBNull.Value ? 0 : Convert.ToInt32(myData["result"]);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static int DeleteFavouritePack(int packageId, int userId)
        {
            int result = 0;

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[DeleteFavouritePackage]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PackageId", packageId);
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    result = myData["result"] == DBNull.Value ? 0 : Convert.ToInt32(myData["result"]);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static double AddRatingToPackage(int packageId, double rating, int userId)
        {
            double result = 0;

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[PackageRating]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PackageId", packageId);
                sqlCommand.Parameters.AddWithValue("@Rating", Convert.ToString(rating));
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    result = myData["result"] == DBNull.Value ? 0 : Convert.ToDouble(myData["result"]);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }
        #endregion

        #region Admin Portal Methods

        internal static int EditPackageInfo(PackagesInfoDTO packages)
        {
            int result = 0;
            try
            {
                packages.VideoURL = string.IsNullOrWhiteSpace(packages.VideoURL) ? string.Empty : packages.VideoURL;
                packages.ThumbNailURL = string.IsNullOrWhiteSpace(packages.ThumbNailURL) ? string.Empty : packages.ThumbNailURL;
                //packages.PricingCatalogId = 0;
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[UpdatePackage]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@VideoURL", packages.VideoURL);
                command.Parameters.AddWithValue("@CreatedBy", packages.CreatedBy);
                command.Parameters.AddWithValue("@ThumbNailURL", packages.ThumbNailURL);
                command.Parameters.AddWithValue("@PackageName", packages.PackageName);
                command.Parameters.AddWithValue("@Description", packages.Description);
                command.Parameters.AddWithValue("@PricingCatalogId", packages.PricingCatalogId);
                command.Parameters.AddWithValue("@Id", packages.PackageId);
                command.Parameters.AddWithValue("@VideoId", packages.PackageIntroVideoId);
                command.Parameters.AddWithValue("@SportId", packages.SportId);
                command.Parameters.AddWithValue("@LoginType", packages.LoginType);
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static int UpdatePackageVideoInfo(int actualVideoId, int preIntroVideoId, int postIntroVideoId, int packageId, int createdBy, int videoId)
        {
            int result = 0;
            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[UpdateVideoInfoToPackage]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@VideoId", videoId);
                command.Parameters.AddWithValue("@ActualVideoId", actualVideoId);
                command.Parameters.AddWithValue("@PreVideoId", preIntroVideoId);
                command.Parameters.AddWithValue("@PostVideoId", postIntroVideoId);
                command.Parameters.AddWithValue("@PackageId", packageId);
                command.Parameters.AddWithValue("@CreatedBy", createdBy);
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<PricingCatalogDTO> GetPricingCatalog()
        {
            List<PricingCatalogDTO> pricing = new List<PricingCatalogDTO>();
            try
            {
                string currency = string.Empty;
                SqlCommand sqlCommand = new SqlCommand("[PricingCatalogPack]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader reader;

                BaseUtils.executeReaderCommandOnSingleShard(33, sqlCommand, out reader);

                while (reader != null && reader.Read())
                {
                    PricingCatalogDTO price = new PricingCatalogDTO();
                    price.Id = reader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(reader["Id"]);
                    currency = reader["Price"] == DBNull.Value ? string.Empty : Convert.ToString(reader["Price"]);
                    price.Price = (currency.ToUpper() != "FREE") ? ("$" + currency) : currency;
                    price.SKUID = reader["SKUID"] == DBNull.Value ? string.Empty : Convert.ToString(reader["SKUID"]);
                    price.CreatedDate = reader["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(reader["CreatedDate"]);
                    price.PricingType = reader["pricingtype"] == DBNull.Value ? 0 : Convert.ToInt32(reader["pricingtype"]);
                    pricing.Add(price);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return pricing;
        }

        internal static List<PackagesInfoDTO> GetPackagesByID(int channelId, int sportId)
        {
            List<PackagesInfoDTO> videos = new List<PackagesInfoDTO>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetPackagesByAdminUserId]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@LibraryId", channelId);
                sqlCommand.Parameters.AddWithValue("@SportId", sportId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(33, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    PackagesInfoDTO v = new PackagesInfoDTO();
                    v.PackageId = myData["PackageId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PackageId"]);
                    v.PackageName = myData["PackageName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["PackageName"]);
                    v.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                    v.LibraryId = myData["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["LibraryId"]);
                    v.Format = myData["Format"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Format"]);
                    v.VideoURL = myData["VideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["VideoURL"]);
                    v.ThumbNailURL = myData["ThumbNailURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ThumbNailURL"]);
                    v.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    v.PricingCatalogId = myData["PricingCatalogId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PricingCatalogId"]);
                    v.ThumbNailURL = string.IsNullOrWhiteSpace(v.ThumbNailURL) ? (myData["ProfilePicUrl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ProfilePicUrl"])) : v.ThumbNailURL;
                    v.LoginType = myData["LoginType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["LoginType"]);
                    videos.Add(v);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return videos;
        }

        internal static List<PackageVideoURLInfo> GetVideoUrlDetails(int packageId)
        {
            List<PackageVideoURLInfo> urlInfo = new List<PackageVideoURLInfo>(); 

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetVideoUrlInfo]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PackageId", packageId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(33, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {

                    PackageVideoURLInfo url = new PackageVideoURLInfo();
                    url.ActualVideoUrl = myData["VideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["VideoURL"]);
                    url.PostIntroVideoUrl = myData["IntroVideoURL2"] == DBNull.Value ? string.Empty : Convert.ToString(myData["IntroVideoURL2"]);
                    url.PreIntroVideoUrl = myData["IntroVideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["IntroVideoURL"]);
                    url.ActualVideoId = myData["ActualVideoId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ActualVideoId"]);
                    url.PreIntroVideoId = myData["PreVideoId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PreVideoId"]);
                    url.PostIntroVideoId = myData["PostVideoId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PostVideoId"]);
                    url.VideoId = myData["ActualVideoId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ActualVideoId"]);
                    urlInfo.Add(url);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return urlInfo;
        }

        internal static PackagesInfoDTO GetPackage(int packageId)
        {
            PackagesInfoDTO package = null;

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetPackageByPackageId]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PackageId", packageId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(33, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    package = new PackagesInfoDTO();
                    package.PackageId = myData["PackageId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PackageId"]);
                    package.PackageName = myData["PackageName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["PackageName"]);
                    package.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                    package.LibraryId = myData["LibraryId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["LibraryId"]);
                    package.Format = myData["Format"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Format"]);
                    package.VideoURL = myData["VideoURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["VideoURL"]);
                    package.ThumbNailURL = myData["ThumbNailURL"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ThumbNailURL"]);
                    package.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    package.PricingCatalogId = myData["PricingCatalogId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PricingCatalogId"]);
                    package.PackageIntroVideoId = myData["PackageIntroVideoId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PackageIntroVideoId"]);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return package;
        }

        internal static int DeletePackage(int packageId)
        {
            int result = 0;

            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[DeletePackage]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@PackageId", packageId);

                MultiShardDataReader myData = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (myData != null && myData.Read())
                {
                    result = myData["result"] == DBNull.Value ? 0 : Convert.ToInt32(myData["result"]);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static int AddPackageInfo(PackagesInfoDTO packages)
        {
            int result = 0;
            try
            {
                packages.VideoURL = string.IsNullOrWhiteSpace(packages.VideoURL) ? string.Empty : packages.VideoURL;
                packages.ThumbNailURL = string.IsNullOrWhiteSpace(packages.ThumbNailURL) ? string.Empty : packages.ThumbNailURL;
                //packages.PricingCatalogId = 0;
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[CreatePackage]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@VideoURL", packages.VideoURL);
                command.Parameters.AddWithValue("@CreatedBy", packages.CreatedBy);
                command.Parameters.AddWithValue("@ThumbNailURL", packages.ThumbNailURL);
                command.Parameters.AddWithValue("@PackageName", packages.PackageName);
                command.Parameters.AddWithValue("@Description", packages.Description);
                command.Parameters.AddWithValue("@PricingCatalogId", packages.PricingCatalogId);
                command.Parameters.AddWithValue("@Id", packages.PackageId);
                command.Parameters.AddWithValue("@SportId", packages.SportId);
                command.Parameters.AddWithValue("@LoginType", packages.LoginType);
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);


                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static int AddPackageVideoInfo(int videoId, int preVideoId, int postVideoId, int packageId, int createdBy)
        {
            int result = 0;
            try
            {
                MultiShardConnection multiShardConnection = BaseUtils.getMultiShardConnection();
                MultiShardCommand command = multiShardConnection.CreateCommand();
                command.CommandText = "[AddVideoInfoToPackage]";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@VideoId", videoId);
                command.Parameters.AddWithValue("@PreVideoId", preVideoId);
                command.Parameters.AddWithValue("@PostVideoId", postVideoId);
                command.Parameters.AddWithValue("@PackageId", packageId);
                command.Parameters.AddWithValue("@CreatedBy", createdBy);
                MultiShardDataReader reader = BaseUtils.ExecuteReaderCommandOnMultiShard(command);

                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        #endregion
    }
}