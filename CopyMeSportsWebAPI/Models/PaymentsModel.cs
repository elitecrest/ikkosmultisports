﻿using CopyMeSportsWebAPI.ShardUtils;
using Microsoft.Azure.SqlDatabase.ElasticScale.Query;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace CopyMeSportsWebAPI.Models
{
    public class PaymentsModel
    {
        internal static int AddSubscription(PaymentDTO payment, int userId)
        {
            int result = 0;

            try
            {
                payment.SportId = (payment.SportId != null) ? payment.SportId : 0;
                SqlCommand sqlCommand = new SqlCommand("[AddPayment]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@PackageId", payment.PackageId);
                sqlCommand.Parameters.AddWithValue("@Token", payment.PaymentToken);
                sqlCommand.Parameters.AddWithValue("@DeviceType", payment.DeviceType);
                sqlCommand.Parameters.AddWithValue("@PaymentType", payment.PaymentType);// Package or Channel
                sqlCommand.Parameters.AddWithValue("@IsSubscription", payment.IsSubscription); //Monthly = 1, Yearly = 2
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@SportId", payment.SportId);
                sqlCommand.Parameters.AddWithValue("@DeviceId", payment.DeviceId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    result = myData["result"] == DBNull.Value ? 0 : Convert.ToInt32(myData["result"]);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;

        }

        internal static PaymentDTO GetPackageDetails(int packageId, int userId)
        {
            PaymentDTO payment = null;

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetPaymentInfo]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@PackageId", packageId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    payment = new PaymentDTO();
                    payment.PaymentId = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                    payment.UserId = myData["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["UserId"]);
                    payment.PackageId = myData["PackageId"] == DBNull.Value ? 0: Convert.ToInt32(myData["PackageId"]);
                    payment.IsSubscription = myData["IsSubscription"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsSubscription"]);
                    payment.PaymentToken = myData["Token"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Token"]);
                    payment.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    payment.ExpiryDate = myData["ExpiryDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["ExpiryDate"]);
                    payment.DeviceType = myData["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["DeviceType"]);
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return payment;
        }

        internal static bool IsPaidChannel(int channelId, int userId, int sportId)
        {
            PaymentDTO payment = null;
            bool result = false;
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[IsPaidChannel]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@ChannelId", channelId);
                sqlCommand.Parameters.AddWithValue("@SportId", sportId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    payment = new PaymentDTO();
                    payment.PaymentId = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                    payment.UserId = myData["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["UserId"]);
                    payment.PackageId = myData["PackageId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PackageId"]);
                    payment.IsSubscription = myData["IsSubscription"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsSubscription"]);
                    payment.PaymentToken = myData["Token"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Token"]);
                    payment.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    payment.ExpiryDate = myData["ExpiryDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["ExpiryDate"]);
                    payment.DeviceType = myData["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["DeviceType"]);
                }

                if(payment != null && payment.ExpiryDate > DateTime.Now)
                {
                    result = true;
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static bool IsPaidPackage(int packageId, int userId)
        {
            PaymentDTO payment = null;
            bool result = false;
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[IsPaidPackage]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@UserId", userId);
                sqlCommand.Parameters.AddWithValue("@ChannelId", packageId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    payment = new PaymentDTO();
                    payment.PaymentId = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]);
                    payment.UserId = myData["UserId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["UserId"]);
                    payment.PackageId = myData["PackageId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PackageId"]);
                    payment.IsSubscription = myData["IsSubscription"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsSubscription"]);
                    payment.PaymentToken = myData["Token"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Token"]);
                    payment.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                    payment.ExpiryDate = myData["ExpiryDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["ExpiryDate"]);
                }

                if (payment != null && payment.ExpiryDate > DateTime.Now)
                {
                    result = true;
                }
                myData.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }
    }
}