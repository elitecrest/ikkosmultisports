﻿using CopyMeSportsWebAPI.ShardUtils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CopyMeSportsWebAPI.Models
{
    public class TrainingModel
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CopyMeGolf"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }
        internal static List<VideosInfoDTO> GetTrainingPlanVideos(int TrainingPlanId, int userId)
        {
            List<VideosInfoDTO> videos = new List<VideosInfoDTO>();

            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetTrainingPlanVideos]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@TrainingPlanId", TrainingPlanId);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);
                while (myData != null && myData.Read())
                {
                    VideosInfoDTO v = new VideosInfoDTO
                    {
                        ID = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]),
                        Segment = myData["segment"] == DBNull.Value ? string.Empty : Convert.ToString(myData["segment"]),
                        Category = myData["category"] == DBNull.Value ? string.Empty : Convert.ToString(myData["category"]),
                        SubCategory = myData["subcategory"] == DBNull.Value ? string.Empty : Convert.ToString(myData["subcategory"]),
                        MovementName = myData["movementname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["movementname"]),
                        ActorName = myData["actorname"] == DBNull.Value ? string.Empty : Convert.ToString(myData["actorname"]),
                        Orientation = myData["orientation"] == DBNull.Value ? 0 : Convert.ToInt32(myData["orientation"]),
                        Facing = myData["facing"] == DBNull.Value ? 0 : Convert.ToInt32(myData["facing"]),
                        Rating = (float)((myData["averagerating"] == DBNull.Value ? 0 : Convert.ToInt32(myData["averagerating"])) / 20.0),
                        URL = myData["url"] == DBNull.Value ? string.Empty : Convert.ToString(myData["url"]),
                        OwnerID = myData["ownerId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ownerId"]),
                        ThumbNailURL = myData["thumbnailurl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["thumbnailurl"]),
                        PlayCount = myData["PlayCount"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PlayCount"])
                    };
                    videos.Add(v);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return videos;
        }

        internal static List<EventTypeDTO> GetEventTypes(int age, int gender, int userId)
        {
            List<EventTypeDTO> eventTypes = new List<EventTypeDTO>();
            try
            {
                SqlCommand sqlCommand = new SqlCommand("[GetEventTypes]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@age", age);
                sqlCommand.Parameters.AddWithValue("@gender", gender);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                while (myData != null && myData.Read())
                {
                    EventTypeDTO v = new EventTypeDTO
                    {
                        Id = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]),
                        Value = myData["Name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Name"]),
                        Subcategory = myData["SubCategory"] == DBNull.Value ? string.Empty : Convert.ToString(myData["SubCategory"])
                    };
                    eventTypes.Add(v);
                }

            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return eventTypes;
        }

        internal static List<BestTimesValuesDTO> GetBestTimes(int eventid, int age, int gender, int userId)
        {
            List<BestTimesValuesDTO> BestTimes = new List<BestTimesValuesDTO>();

            try
            {
                 SqlCommand sqlCommand = new SqlCommand("[GetBestTimes]");
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@eventid", eventid);
                sqlCommand.Parameters.AddWithValue("@age", age);
                sqlCommand.Parameters.AddWithValue("@gender", gender);
                SqlDataReader myData;

                BaseUtils.executeReaderCommandOnSingleShard(userId, sqlCommand, out myData);

                    while (myData != null && myData.Read())
                    {
                        BestTimesValuesDTO v = new BestTimesValuesDTO{
                        BestTimeId = myData["ID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ID"]),
                        Stroke = myData["Stroke"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Stroke"]),
                        BestTimeValue = myData["Value"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Value"]),
                        };                        
                        BestTimes.Add(v);
                    }
              }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return BestTimes;
        }

        internal static PlanDTO GetTrainingPlan(int Planid, int userId)
        {
            throw new NotImplementedException();
        }

        internal static UserTrainingPlanDTO GetUserTrainingPlanDetails(int TrainingTemplateID, int UserId)
        {
            UserTrainingPlanDTO v = new UserTrainingPlanDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserTrainingPlanDetails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter trainingTemplateId = cmd.Parameters.Add("@trainingTemplateId", SqlDbType.Int);
                    trainingTemplateId.Direction = ParameterDirection.Input;
                    trainingTemplateId.Value = TrainingTemplateID;


                    SqlParameter userid = cmd.Parameters.Add("@Userid", SqlDbType.Int);
                    userid.Direction = ParameterDirection.Input;
                    userid.Value = UserId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {


                        v.TrainingPlanId = (int)myData["Id"];
                        v.Text = myData["Text"].ToString();
                        v.UserId = (int)myData["user_id"];
                        v.ShowQuestion = (bool)myData["ShowQuestion"];
                        v.Question = myData["Text"].ToString();
                        v.ShowRecord = (bool)myData["ShowRecord"];
                        v.RecordedVideoId = (int)myData["recorded_video_id"];
                        v.ShowResults = (bool)myData["ShowResults"];
                        v.started = (bool)myData["started"];
                        v.QuestionAns = myData["QuestionAns"].ToString();
                        v.IdealVideoId = (int)myData["IdealVideoId"];
                        v.StartDate = (DateTime)myData["start_date"];
                        v.BestCurrentTime1 = (TimeSpan)myData["Best_current_time1"];
                        v.BestCurrentTime2 = (TimeSpan)myData["Best_current_time2"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }
    }
}