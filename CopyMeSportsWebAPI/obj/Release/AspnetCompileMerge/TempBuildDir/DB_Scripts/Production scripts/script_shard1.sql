USE [master]
GO
/****** Object:  Database [ElasticScaleStarterKit_Shard1]    Script Date: 21-12-2016 17:04:16 ******/
CREATE DATABASE [ElasticScaleStarterKit_Shard1]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ElasticScaleStarterKit_Shard1].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET ARITHABORT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET RECOVERY FULL 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET  MULTI_USER 
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET DB_CHAINING OFF 
GO
USE [ElasticScaleStarterKit_Shard1]
GO
/****** Object:  Schema [__ShardManagement]    Script Date: 21-12-2016 17:04:17 ******/
CREATE SCHEMA [__ShardManagement]
GO
/****** Object:  StoredProcedure [__ShardManagement].[spAddShardLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spAddShardLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int, 
			@lsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@undo int,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier,
			@name nvarchar(50),
			@sm_kind int,
			@sm_keykind int,
			@shardVersion uniqueidentifier,
			@protocol int,
			@serverName nvarchar(128),
			@port int,
			@databaseName nvarchar(128),
			@shardStatus  int,
			@errorMessage nvarchar(max),
			@errorNumber int,
			@errorSeverity int,
			@errorState int,
			@errorLine int,
			@errorProcedure nvarchar(128)
	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'), 
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@undo = x.value('(@Undo)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@name = x.value('(ShardMap/Name)[1]', 'nvarchar(50)'),
		@sm_kind = x.value('(ShardMap/Kind)[1]', 'int'),
		@sm_keykind = x.value('(ShardMap/KeyKind)[1]', 'int'),
		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier'),
		@shardVersion = x.value('(Shard/Version)[1]', 'uniqueidentifier'),
		@protocol = x.value('(Shard/Location/Protocol)[1]', 'int'),
		@serverName = x.value('(Shard/Location/ServerName)[1]', 'nvarchar(128)'),
		@port = x.value('(Shard/Location/Port)[1]', 'int'),
		@databaseName = x.value('(Shard/Location/DatabaseName)[1]', 'nvarchar(128)'),
		@shardStatus = x.value('(Shard/Status)[1]', 'int')
	from 
		@input.nodes('/AddShardLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @shardMapId is null or @operationId is null or @name is null or @sm_kind is null or @sm_keykind is null or 
		@shardId is null or @shardVersion is null or @protocol is null or @serverName is null or 
		@port is null or @databaseName is null or @shardStatus is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	if exists (
		select 
			ShardMapId
		from
			__ShardManagement.ShardMapsLocal
		where
			ShardMapId = @shardMapId and LastOperationId = @operationId)
		goto Success_Exit;

	begin try
		insert into 
			__ShardManagement.ShardMapsLocal 
			(ShardMapId, Name, MapType, KeyType, LastOperationId)
		values 
			(@shardMapId, @name, @sm_kind, @sm_keykind, @operationId)
	end try
	begin catch
	if (@undo != 1)
	begin
		set @errorMessage = error_message();
		set @errorNumber = error_number();
		set @errorSeverity = error_severity();
		set @errorState = error_state();
		set @errorLine = error_line();
		set @errorProcedure  = isnull(error_procedure(), '-');					
			select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage
			raiserror (@errorMessage, @errorSeverity, 1, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
			rollback transaction; -- To avoid extra error message in response.
			goto Error_UnexpectedError;
	end
	end catch

	begin try
		insert into 
			__ShardManagement.ShardsLocal(
			ShardId, 
			Version, 
			ShardMapId, 
			Protocol, 
			ServerName, 
			Port, 
			DatabaseName, 
			Status,
			LastOperationId)
		values (
			@shardId, 
			@shardVersion, 
			@shardMapId,
			@protocol, 
			@serverName, 
			@port, 
			@databaseName, 
			@shardStatus,
			@operationId)
	end try
	begin catch
	if (@undo != 1)
	begin
		set @errorMessage = error_message();
		set @errorNumber = error_number();
		set @errorSeverity = error_severity();
		set @errorState = error_state();
		set @errorLine = error_line();
		set @errorProcedure  = isnull(error_procedure(), '-');
					
			select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage
			raiserror (@errorMessage, @errorSeverity, 1, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
			rollback transaction; -- To avoid extra error message in response.
			goto Error_UnexpectedError;
	end
	end catch 

	goto Success_Exit;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_UnexpectedError:
	set @result = 53
	goto Exit_Procedure;

Success_Exit:
	set @result = 1
	goto Exit_Procedure;
	
Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spBulkOperationShardMappingsLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spBulkOperationShardMappingsLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@operationCode int,
			@undo int,
			@stepsCount int,
			@shardMapId uniqueidentifier,
			@sm_kind int,
			@shardId uniqueidentifier,
			@shardVersion uniqueidentifier

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@undo = x.value('(@Undo)[1]', 'int'),
		@stepsCount = x.value('(@StepsCount)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier'),
		@shardVersion = x.value('(Shard/Version)[1]', 'uniqueidentifier')
	from 
		@input.nodes('/BulkOperationShardMappingsLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @operationId is null or @stepsCount is null or @shardMapId is null or @shardId is null or @shardVersion is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	if exists (
		select 
			ShardId
		from
			__ShardManagement.ShardsLocal
		where
			ShardMapId = @shardMapId and ShardId = @shardId and Version = @shardVersion and LastOperationId = @operationId)
		goto Success_Exit;

	update __ShardManagement.ShardsLocal
	set
		Version = @shardVersion,
		LastOperationId = @operationId
	where
		ShardMapId = @shardMapId and ShardId = @shardId

	declare	@currentStep xml,
			@stepIndex int = 1,
			@stepType int,
			@stepMappingId uniqueidentifier

	while (@stepIndex <= @stepsCount)
	begin
		select 
			@currentStep = x.query('(./Step[@Id = sql:variable("@stepIndex")])[1]') 
		from 
			@input.nodes('/BulkOperationShardMappingsLocal/Steps') as t(x)

		select
			@stepType = x.value('(@Kind)[1]', 'int'),
			@stepMappingId = x.value('(Mapping/Id)[1]', 'uniqueidentifier')
		from
			@currentStep.nodes('./Step') as t(x)
	
		if (@stepType is null or @stepMappingId is null)
			goto Error_MissingParameters;

		if (@stepType = 1)
		begin
			delete
				__ShardManagement.ShardMappingsLocal
			where
				ShardMapId = @shardMapId and MappingId = @stepMappingId
		end
		else
		if (@stepType = 3)
		begin
			declare @stepMinValue varbinary(128),
					@stepMaxValue varbinary(128),
					@stepMappingStatus int

			select 
				@stepMinValue = convert(varbinary(128), x.value('(Mapping/MinValue)[1]', 'varchar(258)'), 1),
				@stepMaxValue = convert(varbinary(128), x.value('(Mapping/MaxValue[@Null="0"])[1]', 'varchar(258)'), 1),
				@stepMappingStatus = x.value('(Mapping/Status)[1]', 'int')
			from
				@currentStep.nodes('./Step') as t(x)

			if (@stepMinValue is null or @stepMappingStatus is null)
				goto Error_MissingParameters;

			begin try
				insert into
					__ShardManagement.ShardMappingsLocal
					(MappingId, 
					 ShardId, 
					 ShardMapId, 
					 MinValue, 
					 MaxValue, 
					 Status,
					 LastOperationId)
				values
					(@stepMappingId, 
					 @shardId, 
					 @shardMapId, 
					 @stepMinValue, 
					 @stepMaxValue, 
					 @stepMappingStatus,
					 @operationId)
			end try
			begin catch
			if (@undo != 1)
			begin
				declare @errorMessage nvarchar(max) = error_message(),
					@errorNumber int = error_number(),
					@errorSeverity int = error_severity(),
					@errorState int = error_state(),
					@errorLine int = error_line(),
					@errorProcedure nvarchar(128) = isnull(error_procedure(), '-');
					
					select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage
					raiserror (@errorMessage, @errorSeverity, 1, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
					rollback transaction; -- To avoid extra error message in response.
					goto Error_UnexpectedError;
			end
			end catch

			set @stepMinValue = null
			set @stepMaxValue = null
			set @stepMappingStatus = null

		end

		set @stepType = null
		set @stepMappingId = null

		set @stepIndex = @stepIndex + 1
	end

	goto Success_Exit;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_UnexpectedError:
	set @result = 53
	goto Exit_Procedure;

Success_Exit:
	set @result = 1
	goto Exit_Procedure;
	
Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spFindShardMappingByKeyLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spFindShardMappingByKeyLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@keyValue varbinary(128)

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@keyValue = convert(varbinary(128), x.value('(Key/Value)[1]', 'varchar(258)'), 1)
	from 
		@input.nodes('/FindShardMappingByKeyLocal') as t(x)
	
	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @shardMapId is null or @keyValue is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	declare @mapType int

	select 
		@mapType = MapType
	from
		__ShardManagement.ShardMapsLocal
	where
		ShardMapId = @shardMapId

	if (@mapType is null)
		goto Error_ShardMapNotFound;

	if (@mapType = 1)
	begin	
		select
			3, m.MappingId, m.ShardMapId, m.MinValue, m.MaxValue, m.Status, m.LockOwnerId,  -- fields for SqlMapping
			s.ShardId, s.Version, s.ShardMapId, s.Protocol, s.ServerName, s.Port, s.DatabaseName, s.Status -- fields for SqlShard, ShardMapId is repeated here
		from
			__ShardManagement.ShardMappingsLocal m
		join 
			__ShardManagement.ShardsLocal s
		on 
			m.ShardId = s.ShardId
		where
			m.ShardMapId = @shardMapId and 
			m.MinValue = @keyValue
	end
	else
	begin
		select 
			3, m.MappingId, m.ShardMapId, m.MinValue, m.MaxValue, m.Status, m.LockOwnerId,  -- fields for SqlMapping
			s.ShardId, s.Version, s.ShardMapId, s.Protocol, s.ServerName, s.Port, s.DatabaseName, s.Status -- fields for SqlShard, ShardMapId is repeated here
		from 
			__ShardManagement.ShardMappingsLocal m 
		join 
			__ShardManagement.ShardsLocal s 
		on 
			m.ShardId = s.ShardId
		where
			m.ShardMapId = @shardMapId and 
			m.MinValue <= @keyValue and (m.MaxValue is null or m.MaxValue > @keyValue)
	end

	if (@@rowcount = 0)
		goto Error_KeyNotFound;

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_KeyNotFound:
	set @result = 304
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetAllShardMappingsLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetAllShardMappingsLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int, 
			@lsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier,
			@minValue varbinary(128),
			@maxValue varbinary(128)

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier'),
		@minValue = convert(varbinary(128), x.value('(Range[@Null="0"]/MinValue)[1]', 'varchar(258)'), 1),
		@maxValue = convert(varbinary(128), x.value('(Range[@Null="0"]/MaxValue[@Null="0"])[1]', 'varchar(258)'), 1)
	from 
		@input.nodes('/GetAllShardMappingsLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @shardMapId is null or @shardId is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	declare @mapType int

	select 
		@mapType = MapType
	from
		__ShardManagement.ShardMapsLocal
	where
		ShardMapId = @shardMapId

	if (@mapType is null)
		goto Error_ShardMapNotFound;

	declare @minValueCalculated varbinary(128) = 0x,
			@maxValueCalculated varbinary(128) = null

	if (@minValue is not null)
		set @minValueCalculated = @minValue

	if (@maxValue is not null)
		set @maxValueCalculated = @maxValue

	if (@mapType = 1)
	begin	
		select 
			3, m.MappingId, m.ShardMapId, m.MinValue, m.MaxValue, m.Status, m.LockOwnerId,  -- fields for SqlMapping
			s.ShardId, s.Version, s.ShardMapId, s.Protocol, s.ServerName, s.Port, s.DatabaseName, s.Status -- fields for SqlShard, ShardMapId is repeated here
		from 
			__ShardManagement.ShardMappingsLocal m 
			join 
			__ShardManagement.ShardsLocal s 
			on 
				m.ShardId = s.ShardId
		where
			m.ShardMapId = @shardMapId and 
			m.ShardId = @shardId and 
			MinValue >= @minValueCalculated and 
			((@maxValueCalculated is null) or (MinValue < @maxValueCalculated))
		order by 
			m.MinValue
	end
	else
	begin
		select 
			3, m.MappingId, m.ShardMapId, m.MinValue, m.MaxValue, m.Status, m.LockOwnerId,  -- fields for SqlMapping
			s.ShardId, s.Version, s.ShardMapId, s.Protocol, s.ServerName, s.Port, s.DatabaseName, s.Status -- fields for SqlShard, ShardMapId is repeated here
		from 
			__ShardManagement.ShardMappingsLocal m 
			join 
			__ShardManagement.ShardsLocal s 
			on 
				m.ShardId = s.ShardId
		where
			m.ShardMapId = @shardMapId and 
			m.ShardId = @shardId and 
			((MaxValue is null) or (MaxValue > @minValueCalculated)) and 
			((@maxValueCalculated is null) or (MinValue < @maxValueCalculated))
		order by 
			m.MinValue
	end

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetAllShardsLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetAllShardsLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int')
	from 
		@input.nodes('/GetAllShardsLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	select 
		1, ShardMapId, Name, MapType, KeyType
	from 
		__ShardManagement.ShardMapsLocal

	select 
		2, ShardId, Version, ShardMapId, Protocol, ServerName, Port, DatabaseName, Status 
	from
		__ShardManagement.ShardsLocal

	goto Success_Exit;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Success_Exit:
	set @result = 1
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetStoreVersionLocalHelper]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetStoreVersionLocalHelper]
as
begin
	select
		5, StoreVersionMajor, StoreVersionMinor
	from 
		__ShardManagement.ShardMapManagerLocal
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spKillSessionsForShardMappingLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spKillSessionsForShardMappingLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int,
			@patternForKill nvarchar(128)

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@patternForKill = x.value('(Pattern)[1]', 'nvarchar(128)')
	from 
		@input.nodes('/KillSessionsForShardMappingLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @patternForKill is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	declare @tvKillCommands table (spid smallint primary key, commandForKill nvarchar(10))

	insert into 
		@tvKillCommands (spid, commandForKill) 
	values 
		(0, N'')

	insert into 
		@tvKillCommands(spid, commandForKill) 
		select 
			session_id, 'kill ' + convert(nvarchar(10), session_id)
		from 
			sys.dm_exec_sessions 
		where 
			session_id > 50 and program_name like '%' + @patternForKill + '%'

	declare @currentSpid int, 
			@currentCommandForKill nvarchar(10)

	declare @current_error int

	select top 1 
		@currentSpid = spid, 
		@currentCommandForKill = commandForKill 
	from 
		@tvKillCommands 
	order by 
		spid desc

	while (@currentSpid > 0)
	begin
		begin try
			exec (@currentCommandForKill)

			delete 
				@tvKillCommands 
			where 
				spid = @currentSpid

			select top 1 
				@currentSpid = spid, 
				@currentCommandForKill = commandForKill 
			from 
				@tvKillCommands 
			order by 
				spid desc
		end try
		begin catch
			if (error_number() <> 6106)
				goto Error_UnableToKillSessions;
		end catch
	end

	set @result = 1
	goto Exit_Procedure;
	
Error_UnableToKillSessions:
	set @result = 305
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spRemoveShardLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spRemoveShardLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int, 
			@lsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier')
	from 
		@input.nodes('/RemoveShardLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @operationId is null or @shardMapId is null or @shardId is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	delete from
		__ShardManagement.ShardsLocal 
	where
		ShardMapId = @shardMapId and ShardId = @shardId

	delete from
		__ShardManagement.ShardMapsLocal 
	where
		ShardMapId = @shardMapId

	set @result = 1
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spUpdateShardLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [__ShardManagement].[spUpdateShardLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int, 
			@lsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier,
			@shardVersion uniqueidentifier,
			@protocol int,
			@serverName nvarchar(128),
			@port int,
			@databaseName nvarchar(128),
			@shardStatus int

	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier'),
		@shardVersion = x.value('(Shard/Version)[1]', 'uniqueidentifier'),
		@protocol = x.value('(Shard/Location/Protocol)[1]', 'int'),
		@serverName = x.value('(Shard/Location/ServerName)[1]', 'nvarchar(128)'),
		@port = x.value('(Shard/Location/Port)[1]', 'int'),
		@databaseName = x.value('(Shard/Location/DatabaseName)[1]', 'nvarchar(128)'),
		@shardStatus = x.value('(Shard/Status)[1]', 'int')
	from 
		@input.nodes('/UpdateShardLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @operationId is null or 
		@shardMapId is null or @shardId is null or @shardVersion is null or @shardStatus is null or
		@protocol is null or @serverName is null or @port is null or @databaseName is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	update 
		__ShardManagement.ShardsLocal
	set
		Version = @shardVersion,
		Status = @shardStatus,
		Protocol = @protocol,
		ServerName = @serverName,
		Port = @port,
		DatabaseName = @databaseName,
		LastOperationId = @operationId
	where
		ShardMapId = @shardMapId and ShardId = @shardId

	if (@@rowcount = 0)
		goto Error_ShardDoesNotExist;

	set @result = 1
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_ShardDoesNotExist:
	set @result = 202
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spValidateShardLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spValidateShardLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier,
			@shardVersion uniqueidentifier
	select 
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'), 
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMapId)[1]', 'uniqueidentifier'),
		@shardId = x.value('(ShardId)[1]', 'uniqueidentifier'),
		@shardVersion = x.value('(ShardVersion)[1]', 'uniqueidentifier')
	from 
		@input.nodes('/ValidateShardLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @shardMapId is null or @shardId is null or @shardVersion is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	declare @currentShardMapId uniqueidentifier
	
	select 
		@currentShardMapId = ShardMapId 
	from 
		__ShardManagement.ShardMapsLocal
	where 
		ShardMapId = @shardMapId

	if (@currentShardMapId is null)
		goto Error_ShardMapNotFound;

	declare @currentShardVersion uniqueidentifier

	select 
		@currentShardVersion = Version 
	from 
		__ShardManagement.ShardsLocal
	where 
		ShardMapId = @shardMapId and ShardId = @shardId

	if (@currentShardVersion is null)
		goto Error_ShardDoesNotExist;

	if (@currentShardVersion <> @shardVersion)
		goto Error_ShardVersionMismatch;

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_ShardDoesNotExist:
	set @result = 202
	goto Exit_Procedure;

Error_ShardVersionMismatch:
	set @result = 204
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [__ShardManagement].[spValidateShardMappingLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spValidateShardMappingLocal]
@input xml,
@result int output
as
begin
	declare @lsmVersionMajorClient int,
			@lsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@mappingId uniqueidentifier

	select
		@lsmVersionMajorClient = x.value('(LsmVersion/MajorVersion)[1]', 'int'),
		@lsmVersionMinorClient = x.value('(LsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMapId)[1]', 'uniqueidentifier'),
		@mappingId = x.value('(MappingId)[1]', 'uniqueidentifier')
	from
		@input.nodes('/ValidateShardMappingLocal') as t(x)

	if (@lsmVersionMajorClient is null or @lsmVersionMinorClient is null or @shardMapId is null or @mappingId is null)
		goto Error_MissingParameters;

	if (@lsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorLocal())
		goto Error_LSMVersionMismatch;

	declare @currentShardMapId uniqueidentifier
	
	select 
		@currentShardMapId = ShardMapId 
	from 
		__ShardManagement.ShardMapsLocal
	where 
		ShardMapId = @shardMapId

	if (@currentShardMapId is null)
		goto Error_ShardMapNotFound;

	declare @m_status_current int

	select 
		@m_status_current = Status
	from
		__ShardManagement.ShardMappingsLocal
	where
		ShardMapId = @shardMapId and MappingId = @mappingId
			
	if (@m_status_current is null)
		goto Error_MappingDoesNotExist;

	if (@m_status_current <> 1)
		goto Error_MappingIsOffline;

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MappingDoesNotExist:
	set @result = 301
	goto Exit_Procedure;

Error_MappingIsOffline:
	set @result = 309
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Error_LSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionLocalHelper
	goto Exit_Procedure;

Exit_Procedure:
end	

GO
/****** Object:  StoredProcedure [dbo].[AddFeedback]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddFeedback] --1,'RaviKiran',1,2,3,'Its good app'
	@PhoneID varchar(100),
	@Name [varchar](100),
	@emailaddress varchar(50),
	@Comment [varchar](4096),
	@UserId int,
	@DeviceType varchar(20),
	@UserRating int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
	insert into [Feedback] ([DeviceId], [Name],[Comment], [Status], [CreatedBy], [CreateDate],UserId,DeviceType,Rating) 
	values (@PhoneID, @Name, @Comment, 1, 1, GETUTCDATE(),@UserId,@DeviceType, @UserRating)
	set @Id = @@Identity
	select @Id as result

END

GO
/****** Object:  StoredProcedure [dbo].[AddPackagesToFavourites]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddPackagesToFavourites] --7
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	insert into FavouritePackages(PackageId, UserId, CreatedDate)
	values(@PackageId,@UserId,GetUtcDate())
	set @Id = @@Identity

	update package
	set IsFavourite = 1
	where packageId = @PackageId

	select @Id as result
END










GO
/****** Object:  StoredProcedure [dbo].[AddPayment]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddPayment] 
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@Token nvarchar(max),
	@DeviceType varchar(100),
	@PaymentType varchar(1024),
	@IsSubscription int,
	@UserId int,
	@SportId int,
	@DeviceId int
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @CurrentDate DateTime
	declare @ExpiryDate DateTime
	declare @Id int
	declare @skuid varchar(100)
	declare @channelId int

	if(@IsSubscription = 2)
	Begin
	set @CurrentDate = (select GetUTCDATE())
	set @ExpiryDate = ( select DateADD(day,30,@CurrentDate))
	set @skuid = (select SKUID from pricingcatalog PC inner join Package P on PC.ID = P.PricingCatalogId where P.PackageId = @PackageId)
	set @channelId = (select LibraryId from package where packageId = @PackageId)
	Update Package
	set IsPaid = 1
	where PackageId = @PackageId
	End
	else
	Begin
	set @CurrentDate = (select GetUTCDATE())
	set @ExpiryDate = ( select DateADD(day,30,@CurrentDate))
	set @skuid = (select SKUID from pricingcatalog PC inner join Libraries L on PC.ID = L.PricingCatalogId where L.LibraryId = @PackageId)
	set @channelId = (select LibraryId from package where packageId = @PackageId)
	update channelsports
	set IsPaid = 1
	where channelId = @PackageId AND sportId = @SportId
	End
	
	insert into paymentinfo(Token, UserId, PackageId, IsSubscription, CreatedDate, ExpiryDate, DeviceType, PaymentType,SKUID, SportId, DeviceId)
	values(@Token,@UserId,@PackageId,@IsSubscription,@CurrentDate,@ExpiryDate,@DeviceType,@PaymentType,@skuid, @SportId, @DeviceId)

	insert into UsageTracking(UserId,SportId,PackageOrChannelId,TypeOfSubscription,DeviceType,CreatedDate,ChannelId,DeviceId)
	values(@UserId,@SportId,@PackageId,@IsSubscription,@DeviceType,getutcdate(),@channelId,@DeviceId)
	
	set @Id = @@Identity
	select @Id as result
END





GO
/****** Object:  StoredProcedure [dbo].[AddSportsToChannel]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddSportsToChannel] 
	-- Add the parameters for the stored procedure here
	@ChannelId int,
	@SportId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;--ChannelSports
	declare @Id int
    -- Insert statements for procedure here
	insert into ChannelSports(ChannelId, SportId, IsDefault, CreatedDate)
	values(@ChannelId, @SportId, 0, getutcdate())

	set @Id = @@Identity

	select @Id as result
END


GO
/****** Object:  StoredProcedure [dbo].[AddUserInfo]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddUserInfo] 
	-- Add the parameters for the stored procedure here
	@userId int,
	@name nvarchar(256),
	@SportId int,
	@email nvarchar(256),
	@facebookId nvarchar(1000),
	@profilepicurl nvarchar(max),
	@Gender int,
	@LoginType int,
	@DeviceId varchar(512),
	@DeviceType int,
	@Password varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
	declare @UId int

	set @UId = (select count(*) from Users where UserId = @UserId)

	if(@UId > 0)
	Begin
	select U.UserId, U.Name, U.SportId, U.email, U.ProfilePicUrl, U.FacebookId, U.Gender, U.LoginType, U.DeviceId, S.SportName, U.DeviceType, S.BannerImageUrl, U.[Password]
	 from Users U inner join Sports S  on S.SportId = U.SportId where UserId =  @userId
	End
else
	Begin
    -- Insert statements for procedure here
	insert into Users(UserId, Name, SportId, email, FacebookId, ProfilePicUrl, Gender, LoginType, DeviceId, DeviceType, CreatedDate, CreatedBy, [Password])
	values(@userId,@name, @SportId, @email, @facebookId, @profilepicurl, @Gender, @LoginType, @DeviceId, @DeviceType, GetUtcDate(), @userId, @Password)
	--set @Id  = @@Identity

	select U.UserId, U.Name, U.SportId, U.email, U.ProfilePicUrl, U.FacebookId, U.Gender, U.LoginType, U.DeviceId, S.SportName, U.DeviceType, S.BannerImageUrl, U.[Password]
	 from Users U inner join Sports S  on S.SportId = U.SportId where UserId =  @userId
	 End
	 
END






GO
/****** Object:  StoredProcedure [dbo].[AddVideoInfoToPackage]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddVideoInfoToPackage]
	-- Add the parameters for the stored procedure here
	@VideoId int,
	@PreVideoId int,
	@PostVideoId int,
	@PackageId int,
	@CreatedBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	declare @Id int
	declare @VID int
	declare @introvideourl1 varchar(1024)
	declare @introvideourl2 varchar(1024)
	declare @actualvideourl varchar(1024)
	declare @thumbnailurl varchar(1024)
	declare @MovementName varchar(50)
	declare @ActorName varchar(512)
	declare @SportId int
	declare @videomode int
	set @introvideourl1 = (select VideoURL from Videos where ID = @PreVideoId)
	set @introvideourl2 = (select VideoURL from Videos where ID = @PostVideoId)
	set @actualvideourl = (select VideoURL from Videos where ID = @VideoId)
	set @thumbnailurl = (select thumbnailurl from Videos where ID = @VideoId)
	set @MovementName = (select MovementName from Videos where ID = @VideoId)
	set @SportId = (Select SportId from Package where PackageId = @PackageId)
	set @ActorName = (Select ActorName from Videos where ID = @VideoId)
	set @videomode = (select videomode from videos where ID = @VideoId)

insert into PackageVideoInfo(PackageId,trainingVideoId,PreIntroVideoId,PostIntroVideoId,Createddate, CreatedBy)
values(@PackageId, @VideoId, @PreVideoId, @PostVideoId, getutcdate(), @CreatedBy)
set @Id = @@Identity



--update videos
--set IntroVideoUrl = @introvideourl1,
--IntroVideoURL2 = @introvideourl2,VideoType = 1,prevideoid=@PreVideoId,postvideoid=@PostVideoId
--where ID = @VideoId


insert into Videos(MovementName, ActorName, [Format], VideoURL, ThumbNailURL, [Status], CreatedBy, CreateDate, PlayCount, videotype, VideoMode, SportId, prevideoid, postvideoid,IntroVideoUrl,IntroVideoURL2)
	values(@MovementName,@ActorName,'mp4',@actualvideourl,@thumbnailurl,1,@CreatedBy,GetUtcDate(),30,4, @videomode, @SportId,@PreVideoId,@PostVideoId,@introvideourl1,@introvideourl2)
set @VID = @@Identity

	insert into packageintrovideos(VideoId, PackageId,CreatedDate,IsIntroVideo)
values(@VID, @PackageId, GetUtcDate(), 0)

select @Id as result
END






GO
/****** Object:  StoredProcedure [dbo].[CreateAdminUser]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreateAdminUser] 
	-- Add the parameters for the stored procedure here
	@Id int,
	@FirstName varchar(100),
	@LastName varchar(100),
	@ChannelName varchar(100),
	@EmailAddress varchar(100),
	@Password varchar(512),
	@SportId int,
	@ProfilePicUrl varchar(512),
	@VideoUrl varchar(1024),
	@ThumbNailUrl varchar(1024),
	@description varchar(1024),
	@PricingCatalogId int,
	@AuthCode nvarchar(512)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @userexists int
declare @count int
declare @videoId int
declare @pricingcatId int
declare @channelId int
    -- Insert statements for procedure here
	set @count = (select count(*) from Libraries where EmailAddress = @EmailAddress)
	set @ChannelId = (select top 1 libraryid from libraries order by libraryid desc)
	if(@count > 0)
	Begin
	set @Id = 0
	select @count as UserExists, @Id as UserId
	End
	else
	Begin
	insert into Libraries(LibraryName,FirstName, LastName, SportId, EmailAddress,[Password],CreatedDate,IsPackageAvailable,LibraryImageUrl,IsPaid,PricingCatalogId, AuthCode, [Description])
	values(@ChannelName,@FirstName,@LastName,@SportId,@EmailAddress,@Password,GetUtcDate(),0,@ProfilePicUrl, 0,@PricingCatalogId,@AuthCode,@description)
	set @Id = @@Identity
	
	set @pricingcatId = (select PricingCatalogId from libraries where libraryid = @ChannelId)
	if(@PricingCatalogId > 0)
	Begin	
	update libraries
	set pricingcatalogId = @pricingcatId + 1
	where libraryId = @Id
	End
	else
	Begin
	update libraries
	set pricingcatalogId = 13
	where libraryId = @Id
	End
	insert into ChannelSports(ChannelId, SportId, IsDefault, CreatedDate)
	values(@Id,@SportId,1,getutcdate())
	if(LEN(@VideoUrl) > 0)
	Begin
	insert into videos(MovementName, ActorName, [Format], VideoURL, ThumbNailUrl,[Status], CreatedBy, CreateDate,Playcount,VideoType, VideoMode, SportId)
	values('Channel Intro Video', 'Channel', 'mp4',@VideoUrl,@ThumbNailUrl,1,@Id,getutcdate(),1,2,1,@SportId)
	set @videoId = @@Identity

	insert into libraryintrovideos(VideoId,LibraryId, CreatedDate, IsIntroVideo)
	values(@videoId,@Id,GetUtcDate(),1)
	End
	-- Update the sport details

	update sports
	set IsGreyOut = 1
	where sportId = @SportId

	
	select @count as UserExists, @Id as UserId
	End
END











GO
/****** Object:  StoredProcedure [dbo].[CreateAdminVideo]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateAdminVideo] 
	-- Add the parameters for the stored procedure here
	@VideoURL varchar(1024),
	@MomentName varchar(50),
	@CreatedBy int,
	@ThumbNailURL varchar(1024),
	@VideoMode int,
	@Id int,
	@SportId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if(@Id > 0)
	Begin
	set @Id = 0
	select @Id as Result
	End
	else
	Begin
	insert into Videos(MovementName, ActorName, [Format], VideoURL, ThumbNailURL, [Status], CreatedBy, CreateDate, PlayCount, videotype, VideoMode, SportId)
	values(@MomentName,'Athlete','mp4',@VideoURL,@ThumbNailURL,1,@CreatedBy,GetUtcDate(),30,1, @VideoMode, @SportId)
	set @Id = @@Identity
	Select @Id as Result
	End
	
END







GO
/****** Object:  StoredProcedure [dbo].[CreateException]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateException] 
	-- Add the parameters for the stored procedure here
	@Name nvarchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
    -- Insert statements for procedure here
	insert into ExceptionLog(ExceptionMessage, createddate)
	values(@Name,getutcdate())
	set @Id = @@Identity

	select @Id as result
END


GO
/****** Object:  StoredProcedure [dbo].[CreatePackage]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CreatePackage]
	-- Add the parameters for the stored procedure here
	@VideoURL varchar(1024),
	@CreatedBy int,
	@ThumbNailURL varchar(1024),
	@PackageName varchar(256),
	@Description nvarchar(max),
	@PricingCatalogId int,
	@Id int,
	@SportId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @packageId int
	declare @videoId int
    -- Insert statements for procedure here
	if(@PricingCatalogId = 0)
	Begin
	insert into package(PackageName,LibraryId,CreatedDate,[Description], PricingCatalogId,IsPaid, SportId)
values(@PackageName,@CreatedBy,GetUtcDate(),@Description,14,1,@SportId)
set @packageId = @@Identity
End
else
Begin
insert into package(PackageName,LibraryId,CreatedDate,[Description], PricingCatalogId,IsPaid, SportId)
values(@PackageName,@CreatedBy,GetUtcDate(),@Description,15,0,@SportId)
set @packageId = @@Identity
End

if(LEN(@VideoURL) > 0)
Begin
insert into videos(MovementName,ActorName,[Format],VideoURL,ThumbNailURL,[Status],CreatedBy,CreateDate,PlayCount,VideoType,SportId,VideoMode)
values(@PackageName,'Package Intro Video','mp4',@VideoURL,@ThumbNailURL,1,@CreatedBy,GetUtcDate(),30,3,@SportId,1)

set @videoId = @@Identity

insert into packageintrovideos(VideoId,PackageId,CreatedDate,IsIntroVideo)
values(@videoId,@packageId,GetUtcDate(),1)
End
else
Begin
insert into packageintrovideos(VideoId,PackageId,CreatedDate,IsIntroVideo)
values(0,@packageId,GetUTCDATE(),1)
End
update libraries
set IsPackageAvailable = 1
where LibraryId = @CreatedBy

select @packageId as result
END




GO
/****** Object:  StoredProcedure [dbo].[DeleteFavouritePackage]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteFavouritePackage] --10,32
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	Delete from favouritepackages where packageId = @PackageId

	update package
	set IsFavourite = 0
	where packageId = @PackageId

	select @PackageId as result
END



GO
/****** Object:  StoredProcedure [dbo].[DeletePackage]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeletePackage] 
	-- Add the parameters for the stored procedure here
	@PackageId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
   delete from packageintrovideos where packageId = @PackageId
   delete from package where packageId = @PackageId

   select @PackageId as result
END









GO
/****** Object:  StoredProcedure [dbo].[DeleteVideos]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteVideos] --10,32
	-- Add the parameters for the stored procedure here
	@VideoId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @cnt1 int
	declare @cnt2 int
	set @cnt1 = (select count(*) from packageintrovideos where videoId = @VideoId)
	set @cnt2 = (select count(*) from Libraryintrovideos where videoId = @VideoId)
    -- Insert statements for procedure here

	if(@cnt1 = 0 AND @cnt2 = 0)
	Begin
	Delete from Videos where Id = @VideoId
	End
	else if(@cnt1 > 0 AND @cnt2 = 0)
	Begin
	Delete from packageintrovideos where videoId = @videoId
	Delete from Videos where Id = @VideoId
	end
	else if(@cnt1 = 0 AND @cnt2 > 0)
	Begin
	Delete from Libraryintrovideos where videoId = @videoId
	Delete from Videos where Id = @VideoId
	End
	else
	Begin
	Delete from packageintrovideos where videoId = @videoId
	Delete from Libraryintrovideos where videoId = @videoId
	Delete from Videos where Id = @VideoId
	End
	

	select @VideoId as result
END



GO
/****** Object:  StoredProcedure [dbo].[FeatureViewTracking]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FeatureViewTracking] --1000,
	-- Add the parameters for the stored procedure here
	@FeatureId int,
	@DeviceId int,
	@DeviceType int,
	@Value int,
	@CreatedBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @count int
	declare @viewcount int
	declare @Id int
	
	set @count = (select count(*) from FeatureTracking where FeatureId = @FeatureId AND DeviceId = @DeviceId AND createdBy = @CreatedBy AND Value = @Value)
	if(@count > 0)
	Begin
	set @Id = (select TrackingId from FeatureTracking where FeatureId = @FeatureId AND DeviceId = @DeviceId AND createdBy = @CreatedBy AND Value = @Value)
	set @viewcount = (select ViewCount from FeatureTracking where FeatureId = @FeatureId AND DeviceId = @DeviceId AND createdBy = @CreatedBy AND Value = @Value)
	set @viewcount = @viewcount + 1
	update FeatureTracking
	set ViewCount = @viewcount, updatedby = @CreatedBy, UpdatedDate = getutcdate()
	where TrackingId = @Id

	select @Id as result
	End
	else
	Begin
	insert into FeatureTracking(FeatureId,DeviceId,DeviceType,Value,ViewCount,CreatedDate,createdBy)
	values(@FeatureId,@DeviceId,@DeviceType,@Value,1,GetUtcDate(),@CreatedBy)
	set @Id = @@Identity
	select @Id as result
	End
END



GO
/****** Object:  StoredProcedure [dbo].[GetAdminUserInfo]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAdminUserInfo] --'sai@gmail.com','123456',10000,0
	-- Add the parameters for the stored procedure here
	@EmailAddress Varchar(50),
	@Password varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @UserCount int
    -- Insert statements for procedure here
	--set @UserCount = (select count(*) from Parents where ChurchId = @ChurchId AND EmailAddress = @EmailAddress AND [Password] = @Password)

	
	select L.LibraryId, L.LibraryName as ChannelName,L.EmailAddress,L.SportId, S.SportName, L.FirstName, L.LastName, L.[Password],
	L.PricingCatalogId, L.LibraryImageUrl as ProfilePicURL, L.[Description] 
	from Libraries L inner join Sports S on L.SportId = S.SportId
	left join LibraryIntrovideos LV on L.LibraryId = LV.LibraryId
	 where EmailAddress = @EmailAddress AND [Password] = @Password
	
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllPackagesVideos]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllPackagesVideos]
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
select P.PackageId, P.PackageName, P.[Description], P.LibraryId, V.[Format],V.URL as VideoURL, 
V.ThumbNailURL, P.CreatedDate from packageintrovideos PV
inner join package P on P.PackageId = PV.PackageId
inner join Video V on V.Id = PV.VideoId
where P.PackageId = @PackageId
END







GO
/****** Object:  StoredProcedure [dbo].[GetAllVideos]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllVideos]  
	@UserID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, 
	v.actorname, v.orientation, v.facing, 
    v.averagerating, v.url, v.thumbnailurl, v.[type],v.PlayCount,isnull(p.videoid,0) as videoId
    from SubCategory c, Video v 
	left join payment p on p.videoid = v.id and p.userid = @UserID
	where v.[status] = 1 and c.[Status] = 1 
	and c.id = v.SubCategoryID and c.id <> 287 	
    order by v.[ID]
    
END




GO
/****** Object:  StoredProcedure [dbo].[GetAllVideosSearch]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllVideosSearch] --[GetAllVideosSearch]  0,'Sports','Golf','ALL','',0,10,0
	@UserID [int],
	@Segment  varchar(50) ,
	@Category  varchar(50) ,
	@SubCategory  varchar(50),
	@SearchText varchar(100),
	@Offset [int],
	@max [int],
	@isSubscribed bit

AS
BEGIN
 
	SET NOCOUNT ON;	

 BEGIN tran
	
declare @MaxVar varchar(max)

set  @MaxVar =  ' select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, 
    v.averagerating, v.url, v.thumbnailurl, v.[type],v.PlayCount 
    from SubCategory c, Video v  
	where v.[status] = 1 and c.[Status] = 1 
	and c.id = v.SubCategoryID and c.id <> 287  '

if(@Segment != 'ALL')
set  @MaxVar = @MaxVar + ' and c.Segment LIKE ''%'+ @Segment +'%'''

if(@Category != 'ALL')
set @MaxVar = @MaxVar +' and c.Category LIKE  ''%' + @Category +'%'''

if(@SubCategory != 'ALL')
set @MaxVar = @MaxVar +' and c.Name LIKE ''%' + @SubCategory +'%'''

if(@SearchText != '')
set @MaxVar = @MaxVar + ' and v.movementname LIKE ''%'+ @SearchText +'%'' or v.actorname LIKE ''%'+ @SearchText +'%'''

if(@isSubscribed = 0)
set @MaxVar = @MaxVar + ' and v.[type]=12'

set @MaxVar = @MaxVar +' order by v.[ID] desc'
set @MaxVar = @MaxVar +' OFFSET '+  cast(@Offset as varchar(10))  + ' ROWS FETCH NEXT '+ cast(@max as varchar(10)) +' ROWS ONLY' 
 
exec (@MaxVar)

commit
 
END


GO
/****** Object:  StoredProcedure [dbo].[GetBestTimes]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetBestTimes]  
 @eventid int,
 @age int,
 @gender int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select * from Besttimes where event_type_id = @eventid and age = @age and gender = @gender
END


GO
/****** Object:  StoredProcedure [dbo].[GetCatalog]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCatalog] 
	@LastID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select [ID], Segment, Category, Name as SubCategory from SubCategory where ID > @LastID and status = 1;
END


GO
/****** Object:  StoredProcedure [dbo].[GetCategories]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCategories] 
	@Segment [varchar](50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select distinct Category from SubCategory where Segment = @Segment and status = 1 order by Category;
END


GO
/****** Object:  StoredProcedure [dbo].[GetChannelIntroVideos]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [dbo].[GetChannelIntroVideos] --2,3
	-- Add the parameters for the stored procedure here
	@LibraryId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
 

	select v.[ID], v.movementname, 
	v.actorname, L.[Description],
    v.VideoUrl, v.thumbnailurl, v.PlayCount, PC.Price, PC.SKUID
    from Libraries L 
	left join LibraryIntroVideos LV on L.LibraryId = LV.LibraryId and LV.IsIntroVideo=1
	left join Videos V on  V.ID = LV.VideoId
	inner join PricingCatalog PC on PC.ID = L.PricingCatalogId
	where L.LibraryId = @LibraryId 
END
 




GO
/****** Object:  StoredProcedure [dbo].[GetChannelsByChannelId]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetChannelsByChannelId]--36
	-- Add the parameters for the stored procedure here
	@ChannelId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select L.LibraryId, L.LibraryName, L.SportId, L.LibraryImageUrl, L.EmailAddress, L.[Password], S.SportName, 
L.FirstName, L.LastName, L.[Description], L.PricingCatalogId, V.VideoUrl, V.ThumbNailUrl from libraries L
left join  LibraryIntroVideos LV on L.LibraryId = LV.LibraryId and LV.IsIntroVideo=1
left join videos V on LV.VideoId = V.ID
inner join Sports S on S.SportId = L.SportId
where L.LibraryId = @ChannelId 


END


GO
/****** Object:  StoredProcedure [dbo].[GetChannelsBySportId]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetChannelsBySportId] --13
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @cnt int

	set @cnt = (select count(*) from channelsports where sportid = @Id)
    -- Insert statements for procedure here
	--SELECT L.LibraryId, L.LibraryName, L.SportId, L.LibraryImageUrl, L.IsPaid,L.PricingCatalogId, P.Price, P.SKUID from 
	--Libraries L inner join PricingCatalog P on L.PricingCatalogId = P.Id
	--where L.sportId = @Id and IsPackageAvailable = 1 order by LibraryName 

	SELECT C.ChannelId as LibraryId, L.LibraryName, L.[Description], C.SportId, L.LibraryImageUrl, 
	C.IsPaid,L.PricingCatalogId, P.Price, P.SKUID from 
	Libraries L inner join PricingCatalog P on L.PricingCatalogId = P.Id
	right join ChannelSports C on C.ChannelId = L.LibraryId and C.SportId = @Id
	where IsPackageAvailable = 1 order by LibraryName 
END








GO
/****** Object:  StoredProcedure [dbo].[GetDeviceInfo]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDeviceInfo] 
	-- Add the parameters for the stored procedure here
	@DeviceId varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Inserdt statements for procedure here
	SELECT ID, PhoneID, [Type], [Status],CreatedBy,CreateDate from Device where PhoneId = @DeviceId
END




GO
/****** Object:  StoredProcedure [dbo].[GetEventTypes]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEventTypes]  
@age int ,
@gender int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	 select et.* from EventTypes et
	 inner join eventage ea on ea.eventid = et.id
	 where ea.age = @age
END


GO
/****** Object:  StoredProcedure [dbo].[GetFavouritePackages]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFavouritePackages]  
	-- Add the parameters for the stored procedure here
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	select Distinct P.PackageId, P.PackageName, P.[Description], P.LibraryId, V.[Format],V.VideoURL as VideoURL, L.LibraryImageUrl,
V.ThumbNailURL, P.CreatedDate, P.PricingCatalogId, PC.Price,PC.SKUID,P.Rating,P.IsFavourite, P.IsPaid
from libraries L inner join package  P on L.LibraryId = P.LibraryId
inner join PackageIntroVideos PL on P.PackageId = PL.PackageId
inner join videos V on V.Id = PL.VideoId
inner join FavouritePackages FP on FP.PackageId = P.PackageId
inner join PricingCatalog PC on PC.Id = P.PricingCatalogId
where FP.UserId = @UserId and PL.IsIntroVideo=1
END



GO
/****** Object:  StoredProcedure [dbo].[GetFeaturedVideo]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFeaturedVideo]  --'2015-03-19',1,1,9213
	@Date [datetime],
	@UserID [int],
	@CategoryID [int],
	@deviceId [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
   
 		SELECT p.id, p.title, p.name, p.[Description], p.thumbnailurl, p.url, p.[Type],p.[Status],EventStartDate,EventEndDate,
		t.Description ActionType
		from Promotion p
		inner join [type] t on p.type = t.id 
		where 
		--startdate <= @Date and enddate >= @Date and
		 [status] = 1 and [type]=17
 -- End


END












GO
/****** Object:  StoredProcedure [dbo].[GetLibrariesBySportId]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLibrariesBySportId] 
	-- Add the parameters for the stored procedure here
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Libraries where sportId = @Id and IsPackageAvailable = 1 order by LibraryName 
END




GO
/****** Object:  StoredProcedure [dbo].[GetLibraryIntroVideos]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetLibraryIntroVideos]
	-- Add the parameters for the stored procedure here
	@LibraryId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, 
	v.actorname, v.orientation, v.facing, L.[Description],
    v.averagerating, v.url as VideoUrl, v.thumbnailurl, v.[type],v.PlayCount
    from Video v inner join SubCategory c on v.SubCategoryId = c.Id
	inner join  LibraryIntroVideos L on L.VideoId = v.Id where v.Id = @videoId
END







GO
/****** Object:  StoredProcedure [dbo].[GetLoginUserExists]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetLoginUserExists]  -- EXEC [GetLoginUserExists] 'mrkiran31@gmail.com'
	@Emailaddress varchar(100) 
  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;  

	declare @Count int
	declare @autToken nvarchar(500)
	    
		select @Count = count(*)  From Libraries where Emailaddress = @Emailaddress  
	   if(@Count > 0)
	   Begin
	   --select cast(newid() as varchar(500)) as Code
	   select AuthCode as Code from Libraries where Emailaddress = @Emailaddress
	   End
	   else
	   Begin
	   select '' as Code
	   End
END





GO
/****** Object:  StoredProcedure [dbo].[GetPackageByPackageId]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPackageByPackageId] --2
	-- Add the parameters for the stored procedure here
	@PackageId int
	
--select * from package order by packageid desc
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	select  P.PackageId, P.PackageName, P.[Description], P.LibraryId, V.[format], V.VideoURL as VideoURL,
V.ThumbNailURL, CASE WHEN P.PricingCatalogId = 14 THEN 0
ELSE 1 END as PricingCatalogId, P.Createddate,V.IntroVideoURL, PC.Price,PC.SKUID, V.ID as PackageIntroVideoId
from Package P left join packageintrovideos  PL on P.packageId = PL.packageId and PL.IsIntroVideo= 1 
left join Videos V on V.Id = PL.VideoId
inner join pricingcatalog PC on PC.Id = P.PricingCatalogId
where P.PackageId = @PackageId
END




GO
/****** Object:  StoredProcedure [dbo].[GetPackagesByAdminUserId]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPackagesByAdminUserId] --1,13
	-- Add the parameters for the stored procedure here
	@LibraryId int,
	@SportId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	
select P.PackageId, P.PackageName, P.[Description], P.LibraryId, V.[format], V.VideoURL as VideoURL,
V.ThumbNailURL, P.PricingCatalogId, P.Createddate,V.IntroVideoURL, PC.Price,PC.SKUID, L.LibraryImageUrl as ProfilePicUrl
from libraries L inner join Package P on L.LibraryId = P.LibraryId
 left join packageintrovideos PL on P.packageId = PL.packageId and PL.IsIntroVideo=1
left join Videos V on V.Id = PL.VideoId
inner join pricingcatalog PC on PC.Id = P.PricingCatalogId
where P.LibraryId = @LibraryId AND P.SportId = @SportId
END


GO
/****** Object:  StoredProcedure [dbo].[GetPackagesIntroVideos]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPackagesIntroVideos] --1,2,13
	-- Add the parameters for the stored procedure here
	@LibraryId int,
	@UserId int,
	@SportId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
select P.PackageId, P.PackageName, P.[Description], P.LibraryId, V.[Format],V.VideoURL as VideoURL, L.LibraryImageUrl,
V.ThumbNailURL, P.CreatedDate, P.PricingCatalogId, PC.Price, PC.SKUID, isnull(FP.PackageId,0) as IsFavourite, P.Rating,
isnull(Pr.UserId,0) as IsPaid
from Libraries L inner join package  P on L.LibraryId = P.LibraryId
left join PackageIntroVideos PL on P.PackageId = PL.PackageId and PL.IsIntroVideo=1
left join videos V on V.Id = PL.VideoId
inner join PricingCatalog PC on PC.Id = P.PricingCatalogId
left join Favouritepackages FP on FP.PackageId = p.PackageId and  FP.userid = @UserId
left join paymentinfo Pr on P.packageId = Pr.PackageId and Pr.UserId = @UserId and Pr.IsSubscription = 2
where P.LibraryId = @LibraryId AND P.SportId = @SportId
END






GO
/****** Object:  StoredProcedure [dbo].[GetPayment]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPayment]  
 @UserId int 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	 select * from payment where plantype = 21 and userid = @UserId
	 
END


GO
/****** Object:  StoredProcedure [dbo].[GetPaymentInfo]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetPaymentInfo]
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select top 1 * from PaymentInfo  where UserId = @UserId and PackageId = @PackageId order by createddate desc
END



GO
/****** Object:  StoredProcedure [dbo].[GetPendingVideo]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPendingVideo] 
	@VideoID [int]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl,v.PlayCount
    from SubCategory c, Video v where v.[ID] = @VideoID and v.status = 7 and c.[Status] = 1 and c.id = v.SubCategoryID
    
END


GO
/****** Object:  StoredProcedure [dbo].[GetPromotionalContentById]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPromotionalContentById] 
@Id int 
 
AS
BEGIN
	 
	SET NOCOUNT ON;
	
	Select p.*,a.Description ActionType  from Promotion p
	inner join  [Type] a on a.id = p.Type
	where p.id = @Id
    
END


GO
/****** Object:  StoredProcedure [dbo].[GetSports]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetSports] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
declare @cnt int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set @cnt = 1
	--While @cnt < 16
	--Begin
	--if((select count(*) from libraries where sportid = @cnt) > 0)
	--Begin
	--update sports
	--set IsGreyOut = 1
	--where SportId = @cnt
	--End
	--End
    -- Insert statements for procedure here
	SELECT * from Sports order by SportName
END




GO
/****** Object:  StoredProcedure [dbo].[GetSportsByChannelId]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetSportsByChannelId] --20GetSportsByChannelId 2
	-- Add the parameters for the stored procedure here
	@ChannelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  C.SportId, S.SportName from ChannelSports C inner join Sports S on C.SportId = S.SportId
	where C.ChannelId = @ChannelId order by C.IsDefault desc
END





GO
/****** Object:  StoredProcedure [dbo].[GetSportsDetails]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSportsDetails]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @sport1 int
	declare @sport2 int
	declare @sport3 int
	declare @sport4 int
	declare @sport5 int
	declare @sport6 int
	declare @sport7 int
	declare @sport8 int
	declare @sport9 int
	declare @sport10 int
	declare @sport11 int
	declare @sport12 int
declare @sport13 int
declare @sport14 int
	declare @sport15 int

	set @sport1 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 1 where L.IspackageAvailable = 1)
	set @sport2 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 2 where L.IspackageAvailable = 1)
	set @sport3 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 3 where L.IspackageAvailable = 1)
	set @sport4 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 4 where L.IspackageAvailable = 1)
	set @sport5 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 5 where L.IspackageAvailable = 1)
	set @sport6 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 6 where L.IspackageAvailable = 1)
	set @sport7 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 7 where L.IspackageAvailable = 1)
	set @sport8 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 8 where L.IspackageAvailable = 1)
	set @sport9 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 9 where L.IspackageAvailable = 1)
	set @sport10 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 10 where L.IspackageAvailable = 1)
	set @sport11 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 11 where L.IspackageAvailable = 1)
	set @sport12 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 12 where L.IspackageAvailable = 1)
	set @sport13 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 13 where L.IspackageAvailable = 1)
	set @sport14 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 14 where L.IspackageAvailable = 1)
	set @sport15 = (select count(*) from libraries L right join channelsports C on L.LibraryId = C.ChannelId AND C.sportid = 15 where L.IspackageAvailable = 1)
    -- Insert statements for procedure here
	if(@sport1 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 1
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 1
	End

	if(@sport2 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 2
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 2
	End

	if(@sport3 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 3
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 3
	End

	if(@sport4 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 4
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 4
	End

	if(@sport5 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 5
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 5
	End

	if(@sport6 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 6
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 6
	End

	if(@sport7 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 7
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 7
	End

	if(@sport8 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 8
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 8
	End

	if(@sport9 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 9
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 9
	End


	if(@sport10 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 10
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 10
	End

	if(@sport11 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 11
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 11
	End

	if(@sport12 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 12
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 12
	End

	if(@sport13 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 13
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 13
	End

	if(@sport14 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 14
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 14
	End

	if(@sport15 > 0)
	Begin
	update sports
	set IsGreyOut = 1
	where sportId = 15
	End
	else
	Begin
	update sports
	set IsGreyOut = 0
	where sportId = 15
	End

	--select SportId, SportName, BannerImageUrl, "SportsImageUrl" =
	--CASE SportsImageUrl
	--   when IsGreyOut = 1 then SportsImageUrl
	--   when IsGreyOut = 0 then SportsImageUrl1
	--END  
	--from Sports

	select * from Sports order by SportName
END






GO
/****** Object:  StoredProcedure [dbo].[GetTrainingPlanVideos]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTrainingPlanVideos]  
	@TrainingPlanId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select v.[ID], c.segment, c.category, c.name as subcategory, v.movementname, v.actorname, v.orientation, v.facing, v.averagerating, v.url, v.thumbnailurl,v.ownerId,v.PlayCount
    from SubCategory c, Video v,TrainingVideos tv where v.[status] = 1 and c.[Status] = 1 and c.id = v.SubCategoryID and tv.videoid = v.id	 
	and tv.TrainingPlanId = @TrainingPlanId

END


GO
/****** Object:  StoredProcedure [dbo].[GetUsageCount]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUsageCount] --8
	-- Add the parameters for the stored procedure here
	@ChannelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @channelcount int
declare @packagecount int
    -- Insert statements for procedure here
set @channelcount = (select count(*) from usagetracking where PackageOrChannelId = @ChannelId and TypeOfSubscription = 1)
set @packagecount = (select count(*) from usagetracking where ChannelId = @ChannelId AND TypeOfSubscription = 2)
select @channelcount as ChannelCount, @packagecount as PackageCount
END



GO
/****** Object:  StoredProcedure [dbo].[GetVideos]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetVideos] --25
	-- Add the parameters for the stored procedure here
	@UserId int,
	@SportId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select ID, MovementName, ActorName, [Format], VideoURL, ThumbNailURL,[Status], CreateDate, PlayCount, IntroVideoUrl, VideoMode
	from videos where ((createdby = @UserId) and (VideoType = 0 or VideoType = 1) and SportId = @SportId) 
    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>
END





GO
/****** Object:  StoredProcedure [dbo].[GetVideosByPackageId]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetVideosByPackageId] --1,12
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @videoId int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
select V.Id, v.movementname, 
	v.actorname, V.[Format],V.VideoURL as VideoURL, V.VideoMode,
V.ThumbNailURL,V.PlayCount, P.CreatedDate, v.IntroVideoUrl, v.IntroVideoUrl2 from packageintrovideos PV
inner join package P on P.PackageId = PV.PackageId
inner join Videos V on V.Id = PV.VideoId
where P.PackageId = @PackageId and PV.IsIntroVideo = 0
END


--select * from Video












GO
/****** Object:  StoredProcedure [dbo].[GetVideoUrlInfo]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetVideoUrlInfo]  --31
@PackageId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	select V.VideoURL, V.IntroVideoURL, V.IntroVideoURL2, V.PreVideoId, V.PostVideoId, V.Id as ActualVideoId from Videos V inner join packageintrovideos P on V.Id = P.VideoId where P.PackageId = @PackageId and P.IsIntroVideo=0

END





GO
/****** Object:  StoredProcedure [dbo].[IsPaidChannel]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[IsPaidChannel] 
	-- Add the parameters for the stored procedure here
	@UserId int,
	@ChannelId int,
	@SportId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @pricingcatalogid int
    declare @CurrentDate DateTime
	declare @ExpiryDate DateTime
	set @CurrentDate = (select GetUTCDATE())
	set @ExpiryDate = ( select DateADD(day,30,@CurrentDate))
	set @pricingcatalogid = (select PricingcatalogId from libraries where libraryId = @ChannelId)
    -- Insert statements for procedure here
	if(@pricingcatalogId = 13)
	Begin
	select 0 as Id, 'This Product is Free' as Token, @UserId as UserId, @ChannelId as PackageId, 1 as IsSubscription,
	@CurrentDate as CreatedDate, @ExpiryDate as ExpiryDate, 1 as DeviceType
	End
	else
	Begin
	if (@SportId = 0)
	Begin
	SELECT Top 1 * from paymentinfo where PackageId = @ChannelId AND UserId = @UserId AND IsSubscription = 1 
	End
	else
	Begin
	SELECT Top 1 * from paymentinfo where PackageId = @ChannelId AND UserId = @UserId AND IsSubscription = 1 AND SportId = @SportId order by ID desc
	End
	End
END






GO
/****** Object:  StoredProcedure [dbo].[IsPaidPackage]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[IsPaidPackage] 
	-- Add the parameters for the stored procedure here
	@UserId int,
	@ChannelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Top 1 * from paymentinfo where PackageId = @ChannelId AND UserId = @UserId AND IsSubscription = 2 order by ID desc
END



GO
/****** Object:  StoredProcedure [dbo].[MobileEmailExists]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MobileEmailExists] --'crestelite@gmail.com',1
	-- Add the parameters for the stored procedure here
		@EmailAddress Varchar(50),
		@LoginType int
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @cnt int
	declare @emailcount int
	
	declare @Logcount int
	--select * from libraries
	set @emailcount = (select count(*) from users where email = @EmailAddress)
	if(@emailcount > 0)
	Begin
	set @Logcount = (select logintype from users where email = @EmailAddress)
	if(@LogCount = 1 AND @LoginType = 2)
	Begin
	set @cnt = -1
	select @cnt as result
	End
	else
	Begin
	set @cnt = 0
	select @cnt as result
	End
	End
	
    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>
END




GO
/****** Object:  StoredProcedure [dbo].[MobileUserExists]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MobileUserExists] --'mrkiran31@gmail.com','123456'
	-- Add the parameters for the stored procedure here
		@EmailAddress Varchar(50),
		@Password varchar(50)
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @cnt int
	declare @emailcount int
	declare @pwdcount int
	declare @Id int
	--select * from libraries
	set @cnt =  (select count(*) from Users where  Email = @EmailAddress AND [Password] = @Password)
	if(@cnt > 0)
	Begin
	set @Id = (select UserId from Users where  Email = @EmailAddress AND [Password] = @Password)
	 select  @cnt as result, @Id as UserId
	End
	else
	Begin
	set @emailcount = (select count(*) from Users where Email = @EmailAddress)
	if(@emailcount > 0)
	Begin
	set @cnt = -2
	set @Id = 0
	 select  @cnt as result, @Id as UserId
	End
	else
	Begin
	set @cnt = -1
	set @Id = 0
	select  @cnt as result, @Id as UserId
	End
	End
    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>
END


GO
/****** Object:  StoredProcedure [dbo].[PackageRating]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PackageRating] --7
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@Rating int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	Update Package
	set Rating = @Rating
	where PackageId = @PackageId
	

	select @Rating as result
END







GO
/****** Object:  StoredProcedure [dbo].[PricingCatalogPack]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PricingCatalogPack]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM PRICINGCATALOG
END



GO
/****** Object:  StoredProcedure [dbo].[spGetAllUsers]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAllUsers]

as

begin 

select * from Users
end


GO
/****** Object:  StoredProcedure [dbo].[spGetUser]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spGetUser] 

@Id int

as

begin

select U.UserId, U.SportId, U.Name, U.email, U.ProfilePicUrl, U.FacebookId, U.Gender, U.LoginType, U.DeviceId, S.SportName, U.DeviceType, S.BannerImageUrl
	 from Users U inner join Sports S  on S.SportId = U.SportId where U.UserId =  @Id
end





GO
/****** Object:  StoredProcedure [dbo].[UpdateAdminVideo]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateAdminVideo] 
	-- Add the parameters for the stored procedure here
	@MomentName varchar(50),
	@CreatedBy int,
	@VideoMode int,
	@ActorName varchar(50),
	@Id int,
	@SportId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	update Videos
	set MovementName =@MomentName, ActorName = @ActorName,
	 [Format] = 'mp4', [Status] = 1,
	VideoMode = @VideoMode,
	 UpdatedBy = @CreatedBy, UpdateDate = GetUtcDate(), PlayCount = 30
	 where Id = @Id
	
	
	Select @Id as Result
	
	
END







GO
/****** Object:  StoredProcedure [dbo].[UpdateChannelInfo]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateChannelInfo] 
	-- Add the parameters for the stored procedure here
	@Id int,
	@FirstName varchar(100),
	@LastName varchar(100),
	@VideoUrl varchar(1024),
	@ThumbNailUrl varchar(1024),
	@description varchar(1024),
	@ProfilePicUrl varchar(512)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @userexists int
declare @count int
declare @videoId int
    -- Insert statements for procedure here

	set @videoId = (select videoid from libraryintrovideos where libraryid = @Id and IsIntroVideo = 1)
	
	update libraries
	set FirstName = @FirstName,
	LastName = @LastName,
	[Description] = @description,
	LibraryImageUrl = @ProfilePicUrl,
	UpdatedDate = GetUtcDate()
	where LibraryId = @Id

	
	if (@VideoId > 0)
	Begin
	update videos
	set videoURL = @VideoUrl,
	ThumbNailUrl = @ThumbNailUrl
	where ID = @videoId
	End
	else
	Begin
	insert into videos(MovementName, ActorName, [Format], VideoURL, ThumbNailUrl,[Status], CreatedBy, CreateDate,Playcount,VideoType)
	values('Channel Intro Video', 'Channel', 'mp4',@VideoUrl,@ThumbNailUrl,1,@Id,getutcdate(),1,2)
	set @videoId = @@Identity

	insert into libraryintrovideos(VideoId,LibraryId, CreatedDate, IsIntroVideo)
	values(@videoId,@Id,GetUtcDate(),1)
	End
	

	-- Update the sport details
	
	
	select @Id as UserId
	End
	





GO
/****** Object:  StoredProcedure [dbo].[UpdateLibraryId]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[UpdateLibraryId] 
@UserId int,
--@LibraryId int
@SportId int

as

begin

update users
set SportId = @SportId
where UserId = @UserId

--update libraries
--set sportId = @SportId
--where libraryId = @LibraryId

select U.UserId, U.Name, U.SportId, U.email, U.ProfilePicUrl, U.FacebookId, U.Gender, U.LoginType, U.DeviceId, S.SportName, U.DeviceType, S.BannerImageUrl
	 from Users U inner join Sports S  on S.SportId = U.SportId where UserId =  @UserId

end

--select * from sports





GO
/****** Object:  StoredProcedure [dbo].[UpdatePackage]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdatePackage]
	-- Add the parameters for the stored procedure here
	@VideoURL varchar(1024),
	@CreatedBy int,
	@ThumbNailURL varchar(1024),
	@PackageName varchar(256),
	@Description nvarchar(max),
	@PricingCatalogId int,
	@VideoId int,
	@Id int,
	@SportId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	if (@PricingCatalogId > 0)
	Begin
	update package
	set PackageName = @PackageName,
	[Description] = @Description,
	PricingCatalogId = 15,
	UpdateDate = getutcdate()
	where PackageId = @Id
	End
	else
	Begin
	update package
	set PackageName = @PackageName,
	[Description] = @Description,
	PricingCatalogId = 14,
	IsPaid = 1,
	UpdateDate = getutcdate()
	where PackageId = @Id
	End

	if(LEN(@VideoURL) > 0 AND @VideoId > 0)
	Begin
	update Videos
	set VideoURL = @VideoURL,
	ThumbnailURL= @ThumbNailURL,
	MovementName = @PackageName,
	updatedby = @CreatedBy,
	updatedate = getutcdate()
	where ID = @VideoId
	End
	else if(LEN(@VideoURL) > 0 AND @VideoId = 0)
	Begin
	insert into videos(MovementName,ActorName,[Format],VideoURL,ThumbNailURL,[Status],CreatedBy,CreateDate,PlayCount,VideoType, SportId)
values(@PackageName,'Package','mp4',@VideoURL,@ThumbNailURL,1,@CreatedBy,GetUtcDate(),30,3, @SportId)

set @videoId = @@Identity
delete from packageintrovideos where packageid = @Id and IsIntroVideo = 1
insert into packageintrovideos(VideoId,PackageId,CreatedDate,IsIntroVideo)
values(@videoId,@Id,GetUtcDate(),1)
	End
select @Id as result
END














GO
/****** Object:  StoredProcedure [dbo].[UpdateParentPassword]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateParentPassword] 
    @Token varchar(500),
	@Emailaddress varchar(100), 
	@Password varchar(100) 

	 
	 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @authToken varchar(500)
	select @authToken = AuthCode from Libraries where EmailAddress = @Emailaddress
	if(@authToken = @Token)
	Begin
	 Update Libraries set password = @Password where emailaddress=@Emailaddress
     select @Emailaddress as Email
	 End
	 Else
	 Begin
	 Select '' as Email
	 End

END





GO
/****** Object:  StoredProcedure [dbo].[UpdateSportId]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[UpdateSportId] 
@UserId int,
--@LibraryId int
@SportId int

as

begin

update users
set SportId = @SportId
where UserId = @UserId

--update libraries
--set sportId = @SportId
--where libraryId = @LibraryId

select U.UserId, U.Name, U.SportId, U.email, U.ProfilePicUrl, U.FacebookId, U.Gender, U.LoginType, U.DeviceId, S.SportName, U.DeviceType, S.BannerImageUrl
	 from Users U inner join Sports S  on S.SportId = U.SportId where UserId =  @UserId

end

--select * from sports





GO
/****** Object:  StoredProcedure [dbo].[UpdateVideoInfoToPackage]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdateVideoInfoToPackage]
	-- Add the parameters for the stored procedure here
	@VideoId int,
	@ActualVideoId int,
	@PreVideoId int,
	@PostVideoId int,
	@PackageId int,
	@CreatedBy int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	declare @Id int
	declare @introvideourl1 varchar(1024)
	declare @introvideourl2 varchar(1024)
	declare @actualvideourl varchar(1024)
	declare @thumbnailurl varchar(1024)
	declare @MovementName varchar(50)
	set @introvideourl1 = (select VideoURL from Videos where ID = @PreVideoId)
	set @introvideourl2 = (select VideoURL from Videos where ID = @PostVideoId)
	set @actualvideourl = (select VideoURL from Videos where ID = @ActualVideoId)
	set @thumbnailurl = (select thumbnailurl from Videos where ID = @ActualVideoId)
	set @MovementName = (select MovementName from Videos where ID = @ActualVideoId)

--Update PackageVideoInfo
--set PackageId = @PackageId,trainingVideoId = @VideoId,PreIntroVideoId = @PreVideoId,PostIntroVideoId = @PostVideoId ,
--updatedate = getutcdate(), UpdatedBy = @CreatedBy
--set @Id = @@Identity


update videos
set MovementName = @MovementName,
videourl =  @actualvideourl,
thumbnailurl = @thumbnailurl,
IntroVideoUrl = @introvideourl1,
IntroVideoURL2 = @introvideourl2,
PrevideoId = @PreVideoId,
PostVideoId = @PostVideoId
where ID = @VideoId
select @VideoId as result
END






GO
/****** Object:  StoredProcedure [dbo].[UserExists]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserExists] --'mrkiran@hotmail.com','789456'
	-- Add the parameters for the stored procedure here
		@EmailAddress Varchar(50),
		@Password varchar(50)
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @cnt int
	declare @emailcount int
	declare @pwdcount int
	--select * from libraries
	set @cnt =  (select count(*) from Libraries where  EmailAddress = @EmailAddress AND [Password] = @Password)
	if(@cnt > 0)
	Begin
	 select  @cnt as result
	End
	else
	Begin
	set @emailcount = (select count(*) from Libraries where EmailAddress = @EmailAddress)
	if(@emailcount > 0)
	Begin
	set @cnt = -2
	 select  @cnt as result
	End
	else
	Begin
	set @cnt = -1
	select  @cnt as result
	End
	End
    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>
END


GO
/****** Object:  UserDefinedFunction [__ShardManagement].[fnGetStoreVersionMajorLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create function [__ShardManagement].[fnGetStoreVersionMajorLocal]()
returns int
as
begin
	return (select StoreVersionMajor from __ShardManagement.ShardMapManagerLocal)
end

GO
/****** Object:  Table [__ShardManagement].[ShardMapManagerLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[ShardMapManagerLocal](
	[StoreVersionMajor] [int] NOT NULL,
	[StoreVersionMinor] [int] NOT NULL,
 CONSTRAINT [pkShardMapManagerLocal_StoreVersionMajor] PRIMARY KEY CLUSTERED 
(
	[StoreVersionMajor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [__ShardManagement].[ShardMappingsLocal]    Script Date: 21-12-2016 17:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [__ShardManagement].[ShardMappingsLocal](
	[MappingId] [uniqueidentifier] NOT NULL,
	[ShardId] [uniqueidentifier] NOT NULL,
	[ShardMapId] [uniqueidentifier] NOT NULL,
	[MinValue] [varbinary](128) NOT NULL,
	[MaxValue] [varbinary](128) NULL,
	[Status] [int] NOT NULL,
	[LockOwnerId] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[LastOperationId] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
 CONSTRAINT [pkShardMappingsLocal_MappingId] PRIMARY KEY CLUSTERED 
(
	[MappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [__ShardManagement].[ShardMapsLocal]    Script Date: 21-12-2016 17:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[ShardMapsLocal](
	[ShardMapId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[MapType] [int] NOT NULL,
	[KeyType] [int] NOT NULL,
	[LastOperationId] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
 CONSTRAINT [pkShardMapsLocal_ShardMapId] PRIMARY KEY CLUSTERED 
(
	[ShardMapId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [__ShardManagement].[ShardsLocal]    Script Date: 21-12-2016 17:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[ShardsLocal](
	[ShardId] [uniqueidentifier] NOT NULL,
	[Version] [uniqueidentifier] NOT NULL,
	[ShardMapId] [uniqueidentifier] NOT NULL,
	[Protocol] [int] NOT NULL,
	[ServerName] [nvarchar](128) NOT NULL,
	[Port] [int] NOT NULL,
	[DatabaseName] [nvarchar](128) NOT NULL,
	[Status] [int] NOT NULL,
	[LastOperationId] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
 CONSTRAINT [pkShardsLocal_ShardId] PRIMARY KEY CLUSTERED 
(
	[ShardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[AdminUsers]    Script Date: 21-12-2016 17:04:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmailAddress] [varchar](100) NULL,
	[UserName] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[Password] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BestTimes]    Script Date: 21-12-2016 17:04:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BestTimes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
	[Gender] [int] NULL,
	[Stroke] [varchar](51) NULL,
	[Age] [int] NULL,
	[Event_Type_Id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChannelSports]    Script Date: 21-12-2016 17:04:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChannelSports](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ChannelId] [int] NULL,
	[SportId] [int] NULL,
	[IsDefault] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[IsPaid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Device]    Script Date: 21-12-2016 17:04:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Device](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PhoneID] [varchar](50) NOT NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[isFirst] [bit] NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EventAge]    Script Date: 21-12-2016 17:04:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventAge](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NULL,
	[Age] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[EventTypes]    Script Date: 21-12-2016 17:04:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EventTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[SubCategory] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ExceptionLog]    Script Date: 21-12-2016 17:04:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExceptionLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExceptionMessage] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[FavouritePackages]    Script Date: 21-12-2016 17:04:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavouritePackages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PackageId] [int] NULL,
	[UserId] [int] NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Features]    Script Date: 21-12-2016 17:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Features](
	[FeatureId] [int] IDENTITY(1000,1) NOT NULL,
	[FeatureName] [varchar](512) NULL,
PRIMARY KEY CLUSTERED 
(
	[FeatureId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FeatureTracking]    Script Date: 21-12-2016 17:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeatureTracking](
	[TrackingId] [int] IDENTITY(1,1) NOT NULL,
	[FeatureId] [int] NULL,
	[DeviceId] [int] NULL,
	[DeviceType] [int] NULL,
	[Value] [int] NULL,
	[ViewCount] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[UpdatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TrackingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 21-12-2016 17:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feedback](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[PhoneID] [int] NULL,
	[Question1] [int] NULL,
	[Question2] [int] NULL,
	[Question3] [int] NULL,
	[Comment] [varchar](4096) NULL,
	[Status] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[UserId] [int] NULL,
	[DeviceId] [varchar](100) NULL,
	[DeviceType] [varchar](20) NULL,
	[Rating] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Libraries]    Script Date: 21-12-2016 17:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Libraries](
	[LibraryId] [int] IDENTITY(1,1) NOT NULL,
	[LibraryName] [nvarchar](256) NOT NULL,
	[SportId] [int] NOT NULL,
	[LibraryImageUrl] [varchar](512) NULL,
	[EmailAddress] [varchar](100) NULL,
	[Password] [varchar](256) NULL,
	[FirstName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[IsPackageAvailable] [int] NULL,
	[IsPaid] [int] NULL,
	[PricingCatalogId] [int] NULL,
	[AuthCode] [nvarchar](512) NULL,
	[Description] [varchar](1024) NULL,
	[UpdatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[LibraryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LibraryIntroVideos]    Script Date: 21-12-2016 17:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LibraryIntroVideos](
	[VideoId] [int] NOT NULL,
	[LibraryId] [int] NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[IsIntroVideo] [int] NULL,
	[Description] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[LoginType]    Script Date: 21-12-2016 17:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoginType](
	[Id] [int] NOT NULL,
	[LoginType] [varchar](50) NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Package]    Script Date: 21-12-2016 17:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Package](
	[PackageId] [int] IDENTITY(1,1) NOT NULL,
	[PackageName] [varchar](256) NULL,
	[LibraryId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[Description] [nvarchar](max) NULL,
	[PricingCatalogId] [int] NULL,
	[IsFavourite] [int] NULL,
	[Rating] [int] NULL,
	[IsPaid] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[SportId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PackageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PackageIntroVideos]    Script Date: 21-12-2016 17:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackageIntroVideos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VideoId] [int] NOT NULL,
	[PackageId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[IsIntroVideo] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[PackageVideoInfo]    Script Date: 21-12-2016 17:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackageVideoInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PackageId] [int] NOT NULL,
	[TrainingVideoId] [int] NOT NULL,
	[PreIntroVideoId] [int] NULL,
	[PostIntroVideoId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL
)

GO
/****** Object:  Table [dbo].[Payment]    Script Date: 21-12-2016 17:04:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Payment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[PaymentToken] [varchar](max) NULL,
	[DeviceType] [int] NULL,
	[PaymentType] [varchar](50) NULL,
	[VideoId] [int] NULL,
	[ExpiryDate] [datetime] NULL,
	[PlanType] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaymentInfo]    Script Date: 21-12-2016 17:04:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaymentInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Token] [nvarchar](max) NULL,
	[UserId] [int] NULL,
	[PackageId] [int] NULL,
	[IsSubscription] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ExpiryDate] [datetime] NULL,
	[PaymentType] [varchar](1024) NULL,
	[DeviceType] [int] NULL,
	[SKUID] [varchar](100) NULL,
	[SportId] [int] NULL,
	[DeviceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PricingCatalog]    Script Date: 21-12-2016 17:04:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PricingCatalog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Price] [varchar](50) NULL,
	[SKUID] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[pricingtype] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Promotion]    Script Date: 21-12-2016 17:04:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Promotion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](100) NULL,
	[Name] [varchar](100) NULL,
	[Description] [varchar](1024) NULL,
	[ThumbNailURL] [varchar](1024) NULL,
	[URL] [varchar](1024) NULL,
	[Type] [int] NULL,
	[Status] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[EventStartDate] [datetime] NULL,
	[EventEndDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sports]    Script Date: 21-12-2016 17:04:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sports](
	[SportId] [int] NOT NULL,
	[SportName] [varchar](50) NULL,
	[SportsImageUrl] [varchar](512) NULL,
	[BannerImageUrl] [varchar](512) NULL,
	[IsGreyOut] [int] NULL,
	[SportsImageUrl1] [varchar](1024) NULL,
 CONSTRAINT [PK_Sports_SportId] PRIMARY KEY CLUSTERED 
(
	[SportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Status]    Script Date: 21-12-2016 17:04:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Status](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Value] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StrokeTypes]    Script Date: 21-12-2016 17:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StrokeTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[SubCategory]    Script Date: 21-12-2016 17:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubCategory](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Category] [varchar](50) NOT NULL,
	[Segment] [varchar](50) NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrainingPlan]    Script Date: 21-12-2016 17:04:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](4000) NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Training_Plan_Template_Id] [int] NULL,
	[Day] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[TrainingPlanTemplate]    Script Date: 21-12-2016 17:04:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrainingPlanTemplate](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BestTime1] [int] NOT NULL,
	[BestTime2] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[Event_Type_Id] [int] NULL,
	[Gender] [int] NULL,
	[Age] [varchar](5) NULL,
	[Default] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrainingVideos]    Script Date: 21-12-2016 17:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingVideos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainingPlanId] [int] NOT NULL,
	[VideoId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Type]    Script Date: 21-12-2016 17:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Type](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsageTracking]    Script Date: 21-12-2016 17:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsageTracking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[SportId] [int] NULL,
	[PackageOrChannelId] [int] NULL,
	[TypeOfSubscription] [int] NULL,
	[DeviceType] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ChannelId] [int] NULL,
	[DeviceId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[Users]    Script Date: 21-12-2016 17:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] NOT NULL,
	[Name] [nvarchar](256) NULL,
	[email] [nvarchar](256) NULL,
	[ProfilePicUrl] [nvarchar](max) NULL,
	[FacebookId] [nvarchar](1000) NULL,
	[Gender] [int] NULL,
	[LoginType] [int] NULL,
	[DeviceId] [varchar](512) NULL,
	[DeviceType] [int] NULL,
	[createddate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[SportId] [int] NULL,
	[Password] [varchar](100) NULL,
	[DeviceToken] [nvarchar](1024) NULL,
 CONSTRAINT [PK_Users_UserId] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserTrainingPlan]    Script Date: 21-12-2016 17:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTrainingPlan](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[training_plan_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[createdBy] [int] NULL,
	[createdDate] [datetime] NULL,
	[start_date] [datetime] NULL,
	[Best_current_time1] [time](7) NULL,
	[Best_current_time2] [time](7) NULL,
	[question_text] [bit] NULL,
	[question_id ] [int] NULL,
	[record_your_video] [bit] NULL,
	[recorded_video_id] [int] NULL,
	[view_your_results] [bit] NULL,
	[question_ans] [nvarchar](50) NULL,
	[started] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[UserTrainingQuestions]    Script Date: 21-12-2016 17:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTrainingQuestions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](2000) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[UserTrainingRecordedVideos]    Script Date: 21-12-2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTrainingRecordedVideos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[training_video_id] [int] NOT NULL,
	[recorded_video_id] [int] NULL,
	[createddate] [datetime] NULL,
	[createdby] [int] NULL,
	[userid] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[UserVideos]    Script Date: 21-12-2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserVideos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MovementName] [varchar](100) NULL,
	[VideoUrl] [varchar](1000) NULL,
	[ThumbNailUrl] [varchar](1000) NULL,
	[ActorName] [varchar](100) NULL,
	[UserId] [int] NULL,
	[PackageId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedDate] [datetime] NULL,
	[VideoFormat] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Video]    Script Date: 21-12-2016 17:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Video](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ShardID] [int] NOT NULL,
	[SubCategoryID] [int] NOT NULL,
	[MovementName] [varchar](50) NOT NULL,
	[ActorName] [varchar](50) NULL,
	[OwnerID] [int] NOT NULL,
	[Format] [varchar](50) NULL,
	[Speed] [varchar](50) NULL,
	[Quality] [varchar](50) NULL,
	[Size] [int] NULL,
	[Duration] [int] NULL,
	[Orientation] [int] NULL,
	[Facing] [int] NULL,
	[AverageRating] [int] NULL,
	[URL] [varchar](1024) NULL,
	[ThumbNailURL] [varchar](1024) NULL,
	[AccessKey] [varchar](1024) NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[PlayCount] [int] NULL,
	[PackageId] [int] NULL,
	[IntroVideoUrl] [varchar](1024) NULL,
	[IntroVideoUrl2] [varchar](1024) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoFlagging]    Script Date: 21-12-2016 17:04:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoFlagging](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Video] [int] NOT NULL,
	[User] [int] NOT NULL,
	[FlagId] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[VideoRating]    Script Date: 21-12-2016 17:04:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoRating](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[Video] [int] NOT NULL,
	[User] [int] NOT NULL,
	[Rating] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL
)

GO
/****** Object:  Table [dbo].[Videos]    Script Date: 21-12-2016 17:04:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Videos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MovementName] [varchar](50) NOT NULL,
	[ActorName] [varchar](50) NULL,
	[Format] [varchar](50) NULL,
	[Duration] [int] NULL,
	[VideoURL] [varchar](1024) NULL,
	[ThumbNailURL] [varchar](1024) NULL,
	[Status] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[PlayCount] [int] NULL,
	[IntroVideoUrl] [varchar](1024) NULL,
	[IntroVideoUrl2] [varchar](1024) NULL,
	[VideoType] [int] NULL,
	[VideoMode] [int] NULL,
	[PreVideoId] [int] NULL,
	[PostVideoId] [int] NULL,
	[SportId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[videoSamples]    Script Date: 21-12-2016 17:04:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[videoSamples](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[VideoUrl] [varchar](512) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoTypes]    Script Date: 21-12-2016 17:04:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VideoType] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoUsage]    Script Date: 21-12-2016 17:04:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoUsage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Video] [int] NULL,
	[User] [int] NULL,
	[Count] [int] NULL,
	[Status] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreateDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
INSERT [__ShardManagement].[ShardMapManagerLocal] ([StoreVersionMajor], [StoreVersionMinor]) VALUES (1, 2)
INSERT [__ShardManagement].[ShardMappingsLocal] ([MappingId], [ShardId], [ShardMapId], [MinValue], [MaxValue], [Status], [LockOwnerId], [LastOperationId]) VALUES (N'03b7dab5-26f7-41df-addc-25ad55d91daf', N'c637e34b-7746-44f9-9400-c539eeb7907e', N'd6fcafac-814b-4547-ab64-1f986b044f8e', 0x80001388, 0x80002710, 1, N'00000000-0000-0000-0000-000000000000', N'ee936846-0abc-4790-8d38-f20ea25ca348')
INSERT [__ShardManagement].[ShardMapsLocal] ([ShardMapId], [Name], [MapType], [KeyType], [LastOperationId]) VALUES (N'd6fcafac-814b-4547-ab64-1f986b044f8e', N'UserIdShardMap', 2, 1, N'dfbc164d-3404-4367-bbe4-eb6b469bbac4')
INSERT [__ShardManagement].[ShardsLocal] ([ShardId], [Version], [ShardMapId], [Protocol], [ServerName], [Port], [DatabaseName], [Status], [LastOperationId]) VALUES (N'c637e34b-7746-44f9-9400-c539eeb7907e', N'ea142c1a-a353-415a-812e-1debde3ad9e8', N'd6fcafac-814b-4547-ab64-1f986b044f8e', 0, N'graa3njllc.database.windows.net,1433', 0, N'ElasticScaleStarterKit_Shard1', 1, N'ee936846-0abc-4790-8d38-f20ea25ca348')
SET IDENTITY_INSERT [dbo].[ChannelSports] ON 

INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (1, 1, 13, 1, CAST(N'2016-11-11 18:52:32.733' AS DateTime), 0)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (6, 4, 1, 1, CAST(N'2016-11-12 03:25:06.570' AS DateTime), 0)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (7, 4, 13, 0, CAST(N'2016-11-12 03:26:22.073' AS DateTime), 0)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (8, 5, 13, 1, CAST(N'2016-11-12 06:47:01.317' AS DateTime), 0)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (9, 6, 13, 1, CAST(N'2016-11-12 07:22:41.693' AS DateTime), 0)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (15, 8, 1, 1, CAST(N'2016-11-21 20:04:16.750' AS DateTime), 0)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (29, 16, 7, 1, CAST(N'2016-12-06 18:05:32.130' AS DateTime), 0)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (30, 17, 12, 1, CAST(N'2016-12-12 07:30:03.090' AS DateTime), 0)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (31, 18, 12, 1, CAST(N'2016-12-12 07:32:59.303' AS DateTime), 0)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (32, 19, 2, 1, CAST(N'2016-12-13 22:45:43.710' AS DateTime), NULL)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (33, 20, 7, 1, CAST(N'2016-12-14 19:35:43.670' AS DateTime), NULL)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (34, 21, 14, 1, CAST(N'2016-12-19 14:28:38.140' AS DateTime), NULL)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (35, 22, 13, 1, CAST(N'2016-12-19 14:33:29.500' AS DateTime), NULL)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (36, 23, 10, 1, CAST(N'2016-12-19 14:42:50.447' AS DateTime), NULL)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (37, 24, 1, 1, CAST(N'2016-12-19 14:48:40.410' AS DateTime), NULL)
INSERT [dbo].[ChannelSports] ([Id], [ChannelId], [SportId], [IsDefault], [CreatedDate], [IsPaid]) VALUES (38, 25, 13, 1, CAST(N'2016-12-19 15:10:16.707' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[ChannelSports] OFF
SET IDENTITY_INSERT [dbo].[Libraries] ON 

INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (1, N'ASCA', 13, N'https://ikkosmultisports.blob.core.windows.net:443/asset-68583b09-c479-4a0d-a300-314789433749/fn1877735107.jpeg?sv=2012-02-12&sr=c&si=691e10ac-bac7-4fe6-b60a-561f93fab187&sig=8E7naxecz9QRS3oMByNh9wC8VpfhaeJPpNMtKj0kIl0%3D&se=2017-12-14T15%3A54%3A13Z', N'gmc@ikkostraining.com', N'123456', N'ASCA1', N'ASCA2', CAST(N'2016-11-11 18:52:32.653' AS DateTime), 1, 0, 1, N'47FEB9F4-2443-46B2-AA86-6555E487422B', N'Thank you for visiting the ASCA Channel. Our channel has been designed to provide coaches and swimming instructors with a unique teaching tool aiding on the development of your learn-to-swim program.

Here you will find a curriculum of skills deployed through an innovative movement learning technology called IKKOS. Whether you are an elite or recreational athlete or coach,our proprietary learn-to-swim curriculum can be now accessed in a simple and effective way to help YOU become the best you can be.

Click on the start Learning tab below and begin your journey with us!
', NULL)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (4, N'The Labs usa', 1, N'https://ikkosmultisports.blob.core.windows.net:443/asset-0f3dc291-5980-4df1-9af8-3e9a399e1c2a/fn1727998808.png?sv=2012-02-12&sr=c&si=7e79f761-b845-406b-be56-a96d5ff8003e&sig=oXhFhlKkSbsm%2F3aehJ0e5DTR25PEtN2yKipfkXQyvk0%3D&se=2017-11-12T03%3A25%3A07Z', N'elitecrest4@gmail.com', N'123456', N'The Labs', N'USA', CAST(N'2016-11-12 03:25:06.523' AS DateTime), 1, 0, 2, N'C7D704AB-A2F2-42C2-A12A-D9D4855E24BD', N'The Labs is a unique,holistic,data and science-based training facility that offers the best of land and swim training. The Labs utilizes expert coaching,leading-edge technology, state-of-art equipment and data feedback in a training environment that makes the very most of each athlete''s time and energy from beginners to elite and professional competitors.
', NULL)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (5, N'KING Sports', 13, N'https://ikkosmultisports.blob.core.windows.net:443/asset-5c1f28a3-f73b-4833-9cb2-b4eca3cd2e05/fn608240460.png?sv=2012-02-12&sr=c&si=23f05541-2187-490d-93e0-7088a6b3c04a&sig=pCJSWsG%2BEWmAWN2EwTI%2FekUu9%2Fr0d2fTVXclJvrwVU8%3D&se=2017-11-12T06%3A46%3A59Z', N'elitecrest5@gmail.com', N'123456', N'KING', N'Sports', CAST(N'2016-11-12 06:47:01.207' AS DateTime), 1, 0, 3, N'0CF1B3BA-4065-44F3-A3B9-7138EDCC92D6', N'Welcome to the king Aquatics IKKOS channel.Here you will find all of the secrets to develop greats swimming technique.
Thank you for visiting our channel and i hope you enjoy your time with us!
', NULL)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (6, N'Kim Brackin ', 13, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1642804a-6806-434d-bf92-87e163f49894/fn1699331902.png?sv=2012-02-12&sr=c&si=1e8d27d1-3508-4716-a987-7ff9d0b906ea&sig=2B0XyAijFrA5j23IJgyvR4%2F7wiA4kRvhlIAgXbzYtbA%3D&se=2017-11-12T07%3A22%3A28Z', N'elitecrest6@gmail.com', N'123456', N'Kim', N'Brackin', CAST(N'2016-11-12 07:22:41.647' AS DateTime), 1, 0, 4, N'607CA8C0-5B81-45B7-82A7-134C50333383', N'Welcome to your first day of greatness.We''re here to help you get the best out of you.Stick with us, and we''ll be a great team!
Watching the learning video of the day before you sleep helps your brain process the information faster and more deeply.
Watching in the morning reinforces that processing to speed your improvement.Committing to these few minutes is the
easiest thing you can do to get better! 30 minutes before bed:Backstroke front.On the next page,select the large ''GO''
button to begin learning.
', NULL)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (8, N'Robert Budd - A Body in Motion', 1, N'https://ikkosmultisports.blob.core.windows.net:443/asset-8b8c0330-b4f9-4669-8769-535e2e5eea50/fn1849412991.png?sv=2012-02-12&sr=c&si=7b1fc32b-41cb-48d1-bdf2-d24afe75c4be&sig=MPPj4zVBjIkpQs9ZoFFBMOBPcOVVDb8uJ%2Fwl0r8eWpM%3D&se=2017-11-21T20%3A04%3A18Z', N'robert.bambamculture@gmail.com', N'RB123456', N'Robert', N'Budd', CAST(N'2016-11-21 20:04:16.700' AS DateTime), 1, 0, 5, N'DA08FE45-7706-4CF2-B510-B538C777E8A9', N'Kettlebell Training: The Basics is made for men and women of all ages. It is the perfect app for those curious about kettlebells, so recommend it to your friends that always ask, “what are kettlebells?” Experienced lifters will find it useful, as they know that re-visiting fundamentals is always important.

The app features instructional training on over 20 basic exercises – including variations for double bells. 

Robert Budd has been training people for over 20 years! His attention to detail and posture is unequaled, and has been sought out by people around the world to train with him. He has trained people from the age of 10 to 90 years old. With a background of being a Special Olympic Power Lifting Coach, Master Kettlebell Instructor, pre and post surgery clients, multiple World Class Athletes, and most often clients dealing with the stressors of getting older and wanting to retain their youth, Robert is a sought out expert in his field of expertise. ', NULL)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (19, N'RVP', 2, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1e812af0-7a16-4330-8a49-7bec097a595f/fn1653625002.jpeg?sv=2012-02-12&sr=c&si=f126937a-86a3-40a1-b956-0518d1af4e50&sig=rZ57mWCTg7ivW5Ocj7myFqcZrTmXje9ivHx7s3GW7gk%3D&se=2017-12-13T22%3A45%3A41Z', N'don@rightviewpro.com', N'sluggo11', N'Don', N'Slaught', CAST(N'2016-12-13 22:45:43.680' AS DateTime), 0, 0, 6, N'1C950C2D-E2DD-4D21-B9DA-886E3882337E', N'Hitting and Pitching Instruction', NULL)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (20, N'Derek Uyeda Golf', 7, N'https://ikkosmultisports.blob.core.windows.net:443/asset-35863c68-502b-4256-94e3-bc04b4b9157e/fn500222358.jpeg?sv=2012-02-12&sr=c&si=6dfae47e-d6c4-4d3f-9bec-dd5bae33c141&sig=2A%2BK%2F9r7Ag1MQOVh3hf5ZKvbMP1YaZTDiIsTHWXIfrs%3D&se=2017-12-14T19%3A35%3A41Z', N'derekuyeda@gmail.com', N'met1231', N'Derek', N'Uyeda', CAST(N'2016-12-14 19:35:43.640' AS DateTime), 0, 0, 7, N'62DEB61E-5913-45B8-AC4B-E8B6A0DBA186', N'High quality video of the top PGA Tour players from the past to the present all from the same camera angles. ', NULL)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (21, N'GPTCA', 14, N'https://ikkosmultisports.blob.core.windows.net:443/asset-6544ffff-9b83-4d96-b7bf-827a604c0881/fn1142840510.jpeg?sv=2012-02-12&sr=c&si=8cdf40f1-5f78-4b74-921d-572d88359387&sig=AfHeCd%2B%2B0fH%2BGJZF8SljwoBiGKUL0KbYK0RQjrczc0c%3D&se=2017-12-19T14%3A28%3A38Z', N'mmatysik@gptcatennis.com', N'MM123456', N'Marcin', N'Matysik', CAST(N'2016-12-19 14:28:38.107' AS DateTime), 0, 0, 8, N'32CFD999-8168-4D24-A571-41D4DFE5C03A', N'The Global Professional Tennis Coach Association (GPTCA) is an international organization dedicated to educating, training, and assisting professional tennis coaches who wish to evolve at the ATP and WTA Tour level. 

The GPTCA IKKOS Channel brings you a unique opportunity to learn from the best professional Tennis coaches in the world. Using the IKKOS technology we are delivering high level learning content to serve anybody from beginners to professionals.

Thanks for joining our channel and enjoy getting better with the best in the world!', NULL)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (22, N'Kurt Niehaus Swim Academy', 13, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4efbb2e3-f151-4e3f-94c5-cb5947f99c5a/fn1779567417.png?sv=2012-02-12&sr=c&si=8de96c6d-b4ff-4555-8ed6-ac74d1477672&sig=4cRF%2B3DZYwEvBdYcXNAvq7xJ0jdSvsaIliYPlDU21BM%3D&se=2017-12-19T14%3A33%3A30Z', N'kniehaus@aguasabiertas.com', N'KH123456', N'Kurt', N'Nihau''s', CAST(N'2016-12-19 14:33:29.487' AS DateTime), 0, 0, 9, N'6878C706-6BF6-4E73-A3D4-5351B30182A8', N'Kurt Niehaus Swim Academy es un Centro especializado en la enseñanza y entrenamiento de la natación.
 
Nuestro programa se enfoca en desarrollar una técnica depurada como clave para que nuestros nadadores sean más eficientes en el agua y de esta forma obtengan un mejor rendimiento.
 
Tenemos experiencia en la enseñanza y perfeccionamiento de la natación tanto en niños, adultos, nadadores masters, aguas abiertas, triatlón, y alto rendimiento. Nuestro éxito se basa en el planeamiento, la pasión por el deporte, la constancia, y atención a los detalles.', CAST(N'2016-12-19 14:37:39.623' AS DateTime))
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (23, N'Kyle Harrison  ', 10, N'https://ikkosmultisports.blob.core.windows.net:443/asset-8789c3af-9bb0-4f1b-be4f-e43c82db754a/fn878835594.jpeg?sv=2012-02-12&sr=c&si=27225389-f714-457f-8f0a-721ae85f7ac5&sig=mFAHUiCLmx39WQA%2Fplq32O%2BmlIkBPUj2bGc%2BBclQ62g%3D&se=2017-12-19T14%3A42%3A51Z', N'kharri18@gmail.com', N'KH123456', N'Kyle', N'Harrison', CAST(N'2016-12-19 14:42:50.430' AS DateTime), 0, 0, 10, N'50FDCA30-AAD7-49C8-A40F-592049D07FA0', N'Kyle Harrison is a profesional lacrosse for the Ohio Machine (of Major League Lacrosse). Originally from Baltimore, MD, Kyle attended Friends School of Baltimore where he was a 4 year starter on the Varsity in soccer, basketball and lacrosse. After graduating from Friends as an HS All American in lacrosse, Kyle attended Johns Hopkins University. A Captain at Hopkins, Kyle was a 3X All American, 2X Midfielder of the Year, 3 Time Tewaaraton Finalist, National Champion and the schools first, and only Tewaaraton Award (Heisman trophy of college lacrosse) winner as a senior. 

After leaving Hopkins, Kyle was drafted to the New Jersey Pride with the first overall pick in the MLL draft. Since turning pro, Kyle Has been a 5X MLL All Star, 2X Member of the US National Team, and created his own brand and equipment line (K18) with STX Lacrosse. Kyle''s current corporate partners are STX Lacrosse, Adrenaline Apparel, Tomahawk Shades, AT PEAK, and Uninterrupted. ', NULL)
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (24, N'RITTER Sports Performance', 1, N'https://ikkosmultisports.blob.core.windows.net:443/asset-a7085740-bdbd-48fd-8bfb-1b583cfd743f/fn1480847041.png?sv=2012-02-12&sr=c&si=e9be3782-5227-43a6-98d5-3b06ae0f5115&sig=4G8CsRwEnLYvroNTcOMCr5Fx9HWWsS%2FX2cQYveX9Q2w%3D&se=2017-12-19T14%3A51%3A54Z', N'chris@rittersp.com', N'CR123456', N'Chris', N'Ritter', CAST(N'2016-12-19 14:48:40.393' AS DateTime), 1, 0, 11, N'B8CD8842-E274-49E4-810D-0BB73E08D6AC', N'Welcome to the RITTER Sports Performance IKKOS channel. Here at RITTERSP, we cover everything from bodyweight to barbell movements. The exercises featured on this channel have been used with everyone from Olympic athletes to your general fitness goer. Our goals are to make our viewers injury free, pain free, help them get stronger, and optimize their performance in any endeavor they choose. If you like our content and want more, head on over to www.rittersp.com. All of the exercises used on this page are used in our online training and also our subscription site, RITTER Masters. 
', CAST(N'2016-12-19 14:53:58.367' AS DateTime))
INSERT [dbo].[Libraries] ([LibraryId], [LibraryName], [SportId], [LibraryImageUrl], [EmailAddress], [Password], [FirstName], [LastName], [CreatedDate], [IsPackageAvailable], [IsPaid], [PricingCatalogId], [AuthCode], [Description], [UpdatedDate]) VALUES (25, N'Flow Swimming', 13, N'https://ikkosmultisports.blob.core.windows.net:443/asset-9f796c4c-2470-469e-a016-23d670f46369/fn453808109.jpeg?sv=2012-02-12&sr=c&si=0e53aab8-134c-4f40-bcd3-94af5c2de90b&sig=t61QKyF4%2FsTh4IrD5AsJj0S7rac2GQMhEAoC9GA1EBE%3D&se=2017-12-19T15%3A10%3A15Z', N'markhillswim@gmail.com', N'MH123456', N'Mark', N'Hill', CAST(N'2016-12-19 15:10:16.660' AS DateTime), 0, 0, 12, N'3E4D4DB9-AAC1-4DEF-9182-D63755574400', N' Flow Swimming understands that there is a lot of information out there in the vast world of swimming. We do not usually think of things as “correct” or “incorrect” unless we are discussing core principles i.e. one’s head must be down in order to obtain buoyancy. 
?
We think of swimmers and coaches as athletes with the ability to try new and different things. We want to challenge bodies to move, interact, experience, and think about the water in unique ways.
?
We teach a variety of styles of each stroke because we believe that an athlete will never “mess themselves up”. We think of skills like tools in a toolbox. If you, as an athlete, ever find yourself in a certain situation that requires a certain tool then we hope that we have given you the tools to use. We also believe that it is the ability to make technical and tactical decisions during a race that gives an athlete the edge over their competitors.
?
We are always open to learn through discussion and would love to hear from you!', NULL)
SET IDENTITY_INSERT [dbo].[Libraries] OFF
SET IDENTITY_INSERT [dbo].[LibraryIntroVideos] ON 

INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (1, 1, 1, CAST(N'2016-11-11 18:52:32.750' AS DateTime), 1, NULL)
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (18, 5, 3, CAST(N'2016-11-12 06:47:01.347' AS DateTime), 1, NULL)
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (32, 6, 4, CAST(N'2016-11-12 07:22:41.710' AS DateTime), 1, NULL)
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (64, 4, 6, CAST(N'2016-11-17 10:17:00.870' AS DateTime), 1, NULL)
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (68, 8, 7, CAST(N'2016-11-22 17:23:09.883' AS DateTime), 1, NULL)
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (74, 12, 8, CAST(N'2016-12-02 10:07:08.690' AS DateTime), 1, NULL)
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (251, 20, 10, CAST(N'2016-12-14 19:37:32.030' AS DateTime), 1, NULL)
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (256, 22, 11, CAST(N'2016-12-19 14:37:39.670' AS DateTime), 1, NULL)
INSERT [dbo].[LibraryIntroVideos] ([VideoId], [LibraryId], [Id], [CreatedDate], [IsIntroVideo], [Description]) VALUES (257, 24, 12, CAST(N'2016-12-19 14:51:54.050' AS DateTime), 1, NULL)
SET IDENTITY_INSERT [dbo].[LibraryIntroVideos] OFF
INSERT [dbo].[LoginType] ([Id], [LoginType]) VALUES (1, N'Facebook')
INSERT [dbo].[LoginType] ([Id], [LoginType]) VALUES (2, N'UserEmail')
INSERT [dbo].[LoginType] ([Id], [LoginType]) VALUES (3, N'Twitter')
INSERT [dbo].[LoginType] ([Id], [LoginType]) VALUES (4, N'Instagram')
INSERT [dbo].[LoginType] ([Id], [LoginType]) VALUES (5, N'Skip To')
SET IDENTITY_INSERT [dbo].[Package] ON 

INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (1, N'Station 1: Blowing Bubbles', 1, CAST(N'2016-11-12 02:54:26.540' AS DateTime), N'In shallow water,hold your breath,then crouch down so your head gets under water. stay in that position for a few seconds, then rise up. But exhale under water through the nose so you blow bubbles.


', 15, 0, 4, 0, CAST(N'2016-12-02 11:34:56.120' AS DateTime), 13)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (2, N'Learning the Backstroke Pull', 5, CAST(N'2016-11-12 07:04:50.113' AS DateTime), N'Welcome to the king Aquatics IKKOS Channel.Here you will find all of the secrets to develop great swimming technique.
Thank you for visiting our our channel and i hope you enjoy your time with us!', 14, 0, 4, 1, CAST(N'2016-12-02 11:38:16.610' AS DateTime), 13)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (3, N'Turn Front and Side', 6, CAST(N'2016-11-12 07:28:43.117' AS DateTime), N'Kim Brackin teaches Crossover turn on your back.', 14, 0, 4, 1, CAST(N'2016-12-02 10:14:04.780' AS DateTime), 13)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (4, N'Box Squat Front', 4, CAST(N'2016-11-12 07:36:47.387' AS DateTime), N'Swim labs is a unique swim training facility designed to teach swimmers of all ages and abilities-beginners to competitive athletes-the absolute best mechanics for water safety and swimming success.
', 14, 0, 4, 0, NULL, 1)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (12, N'Station 2: Float & Glide', 1, CAST(N'2016-11-17 07:29:33.200' AS DateTime), N'In shallow water,hold your breath,then crouch down so your head gets under water. stay in that position for a few seconds,then rise up. But exhale under water through the nose so you blow bubbles.
', 14, 0, 4, 0, CAST(N'2016-11-22 19:30:56.740' AS DateTime), 13)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (13, N'Freestyle Stroke', 4, CAST(N'2016-11-21 20:19:03.873' AS DateTime), N'In this module you''ll find expert instruction in freestyle stroke including appropriate catch and breathing technique.', 14, 0, 4, 0, NULL, 13)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (24, N'Deadlift Technique', 8, CAST(N'2016-12-06 12:49:55.513' AS DateTime), N'This module covers in 2 simple lessons the steps on how to perform the perfect deadlift technique.', 15, NULL, NULL, 0, NULL, 1)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (28, N'Squat (BW and Barbell Back Squat) ', 24, CAST(N'2016-12-19 23:23:00.787' AS DateTime), N'This module is dedicated to mastering the bodyweight squat and the barbell back squat. The cues are similar for both squats. 

This module will have three videos with three focus points. We will start at the ankles, progress to the knees, and then finish with the back/chest. ', 15, NULL, NULL, 0, CAST(N'2016-12-20 17:24:00.457' AS DateTime), 1)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (29, N'Front Squat + Barbell ', 24, CAST(N'2016-12-20 16:39:14.037' AS DateTime), N'Watch the Back Squat/BW Squat module first for tips to help your overall squat performance. 

The front squat has three main focus points. 

1) Feet
2) Chest/Back
3) Elbows ', 15, NULL, NULL, 0, NULL, 1)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (30, N'Broad Jump', 24, CAST(N'2016-12-20 17:02:22.530' AS DateTime), N'The broad jump is a great exercise for increasing total body power. If you''re a swimmer, broad jumps can even help you with your start performance! 

The broad jump is broken into three components. 

1) Countermovement 
2) Jump
3) Landing ', 15, NULL, NULL, 0, NULL, 1)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (31, N'Squat Jump', 24, CAST(N'2016-12-20 17:23:15.673' AS DateTime), N'The squat jump is a great exercise for developing total body power. 

Similar to the broad jump, the squat jump is broken into three key stages.

1) Countermovement 
2) Jump
3) Landing ', 15, NULL, NULL, 0, NULL, 1)
INSERT [dbo].[Package] ([PackageId], [PackageName], [LibraryId], [CreatedDate], [Description], [PricingCatalogId], [IsFavourite], [Rating], [IsPaid], [UpdateDate], [SportId]) VALUES (32, N'Overhead Squat ', 24, CAST(N'2016-12-20 17:47:08.027' AS DateTime), N'The overhead squat is a great test of overall mobility. This exercise can be completed in multiple ways. 

1) Bodyweight-arms overhead
2) PVC/Band-hold a pvc pipe or band overhead 
3) Barbell-hold a barbell overhead 

There are three main focuses with the overhead squat. Reference the squat module for overall squat setup. 

1) Shoulders
2) Chest/Back
3) Knees ', 15, NULL, NULL, 0, NULL, 1)
SET IDENTITY_INSERT [dbo].[Package] OFF
SET IDENTITY_INSERT [dbo].[PackageIntroVideos] ON 

INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (1, 11, 1, CAST(N'2016-11-12 02:54:26.650' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (2, 10, 1, CAST(N'2016-11-12 02:54:26.760' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (3, 3, 1, CAST(N'2016-11-12 02:54:26.887' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (4, 4, 1, CAST(N'2016-11-12 02:54:26.950' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (5, 5, 1, CAST(N'2016-11-12 02:54:27.010' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (6, 31, 2, CAST(N'2016-11-12 07:04:50.193' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (7, 20, 2, CAST(N'2016-11-12 07:04:50.317' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (8, 21, 2, CAST(N'2016-11-12 07:04:50.380' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (9, 19, 2, CAST(N'2016-11-12 07:04:50.443' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (10, 22, 2, CAST(N'2016-11-12 07:04:50.520' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (11, 33, 3, CAST(N'2016-11-12 07:28:43.240' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (12, 34, 3, CAST(N'2016-11-12 07:28:43.320' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (13, 12, 4, CAST(N'2016-11-12 07:36:47.560' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (14, 13, 4, CAST(N'2016-11-12 07:36:47.620' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (24, 0, 3, CAST(N'2016-11-16 09:08:21.187' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (25, 0, 4, CAST(N'2016-11-16 09:08:21.200' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (32, 63, 12, CAST(N'2016-11-17 07:29:33.280' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (33, 54, 12, CAST(N'2016-11-17 07:29:33.340' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (34, 56, 12, CAST(N'2016-11-17 07:29:33.403' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (35, 59, 12, CAST(N'2016-11-17 07:29:33.467' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (36, 61, 12, CAST(N'2016-11-17 07:29:33.530' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (37, 0, 13, CAST(N'2016-11-21 20:19:04.043' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (38, 65, 13, CAST(N'2016-11-21 20:19:04.653' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (39, 66, 13, CAST(N'2016-11-21 20:19:04.780' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (40, 67, 13, CAST(N'2016-11-21 20:19:04.873' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (76, 230, 24, CAST(N'2016-12-06 12:49:55.543' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (77, 231, 24, CAST(N'2016-12-06 12:49:55.730' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (78, 232, 24, CAST(N'2016-12-06 12:49:55.793' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (92, 0, 28, CAST(N'2016-12-19 23:23:00.913' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (93, 280, 28, CAST(N'2016-12-19 23:23:01.193' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (94, 281, 28, CAST(N'2016-12-19 23:23:01.583' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (95, 282, 28, CAST(N'2016-12-19 23:23:01.630' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (96, 0, 29, CAST(N'2016-12-20 16:39:14.287' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (97, 290, 29, CAST(N'2016-12-20 16:39:14.503' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (98, 291, 29, CAST(N'2016-12-20 16:39:14.630' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (99, 292, 29, CAST(N'2016-12-20 16:39:14.693' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (100, 0, 30, CAST(N'2016-12-20 17:02:22.593' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (101, 300, 30, CAST(N'2016-12-20 17:02:22.640' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (102, 301, 30, CAST(N'2016-12-20 17:02:22.687' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (103, 302, 30, CAST(N'2016-12-20 17:02:22.720' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (104, 0, 31, CAST(N'2016-12-20 17:23:15.703' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (105, 310, 31, CAST(N'2016-12-20 17:23:15.783' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (106, 311, 31, CAST(N'2016-12-20 17:23:15.843' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (107, 312, 31, CAST(N'2016-12-20 17:23:15.907' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (108, 0, 32, CAST(N'2016-12-20 17:47:08.103' AS DateTime), 1)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (109, 320, 32, CAST(N'2016-12-20 17:47:08.213' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (110, 321, 32, CAST(N'2016-12-20 17:47:08.277' AS DateTime), 0)
INSERT [dbo].[PackageIntroVideos] ([Id], [VideoId], [PackageId], [CreatedDate], [IsIntroVideo]) VALUES (111, 322, 32, CAST(N'2016-12-20 17:47:08.340' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[PackageIntroVideos] OFF
SET IDENTITY_INSERT [dbo].[PackageVideoInfo] ON 

INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (1, 1, 10, 9, 0, CAST(N'2016-11-12 02:54:26.760' AS DateTime), 1)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (2, 1, 3, 6, 0, CAST(N'2016-11-12 02:54:26.870' AS DateTime), 1)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (3, 1, 4, 7, 0, CAST(N'2016-11-12 02:54:26.950' AS DateTime), 1)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (4, 1, 5, 8, 0, CAST(N'2016-11-12 02:54:27.010' AS DateTime), 1)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (5, 2, 20, 24, 23, CAST(N'2016-11-12 07:04:50.300' AS DateTime), 5)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (6, 2, 21, 25, 26, CAST(N'2016-11-12 07:04:50.363' AS DateTime), 5)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (7, 2, 19, 27, 29, CAST(N'2016-11-12 07:04:50.443' AS DateTime), 5)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (8, 2, 22, 28, 30, CAST(N'2016-11-12 07:04:50.503' AS DateTime), 5)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (9, 3, 33, 37, 35, CAST(N'2016-11-12 07:28:43.223' AS DateTime), 6)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (11, 4, 12, 14, 15, CAST(N'2016-11-12 07:36:47.560' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (12, 4, 13, 16, 17, CAST(N'2016-11-12 07:36:47.620' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (14, 6, 41, 41, 41, CAST(N'2016-11-12 12:05:00.090' AS DateTime), 7)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (16, 7, 41, 41, 41, CAST(N'2016-11-12 12:07:57.437' AS DateTime), 7)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (18, 8, 47, 48, 44, CAST(N'2016-11-12 12:30:39.790' AS DateTime), 0)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (20, 10, 20, 19, 23, CAST(N'2016-11-16 09:35:26.427' AS DateTime), 5)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (21, 11, 33, 0, 35, CAST(N'2016-11-16 09:38:27.793' AS DateTime), 6)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (22, 12, 54, 53, 0, CAST(N'2016-11-17 07:29:33.327' AS DateTime), 1)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (24, 12, 59, 58, 57, CAST(N'2016-11-17 07:29:33.450' AS DateTime), 1)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (25, 12, 61, 60, 0, CAST(N'2016-11-17 07:29:33.530' AS DateTime), 1)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (26, 13, 65, 0, 0, CAST(N'2016-11-21 20:19:04.543' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (27, 13, 66, 0, 0, CAST(N'2016-11-21 20:19:04.763' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (28, 13, 67, 0, 0, CAST(N'2016-11-21 20:19:04.857' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (29, 14, 117, 0, 0, CAST(N'2016-12-02 12:36:50.717' AS DateTime), 12)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (30, 14, 119, 0, 0, CAST(N'2016-12-02 12:36:50.730' AS DateTime), 12)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (31, 14, 122, 0, 0, CAST(N'2016-12-02 12:36:50.763' AS DateTime), 12)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (33, 15, 162, 0, 0, CAST(N'2016-12-02 12:40:10.080' AS DateTime), 10)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (35, 16, 132, 0, 0, CAST(N'2016-12-02 12:42:32.897' AS DateTime), 13)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (36, 16, 137, 0, 0, CAST(N'2016-12-02 12:42:32.943' AS DateTime), 13)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (38, 17, 169, 0, 0, CAST(N'2016-12-02 12:49:14.627' AS DateTime), 8)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (40, 18, 181, 0, 0, CAST(N'2016-12-02 13:04:40.180' AS DateTime), 9)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (43, 19, 189, 0, 0, CAST(N'2016-12-02 13:08:05.797' AS DateTime), 9)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (45, 20, 197, 0, 0, CAST(N'2016-12-02 13:13:13.490' AS DateTime), 11)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (47, 21, 206, 0, 0, CAST(N'2016-12-06 06:41:08.993' AS DateTime), 15)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (50, 22, 201, 0, 0, CAST(N'2016-12-06 06:44:48.860' AS DateTime), 14)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (52, 23, 211, 200, 212, CAST(N'2016-12-06 10:12:35.713' AS DateTime), 14)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (56, 25, 233, 0, 0, CAST(N'2016-12-13 11:04:07.780' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (57, 25, 237, 0, 0, CAST(N'2016-12-13 11:04:07.857' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (58, 25, 236, 0, 0, CAST(N'2016-12-13 11:04:07.903' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (63, 27, 252, 0, 0, CAST(N'2016-12-15 09:56:41.880' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (65, 28, 271, 277, 274, CAST(N'2016-12-19 23:23:01.100' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (67, 28, 273, 276, 275, CAST(N'2016-12-19 23:23:01.600' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (68, 29, 260, 288, 285, CAST(N'2016-12-20 16:39:14.427' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (70, 29, 261, 287, 284, CAST(N'2016-12-20 16:39:14.677' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (71, 30, 299, 296, 293, CAST(N'2016-12-20 17:02:22.627' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (72, 30, 259, 297, 294, CAST(N'2016-12-20 17:02:22.657' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (73, 30, 258, 298, 295, CAST(N'2016-12-20 17:02:22.703' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (74, 31, 268, 307, 304, CAST(N'2016-12-20 17:23:15.767' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (76, 31, 303, 309, 306, CAST(N'2016-12-20 17:23:15.890' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (77, 32, 264, 319, 316, CAST(N'2016-12-20 17:47:08.200' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (79, 32, 313, 318, 315, CAST(N'2016-12-20 17:47:08.323' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (10, 3, 34, 36, 38, CAST(N'2016-11-12 07:28:43.303' AS DateTime), 6)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (13, 5, 41, 41, 41, CAST(N'2016-11-12 11:56:01.133' AS DateTime), 7)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (44, 19, 187, 0, 0, CAST(N'2016-12-02 13:08:05.843' AS DateTime), 9)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (55, 24, 224, 227, 229, CAST(N'2016-12-06 12:49:55.780' AS DateTime), 8)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (59, 25, 234, 0, 0, CAST(N'2016-12-13 11:04:07.967' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (15, 7, 44, 44, 44, CAST(N'2016-11-12 12:07:57.357' AS DateTime), 7)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (17, 8, 46, 0, 0, CAST(N'2016-11-12 12:27:45.223' AS DateTime), 7)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (19, 9, 13, 12, 14, CAST(N'2016-11-16 09:31:24.077' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (23, 12, 56, 55, 0, CAST(N'2016-11-17 07:29:33.390' AS DateTime), 1)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (37, 17, 168, 0, 0, CAST(N'2016-12-02 12:49:14.610' AS DateTime), 8)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (54, 24, 223, 225, 222, CAST(N'2016-12-06 12:49:55.730' AS DateTime), 8)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (62, 26, 249, 0, 0, CAST(N'2016-12-13 12:32:01.273' AS DateTime), 0)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (78, 32, 265, 317, 314, CAST(N'2016-12-20 17:47:08.260' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (32, 14, 123, 0, 0, CAST(N'2016-12-02 12:36:50.780' AS DateTime), 12)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (34, 15, 163, 0, 0, CAST(N'2016-12-02 12:40:10.127' AS DateTime), 10)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (42, 19, 180, 0, 0, CAST(N'2016-12-02 13:08:05.763' AS DateTime), 9)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (48, 21, 207, 0, 0, CAST(N'2016-12-06 06:41:09.053' AS DateTime), 15)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (53, 23, 201, 213, 214, CAST(N'2016-12-06 10:12:35.760' AS DateTime), 14)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (75, 31, 269, 308, 305, CAST(N'2016-12-20 17:23:15.830' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (39, 17, 170, 0, 0, CAST(N'2016-12-02 12:49:14.640' AS DateTime), 8)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (41, 18, 183, 0, 0, CAST(N'2016-12-02 13:04:40.227' AS DateTime), 9)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (46, 20, 194, 0, 0, CAST(N'2016-12-02 13:13:13.537' AS DateTime), 11)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (66, 28, 272, 279, 278, CAST(N'2016-12-19 23:23:01.553' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (49, 22, 200, 0, 0, CAST(N'2016-12-06 06:44:48.843' AS DateTime), 14)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (51, 22, 212, 0, 0, CAST(N'2016-12-06 06:44:48.877' AS DateTime), 14)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (61, 26, 245, 0, 0, CAST(N'2016-12-13 12:30:10.313' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (64, 27, 253, 0, 0, CAST(N'2016-12-15 09:56:41.927' AS DateTime), 4)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (69, 29, 289, 286, 283, CAST(N'2016-12-20 16:39:14.567' AS DateTime), 24)
INSERT [dbo].[PackageVideoInfo] ([Id], [PackageId], [TrainingVideoId], [PreIntroVideoId], [PostIntroVideoId], [CreatedDate], [CreatedBy]) VALUES (60, 26, 244, 0, 0, CAST(N'2016-12-13 12:30:10.267' AS DateTime), 4)
SET IDENTITY_INSERT [dbo].[PackageVideoInfo] OFF
SET IDENTITY_INSERT [dbo].[PricingCatalog] ON 

INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (0, N'0.00', N'com.ikkos.channel_0.00_0', CAST(N'2016-11-26 08:18:06.360' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (1, N'19.99', N'com.ikkos.channel_19.99_1', CAST(N'2016-09-01 11:09:10.327' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (2, N'19.99', N'com.ikkos.channel_19.99_2', CAST(N'2016-09-01 11:09:10.330' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (3, N'19.99', N'com.ikkos.channel_19.99_3', CAST(N'2016-09-01 11:09:10.350' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (4, N'19.99', N'com.ikkos.channel_19.99_4', CAST(N'2016-09-01 11:09:10.353' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (5, N'19.99', N'com.ikkos.channel_19.99_5', CAST(N'2016-09-01 11:09:10.360' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (6, N'19.99', N'com.ikkos.channel_19.99_6', CAST(N'2016-09-01 11:09:10.360' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (7, N'19.99', N'com.ikkos.channel_19.99_7', CAST(N'2016-09-01 11:09:10.367' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (8, N'19.99', N'com.ikkos.channel_19.99_8', CAST(N'2016-09-01 11:09:10.370' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (9, N'19.99', N'com.ikkos.channel_19.99_9', CAST(N'2016-09-01 11:09:10.373' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (10, N'19.99', N'com.ikkos.channel_19.99_10', CAST(N'2016-09-01 11:09:10.377' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (11, N'19.99', N'com.ikkos.channel_19.99_11', CAST(N'2016-09-07 08:46:02.040' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (12, N'19.99', N'com.ikkos.channel_19.99_12', CAST(N'2016-09-20 09:15:35.643' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (13, N'Free', N'com.ikkos.channel_FREE', CAST(N'2016-11-18 11:15:35.660' AS DateTime), 1)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (14, N'Free', N'com.ikkos.package_FREE', CAST(N'2016-11-29 09:11:12.183' AS DateTime), 2)
INSERT [dbo].[PricingCatalog] ([Id], [Price], [SKUID], [CreatedDate], [pricingtype]) VALUES (15, N'Paid', N'com.ikkos.package_PAID', CAST(N'2016-11-29 09:11:31.747' AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[PricingCatalog] OFF
SET IDENTITY_INSERT [dbo].[Videos] ON 

INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (0, N'', N'Channel Intro video', NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2016-11-16 09:10:58.050' AS DateTime), NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (1, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-415e5861-be65-4625-9e3f-68561aeeaee1/fn1368879902.mp4?sv=2012-02-12&sr=c&si=8c362cdb-0b22-4ab0-bf8a-d91d48bd261b&sig=YPoNudI4OJea0XdHnT%2FjJaQ1Xo5UE3j8BziufCthBQw%3D&se=2017-11-11T18%3A52%3A21Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/20798976796533.jpg', 1, 1, CAST(N'2016-11-11 18:52:32.733' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (3, N'Bubbles 1 skill video 2...', N'jim', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-58c06639-edb2-4a88-93a8-842c91c9fa48/fn1828061299.mp4?sv=2012-02-12&sr=c&si=c98fb874-31af-4c2d-95db-acd689c024f9&sig=j2i44yfTNhscvoKd4ubGyluf1NlhjvYO8h%2FntSEBqd4%3D&se=2017-11-11T19%3A38%3A15Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/66049363184395.jpg', 1, 1, CAST(N'2016-11-11 19:38:16.497' AS DateTime), 1, CAST(N'2016-11-11 19:41:39.630' AS DateTime), 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-10c07cc9-763d-4cd9-a705-3f3633ad969f/fn456080007.mp4?sv=2012-02-12&sr=c&si=afc53138-9d58-4307-827b-2b8ec442af6d&sig=VmG2wNuxpW7r4OkWEVx4I%2BUpof78kALznmxd1DeBFMc%3D&se=2017-11-11T19%3A38%3A25Z', NULL, 1, 1, 6, 0, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (4, N'Bubbles 2 Skill Video 1...', N'jim', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-46771d52-fa7e-4ecc-8e8d-240a0dcf68b7/fn146749375.mp4?sv=2012-02-12&sr=c&si=6d63fa50-4108-4fcb-b464-43593719308b&sig=bJ5%2BDhxaIDeMtLxK8gjGOcxLsIobK9TpNcB0yauPnvQ%3D&se=2017-11-11T19%3A38%3A18Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/59840477547183.jpg', 1, 1, CAST(N'2016-11-11 19:38:19.400' AS DateTime), 1, CAST(N'2016-11-11 19:42:22.677' AS DateTime), 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4125937f-eef5-4a20-ade2-8ada54af3cb0/fn1498416971.mp4?sv=2012-02-12&sr=c&si=18f237bf-9468-4e6e-90d2-0fc380040556&sig=%2Fpv0WEGshWjUdfDo684fmnbNlAWTOn5TmtPyQ49BTBc%3D&se=2017-11-11T19%3A38%3A29Z', NULL, 1, 1, 7, 0, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (5, N'Bubbles2-skillvideo2.mp4', N'jim', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-d52eb4a0-c49d-48b2-87af-9882289d0043/fn439663097.mp4?sv=2012-02-12&sr=c&si=5475bc16-9abf-431d-8ab8-ea169889fa61&sig=5jItrGrcR9YAXiMvLL0lvIN1HNWtVyzbO1F6p8aNZeI%3D&se=2017-11-11T19%3A38%3A21Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/74889520053300.jpg', 1, 1, CAST(N'2016-11-11 19:38:21.793' AS DateTime), 1, CAST(N'2016-11-11 19:42:11.427' AS DateTime), 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-07a99c4e-9847-47df-ad68-00fe548fd671/fn1889545502.mp4?sv=2012-02-12&sr=c&si=29c5d96a-ae6c-4406-a9a8-4c7f410628ef&sig=Csoi9FQ4cfpnRyNS8otpk83A6%2B7e9EIq4qKwmlvjU5E%3D&se=2017-11-11T19%3A38%3A34Z', NULL, 1, 1, 8, 0, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (6, N'intro Bubbles 1 skill video 2.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-10c07cc9-763d-4cd9-a705-3f3633ad969f/fn456080007.mp4?sv=2012-02-12&sr=c&si=afc53138-9d58-4307-827b-2b8ec442af6d&sig=VmG2wNuxpW7r4OkWEVx4I%2BUpof78kALznmxd1DeBFMc%3D&se=2017-11-11T19%3A38%3A25Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/71514891448076.jpg', 1, 1, CAST(N'2016-11-11 19:38:26.510' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (7, N'introvideoBubbles 2 Skill Video 1.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4125937f-eef5-4a20-ade2-8ada54af3cb0/fn1498416971.mp4?sv=2012-02-12&sr=c&si=18f237bf-9468-4e6e-90d2-0fc380040556&sig=%2Fpv0WEGshWjUdfDo684fmnbNlAWTOn5TmtPyQ49BTBc%3D&se=2017-11-11T19%3A38%3A29Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/77520993884992.jpg', 1, 1, CAST(N'2016-11-11 19:38:30.560' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (8, N'introvideoBubbles2-skillvideo2.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-07a99c4e-9847-47df-ad68-00fe548fd671/fn1889545502.mp4?sv=2012-02-12&sr=c&si=29c5d96a-ae6c-4406-a9a8-4c7f410628ef&sig=Csoi9FQ4cfpnRyNS8otpk83A6%2B7e9EIq4qKwmlvjU5E%3D&se=2017-11-11T19%3A38%3A34Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/6509551082966.jpg', 1, 1, CAST(N'2016-11-11 19:38:35.560' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (9, N'introvideoTaking Bubbles.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-87939bb6-4966-4672-b4d6-f2efa1c3d844/fn1742198501.mp4?sv=2012-02-12&sr=c&si=ab772f06-6a23-4411-9a97-9e0d9598eb00&sig=%2FWWq5iaeLf1dGfWdsBZqmUUTVPeOBnbNwVFNWuTUqbE%3D&se=2017-11-11T19%3A38%3A38Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/36968447611747.jpg', 1, 1, CAST(N'2016-11-11 19:38:40.027' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (10, N'Taking Bubbles.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-33cd0fec-b30c-43f4-bee0-6027aebbc82e/fn1864089266.mp4?sv=2012-02-12&sr=c&si=bad27887-3d7d-4969-b916-ca2b85ecba45&sig=NJkywceHzonyDCEUADYTH989AGucNSdVN2nvDvgHG5E%3D&se=2017-11-11T19%3A38%3A42Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/3249908958267.jpg', 1, 1, CAST(N'2016-11-11 19:38:42.463' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-87939bb6-4966-4672-b4d6-f2efa1c3d844/fn1742198501.mp4?sv=2012-02-12&sr=c&si=ab772f06-6a23-4411-9a97-9e0d9598eb00&sig=%2FWWq5iaeLf1dGfWdsBZqmUUTVPeOBnbNwVFNWuTUqbE%3D&se=2017-11-11T19%3A38%3A38Z', NULL, 1, 1, 9, 0, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (11, N'Station 1: Blowing Bubbles', N'Package', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ad5a1a82-19e3-4560-b0fa-5a46c645ccea/fn1624215174.mp4?sv=2012-02-12&sr=c&si=12b5366d-e3ec-4487-ab4c-414b62e1943b&sig=U23rQbL%2B%2FsTEB7DWC5Beij0whz9UqZwPJnDEdFW%2FxYM%3D&se=2017-11-12T02%3A54%3A14Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/7391518842094.jpg', 1, 1, CAST(N'2016-11-12 02:54:26.637' AS DateTime), 0, CAST(N'2016-12-02 11:34:56.200' AS DateTime), 30, NULL, NULL, 3, NULL, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (12, N'Box Squat Front View.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-bca1feb6-975e-47e1-8764-2aa6b8633351/fn1538349664.mp4?sv=2012-02-12&sr=c&si=630b83f5-09ce-4799-a05d-fda670e05ff4&sig=fbQyzCvZm00AoQpY6gnFSor6aUrw0H8ne1UrXinb5rQ%3D&se=2017-11-12T03%3A31%3A26Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/28562889136253.jpg', 1, 4, CAST(N'2016-11-12 03:31:28.503' AS DateTime), 4, CAST(N'2016-11-16 07:30:07.187' AS DateTime), 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f01f6ae0-e6da-46d8-ab38-125c9b2e8dcf/fn1580677574.mp4?sv=2012-02-12&sr=c&si=14ca1a04-5db4-4b64-a3b8-6d6e3f607710&sig=DzGurDbdZowvWRTB3OS7vrJ%2B05rkwmp6fjoiOXMEFIA%3D&se=2017-11-12T03%3A31%3A34Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-0a0215f9-8154-4974-b92c-9b871cc6bfb2/fn1021097356.mp4?sv=2012-02-12&sr=c&si=ee290c5d-71c5-495d-9714-4e6add09fe45&sig=NJdOcEAgEjN%2Fo%2FaBpHx%2B8hZ8gqBmrKS362LFFMDwKZg%3D&se=2017-11-12T03%3A31%3A38Z', 1, 1, 14, 15, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (13, N'Box Squat Side View.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-325442aa-2e9b-44cf-b11a-5214fdf05af9/fn419021115.mp4?sv=2012-02-12&sr=c&si=7107a7a0-286e-4ab6-9ea7-07c2ea67cf57&sig=QFufWCv%2BiGNkThl4%2BvZ6RO3zhjFqRVRDXUz4O46rSZw%3D&se=2017-11-12T03%3A31%3A31Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/24101350060374.jpg', 1, 4, CAST(N'2016-11-12 03:31:32.143' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-bca1feb6-975e-47e1-8764-2aa6b8633351/fn1538349664.mp4?sv=2012-02-12&sr=c&si=630b83f5-09ce-4799-a05d-fda670e05ff4&sig=fbQyzCvZm00AoQpY6gnFSor6aUrw0H8ne1UrXinb5rQ%3D&se=2017-11-12T03%3A31%3A26Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-f01f6ae0-e6da-46d8-ab38-125c9b2e8dcf/fn1580677574.mp4?sv=2012-02-12&sr=c&si=14ca1a04-5db4-4b64-a3b8-6d6e3f607710&sig=DzGurDbdZowvWRTB3OS7vrJ%2B05rkwmp6fjoiOXMEFIA%3D&se=2017-11-12T03%3A31%3A34Z', 1, 1, 12, 14, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (14, N'intero1Box Squat Side V...', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f01f6ae0-e6da-46d8-ab38-125c9b2e8dcf/fn1580677574.mp4?sv=2012-02-12&sr=c&si=14ca1a04-5db4-4b64-a3b8-6d6e3f607710&sig=DzGurDbdZowvWRTB3OS7vrJ%2B05rkwmp6fjoiOXMEFIA%3D&se=2017-11-12T03%3A31%3A34Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/9247963366487.jpg', 1, 4, CAST(N'2016-11-12 03:31:35.537' AS DateTime), 4, CAST(N'2016-11-16 07:41:05.630' AS DateTime), 30, NULL, NULL, 0, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (15, N'intro1Box Squat front View.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-0a0215f9-8154-4974-b92c-9b871cc6bfb2/fn1021097356.mp4?sv=2012-02-12&sr=c&si=ee290c5d-71c5-495d-9714-4e6add09fe45&sig=NJdOcEAgEjN%2Fo%2FaBpHx%2B8hZ8gqBmrKS362LFFMDwKZg%3D&se=2017-11-12T03%3A31%3A38Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/984858433405.jpg', 1, 4, CAST(N'2016-11-12 03:31:39.707' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (16, N'intro2Box Squat Front View.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1c065a47-d448-4550-b226-975185c4dd41/fn424163655.mp4?sv=2012-02-12&sr=c&si=326ff508-427a-4555-a579-a7aa1f77dcca&sig=3JYG76XdcxE9vLiC%2B49A3tmRFRnlxmFnA4UPw8HC75Q%3D&se=2017-11-12T03%3A31%3A42Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/57483426767377.jpg', 1, 4, CAST(N'2016-11-12 03:31:43.753' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (17, N'intro2Box Squat Side View.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-c0a10f8f-c15e-4b52-8ea8-322f0bd3a7a9/fn2042497747.mp4?sv=2012-02-12&sr=c&si=cc42f845-05df-447b-b5a6-76e46dd2ded3&sig=24I5aWRe8TmhPmqie3gSVei%2Ba%2BDrfgDZZM7bWiZcAh8%3D&se=2017-11-12T03%3A31%3A46Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/11577397520292.jpg', 1, 4, CAST(N'2016-11-12 03:31:47.613' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (18, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f3419831-64c2-481b-9d59-bf785cb0fa2f/fn1357663702.mp4?sv=2012-02-12&sr=c&si=8f231d68-b45d-4e28-b546-db7ea0dbef42&sig=BcM6FSZ%2FxLm9Amscv%2FUPqpNyr7MFLz6AK9w3UykWHrw%3D&se=2017-11-12T06%3A47%3A00Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/69549661555208.jpg', 1, 5, CAST(N'2016-11-12 06:47:01.330' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (19, N'Backstroke Front Above ...', N'Jared White', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-d321b9de-43dd-4278-9031-09c3b203e5ab/fn365939550.mp4?sv=2012-02-12&sr=c&si=6b386831-714f-411c-bd45-dac876c09c43&sig=g3e0ElOgJwJ48y24oAF0KDjY%2Fim%2BIVeLgsAawEmbL3Y%3D&se=2017-11-12T06%3A51%3A29Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/6557008949750.jpg', 1, 5, CAST(N'2016-11-12 06:51:30.313' AS DateTime), 5, CAST(N'2016-11-12 06:54:01.507' AS DateTime), 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-967ba6ec-be0f-419b-abc4-15090264732e/fn252504010.mp4?sv=2012-02-12&sr=c&si=311d001d-c421-40aa-9b93-21113828ca7f&sig=7w%2BIbiSv1cytLlC5Zpm2K5vzbY%2Bci4hvufG8JI%2BQjJE%3D&se=2017-11-12T06%3A52%3A13Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-a341d627-32ec-4f19-996c-5a9785f1a81b/fn18671334.mp4?sv=2012-02-12&sr=c&si=d26e3de9-d0e3-4e38-b635-8b98c0f8e2e2&sig=9vAblCFZijWaTE7FFA51jlJpKtT55%2B%2FCrMUh88WaLDo%3D&se=2017-11-12T06%3A52%3A24Z', 1, 1, 27, 29, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (20, N'UW Backstroke Pull Fron...', N'Nick Thomas', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f6e89346-2c44-461e-bb4b-312b573ba153/fn1145961568.mp4?sv=2012-02-12&sr=c&si=3c4457b4-2823-4e6d-a746-ab0387e182c8&sig=F7jFHQhbDzo31%2BwB%2FZh75p4Ulqs2UE2%2FRJgg77zuSkE%3D&se=2017-11-12T06%3A51%3A32Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/18577171132009.jpg', 1, 5, CAST(N'2016-11-12 06:51:33.047' AS DateTime), 5, CAST(N'2016-11-12 06:53:30.210' AS DateTime), 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-a357b7e1-1279-42bb-a418-c7453cb78d89/fn2113710104.mp4?sv=2012-02-12&sr=c&si=30186e3b-8982-400f-9ef1-b693c67c4389&sig=nsgnWAXeelMxSaG07ZCRpb6eCJoUx%2FrXeY2942qOPqk%3D&se=2017-11-12T06%3A51%3A54Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-b12f73c7-e061-4f28-bbc1-6af556b5dd70/fn66557772.mp4?sv=2012-02-12&sr=c&si=344e3a04-cec9-4a96-b294-9ca333b594f9&sig=nhDvIy0vV7E25H%2BtLrKdX1IfRWqQJC7CG5KT5HzXd1o%3D&se=2017-11-12T06%3A51%3A49Z', 1, 1, 24, 23, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (21, N'UW Backstroke Pull Full...', N'Nick Thomas', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-74029379-d5b4-4a8f-a155-771ab9363f92/fn1581534515.mp4?sv=2012-02-12&sr=c&si=7423c1fe-80a1-4a20-a129-58fd09e3f512&sig=ssV9qlCmj4eozSl4FlUt1%2F%2B9URnYwujRgVaO22ZsGtg%3D&se=2017-11-12T06%3A51%3A35Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/36790440015453.jpg', 1, 5, CAST(N'2016-11-12 06:51:36.860' AS DateTime), 5, CAST(N'2016-12-08 16:43:18.627' AS DateTime), 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-26e57321-0dbc-4055-a4ea-b3d02fc6c0f2/fn1113869163.mp4?sv=2012-02-12&sr=c&si=b51ea7d3-cb25-43b7-96a2-1f833b2cd95f&sig=df0Ain4VZZFYdl7wXtIPbaLwuNEmr3u22EG%2FCpFYAoY%3D&se=2017-11-12T06%3A52%3A00Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-2fd39687-20bc-4bb8-b0ee-2397c07078f6/fn458309180.mp4?sv=2012-02-12&sr=c&si=fcb6fe92-b743-45ca-8f95-138428687aac&sig=30C%2BtMFBlsTAQK2wTldNqk7Ec%2BOIyyOy83pLMw3vif8%3D&se=2017-11-12T06%3A52%3A08Z', 1, 0, 25, 26, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (22, N'UW Backstroke Side Full...', N'Nick Thomas', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-7bb252b2-32c0-4a03-a499-46a6a5ee069e/fn57063682.mp4?sv=2012-02-12&sr=c&si=a5daab62-2a61-4013-ba38-f36f57378819&sig=eg84%2FNjnpQC1q80OKcWSim96Wl5QXtGbo9oG5csSFJo%3D&se=2017-11-12T06%3A51%3A39Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/21200723236100.jpg', 1, 5, CAST(N'2016-11-12 06:51:40.453' AS DateTime), 5, CAST(N'2016-11-12 06:53:52.833' AS DateTime), 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f8a9bed7-f058-4a65-b3e6-b8e993f605f1/fn337041292.mp4?sv=2012-02-12&sr=c&si=349b1cdf-0b2f-42ed-a53e-f107510443fa&sig=GNxLoH5ipnHynpSAbL4cFo6WMsAJIES8MdMD%2BCMkrgk%3D&se=2017-11-12T06%3A52%3A20Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-50b1500a-b1b3-4009-8400-96b7add7b28c/fn35256357.mp4?sv=2012-02-12&sr=c&si=fbce9b6c-2ac7-4e57-8689-536fd7d398af&sig=R1s6PkQA0L3G%2FDFrYGOlvlgy4wolY8OaV%2BcdCAehKz0%3D&se=2017-11-12T06%3A52%3A28Z', 1, 1, 28, 30, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (23, N'Video 1_Instruction 2.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b12f73c7-e061-4f28-bbc1-6af556b5dd70/fn66557772.mp4?sv=2012-02-12&sr=c&si=344e3a04-cec9-4a96-b294-9ca333b594f9&sig=nhDvIy0vV7E25H%2BtLrKdX1IfRWqQJC7CG5KT5HzXd1o%3D&se=2017-11-12T06%3A51%3A49Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/7478016835522.jpg', 1, 5, CAST(N'2016-11-12 06:51:50.343' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (24, N'Video 1_Instruction Video 1.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-a357b7e1-1279-42bb-a418-c7453cb78d89/fn2113710104.mp4?sv=2012-02-12&sr=c&si=30186e3b-8982-400f-9ef1-b693c67c4389&sig=nsgnWAXeelMxSaG07ZCRpb6eCJoUx%2FrXeY2942qOPqk%3D&se=2017-11-12T06%3A51%3A54Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/5520364846292.jpg', 1, 5, CAST(N'2016-11-12 06:51:55.860' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (25, N'Video 2_Instruction 1.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-26e57321-0dbc-4055-a4ea-b3d02fc6c0f2/fn1113869163.mp4?sv=2012-02-12&sr=c&si=b51ea7d3-cb25-43b7-96a2-1f833b2cd95f&sig=df0Ain4VZZFYdl7wXtIPbaLwuNEmr3u22EG%2FCpFYAoY%3D&se=2017-11-12T06%3A52%3A00Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/50241025584666.jpg', 1, 5, CAST(N'2016-11-12 06:52:01.500' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (26, N'Video 2_Instruction 2.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-2fd39687-20bc-4bb8-b0ee-2397c07078f6/fn458309180.mp4?sv=2012-02-12&sr=c&si=fcb6fe92-b743-45ca-8f95-138428687aac&sig=30C%2BtMFBlsTAQK2wTldNqk7Ec%2BOIyyOy83pLMw3vif8%3D&se=2017-11-12T06%3A52%3A08Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/20808838487293.jpg', 1, 5, CAST(N'2016-11-12 06:52:09.550' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (27, N'Video 3_Instruction 1.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-967ba6ec-be0f-419b-abc4-15090264732e/fn252504010.mp4?sv=2012-02-12&sr=c&si=311d001d-c421-40aa-9b93-21113828ca7f&sig=7w%2BIbiSv1cytLlC5Zpm2K5vzbY%2Bci4hvufG8JI%2BQjJE%3D&se=2017-11-12T06%3A52%3A13Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/22667929638467.jpg', 1, 5, CAST(N'2016-11-12 06:52:14.533' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (28, N'Video 4_Instruction 1.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f8a9bed7-f058-4a65-b3e6-b8e993f605f1/fn337041292.mp4?sv=2012-02-12&sr=c&si=349b1cdf-0b2f-42ed-a53e-f107510443fa&sig=GNxLoH5ipnHynpSAbL4cFo6WMsAJIES8MdMD%2BCMkrgk%3D&se=2017-11-12T06%3A52%3A20Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/9920188718704.jpg', 1, 5, CAST(N'2016-11-12 06:52:21.190' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (29, N'Video_3_Instruction_2.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-a341d627-32ec-4f19-996c-5a9785f1a81b/fn18671334.mp4?sv=2012-02-12&sr=c&si=d26e3de9-d0e3-4e38-b635-8b98c0f8e2e2&sig=9vAblCFZijWaTE7FFA51jlJpKtT55%2B%2FCrMUh88WaLDo%3D&se=2017-11-12T06%3A52%3A24Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/14179044094287.jpg', 1, 5, CAST(N'2016-11-12 06:52:24.957' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (30, N'Video_4_Instruction_2.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-50b1500a-b1b3-4009-8400-96b7add7b28c/fn35256357.mp4?sv=2012-02-12&sr=c&si=fbce9b6c-2ac7-4e57-8689-536fd7d398af&sig=R1s6PkQA0L3G%2FDFrYGOlvlgy4wolY8OaV%2BcdCAehKz0%3D&se=2017-11-12T06%3A52%3A28Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/62708824245464.jpg', 1, 5, CAST(N'2016-11-12 06:52:28.410' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (31, N'Learning the Backstroke Pull', N'Package', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-3e8213c0-58df-45cd-9053-c9e69c745315/fn1847095142.mp4?sv=2012-02-12&sr=c&si=8e5ad487-916d-4497-a14a-567f4a8a8544&sig=Zxquh6P%2BHbv20lZOqcizbSvHH71qInsKzKwwqZIjer0%3D&se=2017-11-12T07%3A04%3A48Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/65952818773161.jpg', 1, 5, CAST(N'2016-11-12 07:04:50.193' AS DateTime), 0, CAST(N'2016-12-02 11:38:16.627' AS DateTime), 30, NULL, NULL, 3, NULL, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (32, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-5449b50f-e421-4d7e-b74f-e02fa16783dd/fn1872758016.mp4?sv=2012-02-12&sr=c&si=55ceb3a1-f0c9-4cde-9024-b2e88686275f&sig=uF8ph4hX8txGjRJSxoTxx8nR%2Fm5NVNUqi6CEOzsXrls%3D&se=2017-11-12T07%3A22%3A40Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/1884984468438.jpg', 1, 6, CAST(N'2016-11-12 07:22:41.710' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (33, N'Cross Over Approach_Tur...', N'Cross Over Approach_Turn Front View', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-e022771a-3c97-4216-8a9e-efb93d794bfc/fn922997571.mp4?sv=2012-02-12&sr=c&si=c016e689-92f5-4c95-af7d-3e7c2090647b&sig=MfA6pgaIntdDazaQfMse1spruYnpv6357bPtu%2F6wSdw%3D&se=2017-11-12T07%3A24%3A20Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/73731032688345.jpg', 1, 6, CAST(N'2016-11-12 07:24:20.823' AS DateTime), 6, CAST(N'2016-11-12 07:26:49.830' AS DateTime), 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-54c7bea7-74be-49b9-9bbf-2a36357eca8a/fn1717319993.mp4?sv=2012-02-12&sr=c&si=ef2f1dd8-b0b4-4839-8623-12bea39bd552&sig=u1a4pEyJLZL5q6saWK4YyvBeGlGyBqTVVWTY9Yv1Vy4%3D&se=2017-11-12T07%3A24%3A38Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-b9bfa3cc-4365-41a4-bff1-65a051042bb1/fn485140005.mp4?sv=2012-02-12&sr=c&si=eaee8e05-b61e-4afe-9c0d-115b2978611d&sig=4z3gOEQwryX0pR9635O5j4WCkWIPxET40wsw%2FtUHDFw%3D&se=2017-11-12T07%3A24%3A27Z', 1, 1, 37, 35, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (34, N'Cross Over Approach_Tur...', N'Cross Over Approach_Turn Side View', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-edc40a61-f88d-4861-9f9a-2b5c590b29be/fn1508488789.mp4?sv=2012-02-12&sr=c&si=4b0a3c83-6bd0-479a-9402-6cd929602000&sig=f89uqz6OM8gw9qK00sIq8v4f8DUK66ac5EBRlYK3Kh4%3D&se=2017-11-12T07%3A24%3A23Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/5705447438338.jpg', 1, 6, CAST(N'2016-11-12 07:24:23.870' AS DateTime), 6, CAST(N'2016-11-12 07:27:07.563' AS DateTime), 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-2d976a1e-8d7c-4905-b631-0049bae3a368/fn883527582.mp4?sv=2012-02-12&sr=c&si=9e65dc8f-a9df-46e7-b472-27e6ab164190&sig=j4KFtSy%2FKM6Oo0uW08ay9vrSvCYobxQWwWuANvO0DWQ%3D&se=2017-11-12T07%3A24%3A33Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-b98caee2-3101-46a7-b82a-de23179b5bdb/fn2108616637.mp4?sv=2012-02-12&sr=c&si=912cd940-5c7a-4bdb-8112-7217007e5fe2&sig=P74ycA%2BIkyJkEeMzGAtD2Jhakrhn8X8JxNvX8TdgSMU%3D&se=2017-11-12T07%3A24%3A44Z', 1, 1, 36, 38, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (35, N'interovideo2.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b9bfa3cc-4365-41a4-bff1-65a051042bb1/fn485140005.mp4?sv=2012-02-12&sr=c&si=eaee8e05-b61e-4afe-9c0d-115b2978611d&sig=4z3gOEQwryX0pR9635O5j4WCkWIPxET40wsw%2FtUHDFw%3D&se=2017-11-12T07%3A24%3A27Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/94794198558981.jpg', 1, 6, CAST(N'2016-11-12 07:24:28.433' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (36, N'intrivideo2.1.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-2d976a1e-8d7c-4905-b631-0049bae3a368/fn883527582.mp4?sv=2012-02-12&sr=c&si=9e65dc8f-a9df-46e7-b472-27e6ab164190&sig=j4KFtSy%2FKM6Oo0uW08ay9vrSvCYobxQWwWuANvO0DWQ%3D&se=2017-11-12T07%3A24%3A33Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/6950112319750.jpg', 1, 6, CAST(N'2016-11-12 07:24:34.480' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (37, N'introvideo1.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-54c7bea7-74be-49b9-9bbf-2a36357eca8a/fn1717319993.mp4?sv=2012-02-12&sr=c&si=ef2f1dd8-b0b4-4839-8623-12bea39bd552&sig=u1a4pEyJLZL5q6saWK4YyvBeGlGyBqTVVWTY9Yv1Vy4%3D&se=2017-11-12T07%3A24%3A38Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/51841890761325.jpg', 1, 6, CAST(N'2016-11-12 07:24:39.387' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (38, N'introvideo2.2.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b98caee2-3101-46a7-b82a-de23179b5bdb/fn2108616637.mp4?sv=2012-02-12&sr=c&si=912cd940-5c7a-4bdb-8112-7217007e5fe2&sig=P74ycA%2BIkyJkEeMzGAtD2Jhakrhn8X8JxNvX8TdgSMU%3D&se=2017-11-12T07%3A24%3A44Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/5283144051163.jpg', 1, 6, CAST(N'2016-11-12 07:24:44.900' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (39, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-5bb7a6d8-d8df-4032-a3d9-f4f15aa78e12/fn1011311295.quicktime?sv=2012-02-12&sr=c&si=c441c10c-e3d7-4ee1-be27-950d8427ab96&sig=sbPdCa1gu964FxSLVWkNYqcbgesgHHvKXOf2lKrIFtE%3D&se=2017-11-12T11%3A45%3A53Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/96282976320268.jpg', 1, 7, CAST(N'2016-11-12 11:30:05.017' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (41, N'MOV_0193.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-252a5643-d5c8-4d0f-b272-29c04a67382c/fn1729836101.mp4?sv=2012-02-12&sr=c&si=2bdf6a31-0992-464d-afc3-609b0c124676&sig=wlR%2FIxU36HJi0RmawF25MX713GAKXBVhPFLCoL2pRaU%3D&se=2017-11-12T11%3A50%3A47Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/16221747496818.jpg', 1, 7, CAST(N'2016-11-12 11:50:48.443' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-252a5643-d5c8-4d0f-b272-29c04a67382c/fn1729836101.mp4?sv=2012-02-12&sr=c&si=2bdf6a31-0992-464d-afc3-609b0c124676&sig=wlR%2FIxU36HJi0RmawF25MX713GAKXBVhPFLCoL2pRaU%3D&se=2017-11-12T11%3A50%3A47Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-252a5643-d5c8-4d0f-b272-29c04a67382c/fn1729836101.mp4?sv=2012-02-12&sr=c&si=2bdf6a31-0992-464d-afc3-609b0c124676&sig=wlR%2FIxU36HJi0RmawF25MX713GAKXBVhPFLCoL2pRaU%3D&se=2017-11-12T11%3A50%3A47Z', 1, 1, 41, 41, 7)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (42, N'satya', N'Package', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-d20c669f-4fea-4b60-bb12-2360608fef0f/fn566218240.mp4?sv=2012-02-12&sr=c&si=bd9b7a3a-4f39-473f-b86a-866055f3622f&sig=gZ3cAdzXsz3PautB9msDDX80MJITRSxSlXOV%2F02%2BkHQ%3D&se=2017-11-12T11%3A56%3A00Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/2680869838216.jpg', 1, 7, CAST(N'2016-11-12 11:56:01.057' AS DateTime), NULL, NULL, 30, NULL, NULL, 3, NULL, NULL, NULL, 7)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (43, N'20 yard shuttle-HD (1).mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-9f72417c-7f17-49b0-a308-674404b47123/fn875119875.mp4?sv=2012-02-12&sr=c&si=95969296-3321-42d9-8d07-025461433013&sig=VXySk0E6kr2w75%2BgHxKqNr%2BElomdqOvel%2FN9Ve88O1c%3D&se=2017-11-12T12%3A02%3A13Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/11206860652928.jpg', 1, 7, CAST(N'2016-11-12 12:02:13.357' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 7)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (44, N'MOV_0193.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4cb3ed89-4525-43f5-8f9d-41ddd64714d1/fn8819071.mp4?sv=2012-02-12&sr=c&si=2e24a480-bb59-4dfe-944a-0156987c89c1&sig=SYzzyFpo%2B1gKssMcnaGB8DzwbnljzqUjWFcgKTyrfyk%3D&se=2017-11-12T12%3A03%3A33Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/15137339717863.jpg', 1, 7, CAST(N'2016-11-12 12:03:34.153' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4cb3ed89-4525-43f5-8f9d-41ddd64714d1/fn8819071.mp4?sv=2012-02-12&sr=c&si=2e24a480-bb59-4dfe-944a-0156987c89c1&sig=SYzzyFpo%2B1gKssMcnaGB8DzwbnljzqUjWFcgKTyrfyk%3D&se=2017-11-12T12%3A03%3A33Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-4cb3ed89-4525-43f5-8f9d-41ddd64714d1/fn8819071.mp4?sv=2012-02-12&sr=c&si=2e24a480-bb59-4dfe-944a-0156987c89c1&sig=SYzzyFpo%2B1gKssMcnaGB8DzwbnljzqUjWFcgKTyrfyk%3D&se=2017-11-12T12%3A03%3A33Z', 1, 1, 44, 44, 7)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (45, N'khfjg', N'Package', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-075bdf92-7da2-4b45-bdcb-2fd59f0ad896/fn313500703.mp4?sv=2012-02-12&sr=c&si=fe86776f-a1fe-471c-85f2-1fab022da1ec&sig=53QCkalUYoO9%2B2wRwMBIuUCqyH6GjoyeXiLPLzgmPYc%3D&se=2017-11-12T12%3A05%3A00Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/5124342411704.jpg', 1, 7, CAST(N'2016-11-12 12:04:59.967' AS DateTime), 0, CAST(N'2016-11-12 12:06:21.450' AS DateTime), 30, NULL, NULL, 3, NULL, NULL, NULL, 7)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (46, N'broad jump-HD.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-76dbdf3f-8fc3-4d63-882e-596d35647b14/fn989787909.mp4?sv=2012-02-12&sr=c&si=92794091-f49d-409e-ab38-c63617330c75&sig=lJamDqbLayGF29WpJDukvzJU1r3%2FeLFAYZlkrZ7fltg%3D&se=2017-11-12T12%3A23%3A46Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/48602216155775.jpg', 1, 7, CAST(N'2016-11-12 12:23:46.537' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, 0, 0, 7)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (47, N'Kettle Bell - Side up.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-e03916fe-578f-4c1e-b002-5280a1e53756/fn11051654.mp4?sv=2012-02-12&sr=c&si=c1dd41d7-7ab1-4ed9-b3a0-f9171ed09bdf&sig=Y2tnPj6WgQE3YcIJIAFdT4qjoAKbWBzK7H4084TQ5xY%3D&se=2017-11-12T12%3A23%3A49Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/269618538690.jpg', 1, 7, CAST(N'2016-11-12 12:23:50.270' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-5b22bddd-0b42-4f80-ad9c-fa2d86030871/fn1943236041.mp4?sv=2012-02-12&sr=c&si=3ffb2d08-34d6-4198-91a8-97a7bd31e598&sig=oQMCKZMKuHEvL8ySiC9iFx%2BcqHXfBsa2%2BRPf4gohaVc%3D&se=2017-11-12T12%3A23%3A53Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-4cb3ed89-4525-43f5-8f9d-41ddd64714d1/fn8819071.mp4?sv=2012-02-12&sr=c&si=2e24a480-bb59-4dfe-944a-0156987c89c1&sig=SYzzyFpo%2B1gKssMcnaGB8DzwbnljzqUjWFcgKTyrfyk%3D&se=2017-11-12T12%3A03%3A33Z', 1, 1, 48, 44, 7)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (49, N'Vertical Jump - BlastMotion-HD.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-392048f5-5ce4-4c1f-baac-6d10503c9e29/fn837921245.mp4?sv=2012-02-12&sr=c&si=05b1d503-9205-4581-94e3-0f79b146c415&sig=MYxWTutQKS6PLgCwdXDbqUd303wlhbfO%2BX1kiMo7m4Y%3D&se=2017-11-12T12%3A23%3A56Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/9692931256657.jpg', 1, 7, CAST(N'2016-11-12 12:23:57.473' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 7)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (50, N'kjt', N'Package', N'mp4', NULL, N'', N'', 1, 0, CAST(N'2016-11-12 12:30:39.647' AS DateTime), 0, CAST(N'2016-11-12 12:34:30.977' AS DateTime), 30, NULL, NULL, 3, NULL, NULL, NULL, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (51, N'testing pack', N'Package', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-c60c8950-b06a-4016-ae64-5f18ff5d7e1f/fn1414036924.mp4?sv=2012-02-12&sr=c&si=2eec11d6-8789-4339-96a7-e8f233021466&sig=gQD5vOJMJALzSt7IYBVn%2FHbNCWaldJMfHRzSyfop%2BTg%3D&se=2017-11-16T09%3A31%3A15Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/58233645047755.jpg', 1, 4, CAST(N'2016-11-16 09:31:23.997' AS DateTime), NULL, NULL, 30, NULL, NULL, 3, NULL, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (52, N'testing pack', N'Package', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-83d0340b-79a9-42fa-9d68-bfa704000324/fn406260065.mp4?sv=2012-02-12&sr=c&si=d5d3670a-1826-4ca9-97f5-709e1506cd2b&sig=UPDof8iqSuC7ka5dQbuV4x1V6wk%2BaJM0uVI0cUcbQMg%3D&se=2017-11-16T09%3A38%3A27Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/26010948423015.jpg', 1, 6, CAST(N'2016-11-16 09:38:27.713' AS DateTime), NULL, NULL, 30, NULL, NULL, 3, NULL, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (53, N'Stage 2 - lesson 1 Pre Instruction.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4bb93624-befa-4e7f-b359-d28ccf153097/fn1405419775.mp4?sv=2012-02-12&sr=c&si=2a5d05a3-4768-48bd-987a-3070d647c00f&sig=l3rhvBJHFHhUrI3Qpbs20OksrP6myhJ%2BIiLZ1yhiCpk%3D&se=2017-11-17T07%3A12%3A03Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/85322594562977.jpg', 1, 1, CAST(N'2016-11-17 07:12:14.483' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (54, N'Stage 2 - Lesson 1 Skill Video.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-6edbb1e3-5f22-448a-bd51-f3a9b96952b2/fn1750323610.mp4?sv=2012-02-12&sr=c&si=d77d0bc1-c72d-42e6-9c4b-8022de2930f7&sig=UzLoLdZKw0DxcsW2pvcciOVPcjtidntAfRac1aXvGV0%3D&se=2017-11-17T07%3A12%3A17Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/96554795128691.jpg', 1, 1, CAST(N'2016-11-17 07:12:17.157' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4bb93624-befa-4e7f-b359-d28ccf153097/fn1405419775.mp4?sv=2012-02-12&sr=c&si=2a5d05a3-4768-48bd-987a-3070d647c00f&sig=l3rhvBJHFHhUrI3Qpbs20OksrP6myhJ%2BIiLZ1yhiCpk%3D&se=2017-11-17T07%3A12%3A03Z', NULL, 1, 1, 53, 0, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (55, N'Stage 2 - Lesson 2 Pre Instruction.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-91f195d4-1bb5-4676-ba43-bbef066f17ef/fn1459981158.mp4?sv=2012-02-12&sr=c&si=c3ccab49-2ffa-484e-a523-a5fb5915437c&sig=tdut3oxh%2BweWVE8TT0Wh21GbuRRnUnsiou7wFSHNgFY%3D&se=2017-11-17T07%3A12%3A21Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/27643588876942.jpg', 1, 1, CAST(N'2016-11-17 07:12:21.423' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (56, N'Stage 2 - Lesson 2 Skill .mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-0e5d3a86-ef82-4310-a0b5-7c720ae90d99/fn1469475248.mp4?sv=2012-02-12&sr=c&si=63931445-a8b7-48c0-9764-b6fb71a8c04e&sig=SSTroFCzmKbqaFe0RDwcIaPzExmGDqQcHkHEIUhraHY%3D&se=2017-11-17T07%3A12%3A24Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/19687556878397.jpg', 1, 1, CAST(N'2016-11-17 07:12:23.767' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-91f195d4-1bb5-4676-ba43-bbef066f17ef/fn1459981158.mp4?sv=2012-02-12&sr=c&si=c3ccab49-2ffa-484e-a523-a5fb5915437c&sig=tdut3oxh%2BweWVE8TT0Wh21GbuRRnUnsiou7wFSHNgFY%3D&se=2017-11-17T07%3A12%3A21Z', NULL, 1, 1, 55, 0, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (57, N'Stage 2 - Lesson 3 Post Instruction.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-e047c411-1da5-467f-b371-75232cf1e61c/fn1373495483.mp4?sv=2012-02-12&sr=c&si=168530a9-7a46-4cf7-933b-aad0201d7a97&sig=sVEjUkn53f54xq3BMyt38YwayEC%2BaHAa%2FmSm5yUAqEI%3D&se=2017-11-17T07%3A12%3A27Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/27433085249045.jpg', 1, 1, CAST(N'2016-11-17 07:12:27.017' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (58, N'Stage 2 - Lesson 3 Pre Instruction.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4d1c1752-70e4-4867-8edf-a2cc7f116e0e/fn1472046518.mp4?sv=2012-02-12&sr=c&si=48bd7183-0fff-4e8a-9bc8-ce6f806a56f9&sig=GBoY1TVp7jTpsroOOWFHXgLb0ZPT2vexlffSYJpwpaA%3D&se=2017-11-17T07%3A12%3A30Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/45976622577829.jpg', 1, 1, CAST(N'2016-11-17 07:12:30.470' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (59, N'Stage 2 - Lesson 3 Skill.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b737a437-6df9-45af-980c-6630c2d5a1bf/fn1518894091.mp4?sv=2012-02-12&sr=c&si=143340c6-07fd-4126-8aab-482b3095e1d4&sig=tGUnZheKTd%2Fy78OJnnJDtEGa%2FivGPXEkORNIp3te7A4%3D&se=2017-11-17T07%3A12%3A34Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/73240653858075.jpg', 1, 1, CAST(N'2016-11-17 07:12:33.720' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4d1c1752-70e4-4867-8edf-a2cc7f116e0e/fn1472046518.mp4?sv=2012-02-12&sr=c&si=48bd7183-0fff-4e8a-9bc8-ce6f806a56f9&sig=GBoY1TVp7jTpsroOOWFHXgLb0ZPT2vexlffSYJpwpaA%3D&se=2017-11-17T07%3A12%3A30Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-e047c411-1da5-467f-b371-75232cf1e61c/fn1373495483.mp4?sv=2012-02-12&sr=c&si=168530a9-7a46-4cf7-933b-aad0201d7a97&sig=sVEjUkn53f54xq3BMyt38YwayEC%2BaHAa%2FmSm5yUAqEI%3D&se=2017-11-17T07%3A12%3A27Z', 1, 1, 58, 57, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (60, N'Stage 2 - Lesson 4 Pre Instruction.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-23ede782-1d50-472a-82ab-7859c1f4575f/fn727429590.mp4?sv=2012-02-12&sr=c&si=e906c9a2-6056-42ca-97ec-297812c68824&sig=C1UKg8URknEJ27%2BFrssJAE2lofV%2Fp8YL7ohr3bizLSw%3D&se=2017-11-17T07%3A12%3A37Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/83063696535396.jpg', 1, 1, CAST(N'2016-11-17 07:12:37.203' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (61, N'Stage 2 - Lesson 4 Skill.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-46994e40-a46d-469d-8556-5adbf644786a/fn587005409.mp4?sv=2012-02-12&sr=c&si=779e24a7-dab5-48d8-8ed5-4ab027cc7e1d&sig=SEWvw0Q0vu7e8HmJq6zJhrRKqv4RjEBvf%2F1jyQ%2B19lk%3D&se=2017-11-17T07%3A12%3A41Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/15891817017379.jpg', 1, 1, CAST(N'2016-11-17 07:12:40.390' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-23ede782-1d50-472a-82ab-7859c1f4575f/fn727429590.mp4?sv=2012-02-12&sr=c&si=e906c9a2-6056-42ca-97ec-297812c68824&sig=C1UKg8URknEJ27%2BFrssJAE2lofV%2Fp8YL7ohr3bizLSw%3D&se=2017-11-17T07%3A12%3A37Z', NULL, 1, 1, 60, 0, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (62, N'Stage 2 Package Intro.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-de851fa3-2813-4749-b23e-64d52db416fd/fn917440727.mp4?sv=2012-02-12&sr=c&si=7fdba7a9-4785-4a95-9b78-1e464780e96e&sig=IFTWebMHqmCjwR0ARJGvRpaxBOeKu8s0I5ig612dYFE%3D&se=2017-11-17T07%3A12%3A46Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/79800811720013.jpg', 1, 1, CAST(N'2016-11-17 07:12:46.297' AS DateTime), NULL, NULL, 30, NULL, NULL, 0, 1, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (63, N'Station 2: Float & Glide', N'Package', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b36b1c84-2ae0-4fc4-a3bc-ac6398e90d8f/fn1812430439.mp4?sv=2012-02-12&sr=c&si=27251e13-bc7e-4e2c-9670-3d5f583b4a1b&sig=MqwKBq4bzZXdjZKPmJrnARIruAUVstPHEbc1mniQ9GU%3D&se=2017-11-17T07%3A29%3A33Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/8418738468808.jpg', 1, 1, CAST(N'2016-11-17 07:29:33.263' AS DateTime), 0, CAST(N'2016-11-22 19:30:56.753' AS DateTime), 30, NULL, NULL, 3, NULL, NULL, NULL, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (64, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'', N'', 1, 4, CAST(N'2016-11-17 10:17:00.857' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (65, N'Side Breath.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-942b9dad-973a-4b7c-aa38-a370f4adca52/fn1553705891.mp4?sv=2012-02-12&sr=c&si=aa622700-4639-4497-9230-6cad988a5d77&sig=9I9fV8UPOzJK8Ydl7jq9p%2BZvmajrjsV2rFsDfBrj%2Bqk%3D&se=2017-11-21T20%3A11%3A32Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/85561076284106.jpg', 1, 4, CAST(N'2016-11-21 20:11:41.410' AS DateTime), 4, CAST(N'2016-11-21 20:13:14.557' AS DateTime), 30, NULL, NULL, 1, 1, 0, 0, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (66, N'Front In Breathe.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f3910a22-d38d-4731-b568-4765260bbb63/fn1124016899.mp4?sv=2012-02-12&sr=c&si=13c9e7b6-0e75-4a1c-8178-3f28a255c74f&sig=Ti2dmH3AlutQApAxOa%2FvAmiK360fAVZOwctmIVWITns%3D&se=2017-11-21T20%3A12%3A10Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/20719524459430.jpg', 1, 4, CAST(N'2016-11-21 20:12:10.943' AS DateTime), 4, CAST(N'2016-11-21 20:13:37.607' AS DateTime), 30, NULL, NULL, 1, 1, 0, 0, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (67, N'Catch.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1232d0e8-3cb9-4ca0-998c-03eb4c2ed936/fn672768457.mp4?sv=2012-02-12&sr=c&si=e52ead8b-8b64-4553-9399-5b7d150d87e2&sig=VdpynmpNRzwFmhnNwIM2CZu%2B6PWmgH311EbT4NouZkg%3D&se=2017-11-21T20%3A12%3A36Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/89238844762372.jpg', 1, 4, CAST(N'2016-11-21 20:12:35.993' AS DateTime), 4, CAST(N'2016-11-21 20:14:10.077' AS DateTime), 30, NULL, NULL, 1, 1, 0, 0, 13)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (68, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'', N'', 1, 8, CAST(N'2016-11-22 17:23:09.870' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (175, N'Video (1).mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-31b3b991-b345-495d-9d8c-d1b4c766981b/fn843360437.mp4?sv=2012-02-12&sr=c&si=3a82bb2f-8e72-40a1-9901-4a124a6cb64a&sig=0ZiprfzwD81pSVTzMjpjeWzU9FwTcBC7S8m7SyAjmhY%3D&se=2017-12-02T12%3A47%3A15Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/49094986850831.jpg', 1, 8, CAST(N'2016-12-02 12:49:14.610' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 1, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (176, N'Video.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ad625787-1b10-4a50-bd45-e69c70ef29a8/fn1585860859.mp4?sv=2012-02-12&sr=c&si=94add86f-0cfe-4224-8230-78d6eb356770&sig=kICVQmtiaqJagWB%2BsgeebURKXKawiMl3w7be4rgx1S0%3D&se=2017-12-02T12%3A47%3A18Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/28593797732818.jpg', 1, 8, CAST(N'2016-12-02 12:49:14.627' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 1, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (177, N'Video_1.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f7511b85-b7b0-4f90-800c-65dedacb8da4/fn2035615672.mp4?sv=2012-02-12&sr=c&si=c5ebb601-8097-4bb2-9b8e-db5f57d6cbe3&sig=mzjdxMz4p7g7jjgWZdIuOdXBamtgHIJcjMaA4a%2FFwms%3D&se=2017-12-02T12%3A47%3A21Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/1158702737470.jpg', 1, 8, CAST(N'2016-12-02 12:49:14.640' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 1, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (222, N'After Skill Instructions Deadlift.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-41cdc93a-2d8a-4edd-bc09-87d12c1042be/fn214738815.mp4?sv=2012-02-12&sr=c&si=d86fcd10-f21b-4a90-a20d-4f8eef96e4cc&sig=csIqdj6Dv%2B%2Bk4HK0AEEnrEajDhEwVdBakmfbVEBUuCQ%3D&se=2017-12-06T12%3A38%3A38Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/7870819574603.jpg', 1, 8, CAST(N'2016-12-06 12:38:40.607' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (223, N'Deadlift Front View.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-20fddea4-86e6-4206-b4a2-37444c9290f3/fn844842562.mp4?sv=2012-02-12&sr=c&si=e6cd4b55-4798-4390-9f97-fb70c947a013&sig=wmsjob4VBG0UNbaqkEwl4w4w6xhmE3hc772GN8sfGP8%3D&se=2017-12-06T12%3A38%3A41Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/86783892620591.jpg', 1, 8, CAST(N'2016-12-06 12:38:43.593' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (224, N'Deadlift side view.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-e734669d-dc3c-4625-8215-6e8c076c5a81/fn1624696467.mp4?sv=2012-02-12&sr=c&si=4b2b2c80-b75d-4212-b848-2e373468ad1f&sig=ThyySiyzfDpf1W0EGvsoMgydO4ZDUOcMvSP7i24h2Gg%3D&se=2017-12-06T12%3A38%3A43Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/32479618039779.jpg', 1, 8, CAST(N'2016-12-06 12:38:45.860' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (225, N'Instruction Front View.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1415c0cd-b8ba-470b-b202-836562fa1e5e/fn1648204310.mp4?sv=2012-02-12&sr=c&si=8cef8776-94c5-46ba-8724-d60659c0a25b&sig=aRCpQqgpC76AsG1ps2Z93t440YL0IADdArT64J8MOqg%3D&se=2017-12-06T12%3A38%3A46Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/79592322074962.jpg', 1, 8, CAST(N'2016-12-06 12:38:48.830' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (226, N'Intro Video - Deadlift Module.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b7ff61a0-32c9-462a-9d73-7a29d2d72382/fn1821630424.mp4?sv=2012-02-12&sr=c&si=59ac51f7-ab39-4fab-b852-b5a49408f9c0&sig=WFqwe3DKula5JK9tn3g8lnizQIncaZystylzL33bTro%3D&se=2017-12-06T12%3A38%3A49Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/43067122170017.jpg', 1, 8, CAST(N'2016-12-06 12:38:51.923' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (227, N'Pre- Side view deadlift Instructions.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-836ce3f3-4112-4741-bd3d-d2c6c789722e/fn147241320.mp4?sv=2012-02-12&sr=c&si=66ba8850-f1bb-4177-80e6-2ab34350957f&sig=hjjGkG1RE0uKtQg6OS44BTM3jyGklAmUmJM9hyjD3Z8%3D&se=2017-12-06T12%3A38%3A52Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/15262417916536.jpg', 1, 8, CAST(N'2016-12-06 12:38:54.830' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (228, N'Pre-FrontView deadlift - Instructions.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-65d96c62-3997-4413-a6cb-177c27cf8315/fn859311079.mp4?sv=2012-02-12&sr=c&si=b58fa614-5ad4-42a5-ae0a-948d9327b28c&sig=7y4hOsoaqSJvPTDfX8FiQt7qypqE%2FjrCGsRrEylqcT0%3D&se=2017-12-06T12%3A38%3A55Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/17774125830786.jpg', 1, 8, CAST(N'2016-12-06 12:38:57.957' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (229, N'Post Skill Front View.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-d411fa49-cdb8-423f-b6d9-f3a19e42b53e/fn621607384.mp4?sv=2012-02-12&sr=c&si=1a0b68b4-83f9-4ca9-b93f-21066e0782c4&sig=1hPyPcEpHePIYwBoDcuLTnyJIuMK31YyL01AaWpRqoI%3D&se=2017-12-06T12%3A45%3A54Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/9002078759527.jpg', 1, 8, CAST(N'2016-12-06 12:45:57.703' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (230, N'Deadlift Technique', N'Package Intro Video', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-5aeb8c2b-fea1-4b71-af9c-12032702e01b/fn371306013.mp4?sv=2012-02-12&sr=c&si=17ea69e9-3ff0-4e57-ac27-9414c4e6f955&sig=05ymvOun36YM5cOqSeMVlUy7C3iQG1pQy8Y6wQXBvaQ%3D&se=2017-12-06T12%3A49%3A52Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/15985161267690.jpg', 1, 8, CAST(N'2016-12-06 12:49:55.543' AS DateTime), NULL, NULL, 30, NULL, NULL, 3, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (231, N'Deadlift Front View.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-20fddea4-86e6-4206-b4a2-37444c9290f3/fn844842562.mp4?sv=2012-02-12&sr=c&si=e6cd4b55-4798-4390-9f97-fb70c947a013&sig=wmsjob4VBG0UNbaqkEwl4w4w6xhmE3hc772GN8sfGP8%3D&se=2017-12-06T12%3A38%3A41Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/86783892620591.jpg', 1, 8, CAST(N'2016-12-06 12:49:55.730' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1415c0cd-b8ba-470b-b202-836562fa1e5e/fn1648204310.mp4?sv=2012-02-12&sr=c&si=8cef8776-94c5-46ba-8724-d60659c0a25b&sig=aRCpQqgpC76AsG1ps2Z93t440YL0IADdArT64J8MOqg%3D&se=2017-12-06T12%3A38%3A46Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-41cdc93a-2d8a-4edd-bc09-87d12c1042be/fn214738815.mp4?sv=2012-02-12&sr=c&si=d86fcd10-f21b-4a90-a20d-4f8eef96e4cc&sig=csIqdj6Dv%2B%2Bk4HK0AEEnrEajDhEwVdBakmfbVEBUuCQ%3D&se=2017-12-06T12%3A38%3A38Z', 4, 1, 225, 222, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (232, N'Deadlift side view.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-e734669d-dc3c-4625-8215-6e8c076c5a81/fn1624696467.mp4?sv=2012-02-12&sr=c&si=4b2b2c80-b75d-4212-b848-2e373468ad1f&sig=ThyySiyzfDpf1W0EGvsoMgydO4ZDUOcMvSP7i24h2Gg%3D&se=2017-12-06T12%3A38%3A43Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/32479618039779.jpg', 1, 8, CAST(N'2016-12-06 12:49:55.780' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-836ce3f3-4112-4741-bd3d-d2c6c789722e/fn147241320.mp4?sv=2012-02-12&sr=c&si=66ba8850-f1bb-4177-80e6-2ab34350957f&sig=hjjGkG1RE0uKtQg6OS44BTM3jyGklAmUmJM9hyjD3Z8%3D&se=2017-12-06T12%3A38%3A52Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-d411fa49-cdb8-423f-b6d9-f3a19e42b53e/fn621607384.mp4?sv=2012-02-12&sr=c&si=1a0b68b4-83f9-4ca9-b93f-21066e0782c4&sig=1hPyPcEpHePIYwBoDcuLTnyJIuMK31YyL01AaWpRqoI%3D&se=2017-12-06T12%3A45%3A54Z', 4, 1, 227, 229, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (239, N'breaststroke kick ', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-3634820c-0de4-494f-8093-da08bb56aae6/fn1829489125.mp4?sv=2012-02-12&sr=c&si=45aef349-d8d2-4eaf-981e-fe6a3342ec41&sig=WlhlmfqYElmwYsrmRvsrmQCgDarQhTEYyWNr%2FWDTI6g%3D&se=2017-12-13T10%3A28%3A33Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/22399051691889.jpg', 1, 4, CAST(N'2016-12-13 11:04:07.810' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f01f6ae0-e6da-46d8-ab38-125c9b2e8dcf/fn1580677574.mp4?sv=2012-02-12&sr=c&si=14ca1a04-5db4-4b64-a3b8-6d6e3f607710&sig=DzGurDbdZowvWRTB3OS7vrJ%2B05rkwmp6fjoiOXMEFIA%3D&se=2017-11-12T03%3A31%3A34Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-c0a10f8f-c15e-4b52-8ea8-322f0bd3a7a9/fn2042497747.mp4?sv=2012-02-12&sr=c&si=cc42f845-05df-447b-b5a6-76e46dd2ded3&sig=24I5aWRe8TmhPmqie3gSVei%2Ba%2BDrfgDZZM7bWiZcAh8%3D&se=2017-11-12T03%3A31%3A46Z', 4, 1, 14, 17, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (240, N'broad jump-HD.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-87d5a8da-6f04-4d6e-a090-302fee214091/fn1101989536.mp4?sv=2012-02-12&sr=c&si=b9824a79-e084-41ca-bdad-bf67a0d09aa2&sig=ghTuEYINw8TqP7i6tpCYZgc8hc2lEqibYacNEntKbJ4%3D&se=2017-12-13T10%3A31%3A32Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/77492575174384.jpg', 1, 4, CAST(N'2016-12-13 11:04:07.857' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 1, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (241, N'breaststroke kick above_sum.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-8a33e3eb-e4f2-4fe2-8cbf-c19e7f74a2cd/fn935654355.mp4?sv=2012-02-12&sr=c&si=337e9518-cb41-41c2-b56f-32538500aa5e&sig=GaL36TdJ2TP%2F8XAlPyeEWMlgE4Fz81WbGK5HPpmpvGo%3D&se=2017-12-13T10%3A31%3A29Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/321270196535.jpg', 1, 4, CAST(N'2016-12-13 11:04:07.903' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 1, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (242, N'broad jump', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-9cebc892-a708-459b-97d2-1f098ad9a07b/fn167165381.mp4?sv=2012-02-12&sr=c&si=c8e0ddad-dd58-44f2-b25e-98bcddc882e3&sig=Ivebb3ZGdV6pSSPsSDa6HgGxwa40hhGUNc0EfQj4lgA%3D&se=2017-12-13T10%3A28%3A45Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/21424086311075.jpg', 1, 4, CAST(N'2016-12-13 11:04:07.967' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 0, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (243, N'test', N'Package', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b2c8291f-9267-4e2c-b42e-14b29a77c099/fn2090005107.mp4?sv=2012-02-12&sr=c&si=2a9df9c6-123d-44bd-9417-a0d97976dc0b&sig=8ZCGFPe22BW7Z%2Fjq69cyva2eU07VSoYBLWC%2FdgI%2FxrA%3D&se=2017-12-13T12%3A04%3A09Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/2110089185114.jpg', 1, 0, CAST(N'2016-12-13 11:57:18.063' AS DateTime), 0, CAST(N'2016-12-13 12:04:14.110' AS DateTime), 30, NULL, NULL, 3, NULL, NULL, NULL, 0)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (246, N'test', N'Package Intro Video', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-785c16c0-d0ea-4d1c-a0d2-793263b0831c/fn893162954.mp4?sv=2012-02-12&sr=c&si=de5f5e0e-453a-4942-9a48-f674b6504ad1&sig=%2BQLi1Wi7ahqGi61DGZjucQ6ciQxm9sAcKtGZKXvN7yM%3D&se=2017-12-13T12%3A33%3A18Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/95032366821048.jpg', 1, 4, CAST(N'2016-12-13 12:30:10.220' AS DateTime), 0, CAST(N'2016-12-13 12:33:24.163' AS DateTime), 30, NULL, NULL, 3, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (247, N'back above_sum.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b354afa9-d3ef-4e8c-bcf0-48592930c746/fn941555941.mp4?sv=2012-02-12&sr=c&si=2f2fa876-1b7c-48bb-b54f-3e513442755a&sig=Dcrr0IhtsgLHPT0KYuD6YLjZ9RxhLZ5Bdy2io%2Fu3MpU%3D&se=2017-12-13T12%3A29%3A05Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/6205783638166.jpg', 1, 4, CAST(N'2016-12-13 12:30:10.267' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 1, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (248, N'breaststroke kick above_sum(1).mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f45cd001-e69a-439e-b01a-a202b6312488/fn1370206068.mp4?sv=2012-02-12&sr=c&si=f9c72fc9-77e5-4601-9794-32310800a15e&sig=0ENxu2qgrJD%2FQ9PkJz4MXpHKchRZ8k5F4x9hv%2B0TIEE%3D&se=2017-12-13T12%3A29%3A08Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/4679835514152.jpg', 1, 4, CAST(N'2016-12-13 12:30:10.313' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 1, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (250, N'breaststroke ', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-8a6c327f-6d9a-4fb1-a551-8e90e7d6aa58/fn2105822446.mp4?sv=2012-02-12&sr=c&si=66cab3df-bd26-4196-ac8a-5f5fd7276fa3&sig=2QGZjVfVCblh7MSPxKhb628qm6XzHWdsjIhRGaLgoVE%3D&se=2017-12-13T12%3A30%3A30Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/34374754736816.jpg', 1, 0, CAST(N'2016-12-13 12:32:01.290' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 0, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (251, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-5894823f-4f62-47ff-b2b2-9c61f6c11be4/fn1963170212.mp4?sv=2012-02-12&sr=c&si=76257970-71e0-4408-8aa8-9aca0de874de&sig=d7tKDOkSrEL2lI95JKaLw8dLMwPNu3pXmUquHg5SrAo%3D&se=2017-12-14T19%3A37%3A22Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/55627483180806.jpg', 1, 20, CAST(N'2016-12-14 19:37:32.013' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (252, N'Vertical Jump ', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-706d001a-1a7c-4197-bb2a-702de790e7b3/fn770297334.mp4?sv=2012-02-12&sr=c&si=0f065a69-cfac-4433-aaca-72c54fecac88&sig=2hLg3tjMkUZ9mFhPpnznuScaIFrJqZgZRaMg4m%2BnRiM%3D&se=2017-12-15T09%3A54%3A59Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/2542316884935.jpg', 1, 4, CAST(N'2016-12-15 09:55:12.157' AS DateTime), 4, CAST(N'2016-12-15 09:55:48.697' AS DateTime), 30, NULL, NULL, 1, 0, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (253, N'Vertical Jump - BlastMotion-HD.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ee44016e-4c21-4244-b266-2cae41c73831/fn307606409.mp4?sv=2012-02-12&sr=c&si=4c3ab338-e98d-4529-b4c9-42c7ad4a359d&sig=Ar984R6moshsfAbccvkKSgw84QMNn92K5bXz17xs6ag%3D&se=2017-12-15T09%3A55%3A28Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/70817648985772.jpg', 1, 4, CAST(N'2016-12-15 09:55:31.957' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (254, N'Vertical Jump ', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-706d001a-1a7c-4197-bb2a-702de790e7b3/fn770297334.mp4?sv=2012-02-12&sr=c&si=0f065a69-cfac-4433-aaca-72c54fecac88&sig=2hLg3tjMkUZ9mFhPpnznuScaIFrJqZgZRaMg4m%2BnRiM%3D&se=2017-12-15T09%3A54%3A59Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/2542316884935.jpg', 1, 4, CAST(N'2016-12-15 09:56:41.880' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 0, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (255, N'Vertical Jump - BlastMotion-HD.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ee44016e-4c21-4244-b266-2cae41c73831/fn307606409.mp4?sv=2012-02-12&sr=c&si=4c3ab338-e98d-4529-b4c9-42c7ad4a359d&sig=Ar984R6moshsfAbccvkKSgw84QMNn92K5bXz17xs6ag%3D&se=2017-12-15T09%3A55%3A28Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/70817648985772.jpg', 1, 4, CAST(N'2016-12-15 09:56:41.943' AS DateTime), NULL, NULL, 30, NULL, NULL, 4, 1, 0, 0, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (256, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-c04d2c99-b9a0-4817-9ed2-bdedcc014b47/fn1461313787.mp4?sv=2012-02-12&sr=c&si=460de35d-0744-41d5-b888-a340fcb82ff0&sig=3U3D0VNGKbCGBMHQtCpy5mSWHeHte7jcphWE0hCAwIc%3D&se=2017-12-19T14%3A37%3A28Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/6108773244860.jpg', 1, 22, CAST(N'2016-12-19 14:37:39.640' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (257, N'Channel Intro Video', N'Channel', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4f5c77b7-bb92-445f-881c-d9a4256d4c0c/fn861353801.mp4?sv=2012-02-12&sr=c&si=62532727-6bdb-4cf0-9a0e-6131a4c7c73c&sig=lBZfVSyQYhQVoBv9fW2f5AmAq95uAkYD6sxBqBkkknA%3D&se=2017-12-19T14%3A53%3A55Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/5016454313269.jpg', 1, 24, CAST(N'2016-12-19 14:51:54.033' AS DateTime), NULL, NULL, 1, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (258, N'Broad Jump-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b3b5ce92-4db4-43fe-8455-0b38f2391633/fn465739314.mp4?sv=2012-02-12&sr=c&si=0093c973-54d9-48fa-946b-6c30774e4b05&sig=Hit8OuP2BIVcG7xJXFuRewQS14hMxS%2FudMPyx9M959o%3D&se=2017-12-19T22%3A53%3A03Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/96514339627216.jpg', 1, 24, CAST(N'2016-12-19 22:53:13.007' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (259, N'Broad Jump-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-35d7c181-2146-4203-b5c3-7d52538ec9b4/fn1227227916.mp4?sv=2012-02-12&sr=c&si=7e33b8c8-f494-4487-969c-2ecbd20dba28&sig=5EaH2fC5FwFn%2FAX8ZvvCIN7LL3oZ8b1Yy3RxjOdUa5Y%3D&se=2017-12-19T22%3A53%3A14Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/9824057699601.jpg', 1, 24, CAST(N'2016-12-19 22:53:16.120' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (260, N'Front Squat-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-522db8e9-eacc-40f1-851f-dfc6f40c82ad/fn2119478496.mp4?sv=2012-02-12&sr=c&si=7e90071e-c9c0-4885-8a38-8ce2ada0ecff&sig=ZP8MfvJjaS6vM%2FfzbidUWSITmm%2FoFw8rqYE47wSacHA%3D&se=2017-12-19T22%3A53%3A16Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/89000320463850.jpg', 1, 24, CAST(N'2016-12-19 22:53:19.790' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
GO
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (261, N'Front Squat-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-80011210-95c0-43b4-bf88-06abbc6838dd/fn714495271.mp4?sv=2012-02-12&sr=c&si=02ce8d0f-f534-49fc-a58f-6f761dceb1a1&sig=evlgSKfHC0hiX1mFio8ZfrneRQRL45XolTC%2FOnvd5V8%3D&se=2017-12-19T22%3A53%3A20Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/66429535066766.jpg', 1, 24, CAST(N'2016-12-19 22:53:22.880' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (262, N'Lunge-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-d89560a6-5e8b-47e2-b33e-089013c4cd75/fn768433777.mp4?sv=2012-02-12&sr=c&si=1ea8cd6d-d342-4983-8349-b66d32cf65fe&sig=vRBdluv7QXxJI%2F21rfQZlHox5oWqJAd%2B5%2FHNCAscO7E%3D&se=2017-12-19T22%3A53%3A23Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/40372077974617.jpg', 1, 24, CAST(N'2016-12-19 22:53:26.333' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (263, N'Lunge-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1d316f9c-479f-49df-96ff-d70b4ffeff1c/fn1862306090.mp4?sv=2012-02-12&sr=c&si=a6a8cd2b-24b9-45d3-8e65-b8457df4d541&sig=m6HWrtNcEfrBBwy909l9sYspBCqE6pkD%2FZRjwfZecZU%3D&se=2017-12-19T22%3A53%3A27Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/35572449425798.jpg', 1, 24, CAST(N'2016-12-19 22:53:29.067' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (264, N'Overhead Squat-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-db338b10-03e8-468d-a1c0-3580b65155b5/fn689039035.mp4?sv=2012-02-12&sr=c&si=a939e092-86ed-4544-be5b-8ca35befa6bd&sig=adKefZN1cpnekr7bZbF0JJG%2FX04pwF9ZF23%2Fk%2F3r0nE%3D&se=2017-12-19T22%3A53%3A29Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/38414325585387.jpg', 1, 24, CAST(N'2016-12-19 22:53:31.783' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (265, N'Overhead Squat-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f74b6772-a32a-4a5b-a48e-496d94821869/fn2127024193.mp4?sv=2012-02-12&sr=c&si=a8ac93bf-b60e-4c32-896a-f3995da9e390&sig=zgpnmTRHk4wnDejqWxuYP6tY38c80BwcQUq1BuHEG6c%3D&se=2017-12-19T22%3A53%3A34Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/24571955367367.jpg', 1, 24, CAST(N'2016-12-19 22:53:36.127' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (266, N'Push Up-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4f7a7f3d-e04d-4516-97ff-dbbfe3f6ef2c/fn871959239.mp4?sv=2012-02-12&sr=c&si=51e9b699-316f-4e97-9dc3-57b998fc7c6b&sig=NsZUAsspVF00asF21x8qbICuYi8jN0MnDMcH4CR9PxE%3D&se=2017-12-19T22%3A53%3A37Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/41368146134817.jpg', 1, 24, CAST(N'2016-12-19 22:53:38.970' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (267, N'Push Up-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-0d47a460-f907-4a4b-8fe0-12ef9b0f2465/fn1000772824.mp4?sv=2012-02-12&sr=c&si=d1e14fe2-3a36-48be-969f-ae17c2f55da9&sig=JVD3OlbhbC5Ywr6fyOwy5Vw%2BqSwTkodNb5bGNvdvXJc%3D&se=2017-12-19T22%3A53%3A40Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/49443942450805.jpg', 1, 24, CAST(N'2016-12-19 22:53:41.950' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (268, N'Squat Jump-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1ca02834-e81b-41d8-9c99-4d3d83558d25/fn276960922.mp4?sv=2012-02-12&sr=c&si=4bccbc36-8d49-487e-a6a9-618ef5e57d61&sig=bGG6a4wzSvl6rgTHmAM3WB52hSzRxpuS3jA%2BnANYjG4%3D&se=2017-12-19T22%3A58%3A38Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/93160161812537.jpg', 1, 24, CAST(N'2016-12-19 22:58:40.373' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (269, N'Squat Jump-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ad1bb0e5-bc56-40e7-a925-ea05f4099716/fn541679025.mp4?sv=2012-02-12&sr=c&si=d2ecb6d0-b31d-47f5-b02e-0ddecc1f82da&sig=rbjKkm2%2B4jXacV4kClEwU%2BkHlNwRGbHKPWE3BtIf8yQ%3D&se=2017-12-19T22%3A58%3A41Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/22831877844795.jpg', 1, 24, CAST(N'2016-12-19 22:58:43.200' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (270, N'Squat-Back.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4ea555e1-f60d-486d-8b68-d4c4c4ae5408/fn1253748784.mp4?sv=2012-02-12&sr=c&si=f4c44c17-4486-4dfc-b8e1-570d70c86b9f&sig=TXvdTgRrtSM4YrZD8mLNqSwVsS4byfo2fcOu7gbCizA%3D&se=2017-12-19T22%3A58%3A44Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/78995242376779.jpg', 1, 24, CAST(N'2016-12-19 22:58:46.027' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (271, N'Squat-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-a8f9b419-d987-4776-b63d-ea1e9932cf79/fn125094258.mp4?sv=2012-02-12&sr=c&si=6c940f62-8f0e-4cf6-84cd-c4eb87151e00&sig=3DlaYCl98hHPYHd5WcyV1N2eUQvFGKal9kMPdQvvlgM%3D&se=2017-12-19T22%3A58%3A47Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/54677211318362.jpg', 1, 24, CAST(N'2016-12-19 22:58:48.590' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (272, N'Squat-Rear.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-635672a3-4aab-4ad0-a07f-ed50c9c77e85/fn1869838778.mp4?sv=2012-02-12&sr=c&si=e9494b9b-6bed-42b9-9f76-7e827f7cc91a&sig=6%2BioIrEFYmusbxiuZXZr1%2BB6e3XJUUP0FqHTSR2pK%2BY%3D&se=2017-12-19T22%3A58%3A49Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/48798694926489.jpg', 1, 24, CAST(N'2016-12-19 22:58:51.370' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (273, N'Squat-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-6061e9e0-f225-430c-a841-57fc0692fb15/fn1998652363.mp4?sv=2012-02-12&sr=c&si=2e065c92-81f5-4248-94ec-32cd75618054&sig=f%2FtwO482dH9oOrGbKbhUMu2%2FrNhxEi4Cjc%2BrpklQkQA%3D&se=2017-12-19T22%3A58%3A52Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/60361064137541.jpg', 1, 24, CAST(N'2016-12-19 22:58:54.053' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (274, N'Squat- Feet (Post).mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ce827723-db9c-4fbb-8d24-e2b2048dc76a/fn872010773.mp4?sv=2012-02-12&sr=c&si=613b08cd-af7c-46de-9692-30a55e2ae4b9&sig=4dw3nXpbfy2uRTEaZIeEHtNU0uFNMf3Jd6K3d%2FuXXuw%3D&se=2017-12-19T23%3A06%3A28Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/93724971745699.jpg', 1, 24, CAST(N'2016-12-19 23:06:30.080' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (275, N'Squat-Chest:Back (Post).mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-5242e231-5a7d-4031-8ec6-e45f51ea4c11/fn1631096218.mp4?sv=2012-02-12&sr=c&si=8458152d-c962-4ac1-8508-b04b7d881a47&sig=P5genajhUL%2BoElb4YMKvsXrud7VIgcomtB2mxi6k4gA%3D&se=2017-12-19T23%3A06%3A32Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/53052761755011.jpg', 1, 24, CAST(N'2016-12-19 23:06:33.910' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (276, N'Squat-Chest:Back (Pre).mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-57f20bf9-341f-4c04-b90f-d8f401f88536/fn99534452.mp4?sv=2012-02-12&sr=c&si=bd1da696-072d-423a-82e9-74b7f12d8b26&sig=3PsqS%2FrPi6OwyNawMNOyLwHx%2Bu%2Fp1lA7U06g60TUnKM%3D&se=2017-12-19T23%3A06%3A36Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/71273850166057.jpg', 1, 24, CAST(N'2016-12-19 23:06:38.750' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (277, N'Squat-Feet (Pre).mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-a98f6bb8-ba85-4ceb-8f11-7e59334b1f34/fn797590458.mp4?sv=2012-02-12&sr=c&si=62fa880f-ab92-466e-8694-48e282a8950a&sig=TTY4tnpm9yZqAQgz5Wl5g%2FgH3UykLIavN2YyzQKqeEQ%3D&se=2017-12-19T23%3A06%3A43Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/477281388158.jpg', 1, 24, CAST(N'2016-12-19 23:06:45.340' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (278, N'Squat-Knees (Post).mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-a24cd9c6-6172-463f-b033-0500cd30f8f7/fn2020276356.mp4?sv=2012-02-12&sr=c&si=a5c52461-c42d-49ee-8ecd-2dd823a6dd94&sig=G%2BflMI7v%2FnQb%2B8qoq4QpsCt7nDtFfoq%2FKKBAOirVC9k%3D&se=2017-12-19T23%3A06%3A48Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/58637953936134.jpg', 1, 24, CAST(N'2016-12-19 23:06:49.840' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (279, N'Squat-Pre (knees).mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-d4b79710-afb2-4c77-8015-c615d8431d2c/fn1296764114.mp4?sv=2012-02-12&sr=c&si=9bf4c979-2a1a-4d66-8bfd-387b12619c06&sig=Epmsl6kOCDeKewv13fsL8%2FrLcFk7DVwkS5RchWaT9yQ%3D&se=2017-12-19T23%3A06%3A52Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/57340639437582.jpg', 1, 24, CAST(N'2016-12-19 23:06:54.620' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (280, N'Squat-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-a8f9b419-d987-4776-b63d-ea1e9932cf79/fn125094258.mp4?sv=2012-02-12&sr=c&si=6c940f62-8f0e-4cf6-84cd-c4eb87151e00&sig=3DlaYCl98hHPYHd5WcyV1N2eUQvFGKal9kMPdQvvlgM%3D&se=2017-12-19T22%3A58%3A47Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/54677211318362.jpg', 1, 24, CAST(N'2016-12-19 23:23:01.180' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-a98f6bb8-ba85-4ceb-8f11-7e59334b1f34/fn797590458.mp4?sv=2012-02-12&sr=c&si=62fa880f-ab92-466e-8694-48e282a8950a&sig=TTY4tnpm9yZqAQgz5Wl5g%2FgH3UykLIavN2YyzQKqeEQ%3D&se=2017-12-19T23%3A06%3A43Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-ce827723-db9c-4fbb-8d24-e2b2048dc76a/fn872010773.mp4?sv=2012-02-12&sr=c&si=613b08cd-af7c-46de-9692-30a55e2ae4b9&sig=4dw3nXpbfy2uRTEaZIeEHtNU0uFNMf3Jd6K3d%2FuXXuw%3D&se=2017-12-19T23%3A06%3A28Z', 4, 1, 277, 274, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (281, N'Squat-Rear.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-635672a3-4aab-4ad0-a07f-ed50c9c77e85/fn1869838778.mp4?sv=2012-02-12&sr=c&si=e9494b9b-6bed-42b9-9f76-7e827f7cc91a&sig=6%2BioIrEFYmusbxiuZXZr1%2BB6e3XJUUP0FqHTSR2pK%2BY%3D&se=2017-12-19T22%3A58%3A49Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/48798694926489.jpg', 1, 24, CAST(N'2016-12-19 23:23:01.583' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-d4b79710-afb2-4c77-8015-c615d8431d2c/fn1296764114.mp4?sv=2012-02-12&sr=c&si=9bf4c979-2a1a-4d66-8bfd-387b12619c06&sig=Epmsl6kOCDeKewv13fsL8%2FrLcFk7DVwkS5RchWaT9yQ%3D&se=2017-12-19T23%3A06%3A52Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-a24cd9c6-6172-463f-b033-0500cd30f8f7/fn2020276356.mp4?sv=2012-02-12&sr=c&si=a5c52461-c42d-49ee-8ecd-2dd823a6dd94&sig=G%2BflMI7v%2FnQb%2B8qoq4QpsCt7nDtFfoq%2FKKBAOirVC9k%3D&se=2017-12-19T23%3A06%3A48Z', 4, 1, 279, 278, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (282, N'Squat-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-6061e9e0-f225-430c-a841-57fc0692fb15/fn1998652363.mp4?sv=2012-02-12&sr=c&si=2e065c92-81f5-4248-94ec-32cd75618054&sig=f%2FtwO482dH9oOrGbKbhUMu2%2FrNhxEi4Cjc%2BrpklQkQA%3D&se=2017-12-19T22%3A58%3A52Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/60361064137541.jpg', 1, 24, CAST(N'2016-12-19 23:23:01.617' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-57f20bf9-341f-4c04-b90f-d8f401f88536/fn99534452.mp4?sv=2012-02-12&sr=c&si=bd1da696-072d-423a-82e9-74b7f12d8b26&sig=3PsqS%2FrPi6OwyNawMNOyLwHx%2Bu%2Fp1lA7U06g60TUnKM%3D&se=2017-12-19T23%3A06%3A36Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-5242e231-5a7d-4031-8ec6-e45f51ea4c11/fn1631096218.mp4?sv=2012-02-12&sr=c&si=8458152d-c962-4ac1-8508-b04b7d881a47&sig=P5genajhUL%2BoElb4YMKvsXrud7VIgcomtB2mxi6k4gA%3D&se=2017-12-19T23%3A06%3A32Z', 4, 1, 276, 275, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (283, N'Front Squat (Post)-Chest .mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4a460c0c-0c10-4f06-a45e-c02c8fe03dae/fn244449725.mp4?sv=2012-02-12&sr=c&si=17923503-3232-4599-bc32-f037088f7fab&sig=qFpT%2F9lskV60u4GhUA0PPEpB8UEmEufBWkeCbeonRjw%3D&se=2017-12-20T16%3A21%3A09Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/74106680835637.jpg', 1, 24, CAST(N'2016-12-20 16:21:21.817' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (284, N'Front Squat (Post)-Elbows.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-0d93f712-2357-4858-a190-17f93166acd4/fn783548134.mp4?sv=2012-02-12&sr=c&si=be3541be-3c50-43ba-b884-8717aac58efb&sig=pAVDDcoN63XS8xowCwFdC7JQhrXNgM6mzAHqJwS3maA%3D&se=2017-12-20T16%3A21%3A24Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/49474022487083.jpg', 1, 24, CAST(N'2016-12-20 16:21:27.177' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (285, N'Front Squat (Post)-Feet.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-51d2b0e2-c8fd-4a7f-baaa-f82801b5ab1c/fn1825885098.mp4?sv=2012-02-12&sr=c&si=e1c8320c-64e8-4f8c-9f85-59378c16f4f7&sig=4gPW9UxL7IjlfL2KKwddQWk76k%2BC0CN5gxgUyelGYdw%3D&se=2017-12-20T16%3A21%3A28Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/81971974023728.jpg', 1, 24, CAST(N'2016-12-20 16:21:30.723' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (286, N'Front Squat (Pre)-Chest .mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-46a221d2-25cf-433a-8051-38cc5374b555/fn420901873.mp4?sv=2012-02-12&sr=c&si=ad818c46-c4cd-489d-a242-a465a8aaf7e5&sig=ScEMBXpXeCc%2BMHp2UTYrJ6yA%2BYyQXi5bqIfqizFybRU%3D&se=2017-12-20T16%3A21%3A33Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/249455856769.jpg', 1, 24, CAST(N'2016-12-20 16:21:35.800' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (287, N'Front Squat (Pre)-Elbows.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-bd950ef1-e73d-455d-b175-fc4dbbc32175/fn992379338.mp4?sv=2012-02-12&sr=c&si=bb3b2dce-956f-4785-88c5-19c16aae7f2a&sig=r09%2FNvFARrqywztJcyqQvenfWTQ2Z8rfdWkZjX49E94%3D&se=2017-12-20T16%3A21%3A37Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/11103015482749.jpg', 1, 24, CAST(N'2016-12-20 16:21:40.097' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (288, N'Front Squat (Pre)-Feet.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-59d28283-e9ff-4210-b255-631675baa288/fn695114066.mp4?sv=2012-02-12&sr=c&si=30a18181-9fb4-4aef-b41e-2cd9fd9f2def&sig=3rzzv8b%2F09ZRWsyp%2B4oKdsTuC%2BSL1%2Bs78cyl5jvrW84%3D&se=2017-12-20T16%3A21%3A43Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/755039587395.jpg', 1, 24, CAST(N'2016-12-20 16:21:45.347' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (289, N'Front Squat-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-29be844f-7948-4b31-bd37-7bea98434e7c/fn1959347432.mp4?sv=2012-02-12&sr=c&si=1964cfea-ba4c-4d09-a84e-fdd7e99d1d3b&sig=UGqAL4dCoEag%2BgJ3JfE4IyEajXH%2B1Pq3pHeqElvpJE0%3D&se=2017-12-20T16%3A36%3A47Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/16763811056020.jpg', 1, 24, CAST(N'2016-12-20 16:36:49.093' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (290, N'Front Squat-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-522db8e9-eacc-40f1-851f-dfc6f40c82ad/fn2119478496.mp4?sv=2012-02-12&sr=c&si=7e90071e-c9c0-4885-8a38-8ce2ada0ecff&sig=ZP8MfvJjaS6vM%2FfzbidUWSITmm%2FoFw8rqYE47wSacHA%3D&se=2017-12-19T22%3A53%3A16Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/89000320463850.jpg', 1, 24, CAST(N'2016-12-20 16:39:14.503' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-59d28283-e9ff-4210-b255-631675baa288/fn695114066.mp4?sv=2012-02-12&sr=c&si=30a18181-9fb4-4aef-b41e-2cd9fd9f2def&sig=3rzzv8b%2F09ZRWsyp%2B4oKdsTuC%2BSL1%2Bs78cyl5jvrW84%3D&se=2017-12-20T16%3A21%3A43Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-51d2b0e2-c8fd-4a7f-baaa-f82801b5ab1c/fn1825885098.mp4?sv=2012-02-12&sr=c&si=e1c8320c-64e8-4f8c-9f85-59378c16f4f7&sig=4gPW9UxL7IjlfL2KKwddQWk76k%2BC0CN5gxgUyelGYdw%3D&se=2017-12-20T16%3A21%3A28Z', 4, 1, 288, 285, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (291, N'Front Squat-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-29be844f-7948-4b31-bd37-7bea98434e7c/fn1959347432.mp4?sv=2012-02-12&sr=c&si=1964cfea-ba4c-4d09-a84e-fdd7e99d1d3b&sig=UGqAL4dCoEag%2BgJ3JfE4IyEajXH%2B1Pq3pHeqElvpJE0%3D&se=2017-12-20T16%3A36%3A47Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/16763811056020.jpg', 1, 24, CAST(N'2016-12-20 16:39:14.583' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-46a221d2-25cf-433a-8051-38cc5374b555/fn420901873.mp4?sv=2012-02-12&sr=c&si=ad818c46-c4cd-489d-a242-a465a8aaf7e5&sig=ScEMBXpXeCc%2BMHp2UTYrJ6yA%2BYyQXi5bqIfqizFybRU%3D&se=2017-12-20T16%3A21%3A33Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-4a460c0c-0c10-4f06-a45e-c02c8fe03dae/fn244449725.mp4?sv=2012-02-12&sr=c&si=17923503-3232-4599-bc32-f037088f7fab&sig=qFpT%2F9lskV60u4GhUA0PPEpB8UEmEufBWkeCbeonRjw%3D&se=2017-12-20T16%3A21%3A09Z', 4, 1, 286, 283, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (292, N'Front Squat-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-80011210-95c0-43b4-bf88-06abbc6838dd/fn714495271.mp4?sv=2012-02-12&sr=c&si=02ce8d0f-f534-49fc-a58f-6f761dceb1a1&sig=evlgSKfHC0hiX1mFio8ZfrneRQRL45XolTC%2FOnvd5V8%3D&se=2017-12-19T22%3A53%3A20Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/66429535066766.jpg', 1, 24, CAST(N'2016-12-20 16:39:14.693' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-bd950ef1-e73d-455d-b175-fc4dbbc32175/fn992379338.mp4?sv=2012-02-12&sr=c&si=bb3b2dce-956f-4785-88c5-19c16aae7f2a&sig=r09%2FNvFARrqywztJcyqQvenfWTQ2Z8rfdWkZjX49E94%3D&se=2017-12-20T16%3A21%3A37Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-0d93f712-2357-4858-a190-17f93166acd4/fn783548134.mp4?sv=2012-02-12&sr=c&si=be3541be-3c50-43ba-b884-8717aac58efb&sig=pAVDDcoN63XS8xowCwFdC7JQhrXNgM6mzAHqJwS3maA%3D&se=2017-12-20T16%3A21%3A24Z', 4, 1, 287, 284, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (293, N'Broad Jump (Post)-Countermovement.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-2ac24a1e-613f-467a-8fcf-7371b28db6b2/fn1462445135.mp4?sv=2012-02-12&sr=c&si=5a7cacd3-bb1e-4a78-aafb-0f32bd21fffd&sig=cypI9X8m16tSUWyy0dic6fcjbQR%2BmtcVuTK2KLCWO%2FY%3D&se=2017-12-20T16%3A58%3A11Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/20132464216390.jpg', 1, 24, CAST(N'2016-12-20 16:58:13.770' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (294, N'Broad Jump (Post)-Jump.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-a5ecb578-869c-47b8-9d25-043f0cfc8ff8/fn1727331351.mp4?sv=2012-02-12&sr=c&si=bb16a778-59cb-4a86-80d7-90d73e7264c1&sig=XDPDKohx4eBcxcUCTasg8DxMUyC%2Fpc%2BCsHc3xhHkB6Q%3D&se=2017-12-20T16%3A58%3A16Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/63105977293430.jpg', 1, 24, CAST(N'2016-12-20 16:58:18.803' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (295, N'Broad Jump (post)-landing.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-48b31a6c-8f17-4aff-8aa2-0e4fb4c5fb42/fn83541023.mp4?sv=2012-02-12&sr=c&si=e3fd13cf-65ad-4dd6-9c7c-3262aa17476e&sig=knvjDwS1ShK%2FB4PKxtmuApUUY5SlqxA6CYvd69GXx2M%3D&se=2017-12-20T16%3A58%3A21Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/433224309140.jpg', 1, 24, CAST(N'2016-12-20 16:58:23.410' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (296, N'Broad Jump (Pre)-Counter Movement.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b6df206f-8cba-422f-a0cb-639573f79da2/fn781597029.mp4?sv=2012-02-12&sr=c&si=4168c5e0-7c2d-42fd-91a0-5ba40210dbce&sig=yGZsPiExjZd6fFoBkSHPCWv4o4EnV6JfZwrRc3GijO4%3D&se=2017-12-20T16%3A58%3A26Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/82856087510312.jpg', 1, 24, CAST(N'2016-12-20 16:58:28.910' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (297, N'Broad Jump (Pre)-Jump.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-8c0de0ad-e376-4b51-9f6b-ca2e002d23da/fn1577867844.mp4?sv=2012-02-12&sr=c&si=3dbb8c3c-ca46-4548-90e8-548d99b73a35&sig=ZBnykx0MzmDYou6vZzyLePLYCB%2BjZKJeC9s%2BL01sCbc%3D&se=2017-12-20T16%3A58%3A32Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/80898336421082.jpg', 1, 24, CAST(N'2016-12-20 16:58:34.350' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (298, N'Broad Jump (pre)-landing.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-026247f3-f7c6-4978-9f14-d7062a001807/fn397846082.mp4?sv=2012-02-12&sr=c&si=4b7a0541-599f-4c7b-ad1d-6d2eb6ccbc7c&sig=bRCSHLzz%2FisVQhtf8cI9QHA7iVL7km0Zk49QCN%2FrRuM%3D&se=2017-12-20T16%3A58%3A37Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/83087696017594.jpg', 1, 24, CAST(N'2016-12-20 16:58:39.130' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (299, N'Broad Jump-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4fc18aaf-6e3c-4940-a87d-4c4604710e4a/fn263305856.mp4?sv=2012-02-12&sr=c&si=856afef1-e823-45dd-9fbd-663dbcb60ab6&sig=GeUICb4hA8Tg8d7FfycpSWSS2TOnksJBrPaageCcpTw%3D&se=2017-12-20T16%3A59%3A21Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/18919862535381.jpg', 1, 24, CAST(N'2016-12-20 16:59:22.973' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (300, N'Broad Jump-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-4fc18aaf-6e3c-4940-a87d-4c4604710e4a/fn263305856.mp4?sv=2012-02-12&sr=c&si=856afef1-e823-45dd-9fbd-663dbcb60ab6&sig=GeUICb4hA8Tg8d7FfycpSWSS2TOnksJBrPaageCcpTw%3D&se=2017-12-20T16%3A59%3A21Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/18919862535381.jpg', 1, 24, CAST(N'2016-12-20 17:02:22.627' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b6df206f-8cba-422f-a0cb-639573f79da2/fn781597029.mp4?sv=2012-02-12&sr=c&si=4168c5e0-7c2d-42fd-91a0-5ba40210dbce&sig=yGZsPiExjZd6fFoBkSHPCWv4o4EnV6JfZwrRc3GijO4%3D&se=2017-12-20T16%3A58%3A26Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-2ac24a1e-613f-467a-8fcf-7371b28db6b2/fn1462445135.mp4?sv=2012-02-12&sr=c&si=5a7cacd3-bb1e-4a78-aafb-0f32bd21fffd&sig=cypI9X8m16tSUWyy0dic6fcjbQR%2BmtcVuTK2KLCWO%2FY%3D&se=2017-12-20T16%3A58%3A11Z', 4, 1, 296, 293, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (301, N'Broad Jump-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-35d7c181-2146-4203-b5c3-7d52538ec9b4/fn1227227916.mp4?sv=2012-02-12&sr=c&si=7e33b8c8-f494-4487-969c-2ecbd20dba28&sig=5EaH2fC5FwFn%2FAX8ZvvCIN7LL3oZ8b1Yy3RxjOdUa5Y%3D&se=2017-12-19T22%3A53%3A14Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/9824057699601.jpg', 1, 24, CAST(N'2016-12-20 17:02:22.657' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-8c0de0ad-e376-4b51-9f6b-ca2e002d23da/fn1577867844.mp4?sv=2012-02-12&sr=c&si=3dbb8c3c-ca46-4548-90e8-548d99b73a35&sig=ZBnykx0MzmDYou6vZzyLePLYCB%2BjZKJeC9s%2BL01sCbc%3D&se=2017-12-20T16%3A58%3A32Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-a5ecb578-869c-47b8-9d25-043f0cfc8ff8/fn1727331351.mp4?sv=2012-02-12&sr=c&si=bb16a778-59cb-4a86-80d7-90d73e7264c1&sig=XDPDKohx4eBcxcUCTasg8DxMUyC%2Fpc%2BCsHc3xhHkB6Q%3D&se=2017-12-20T16%3A58%3A16Z', 4, 1, 297, 294, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (302, N'Broad Jump-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-b3b5ce92-4db4-43fe-8455-0b38f2391633/fn465739314.mp4?sv=2012-02-12&sr=c&si=0093c973-54d9-48fa-946b-6c30774e4b05&sig=Hit8OuP2BIVcG7xJXFuRewQS14hMxS%2FudMPyx9M959o%3D&se=2017-12-19T22%3A53%3A03Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/96514339627216.jpg', 1, 24, CAST(N'2016-12-20 17:02:22.703' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-026247f3-f7c6-4978-9f14-d7062a001807/fn397846082.mp4?sv=2012-02-12&sr=c&si=4b7a0541-599f-4c7b-ad1d-6d2eb6ccbc7c&sig=bRCSHLzz%2FisVQhtf8cI9QHA7iVL7km0Zk49QCN%2FrRuM%3D&se=2017-12-20T16%3A58%3A37Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-48b31a6c-8f17-4aff-8aa2-0e4fb4c5fb42/fn83541023.mp4?sv=2012-02-12&sr=c&si=e3fd13cf-65ad-4dd6-9c7c-3262aa17476e&sig=knvjDwS1ShK%2FB4PKxtmuApUUY5SlqxA6CYvd69GXx2M%3D&se=2017-12-20T16%3A58%3A21Z', 4, 1, 298, 295, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (303, N'Squat Jump-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-51cd2bf2-efa3-4aa7-b9b4-deb167875829/fn189221516.mp4?sv=2012-02-12&sr=c&si=c048d8ae-f8e8-4ab8-b1ff-d8c02a3105fa&sig=FYg7MOmUNIBOPeEWtNf59V%2FOLqvfFhmJev%2BGobN1Pic%3D&se=2017-12-20T17%3A16%3A17Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/7512009070123.jpg', 1, 24, CAST(N'2016-12-20 17:16:20.080' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (304, N'Squat Jump (Post)-Countermovement.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-6cdcc8a8-ac1e-4298-a4bb-365594dec6c0/fn2067415612.mp4?sv=2012-02-12&sr=c&si=4926b6ee-3edc-4861-998b-cb8d8cb38216&sig=nhaJFWiwBk%2Fmbv5hexbWChsxcTh9%2F01tJ5bk19M78ZI%3D&se=2017-12-20T17%3A20%3A07Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/34333512266063.jpg', 1, 24, CAST(N'2016-12-20 17:20:09.083' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (305, N'Squat Jump (Post)-Jump.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-7c5a0cff-6ba6-49ee-a41b-8eb8e551614b/fn1381256853.mp4?sv=2012-02-12&sr=c&si=f157a9b2-fe8f-46b0-87ad-cbb4708951ee&sig=f3TnzJFlUomYTN6k31ZoTy0i42V7rJr4am5a9lqg6eE%3D&se=2017-12-20T17%3A20%3A11Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/2954174644845.jpg', 1, 24, CAST(N'2016-12-20 17:20:13.020' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (306, N'Squat Jump (Post)-Landing.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-c983940b-1055-4196-a5e7-9b17d013807c/fn1629726159.mp4?sv=2012-02-12&sr=c&si=57a0ddfb-e568-4b27-9841-1d97bd51227d&sig=OmN7%2BKp8pFutwwxzuVtFfAem64X5ln8qx6rligBH92M%3D&se=2017-12-20T17%3A20%3A15Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/94103216310556.jpg', 1, 24, CAST(N'2016-12-20 17:20:17.177' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (307, N'Squat Jump (Pre)-Countermovement.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-02991be2-4f03-4101-9953-503357390fcd/fn868692321.mp4?sv=2012-02-12&sr=c&si=00331cc8-8326-4cf9-b3d8-d9e90f35b340&sig=8TUAQPeT0GBsQBcC0zixrGj2n2zo%2FlMm4HQzrI%2FPmsE%3D&se=2017-12-20T17%3A20%3A19Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/55178253281200.jpg', 1, 24, CAST(N'2016-12-20 17:20:21.457' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (308, N'Squat Jump (Pre)-Jump.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-54a2b481-497d-4f3a-8f75-016b6f9ec55b/fn990583086.mp4?sv=2012-02-12&sr=c&si=0f3bb5e5-99d4-4ec4-b39f-9d3333d097f5&sig=vCXOht15YBkb1jj4wjkGa39mfq7oxn7Lpkg2UAjJRBs%3D&se=2017-12-20T17%3A20%3A23Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/5785550539050.jpg', 1, 24, CAST(N'2016-12-20 17:20:25.647' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (309, N'Squat Jump (Pre)-Landing.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-290aa129-43f7-4a79-b9da-f11145a59978/fn2070273533.mp4?sv=2012-02-12&sr=c&si=8312fd10-af92-4eb9-be86-68fa76f8925e&sig=XeV6XCupO0HebmFE%2BneTm69VWLzvBRRfy7ehR7h2aOs%3D&se=2017-12-20T17%3A20%3A27Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/30979985540225.jpg', 1, 24, CAST(N'2016-12-20 17:20:29.943' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (310, N'Squat Jump-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-1ca02834-e81b-41d8-9c99-4d3d83558d25/fn276960922.mp4?sv=2012-02-12&sr=c&si=4bccbc36-8d49-487e-a6a9-618ef5e57d61&sig=bGG6a4wzSvl6rgTHmAM3WB52hSzRxpuS3jA%2BnANYjG4%3D&se=2017-12-19T22%3A58%3A38Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/93160161812537.jpg', 1, 24, CAST(N'2016-12-20 17:23:15.767' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-02991be2-4f03-4101-9953-503357390fcd/fn868692321.mp4?sv=2012-02-12&sr=c&si=00331cc8-8326-4cf9-b3d8-d9e90f35b340&sig=8TUAQPeT0GBsQBcC0zixrGj2n2zo%2FlMm4HQzrI%2FPmsE%3D&se=2017-12-20T17%3A20%3A19Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-6cdcc8a8-ac1e-4298-a4bb-365594dec6c0/fn2067415612.mp4?sv=2012-02-12&sr=c&si=4926b6ee-3edc-4861-998b-cb8d8cb38216&sig=nhaJFWiwBk%2Fmbv5hexbWChsxcTh9%2F01tJ5bk19M78ZI%3D&se=2017-12-20T17%3A20%3A07Z', 4, 1, 307, 304, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (311, N'Squat Jump-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ad1bb0e5-bc56-40e7-a925-ea05f4099716/fn541679025.mp4?sv=2012-02-12&sr=c&si=d2ecb6d0-b31d-47f5-b02e-0ddecc1f82da&sig=rbjKkm2%2B4jXacV4kClEwU%2BkHlNwRGbHKPWE3BtIf8yQ%3D&se=2017-12-19T22%3A58%3A41Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/22831877844795.jpg', 1, 24, CAST(N'2016-12-20 17:23:15.843' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-54a2b481-497d-4f3a-8f75-016b6f9ec55b/fn990583086.mp4?sv=2012-02-12&sr=c&si=0f3bb5e5-99d4-4ec4-b39f-9d3333d097f5&sig=vCXOht15YBkb1jj4wjkGa39mfq7oxn7Lpkg2UAjJRBs%3D&se=2017-12-20T17%3A20%3A23Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-7c5a0cff-6ba6-49ee-a41b-8eb8e551614b/fn1381256853.mp4?sv=2012-02-12&sr=c&si=f157a9b2-fe8f-46b0-87ad-cbb4708951ee&sig=f3TnzJFlUomYTN6k31ZoTy0i42V7rJr4am5a9lqg6eE%3D&se=2017-12-20T17%3A20%3A11Z', 4, 1, 308, 305, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (312, N'Squat Jump-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-51cd2bf2-efa3-4aa7-b9b4-deb167875829/fn189221516.mp4?sv=2012-02-12&sr=c&si=c048d8ae-f8e8-4ab8-b1ff-d8c02a3105fa&sig=FYg7MOmUNIBOPeEWtNf59V%2FOLqvfFhmJev%2BGobN1Pic%3D&se=2017-12-20T17%3A16%3A17Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/7512009070123.jpg', 1, 24, CAST(N'2016-12-20 17:23:15.907' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-290aa129-43f7-4a79-b9da-f11145a59978/fn2070273533.mp4?sv=2012-02-12&sr=c&si=8312fd10-af92-4eb9-be86-68fa76f8925e&sig=XeV6XCupO0HebmFE%2BneTm69VWLzvBRRfy7ehR7h2aOs%3D&se=2017-12-20T17%3A20%3A27Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-c983940b-1055-4196-a5e7-9b17d013807c/fn1629726159.mp4?sv=2012-02-12&sr=c&si=57a0ddfb-e568-4b27-9841-1d97bd51227d&sig=OmN7%2BKp8pFutwwxzuVtFfAem64X5ln8qx6rligBH92M%3D&se=2017-12-20T17%3A20%3A15Z', 4, 1, 309, 306, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (313, N'Overhead Squat-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f8517261-bf82-4429-83d5-1703d5cb7846/fn2046478788.mp4?sv=2012-02-12&sr=c&si=4af8dabd-f144-4f81-8499-034890ca1e71&sig=VViSzO5uHMkPY8m5RYjfmCyp9K3HAordmtctKW%2BhRzc%3D&se=2017-12-20T17%3A36%3A47Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/45903741272885.jpg', 1, 24, CAST(N'2016-12-20 17:36:49.473' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (314, N'Overhead Squat (Post)-Chest.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-11207006-db50-4726-9afe-c92683e5dc9b/fn531618373.mp4?sv=2012-02-12&sr=c&si=87f035d1-edeb-4a38-81a3-473e7d0652ea&sig=W4B%2FiIao7Ql%2B74jedL1q%2Fz95V%2Fnk%2BOkXcoATjaDwN74%3D&se=2017-12-20T17%3A40%3A46Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/82343675371446.jpg', 1, 24, CAST(N'2016-12-20 17:40:48.880' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (315, N'Overhead Squat (Post)-Knees.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-5aba0516-4ade-4d4e-9cde-6db39e2e2b88/fn1147708367.mp4?sv=2012-02-12&sr=c&si=5b6a1a75-74a7-42ed-b26d-bb02b484133e&sig=dAh%2FYhcbvwyMM6819ci6S1mNlB99XuuLcnpzmcIehSw%3D&se=2017-12-20T17%3A40%3A50Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/62937015951689.jpg', 1, 24, CAST(N'2016-12-20 17:40:53.130' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (316, N'Overhead Squat (Post)-Shoulders.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-ecb56a4a-f923-47e4-a87b-556607ffc7b8/fn2115002139.mp4?sv=2012-02-12&sr=c&si=027bcc30-81b4-44b3-aeb6-435412e66c52&sig=0RBtDpFkampyfm8cVVKWHbEhfko%2FgQNzUbNqY5DYCzo%3D&se=2017-12-20T17%3A40%3A54Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/72429991183671.jpg', 1, 24, CAST(N'2016-12-20 17:40:56.910' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (317, N'Overhead Squat (Pre)-Chest .mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-64288f8f-4522-44e4-b1e2-c0d47668ceb1/fn867028118.mp4?sv=2012-02-12&sr=c&si=4ef1fd8f-e12f-43ac-bdd1-2a73ce0124dd&sig=ZMC11knzwzymslxwr2Dby1xPE55J6Lyh6roD8Fotrrs%3D&se=2017-12-20T17%3A41%3A00Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/31435232875245.jpg', 1, 24, CAST(N'2016-12-20 17:41:02.287' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (318, N'Overhead Squat (Pre)-Knees.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-8ccd0795-d97f-471c-ad8f-fb7075ab0cc1/fn1670389866.mp4?sv=2012-02-12&sr=c&si=ace072df-1e74-4208-ad87-06516eb051af&sig=XaoC5HVcnWq%2F2PbWk9kL6KtN17YumFAVDjActveKA%2Bk%3D&se=2017-12-20T17%3A41%3A04Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/83789677194427.jpg', 1, 24, CAST(N'2016-12-20 17:41:06.740' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (319, N'Overhead Squat (Pre)-Shoulders.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-dc245297-648d-411e-b7b7-f27589eb7318/fn1366033661.mp4?sv=2012-02-12&sr=c&si=f7c59c6c-b012-44df-a261-90513bbd397e&sig=jRntB6UZQkfCUqfJUB%2BLDArVvYgcx1xB36TAauP%2FdoQ%3D&se=2017-12-20T17%3A41%3A09Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/55332454877869.jpg', 1, 24, CAST(N'2016-12-20 17:41:11.490' AS DateTime), NULL, NULL, 30, NULL, NULL, 1, 1, NULL, NULL, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (320, N'Overhead Squat-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-db338b10-03e8-468d-a1c0-3580b65155b5/fn689039035.mp4?sv=2012-02-12&sr=c&si=a939e092-86ed-4544-be5b-8ca35befa6bd&sig=adKefZN1cpnekr7bZbF0JJG%2FX04pwF9ZF23%2Fk%2F3r0nE%3D&se=2017-12-19T22%3A53%3A29Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/38414325585387.jpg', 1, 24, CAST(N'2016-12-20 17:47:08.200' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-dc245297-648d-411e-b7b7-f27589eb7318/fn1366033661.mp4?sv=2012-02-12&sr=c&si=f7c59c6c-b012-44df-a261-90513bbd397e&sig=jRntB6UZQkfCUqfJUB%2BLDArVvYgcx1xB36TAauP%2FdoQ%3D&se=2017-12-20T17%3A41%3A09Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-ecb56a4a-f923-47e4-a87b-556607ffc7b8/fn2115002139.mp4?sv=2012-02-12&sr=c&si=027bcc30-81b4-44b3-aeb6-435412e66c52&sig=0RBtDpFkampyfm8cVVKWHbEhfko%2FgQNzUbNqY5DYCzo%3D&se=2017-12-20T17%3A40%3A54Z', 4, 1, 319, 316, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (321, N'Overhead Squat-Side.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f74b6772-a32a-4a5b-a48e-496d94821869/fn2127024193.mp4?sv=2012-02-12&sr=c&si=a8ac93bf-b60e-4c32-896a-f3995da9e390&sig=zgpnmTRHk4wnDejqWxuYP6tY38c80BwcQUq1BuHEG6c%3D&se=2017-12-19T22%3A53%3A34Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/24571955367367.jpg', 1, 24, CAST(N'2016-12-20 17:47:08.277' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-64288f8f-4522-44e4-b1e2-c0d47668ceb1/fn867028118.mp4?sv=2012-02-12&sr=c&si=4ef1fd8f-e12f-43ac-bdd1-2a73ce0124dd&sig=ZMC11knzwzymslxwr2Dby1xPE55J6Lyh6roD8Fotrrs%3D&se=2017-12-20T17%3A41%3A00Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-11207006-db50-4726-9afe-c92683e5dc9b/fn531618373.mp4?sv=2012-02-12&sr=c&si=87f035d1-edeb-4a38-81a3-473e7d0652ea&sig=W4B%2FiIao7Ql%2B74jedL1q%2Fz95V%2Fnk%2BOkXcoATjaDwN74%3D&se=2017-12-20T17%3A40%3A46Z', 4, 1, 317, 314, 1)
INSERT [dbo].[Videos] ([ID], [MovementName], [ActorName], [Format], [Duration], [VideoURL], [ThumbNailURL], [Status], [CreatedBy], [CreateDate], [UpdatedBy], [UpdateDate], [PlayCount], [IntroVideoUrl], [IntroVideoUrl2], [VideoType], [VideoMode], [PreVideoId], [PostVideoId], [SportId]) VALUES (322, N'Overhead Squat-Front.mp4', N'Athlete', N'mp4', NULL, N'https://ikkosmultisports.blob.core.windows.net:443/asset-f8517261-bf82-4429-83d5-1703d5cb7846/fn2046478788.mp4?sv=2012-02-12&sr=c&si=4af8dabd-f144-4f81-8499-034890ca1e71&sig=VViSzO5uHMkPY8m5RYjfmCyp9K3HAordmtctKW%2BhRzc%3D&se=2017-12-20T17%3A36%3A47Z', N'https://ikkosmultisports.blob.core.windows.net/mycontainer/45903741272885.jpg', 1, 24, CAST(N'2016-12-20 17:47:08.340' AS DateTime), NULL, NULL, 30, N'https://ikkosmultisports.blob.core.windows.net:443/asset-8ccd0795-d97f-471c-ad8f-fb7075ab0cc1/fn1670389866.mp4?sv=2012-02-12&sr=c&si=ace072df-1e74-4208-ad87-06516eb051af&sig=XaoC5HVcnWq%2F2PbWk9kL6KtN17YumFAVDjActveKA%2Bk%3D&se=2017-12-20T17%3A41%3A04Z', N'https://ikkosmultisports.blob.core.windows.net:443/asset-5aba0516-4ade-4d4e-9cde-6db39e2e2b88/fn1147708367.mp4?sv=2012-02-12&sr=c&si=5b6a1a75-74a7-42ed-b26d-bb02b484133e&sig=dAh%2FYhcbvwyMM6819ci6S1mNlB99XuuLcnpzmcIehSw%3D&se=2017-12-20T17%3A40%3A50Z', 4, 1, 318, 315, 1)
SET IDENTITY_INSERT [dbo].[Videos] OFF
SET IDENTITY_INSERT [dbo].[VideoTypes] ON 

INSERT [dbo].[VideoTypes] ([Id], [VideoType]) VALUES (1, N'Only Videos')
INSERT [dbo].[VideoTypes] ([Id], [VideoType]) VALUES (2, N'Channel Intro')
INSERT [dbo].[VideoTypes] ([Id], [VideoType]) VALUES (3, N'Package Intro')
INSERT [dbo].[VideoTypes] ([Id], [VideoType]) VALUES (4, N'Actual Video')
SET IDENTITY_INSERT [dbo].[VideoTypes] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [ucShardMappingsLocal_ShardMapId_MinValue]    Script Date: 21-12-2016 17:04:52 ******/
ALTER TABLE [__ShardManagement].[ShardMappingsLocal] ADD  CONSTRAINT [ucShardMappingsLocal_ShardMapId_MinValue] UNIQUE NONCLUSTERED 
(
	[ShardMapId] ASC,
	[MinValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ucShardsLocal_ShardMapId_Location]    Script Date: 21-12-2016 17:04:52 ******/
ALTER TABLE [__ShardManagement].[ShardsLocal] ADD  CONSTRAINT [ucShardsLocal_ShardMapId_Location] UNIQUE NONCLUSTERED 
(
	[ShardMapId] ASC,
	[Protocol] ASC,
	[ServerName] ASC,
	[DatabaseName] ASC,
	[Port] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
ALTER TABLE [__ShardManagement].[ShardMappingsLocal]  WITH CHECK ADD  CONSTRAINT [fkShardMappingsLocal_ShardId] FOREIGN KEY([ShardId])
REFERENCES [__ShardManagement].[ShardsLocal] ([ShardId])
GO
ALTER TABLE [__ShardManagement].[ShardMappingsLocal] CHECK CONSTRAINT [fkShardMappingsLocal_ShardId]
GO
ALTER TABLE [__ShardManagement].[ShardMappingsLocal]  WITH CHECK ADD  CONSTRAINT [fkShardMappingsLocal_ShardMapId] FOREIGN KEY([ShardMapId])
REFERENCES [__ShardManagement].[ShardMapsLocal] ([ShardMapId])
GO
ALTER TABLE [__ShardManagement].[ShardMappingsLocal] CHECK CONSTRAINT [fkShardMappingsLocal_ShardMapId]
GO
ALTER TABLE [__ShardManagement].[ShardsLocal]  WITH CHECK ADD  CONSTRAINT [fkShardsLocal_ShardMapId] FOREIGN KEY([ShardMapId])
REFERENCES [__ShardManagement].[ShardMapsLocal] ([ShardMapId])
GO
ALTER TABLE [__ShardManagement].[ShardsLocal] CHECK CONSTRAINT [fkShardsLocal_ShardMapId]
GO
USE [master]
GO
ALTER DATABASE [ElasticScaleStarterKit_Shard1] SET  READ_WRITE 
GO
