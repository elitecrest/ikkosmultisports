USE [master]
GO
/****** Object:  Database [ElasticScaleStarterKit_ShardMapManagerDb]    Script Date: 21-12-2016 16:58:31 ******/
CREATE DATABASE [ElasticScaleStarterKit_ShardMapManagerDb]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ElasticScaleStarterKit_ShardMapManagerDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET RECOVERY FULL 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET  MULTI_USER 
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET DB_CHAINING OFF 
GO
USE [ElasticScaleStarterKit_ShardMapManagerDb]
GO
/****** Object:  Schema [__ShardManagement]    Script Date: 21-12-2016 16:58:32 ******/
CREATE SCHEMA [__ShardManagement]
GO
/****** Object:  StoredProcedure [__ShardManagement].[spAddShardingSchemaInfoGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spAddShardingSchemaInfoGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@name nvarchar(128),
			@schemaInfo xml

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@name = x.value('(SchemaInfo/Name)[1]', 'nvarchar(128)'),
		@schemaInfo = x.query('SchemaInfo/Info/*')
	from 
		@input.nodes('/AddShardingSchemaInfoGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @name is null or @schemaInfo is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	if exists (
		select 
			Name 
		from 
			__ShardManagement.ShardedDatabaseSchemaInfosGlobal 
		where 
			Name = @name)
		goto Error_SchemaInfoAlreadyExists;
	
	insert into
		__ShardManagement.ShardedDatabaseSchemaInfosGlobal
		(Name, SchemaInfo)
	values
		(@name, @schemaInfo)

	set @result = 1
	goto Exit_Procedure;

Error_SchemaInfoAlreadyExists:
	set @result = 402
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spAddShardMapGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spAddShardMapGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@name nvarchar(50),
			@mapType int,
			@keyType int

	select 
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'),
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@name = x.value('(ShardMap/Name)[1]', 'nvarchar(50)'),
		@mapType = x.value('(ShardMap/Kind)[1]', 'int'),
		@keyType = x.value('(ShardMap/KeyKind)[1]', 'int')
	from 
		@input.nodes('/AddShardMapGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @shardMapId is null or @name is null or @mapType is null or @keyType is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;
		
	begin try
		insert into 
			__ShardManagement.ShardMapsGlobal 
			(ShardMapId, Name, ShardMapType, KeyType)
		values 
			(@shardMapId, @name, @mapType, @keyType) 
	end try
	begin catch
		if (error_number() = 2627)
			goto Error_ShardMapAlreadyExists;
		else
		begin
			declare @errorMessage nvarchar(max) = error_message(),
					@errorNumber int = error_number(),
					@errorSeverity int = error_severity(),
					@errorState int = error_state(),
					@errorLine int = error_line(),
					@errorProcedure nvarchar(128) = isnull(error_procedure(), '-');

			select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage;
			
			raiserror (@errorMessage, @errorSeverity, 1, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
			
			rollback transaction; -- To avoid extra error message in response.
			goto Error_UnexpectedError;
		end
	end catch
		
	set @result = 1
	goto Exit_Procedure;
		
Error_ShardMapAlreadyExists:
	set @result = 101
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_UnexpectedError:
	set @result = 53
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spAttachShardGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spAttachShardGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int, 
			@gsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@name nvarchar(50),
			@mapType int,
			@keyType int,
			@shardId uniqueidentifier,
			@shardVersion uniqueidentifier,
			@protocol int,
			@serverName nvarchar(128),
			@port int,
			@databaseName nvarchar(128),
			@shardStatus int

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@name = x.value('(ShardMap/Name)[1]', 'nvarchar(50)'),
		@mapType = x.value('(ShardMap/Kind)[1]', 'int'),
		@keyType = x.value('(ShardMap/KeyKind)[1]', 'int'),

		@shardId = x.value('(Shard/Id)[1]', 'uniqueidentifier'),
		@shardVersion = x.value('(Shard/Version)[1]', 'uniqueidentifier'),
		@protocol = x.value('(Shard/Location/Protocol)[1]', 'int'),
		@serverName = x.value('(Shard/Location/ServerName)[1]', 'nvarchar(128)'),
		@port = x.value('(Shard/Location/Port)[1]', 'int'),
		@databaseName = x.value('(Shard/Location/DatabaseName)[1]', 'nvarchar(128)'),
		@shardStatus = x.value('(Shard/Status)[1]', 'int')
	from
		@input.nodes('/AttachShardGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @shardMapId is null or @name is null or @mapType is null or @keyType is null or
		@shardId is null or @shardVersion is null or @protocol is null or @serverName is null or 
		@port is null or @databaseName is null or @shardStatus is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	if exists (
	select 
		ShardMapId
	from
		__ShardManagement.ShardMapsGlobal 
	where
		(ShardMapId = @shardMapId and Name <> @name) or (ShardMapId <> @shardMapId and Name = @name))
		goto Error_ShardMapAlreadyExists;

	begin try
		insert into 
			__ShardManagement.ShardMapsGlobal 
			(ShardMapId, Name, ShardMapType, KeyType)
		values 
			(@shardMapId, @name, @mapType, @keyType) 
	end try
	begin catch
		if (error_number() <> 2627)
		begin
			declare @errorMessage nvarchar(max) = error_message(),
					@errorNumber int = error_number(),
					@errorSeverity int = error_severity(),
					@errorState int = error_state(),
					@errorLine int = error_line(),
					@errorProcedure nvarchar(128) = isnull(error_procedure(), '-');

			select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage;
			
			raiserror (@errorMessage, @errorSeverity, 1, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
			
			rollback transaction; -- To avoid extra error message in response.
			goto Error_UnexpectedError;
		end
	end catch

	begin try
		insert into 
			__ShardManagement.ShardsGlobal (
			ShardId, 
			Readable, 
			Version, 
			ShardMapId, 
			OperationId, 
			Protocol, 
			ServerName, 
			Port, 
			DatabaseName, 
			Status)
		values (
			@shardId, 
			1, 
			@shardVersion, 
			@shardMapId, 
			null, 
			@protocol, 
			@serverName, 
			@port, 
			@databaseName, 
			@shardStatus) 
	end try
	begin catch
		if (error_number() = 2627)
			goto Error_ShardLocationAlreadyExists;
		else
		begin
			set @errorMessage = error_message()
			set	@errorNumber = error_number()
			set @errorSeverity = error_severity()
			set @errorState = error_state()
			set @errorLine = error_line()
			set @errorProcedure = isnull(error_procedure(), '-')

			select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage;
			
			raiserror (@errorMessage, @errorSeverity, 2, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
			
			rollback transaction; -- To avoid extra error message in response.
			goto Error_UnexpectedError;
		end
	end catch
	
	set @result = 1
	goto Exit_Procedure;

Error_ShardMapAlreadyExists:
	set @result = 101
	goto Exit_Procedure;

Error_ShardLocationAlreadyExists:
	set @result = 205
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_UnexpectedError:
	set @result = 53
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spBulkOperationShardMappingsGlobalBegin]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spBulkOperationShardMappingsGlobalBegin]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@operationCode int,
			@stepsCount int,
			@shardMapId uniqueidentifier

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@operationCode = x.value('(@OperationCode)[1]', 'int'),
		@stepsCount = x.value('(@StepsCount)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier')
	from
		@input.nodes('/BulkOperationShardMappingsGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @operationId is null or @operationCode is null or 
		@stepsCount is null or @shardMapId is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	declare @shardMapType int

	select 
		@shardMapType = ShardMapType
	from
		__ShardManagement.ShardMapsGlobal with (updlock)
	where
		ShardMapId = @shardMapId

	if (@shardMapType is null)
		goto Error_ShardMapNotFound;

	declare @shardIdForRemoves uniqueidentifier,
			@originalShardVersionForRemoves uniqueidentifier,
			@shardIdForAdds uniqueidentifier,
			@originalShardVersionForAdds uniqueidentifier,
			@currentShardOperationId uniqueidentifier

	select 
		@shardIdForRemoves = x.value('(Removes/Shard/Id)[1]', 'uniqueidentifier'),
		@shardIdForAdds = x.value('(Adds/Shard/Id)[1]', 'uniqueidentifier')
	from 
		@input.nodes('/BulkOperationShardMappingsGlobal') as t(x)

	if (@shardIdForRemoves is null or @shardIdForAdds is null)
		goto Error_MissingParameters;

	select
		@originalShardVersionForRemoves = Version,
		@currentShardOperationId = OperationId
	from
		__ShardManagement.ShardsGlobal with (updlock)
	where
		ShardMapId = @shardMapId and ShardId = @shardIdForRemoves and Readable = 1
	
	if (@currentShardOperationId = @operationId)
		goto Success_Exit;

	if (@currentShardOperationId is not null)
		goto Error_ShardPendingOperation;

	if (@originalShardVersionForRemoves is null)
		goto Error_ShardDoesNotExist;

	update __ShardManagement.ShardsGlobal
	set
		OperationId = @operationId
	where
		ShardMapId = @shardMapId and ShardId = @shardIdForRemoves

	set @currentShardOperationId = null;

	if (@shardIdForRemoves <> @shardIdForAdds)
	begin
		select
			@originalShardVersionForAdds = Version,
			@currentShardOperationId = OperationId
		from
			__ShardManagement.ShardsGlobal with (updlock)
		where
			ShardMapId = @shardMapId and ShardId = @shardIdForAdds and Readable = 1
	
		if (@currentShardOperationId = @operationId)
			goto Success_Exit;

		if (@currentShardOperationId is not null)
			goto Error_ShardPendingOperation;

		if (@originalShardVersionForAdds is null)
			goto Error_ShardDoesNotExist;

		update __ShardManagement.ShardsGlobal
		set
			OperationId = @operationId
		where
			ShardMapId = @shardMapId and ShardId = @shardIdForAdds
	end
	else
	begin
		set @originalShardVersionForAdds = @originalShardVersionForRemoves
	end
	
	begin try
		insert into __ShardManagement.OperationsLogGlobal(
			OperationId,
			OperationCode,
			Data,
			ShardVersionRemoves,
			ShardVersionAdds)
		values (
			@operationId,
			@operationCode,
			@input,
			@originalShardVersionForRemoves,
			@originalShardVersionForAdds)
	end try
	begin catch
		if (error_number() <> 2627)
		begin
			declare @errorMessage nvarchar(max) = error_message(),
					@errorNumber int = error_number(),
					@errorSeverity int = error_severity(),
					@errorState int = error_state(),
					@errorLine int = error_line(),
					@errorProcedure nvarchar(128) = isnull(error_procedure(), '-');

			select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage;
			
			raiserror (@errorMessage, @errorSeverity, 1, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
			
			rollback transaction; -- To avoid extra error message in response.
			goto Error_UnexpectedError;
		end
	end catch

	declare	@currentStep xml,
			@stepIndex int = 1,
			@stepType int,
			@stepMappingId uniqueidentifier,
			@stepLockOwnerId uniqueidentifier

	declare	@currentLockOwnerId uniqueidentifier,
			@currentStatus int

	declare @stepStatus int,
			@stepShouldValidate bit,
			@stepMinValue varbinary(128),
			@stepMaxValue varbinary(128),
			@mappingIdFromValidate uniqueidentifier

	while (@stepIndex <= @stepsCount)
	begin
		select 
			@currentStep = x.query('(./Step[@Id = sql:variable("@stepIndex")])[1]') 
		from 
			@input.nodes('/BulkOperationShardMappingsGlobal/Steps') as t(x)

		select
			@stepType = x.value('(@Kind)[1]', 'int'),
			@stepMappingId = x.value('(Mapping/Id)[1]', 'uniqueidentifier')
		from
			@currentStep.nodes('./Step') as t(x)
	
		if (@stepType is null or @stepMappingId is null)
			goto Error_MissingParameters;

		if (@stepType = 1)
		begin

			select 
				@stepLockOwnerId = x.value('(Lock/Id)[1]', 'uniqueidentifier')
			from 
				@currentStep.nodes('./Step') as t(x)

			if (@stepLockOwnerId is null)
				goto Error_MissingParameters;

			select 
				@currentLockOwnerId = LockOwnerId,
				@currentStatus = Status
			from
				__ShardManagement.ShardMappingsGlobal with (updlock)
			where
				ShardMapId = @shardMapId and MappingId = @stepMappingId and Readable = 1

			if (@currentLockOwnerId is null)	
				goto Error_MappingDoesNotExist;

			if (@currentLockOwnerId <> @stepLockOwnerId)
				goto Error_MappingLockOwnerIdMismatch;

			if ((@currentStatus & 1) <> 0 and 
				(@operationCode = 5 or 
					@operationCode = 9 or 
					@operationCode = 13))
				goto Error_MappingIsNotOffline;

			update 
				__ShardManagement.ShardMappingsGlobal
			set
				OperationId = @operationId
			where
				ShardMapId = @shardMapId and MappingId = @stepMappingId

			set @currentLockOwnerId = null
			set @currentStatus = null
		end
		else
		if (@stepType = 2)
		begin

			select 
				@stepLockOwnerId = x.value('(Lock/Id)[1]', 'uniqueidentifier'),
				@stepStatus = x.value('(Update/Mapping/Status)[1]', 'int')
			from 
				@currentStep.nodes('./Step') as t(x)

			if (@stepLockOwnerId is null or @stepStatus is null)
				goto Error_MissingParameters;

			select
				@currentLockOwnerId = LockOwnerId,
				@currentStatus = Status
			from
				__ShardManagement.ShardMappingsGlobal with (updlock)
			where
				ShardMapId = @shardMapId and MappingId = @stepMappingId and Readable = 1

			if (@currentLockOwnerId is null)	
				goto Error_MappingDoesNotExist;

			if (@currentLockOwnerId <> @stepLockOwnerId)
				goto Error_MappingLockOwnerIdMismatch;

			if ((@currentStatus & 1) = 1 and (@stepStatus & 1) = 1 and @shardIdForRemoves <> @shardIdForAdds)
				goto Error_MappingIsNotOffline;

			update 
				__ShardManagement.ShardMappingsGlobal
			set
				OperationId = @operationId
			where
				ShardMapId = @shardMapId and MappingId = @stepMappingId

			set @currentLockOwnerId = null
			set @currentStatus = null

			set @stepStatus = null
		end
		else
		if (@stepType = 3)
		begin
			select 
				@stepShouldValidate = x.value('(@Validate)[1]', 'bit'),
				@stepMappingId = x.value('(Mapping/Id)[1]', 'uniqueidentifier'),
				@stepMinValue = convert(varbinary(128), x.value('(Mapping/MinValue)[1]', 'varchar(258)'), 1),
				@stepMaxValue = convert(varbinary(128), x.value('(Mapping/MaxValue[@Null="0"])[1]', 'varchar(258)'), 1),
				@stepStatus = x.value('(Mapping/Status)[1]', 'int'),
				@stepLockOwnerId = x.value('(Mapping/LockOwnerId)[1]', 'uniqueidentifier')
			from
				@currentStep.nodes('./Step') as t(x)

			if (@stepShouldValidate is null or @stepMappingId is null or @stepMinValue is null or @stepStatus is null or @stepLockOwnerId is null)
				goto Error_MissingParameters;

			if (@stepShouldValidate = 1)
			begin
				if (@shardMapType = 1)
				begin
					select 
						@mappingIdFromValidate = MappingId,
						@currentShardOperationId = OperationId
					from
					__ShardManagement.ShardMappingsGlobal
					where
						ShardMapId = @shardMapId and
						MinValue = @stepMinValue

					if (@mappingIdFromValidate is not null)
					begin
						if (@currentShardOperationId is null or @currentShardOperationId = @operationId)
							goto Error_PointAlreadyMapped;
						else
							goto Error_ShardPendingOperation;
					end
				end
				else
				begin
					select 
						@mappingIdFromValidate = MappingId,
						@currentShardOperationId = OperationId
					from
						__ShardManagement.ShardMappingsGlobal
					where
						ShardMapId = @shardMapId and
						(MaxValue is null or MaxValue > @stepMinValue) and 
						(@stepMaxValue is null or MinValue < @stepMaxValue)

					if (@mappingIdFromValidate is not null)
					begin
						if (@currentShardOperationId is null or @currentShardOperationId = @operationId)
							goto Error_RangeAlreadyMapped;
						else
							goto Error_ShardPendingOperation;
					end
				end
			end

			insert into
				__ShardManagement.ShardMappingsGlobal(
				MappingId, 
				Readable,
				ShardId, 
				ShardMapId, 
				OperationId, 
				MinValue, 
				MaxValue, 
				Status,
				LockOwnerId)
			values (
				@stepMappingId, 
				0,
				@shardIdForAdds, 
				@shardMapId, 
				@operationId, 
				@stepMinValue, 
				@stepMaxValue, 
				@stepStatus,
				@stepLockOwnerId)

			set @stepStatus = null

			set @stepShouldValidate = null
			set @stepMinValue = null
			set @stepMaxValue = null
			set @mappingIdFromValidate = null
		end

		set @stepType = null
		set @stepMappingId = null
		set @stepLockOwnerId = null

		set @stepIndex = @stepIndex + 1
	end

	goto Success_Exit;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_ShardDoesNotExist:
	set @result = 202
	goto Exit_Procedure;

Error_MappingDoesNotExist:
	set @result = 301
	goto Exit_Procedure;

Error_RangeAlreadyMapped:
	set @result = 302
	goto Exit_Procedure;

Error_PointAlreadyMapped:
	set @result = 303
	goto Exit_Procedure;

Error_MappingLockOwnerIdMismatch:
	set @result = 307
	goto Exit_Procedure;

Error_MappingIsNotOffline:
	set @result = 306
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_ShardPendingOperation:
	set @result = 52
	exec __ShardManagement.spGetOperationLogEntryGlobalHelper @currentShardOperationId
	goto Exit_Procedure;

Error_UnexpectedError:
	set @result = 53
	goto Exit_Procedure;

Success_Exit:
	set @result = 1
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spBulkOperationShardMappingsGlobalEnd]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spBulkOperationShardMappingsGlobalEnd]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@operationCode int,
			@undo int,
			@stepsCount int,
			@shardMapId uniqueidentifier

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@operationCode = x.value('(@OperationCode)[1]', 'int'),
		@undo = x.value('(@Undo)[1]', 'int'),
		@stepsCount = x.value('(@StepsCount)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier')
	from
		@input.nodes('/BulkOperationShardMappingsGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @operationId is null or @operationCode is null or @undo is null or
		@stepsCount is null or @shardMapId is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	if not exists (
		select 
			ShardMapId 
		from 
			__ShardManagement.ShardMapsGlobal with (updlock)
		where
			ShardMapId = @shardMapId)
		goto Error_ShardMapNotFound;

	declare @shardIdForRemoves uniqueidentifier,
			@shardVersionForRemoves uniqueidentifier,
			@shardIdForAdds uniqueidentifier,
			@shardVersionForAdds uniqueidentifier

	select 
		@shardIdForRemoves = x.value('(Removes/Shard/Id)[1]', 'uniqueidentifier'),
		@shardIdForAdds = x.value('(Adds/Shard/Id)[1]', 'uniqueidentifier'),
		@shardVersionForRemoves = x.value('(Removes/Shard/Version)[1]', 'uniqueidentifier'),
		@shardVersionForAdds = x.value('(Adds/Shard/Version)[1]', 'uniqueidentifier')
	from 
		@input.nodes('/BulkOperationShardMappingsGlobal') as t(x)

	if (@shardIdForRemoves is null or @shardIdForAdds is null or @shardVersionForRemoves is null or @shardVersionForAdds is null)
		goto Error_MissingParameters;

	if (@undo = 1)
	begin
		update 
			__ShardManagement.ShardsGlobal
		set
			OperationId = null
		where
			ShardMapId = @shardMapId and ShardId = @shardIdForRemoves

		if (@shardIdForRemoves <> @shardIdForAdds)
		begin
			update 
				__ShardManagement.ShardsGlobal
			set
				OperationId = null
			where
				ShardMapId = @shardMapId and ShardId = @shardIdForAdds
		end
	end
	else
	begin
		update 
			__ShardManagement.ShardsGlobal
		set
			Version = @shardVersionForRemoves,
			OperationId = null
		where
			ShardMapId = @shardMapId and ShardId = @shardIdForRemoves

		if (@shardIdForRemoves <> @shardIdForAdds)
		begin
		update 
			__ShardManagement.ShardsGlobal
		set
			Version = @shardVersionForAdds,
			OperationId = null
		where
			ShardMapId = @shardMapId and ShardId = @shardIdForAdds
		end
	end

	declare @currentStep xml,
			@stepIndex int = 1,
			@stepType int,
			@stepMappingId uniqueidentifier
	
	while (@stepIndex <= @stepsCount)
	begin
		select 
			@currentStep = x.query('(./Step[@Id = sql:variable("@stepIndex")])[1]') 
		from
		@input.nodes('/BulkOperationShardMappingsGlobal/Steps') as t(x)

		select 
			@stepType = x.value('(@Kind)[1]', 'int'),
			@stepMappingId = x.value('(Mapping/Id)[1]', 'uniqueidentifier')
		from
			@currentStep.nodes('./Step') as t(x)

		if (@stepType is null or @stepMappingId is null)
			goto Error_MissingParameters;

		if (@stepType = 1)
		begin
			if (@undo = 1)
			begin
				update 
					__ShardManagement.ShardMappingsGlobal
				set
					OperationId = null
				where
					ShardMapId = @shardMapId and MappingId = @stepMappingId
			end
			else
			begin
				delete from 
					__ShardManagement.ShardMappingsGlobal
				where
					ShardMapId = @shardMapId and MappingId = @stepMappingId
			end
		end
		else
		if (@stepType = 2)
		begin
			declare @newMappingId uniqueidentifier,
					@newMappingStatus int

			if (@undo = 1)
			begin
				update 
					__ShardManagement.ShardMappingsGlobal
				set
					OperationId = null
				where
					ShardMapId = @shardMapId and MappingId = @stepMappingId
			end
			else
			begin
				select
					@newMappingId = x.value('(Update/Mapping/Id)[1]', 'uniqueidentifier'),
					@newMappingStatus = x.value('(Update/Mapping/Status)[1]', 'int')
				from
					@currentStep.nodes('./Step') as t(x)

				update 
					__ShardManagement.ShardMappingsGlobal
				set
					MappingId = @newMappingId,
					ShardId = @shardIdForAdds,
					Status = @newMappingStatus,
					OperationId = null
				where
					ShardMapId = @shardMapId and MappingId = @stepMappingId
			end

			set @newMappingId = null
			set @newMappingStatus = null
		end
		else
		if (@stepType = 3)
		begin
			if (@undo = 1)
			begin
				delete from 
					__ShardManagement.ShardMappingsGlobal
				where
					ShardMapId = @shardMapId and MappingId = @stepMappingId
			end
			else
			begin
				update 
					__ShardManagement.ShardMappingsGlobal
				set
					Readable = 1,
					OperationId = null
				where
					ShardMapId = @shardMapId and MappingId = @stepMappingId
			end
		end

		set @stepMappingId = null

		set @stepIndex = @stepIndex + 1
	end

	delete from 
		__ShardManagement.OperationsLogGlobal
	where
		OperationId = @operationId

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spBulkOperationShardsGlobalBegin]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spBulkOperationShardsGlobalBegin]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@operationCode int,
			@stepsCount int,
			@shardMapId uniqueidentifier

	select 
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@operationCode = x.value('(@OperationCode)[1]', 'int'),
		@stepsCount = x.value('(@StepsCount)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier')
	from
		@input.nodes('/BulkOperationShardsGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @operationId is null or @operationCode is null or 
		@stepsCount is null or @shardMapId is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	if not exists (
		select 
			ShardMapId 
		from 
			__ShardManagement.ShardMapsGlobal with (updlock)
		where
			ShardMapId = @shardMapId)
		goto Error_ShardMapNotFound;

	begin try
		insert into __ShardManagement.OperationsLogGlobal(
			OperationId,
			OperationCode,
			Data,
			ShardVersionRemoves,
			ShardVersionAdds)
		values (
			@operationId,
			@operationCode,
			@input,
			null,
			null)
	end try
	begin catch
		if (error_number() <> 2627)
		begin
			declare @errorMessage nvarchar(max) = error_message(),
					@errorNumber int = error_number(),
					@errorSeverity int = error_severity(),
					@errorState int = error_state(),
					@errorLine int = error_line(),
					@errorProcedure nvarchar(128) = isnull(error_procedure(), '-');

			select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage;
			
			raiserror (@errorMessage, @errorSeverity, 1, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
			
			rollback transaction; -- To avoid extra error message in response.
			goto Error_UnexpectedError;
		end
	end catch

	declare @currentStep xml,
			@stepIndex int = 1,
			@stepType int,
			@stepShardId uniqueidentifier,
			@stepShardVersion uniqueidentifier,
			@currentShardVersion uniqueidentifier,
			@currentShardOperationId uniqueidentifier

	declare	@stepProtocol int,
			@stepServerName nvarchar(128),
			@stepPort int,
			@stepDatabaseName nvarchar(128),
			@stepShardStatus int

	while (@stepIndex <= @stepsCount)
	begin
		select 
			@currentStep = x.query('(./Step[@Id = sql:variable("@stepIndex")])[1]') 
		from
			@input.nodes('/BulkOperationShardsGlobal/Steps') as t(x)

		select 
			@stepType = x.value('(@Kind)[1]', 'int'),
			@stepShardId = x.value('(Shard/Id)[1]', 'uniqueidentifier'),
			@stepShardVersion = x.value('(Shard/Version)[1]', 'uniqueidentifier')
		from 
			@currentStep.nodes('./Step') as t(x)

		if (@stepType is null or @stepShardId is null or @stepShardVersion is null)
			goto Error_MissingParameters;

		if (@stepType = 1 or @stepType = 2)
		begin

			select
				@currentShardVersion = Version,
				@currentShardOperationId = OperationId
			from
				__ShardManagement.ShardsGlobal with (updlock)
			where
				ShardMapId = @shardMapId and ShardId = @stepShardId and Readable = 1

			if (@currentShardOperationId = @operationId)
				goto Success_Exit;

			if (@currentShardOperationId is not null)
				goto Error_ShardPendingOperation;

			if (@currentShardVersion is null)
				goto Error_ShardDoesNotExist;

			if (@currentShardVersion <> @stepShardVersion)
				goto Error_ShardVersionMismatch;

			if (@stepType = 1)
			begin
			if exists (
				select 
					ShardId 
				from 
					__ShardManagement.ShardMappingsGlobal 
				where 
					ShardMapId = @shardMapId and ShardId = @stepShardId)
				goto Error_ShardHasMappings;
			end

			update 
				__ShardManagement.ShardsGlobal
			set
				OperationId = @operationId
			where
				ShardMapId = @shardMapId and ShardId = @stepShardId
		end
		else
		if (@stepType = 3)
		begin

			select
				@stepProtocol = x.value('(Shard/Location/Protocol)[1]', 'int'),
				@stepServerName = x.value('(Shard/Location/ServerName)[1]', 'nvarchar(128)'),
				@stepPort = x.value('(Shard/Location/Port)[1]', 'int'),
				@stepDatabaseName = x.value('(Shard/Location/DatabaseName)[1]', 'nvarchar(128)'),
				@stepShardStatus = x.value('(Shard/Status)[1]', 'int')
			from
				@currentStep.nodes('./Step') as t(x)

			if (@stepProtocol is null or @stepServerName is null or @stepPort is null or @stepDatabaseName is null or @stepShardStatus is null)
				goto Error_MissingParameters;

			select 
				@currentShardVersion = Version,
				@currentShardOperationId = OperationId
			from
				__ShardManagement.ShardsGlobal with (updlock)
			where
				ShardMapId = @shardMapId and ShardId = @stepShardId

			if (@currentShardOperationId = @operationId)
				goto Success_Exit;

			if (@currentShardOperationId is not null)
				goto Error_ShardPendingOperation;
	
			if (@currentShardVersion is not null)
				goto Error_ShardAlreadyExists;

			set @currentShardVersion = null
			set @currentShardOperationId = null

			select 
				@currentShardVersion = Version, 
				@currentShardOperationId = OperationId
			from  
				__ShardManagement.ShardsGlobal 
			where
				ShardMapId = @shardMapId and
				Protocol = @stepProtocol and 
				ServerName = @stepServerName and
				Port = @stepPort and
				DatabaseName = @stepDatabaseName

			if (@currentShardOperationId is not null)
				goto Error_ShardPendingOperation;

			if (@currentShardVersion is not null)
				goto Error_ShardLocationAlreadyExists;

			begin try
				insert into 
					__ShardManagement.ShardsGlobal(
					ShardId, 
					Readable, 
					Version, 
					ShardMapId, 
					OperationId, 
					Protocol, 
					ServerName, 
					Port, 
					DatabaseName, 
					Status)
				values (
					@stepShardId, 
					0,
					@stepShardVersion, 
					@shardMapId,
					@operationId, 
					@stepProtocol, 
					@stepServerName, 
					@stepPort, 
					@stepDatabaseName, 
					@stepShardStatus) 
			end try
			begin catch
				if (error_number() = 2627)
					goto Error_ShardLocationAlreadyExists;
				else
				begin
					set @errorMessage = error_message()
					set	@errorNumber = error_number()
					set @errorSeverity = error_severity()
					set @errorState = error_state()
					set @errorLine = error_line()
					set @errorProcedure = isnull(error_procedure(), '-')

					select @errorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, Message: ' + @errorMessage;
			
					raiserror (@errorMessage, @errorSeverity, 2, @errorNumber, @errorSeverity, @errorState, @errorProcedure, @errorLine);
			
					rollback transaction; -- To avoid extra error message in response.
					goto Error_UnexpectedError;
				end
			end catch

			set @stepProtocol = null
			set @stepServerName = null
			set @stepPort = null
			set @stepDatabaseName = null
			set @stepShardStatus = null
		end

		set @stepType = null
		set @stepShardId = null
		set @stepShardVersion = null
		set @currentShardVersion = null
		set @currentShardOperationId = null

		set @stepIndex = @stepIndex + 1
	end

	goto Success_Exit;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_ShardAlreadyExists:
	set @result = 201
	goto Exit_Procedure;

Error_ShardDoesNotExist:
	set @result = 202
	goto Exit_Procedure;

Error_ShardHasMappings:
	set @result = 203
	goto Exit_Procedure;

Error_ShardVersionMismatch:
	set @result = 204
	goto Exit_Procedure;

Error_ShardLocationAlreadyExists:
	set @result = 205
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_ShardPendingOperation:
	set @result = 52
	exec __ShardManagement.spGetOperationLogEntryGlobalHelper @currentShardOperationId
	goto Exit_Procedure;

Error_UnexpectedError:
	set @result = 53
	goto Exit_Procedure;

Success_Exit:
	set @result = 1
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spBulkOperationShardsGlobalEnd]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spBulkOperationShardsGlobalEnd]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int, 
			@gsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@operationCode int,
			@undo bit,
			@stepsCount int,
			@shardMapId uniqueidentifier

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@operationCode = x.value('(@OperationCode)[1]', 'int'),
		@undo = x.value('(@Undo)[1]', 'bit'),
		@stepsCount = x.value('(@StepsCount)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier')
	from
		@input.nodes('/BulkOperationShardsGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @operationId is null or @operationCode is null or @undo is null or
		@stepsCount is null or @shardMapId is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	if not exists (
		select 
			ShardMapId 
		from 
			__ShardManagement.ShardMapsGlobal with (updlock)
		where
			ShardMapId = @shardMapId)
		goto Error_ShardMapNotFound;

	declare @currentStep xml,
			@stepIndex int = 1,
			@stepType int,
			@stepShardId uniqueidentifier
	
	while (@stepIndex <= @stepsCount)
	begin
		select 
			@currentStep = x.query('(./Step[@Id = sql:variable("@stepIndex")])[1]') 
		from
			@input.nodes('/BulkOperationShardsGlobal/Steps') as t(x)

		select 
			@stepType = x.value('(@Kind)[1]', 'int'),
			@stepShardId = x.value('(Shard/Id)[1]', 'uniqueidentifier')
		from 
			@currentStep.nodes('./Step') as t(x)

		if (@stepType is null or @stepShardId is null)
			goto Error_MissingParameters;

		if (@stepType = 1)
		begin
			if (@undo = 1)
			begin
				update 
					__ShardManagement.ShardsGlobal
				set
					OperationId = null
				where
					ShardMapId = @shardMapId and ShardId = @stepShardId and OperationId = @operationId 
			end
			else
			begin
				delete from 
					__ShardManagement.ShardsGlobal
				where
					ShardMapId = @shardMapId and ShardId = @stepShardId and OperationId = @operationId
			end
		end
		else
		if (@stepType = 2)
		begin
			declare @newShardVersion uniqueidentifier,
					@newStatus int

			if (@undo = 1)
			begin
				update 
					__ShardManagement.ShardsGlobal
				set
					OperationId = null
				where
					ShardMapId = @shardMapId and ShardId = @stepShardId and OperationId = @operationId 
			end
			else
			begin
				select 
					@newShardVersion = x.value('(Update/Shard/Version)[1]', 'uniqueidentifier'),
					@newStatus = x.value('(Update/Shard/Status)[1]', 'int')
				from 
					@currentStep.nodes('./Step') as t(x)

				update 
					__ShardManagement.ShardsGlobal
				set
					Version = @newShardVersion,
					Status = @newStatus,
					OperationId = null
				where
					ShardMapId = @shardMapId and ShardId = @stepShardId and OperationId = @operationId 
			end

			set @newShardVersion = null
			set @newStatus = null
		end
		else
		if (@stepType = 3)
		begin
			if (@undo = 1)
			begin
				delete from 
					__ShardManagement.ShardsGlobal
				where
					ShardMapId = @shardMapId and ShardId = @stepShardId and OperationId = @operationId
			end
			else
			begin
				update 
					__ShardManagement.ShardsGlobal
				set
					Readable = 1,
					OperationId = null
				where
					ShardMapId = @shardMapId and ShardId = @stepShardId and OperationId = @operationId 
			end
		end

		set @stepShardId = null

		set @stepIndex = @stepIndex + 1
	end

	delete from
		__ShardManagement.OperationsLogGlobal
	where 
		OperationId = @operationId

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spDetachShardGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spDetachShardGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int, 
			@gsmVersionMinorClient int,
			@protocol int,
			@serverName nvarchar(128),
			@port int,
			@databaseName nvarchar(128),
			@name nvarchar(50)
	
	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@protocol = x.value('(Location/Protocol)[1]', 'int'),
		@serverName = x.value('(Location/ServerName)[1]', 'nvarchar(128)'),
		@port = x.value('(Location/Port)[1]', 'int'),
		@databaseName = x.value('(Location/DatabaseName)[1]', 'nvarchar(128)'),
		@name = x.value('(Shardmap[@Null="0"]/Name)[1]', 'nvarchar(50)')
	from
		@input.nodes('/DetachShardGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @protocol is null or @serverName is null or @port is null or @databaseName is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	declare @tvShardsToDetach table (ShardMapId uniqueidentifier, ShardId uniqueidentifier)

	insert into 
		@tvShardsToDetach
	select 
		tShardMaps.ShardMapId, tShards.ShardId
	from
		__ShardManagement.ShardMapsGlobal tShardMaps 
		join
		__ShardManagement.ShardsGlobal tShards
		on 
			tShards.ShardMapId = tShardMaps.ShardMapId and 
			tShards.Protocol = @protocol and
			tShards.ServerName = @serverName and 
			tShards.Port = @port and
			tShards.DatabaseName = @databaseName
	where
		@name is null or tShardMaps.Name = @name

	delete 
		tShardMappings 
	from
		__ShardManagement.ShardMappingsGlobal tShardMappings 
		join
		@tvShardsToDetach tShardsToDetach
		on 
		tShardsToDetach.ShardMapId = tShardMappings.ShardMapId and tShardsToDetach.ShardId = tShardMappings.ShardId

	delete 
		tShards
	from
		__ShardManagement.ShardsGlobal tShards 
		join
		@tvShardsToDetach tShardsToDetach
		on 
		tShardsToDetach.ShardMapId = tShards.ShardMapId and tShardsToDetach.ShardId = tShards.ShardId

	set @result = 1
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spFindAndUpdateOperationLogEntryByIdGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spFindAndUpdateOperationLogEntryByIdGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@operationId uniqueidentifier,
			@undoStartState int

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'),
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@operationId = x.value('(@OperationId)[1]', 'uniqueidentifier'),
		@undoStartState = x.value('(@UndoStartState)[1]', 'int')
	from
		@input.nodes('/FindAndUpdateOperationLogEntryByIdGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @operationId is null or @undoStartState is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	update 
		__ShardManagement.OperationsLogGlobal
	set
		UndoStartState = @undoStartState
	where
		OperationId = @operationId

	set @result = 1
	exec __ShardManagement.spGetOperationLogEntryGlobalHelper @operationId
	goto Exit_Procedure;
	
Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spFindShardByLocationGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spFindShardByLocationGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@protocol int,
			@serverName nvarchar(128),
			@port int,
			@databaseName nvarchar(128)

	select 
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'),
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@protocol = x.value('(Location/Protocol)[1]', 'int'),
		@serverName = x.value('(Location/ServerName)[1]', 'nvarchar(128)'),
		@port = x.value('(Location/Port)[1]', 'int'),
		@databaseName = x.value('(Location/DatabaseName)[1]', 'nvarchar(128)')
	from
		@input.nodes('/FindShardByLocationGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @shardMapId is null or 
		@protocol is null or @serverName is null or @port is null or @databaseName is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	if not exists (
	select 
		ShardMapId 
	from
		__ShardManagement.ShardMapsGlobal
	where
		ShardMapId = @shardMapId)
		goto Error_ShardMapNotFound;

	select 
		2, ShardId, Version, ShardMapId, Protocol, ServerName, Port, DatabaseName, Status
	from 
		__ShardManagement.ShardsGlobal 
	where
		ShardMapId = @shardMapId and
		Protocol = @protocol and ServerName = @serverName and Port = @port and DatabaseName = @databaseName and 
		Readable = 1

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spFindShardingSchemaInfoByNameGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spFindShardingSchemaInfoByNameGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@name nvarchar(128)

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@name = x.value('(SchemaInfo/Name)[1]', 'nvarchar(128)')
	from 
		@input.nodes('/FindShardingSchemaInfoGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @name is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	select
		7, Name, SchemaInfo
	from 
		__ShardManagement.ShardedDatabaseSchemaInfosGlobal
	where
		Name = @name

	if (@@rowcount = 0)
		goto Error_SchemaInfoNameDoesNotExist;

	set @result = 1
	goto Exit_Procedure;

Error_SchemaInfoNameDoesNotExist:
	set @result = 401
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spFindShardMapByNameGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spFindShardMapByNameGlobal]
@input xml,
@result int output
as
begin
declare @gsmVersionMajorClient int,
	@gsmVersionMinorClient int,
			@name  nvarchar(50)

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'),
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@name = x.value('(ShardMap/Name)[1]', ' nvarchar(50)')
	from 
		@input.nodes('/FindShardMapByNameGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @name is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	select
		1, ShardMapId, Name, ShardMapType, KeyType
	from
		__ShardManagement.ShardMapsGlobal
	where 
		Name = @name

	set @result = 1
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spFindShardMappingByIdGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spFindShardMappingByIdGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@mappingId uniqueidentifier

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@mappingId = x.value('(Mapping/Id)[1]', 'uniqueidentifier')
	from
		@input.nodes('/FindShardMappingByIdGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @shardMapId is null or @mappingId is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	declare @shardMapType int

	select 
		@shardMapType = ShardMapType
	from
		__ShardManagement.ShardMapsGlobal
	where
		ShardMapId = @shardMapId

	if (@shardMapType is null)
		goto Error_ShardMapNotFound;
		
	declare @currentShardId uniqueidentifier,
			@currentMinValue varbinary(128),
			@currentMaxValue varbinary(128),
			@currentStatus int,
			@currentLockOwnerId uniqueidentifier

	select
		@currentMinValue = MinValue
	from
		__ShardManagement.ShardMappingsGlobal
	where
		ShardMapId = @shardMapId and 
		Readable = 1 and
		MappingId = @mappingId

	if (@@rowcount = 0)
		goto Error_MappingDoesNotExist;

	select
		@currentShardId = ShardId,
		@currentMaxValue = MaxValue,
		@currentStatus = Status,
		@currentLockOwnerId = LockOwnerId
	from
		__ShardManagement.ShardMappingsGlobal
	where
		ShardMapId = @shardMapId and 
		MinValue = @currentMinValue

	if (@@rowcount = 0)
		goto Error_MappingDoesNotExist;

	select
		3, @mappingId as MappingId, ShardMapId, @currentMinValue, @currentMaxValue, @currentStatus, @currentLockOwnerId, -- fields for SqlMapping
		ShardId, Version, ShardMapId, Protocol, ServerName, Port, DatabaseName, Status -- fields for SqlShard, ShardMapId is repeated here
	from
		__ShardManagement.ShardsGlobal
	where
		ShardId = @currentShardId and
		ShardMapId = @shardMapId

	if (@@rowcount = 0)
		goto Error_MappingDoesNotExist;

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MappingDoesNotExist:
	set @result = 301
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spFindShardMappingByKeyGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spFindShardMappingByKeyGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@keyValue varbinary(128)

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@keyValue = convert(varbinary(128), x.value('(Key/Value)[1]', 'varchar(258)'), 1)
	from
		@input.nodes('/FindShardMappingByKeyGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @shardMapId is null or @keyValue is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	declare @shardMapType int

	select 
		@shardMapType = ShardMapType
	from
		__ShardManagement.ShardMapsGlobal
	where
		ShardMapId = @shardMapId

	if (@shardMapType is null)
		goto Error_ShardMapNotFound;
		
	declare @currentMappingId uniqueidentifier,
			@currentShardId uniqueidentifier,
			@currentMinValue varbinary(128),
			@currentMaxValue varbinary(128),
			@currentStatus int,
			@currentLockOwnerId uniqueidentifier

	if (@shardMapType = 1)
	begin	
		select
			@currentMappingId = MappingId,
			@currentShardId = ShardId,
			@currentMinValue = MinValue,
			@currentMaxValue = MaxValue,
			@currentStatus = Status,
			@currentLockOwnerId = LockOwnerId
		from
			__ShardManagement.ShardMappingsGlobal
		where
			ShardMapId = @shardMapId and 
			Readable = 1 and
			MinValue = @keyValue
	end
	else
	begin
		select 
			@currentMappingId = MappingId,
			@currentShardId = ShardId,
			@currentMinValue = MinValue,
			@currentMaxValue = MaxValue,
			@currentStatus = Status,
			@currentLockOwnerId = LockOwnerId
		from 
			__ShardManagement.ShardMappingsGlobal
		where
			ShardMapId = @shardMapId and 
			Readable = 1 and
			MinValue <= @keyValue and (MaxValue is null or MaxValue > @keyValue)
	end

	if (@@rowcount = 0)
		goto Error_KeyNotFound;

	select 
		3, @currentMappingId as MappingId, ShardMapId, @currentMinValue, @currentMaxValue, @currentStatus, @currentLockOwnerId, -- fields for SqlMapping
		ShardId, Version, ShardMapId, Protocol, ServerName, Port, DatabaseName, Status -- fields for SqlShard, ShardMapId is repeated here
	from 
		__ShardManagement.ShardsGlobal
	where
		ShardId = @currentShardId and
		ShardMapId = @shardMapId
	
	if (@@rowcount = 0)
		goto Error_KeyNotFound;

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_KeyNotFound:
	set @result = 304
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetAllDistinctShardLocationsGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetAllDistinctShardLocationsGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int

	select 
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'),
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int')
	from 
		@input.nodes('/GetAllDistinctShardLocationsGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	select distinct 
		4, Protocol, ServerName, Port, DatabaseName 
	from 
		__ShardManagement.ShardsGlobal
	where
		Readable = 1

	set @result = 1
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetAllShardingSchemaInfosGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetAllShardingSchemaInfosGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'),
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int')
	from 
		@input.nodes('/GetAllShardingSchemaInfosGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	select
		7, Name, SchemaInfo
	from
		__ShardManagement.ShardedDatabaseSchemaInfosGlobal

	set @result = 1
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetAllShardMappingsGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetAllShardMappingsGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int, 
			@gsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@shardId uniqueidentifier,
			@shardVersion uniqueidentifier,
			@minValue varbinary(128),
			@maxValue varbinary(128)

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@shardId = x.value('(Shard[@Null="0"]/Id)[1]', 'uniqueidentifier'),
		@shardVersion = x.value('(Shard[@Null="0"]/Version)[1]', 'uniqueidentifier'),
		@minValue = convert(varbinary(128), x.value('(Range[@Null="0"]/MinValue)[1]', 'varchar(258)'), 1),
		@maxValue = convert(varbinary(128), x.value('(Range[@Null="0"]/MaxValue[@Null="0"])[1]', 'varchar(258)'), 1)
	from
		@input.nodes('/GetAllShardMappingsGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @shardMapId is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	declare @shardMapType int

	select 
		@shardMapType = ShardMapType
	from
		__ShardManagement.ShardMapsGlobal
	where
		ShardMapId = @shardMapId

	if (@shardMapType is null)
		goto Error_ShardMapNotFound;

	declare @currentShardVersion uniqueidentifier

	if (@shardId is not null)
	begin
		if (@shardVersion is null)
			goto Error_MissingParameters;
			
		select 
			@currentShardVersion = Version
		from
			__ShardManagement.ShardsGlobal
		where
			ShardMapId = @shardMapId and ShardId = @shardId and Readable = 1

		if (@currentShardVersion is null)
			goto Error_ShardDoesNotExist;

	end
			
	declare @tvShards table (
		ShardId uniqueidentifier not null, 
		Version uniqueidentifier not null, 
		Protocol int not null,
		ServerName nvarchar(128) collate SQL_Latin1_General_CP1_CI_AS not null, 
		Port int not null,
		DatabaseName nvarchar(128) collate SQL_Latin1_General_CP1_CI_AS not null, 
		Status int not null,
		primary key (ShardId)
	)

	insert into
		@tvShards
	select
		ShardId = s.ShardId,
		Version = s.Version,
		Protocol = s.Protocol,
		ServerName = s.ServerName,
		Port = s.Port,
		DatabaseName = s.DatabaseName,
		Status = s.Status
	from
		__ShardManagement.ShardsGlobal s
	where
		(@shardId is null or s.ShardId = @shardId) and s.ShardMapId = @shardMapId
		

	declare @minValueCalculated varbinary(128) = 0x,
			@maxValueCalculated varbinary(128) = null

	if (@minValue is not null)
		set @minValueCalculated = @minValue

	if (@maxValue is not null)
		set @maxValueCalculated = @maxValue

	if (@shardMapType = 1)
	begin
		select
			3, m.MappingId, m.ShardMapId, m.MinValue, m.MaxValue, m.Status, m.LockOwnerId,  -- fields for SqlMapping
			s.ShardId, s.Version, m.ShardMapId, s.Protocol, s.ServerName, s.Port, s.DatabaseName, s.Status -- fields for SqlShard, ShardMapId is repeated here
		from
			__ShardManagement.ShardMappingsGlobal m
		join 
			@tvShards s 
		on 
			m.ShardId = s.ShardId
		where
			m.ShardMapId = @shardMapId and 
			m.Readable = 1 and
			(@shardId is null or m.ShardId = @shardId) and 
			MinValue >= @minValueCalculated and 
			((@maxValueCalculated is null) or (MinValue < @maxValueCalculated))
		order by 
			m.MinValue
	end
	else
	begin
		select
			3, m.MappingId, m.ShardMapId, m.MinValue, m.MaxValue, m.Status, m.LockOwnerId,  -- fields for SqlMapping
			s.ShardId, s.Version, m.ShardMapId, s.Protocol, s.ServerName, s.Port, s.DatabaseName, s.Status -- fields for SqlShard, ShardMapId is repeated here
		from
			__ShardManagement.ShardMappingsGlobal m
		join 
			@tvShards s 
		on 
			m.ShardId = s.ShardId
		where
			m.ShardMapId = @shardMapId and 
			m.Readable = 1 and
			(@shardId is null or m.ShardId = @shardId) and 
			((MaxValue is null) or (MaxValue > @minValueCalculated)) and 
			((@maxValueCalculated is null) or (MinValue < @maxValueCalculated))
		order by
			m.MinValue
	end

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_ShardDoesNotExist:
	set @result = 202
	goto Exit_Procedure;


Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetAllShardMapsGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetAllShardMapsGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int

	select 
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'),
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int')
	from 
		@input.nodes('/GetAllShardMapsGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	select 
		1, ShardMapId, Name, ShardMapType, KeyType 
	from 
		__ShardManagement.ShardMapsGlobal

	set @result = 1
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetAllShardsGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetAllShardsGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@shardMapId uniqueidentifier

	select 
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'),
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier')
	from 
		@input.nodes('/GetAllShardsGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @shardMapId is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	if not exists (
		select 
			ShardMapId 
		from 
			__ShardManagement.ShardMapsGlobal 
		where 
			ShardMapId = @shardMapId)
		goto Error_ShardMapNotFound;

	select 
		2, ShardId, Version, ShardMapId, Protocol, ServerName, Port, DatabaseName, Status
	from 
		__ShardManagement.ShardsGlobal 
	where 
		ShardMapId = @shardMapId and Readable = 1

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetOperationLogEntryGlobalHelper]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetOperationLogEntryGlobalHelper]
@operationId uniqueidentifier
as
begin
	select
		6, OperationId, OperationCode, Data, UndoStartState, ShardVersionRemoves, ShardVersionAdds
	from
		__ShardManagement.OperationsLogGlobal
	where
		OperationId = @operationId
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spGetStoreVersionGlobalHelper]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spGetStoreVersionGlobalHelper]
as
begin
	select
		5, StoreVersionMajor, StoreVersionMinor
	from 
		__ShardManagement.ShardMapManagerGlobal
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spLockOrUnlockShardMappingsGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [__ShardManagement].[spLockOrUnlockShardMappingsGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int, 
			@gsmVersionMinorClient int,
			@shardMapId uniqueidentifier,
			@mappingId uniqueidentifier,
			@lockOwnerId uniqueidentifier,
			@lockOperationType int

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier'),
		@mappingId = x.value('(Mapping/Id)[1]', 'uniqueidentifier'),
		@lockOwnerId = x.value('(Lock/Id)[1]', 'uniqueidentifier'),
		@lockOperationType = x.value('(Lock/Operation)[1]', 'int')
	from
		@input.nodes('/LockOrUnlockShardMappingsGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @shardMapId is null or @lockOwnerId is null or @lockOperationType is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	if (@lockOperationType < 2 and @mappingId is null)
		goto Error_MissingParameters;

	if not exists (
		select 
			ShardMapId 
		from 
			__ShardManagement.ShardMapsGlobal with (updlock)
		where
			ShardMapId = @shardMapId)
		goto Error_ShardMapNotFound;

	declare @DefaultLockOwnerId uniqueidentifier = '00000000-0000-0000-0000-000000000000',
			@currentOperationId uniqueidentifier

	if (@lockOperationType < 2)
	begin			
		declare @ForceUnLockLockOwnerId uniqueidentifier = 'FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF',
				@currentLockOwnerId uniqueidentifier

		select 
			@currentOperationId = OperationId,
			@currentLockOwnerId = LockOwnerId
		from 
			__ShardManagement.ShardMappingsGlobal with (updlock)
		where
			ShardMapId = @shardMapId and MappingId = @mappingId

		if (@currentLockOwnerId is null)
			goto Error_MappingDoesNotExist;

		if (@currentOperationId is not null)
			goto Error_ShardPendingOperation;

		if(@lockOperationType = 0 and @currentLockOwnerId <> @DefaultLockOwnerId)
			goto Error_MappingAlreadyLocked;

		if (@lockOperationType = 1 and (@lockOwnerId <> @currentLockOwnerId) and (@lockOwnerId <> @ForceUnLockLockOwnerId))
			goto Error_MappingLockOwnerIdMismatch;
	end

	update
		__ShardManagement.ShardMappingsGlobal
	set 
		LockOwnerId = case 
		when 
			@lockOperationType = 0 
		then 
			@lockOwnerId 
		when  
			@lockOperationType = 1 or @lockOperationType = 2 or @lockOperationType = 3
		then 
			@DefaultLockOwnerId 
		end
		where
			ShardMapId = @shardMapId and (@lockOperationType = 3 or -- unlock all mappings
										  (@lockOperationType = 2 and LockOwnerId = @lockOwnerId) or -- unlock all mappings for specified LockOwnerId
										  MappingId = @mappingId) -- lock/unlock specified mapping with specified LockOwnerId

Success_Exit:
	set @result = 1 -- success
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MappingDoesNotExist:
	set @result = 301
	goto Exit_Procedure;

Error_MappingLockOwnerIdMismatch:
	set @result = 307
	goto Exit_Procedure;

Error_MappingAlreadyLocked:
	set @result = 308
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_ShardPendingOperation:
	set @result = 52
	exec __ShardManagement.spGetOperationLogEntryGlobalHelper @currentOperationId
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spRemoveShardingSchemaInfoGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spRemoveShardingSchemaInfoGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@name nvarchar(128)

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@name = x.value('(SchemaInfo/Name)[1]', 'nvarchar(128)')
	from 
		@input.nodes('/RemoveShardingSchemaInfoGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @name is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	delete from
		__ShardManagement.ShardedDatabaseSchemaInfosGlobal
	where
		Name = @name

	if (@@rowcount = 0)
		goto Error_SchemaInfoNameDoesNotExist;

	set @result = 1
	goto Exit_Procedure;

Error_SchemaInfoNameDoesNotExist:
	set @result = 401
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spRemoveShardMapGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spRemoveShardMapGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@shardMapId uniqueidentifier

	select 
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'),
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', ' uniqueidentifier')
	from 
		@input.nodes('/RemoveShardMapGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @shardMapId is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	declare @currentShardMapId uniqueidentifier

	select 
		@currentShardMapId = ShardMapId
	from
		__ShardManagement.ShardMapsGlobal with (updlock)
	where
		ShardMapId = @shardMapId

	if (@currentShardMapId is null)
		goto Error_ShardMapNotFound;

	if exists (
		select 
			ShardId 
		from 
			__ShardManagement.ShardsGlobal 
		where 
			ShardMapId = @shardMapId)
		goto Error_ShardMapHasShards;

	delete from 
		__ShardManagement.ShardMapsGlobal 
	where 
		ShardMapId = @shardMapId 

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_ShardMapHasShards:
	set @result = 103
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spReplaceShardMappingsGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spReplaceShardMappingsGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int, 
			@gsmVersionMinorClient int,
			@removeStepsCount int,
			@addStepsCount int,
			@shardMapId uniqueidentifier
	
	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@removeStepsCount = x.value('(@RemoveStepsCount)[1]', 'int'),
		@addStepsCount = x.value('(@AddStepsCount)[1]', 'int'),
		@shardMapId = x.value('(ShardMap/Id)[1]', 'uniqueidentifier')
	from
		@input.nodes('ReplaceShardMappingsGlobal') as t(x)
	
	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @removeStepsCount is null or @addStepsCount is null or @shardMapId is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	if not exists (
		select 
			ShardMapId 
		from 
			__ShardManagement.ShardMapsGlobal with (updlock)
		where
			ShardMapId = @shardMapId)
		goto Error_ShardMapNotFound;

	declare	@stepShardId uniqueidentifier,
			@stepMappingId uniqueidentifier

	if (@removeStepsCount > 0)
	begin
		select 
			@stepShardId = x.value('(Shard/Id)[1]', 'uniqueidentifier')
		from 
			@input.nodes('ReplaceShardMappingsGlobal/RemoveSteps') as t(x)

		if (@stepShardId is null)
			goto Error_MissingParameters;
	
		declare @currentRemoveStep xml,
				@removeStepIndex int = 1

		while (@removeStepIndex <= @removeStepsCount)
		begin
			select 
				@currentRemoveStep = x.query('(./Step[@Id = sql:variable("@removeStepIndex")])[1]') 
			from
				@input.nodes('ReplaceShardMappingsGlobal/RemoveSteps') as t(x)

			select 
				@stepMappingId = x.value('(Mapping/Id)[1]', 'uniqueidentifier')
			from
				@currentRemoveStep.nodes('./Step') as t(x)

			if (@stepMappingId is null)
				goto Error_MissingParameters;

			delete from 
				__ShardManagement.ShardMappingsGlobal
			where
				ShardMapId = @shardMapId and MappingId = @stepMappingId and ShardId = @stepShardId

			set @stepMappingId = null

			set @removeStepIndex = @removeStepIndex + 1
		end

		set @stepShardId = null
	end

	if (@addStepsCount > 0)
	begin
		select 
			@stepShardId = x.value('(Shard/Id)[1]', 'uniqueidentifier')
		from 
			@input.nodes('ReplaceShardMappingsGlobal/AddSteps') as t(x)

		if (@stepShardId is null)
			goto Error_MissingParameters;

		declare @currentAddStep xml,
				@addStepIndex int = 1,
				@stepMinValue varbinary(128),
				@stepMaxValue varbinary(128),
				@stepStatus int
		
		while (@addStepIndex <= @addStepsCount)
		begin
			select 
				@currentAddStep = x.query('(./Step[@Id = sql:variable("@addStepIndex")])[1]') 
			from
				@input.nodes('ReplaceShardMappingsGlobal/AddSteps') as t(x)
		
			select
				@stepMappingId = x.value('(Mapping/Id)[1]', 'uniqueidentifier'),
				@stepMinValue = convert(varbinary(128), x.value('(Mapping/MinValue)[1]', 'varchar(258)'), 1),
				@stepMaxValue = convert(varbinary(128), x.value('(Mapping/MaxValue[@Null="0"])[1]', 'varchar(258)'), 1),
				@stepStatus = x.value('(Mapping/Status)[1]', 'int')
			from
				@currentAddStep.nodes('./Step') as t(x)
	
			if (@stepMappingId is null or @stepMinValue is null or @stepStatus is null)
				goto Error_MissingParameters;

			insert into
				__ShardManagement.ShardMappingsGlobal(
				MappingId, 
				Readable,
				ShardId, 
				ShardMapId, 
				OperationId, 
				MinValue, 
				MaxValue, 
				Status)
			values (
				@stepMappingId, 
				1,
				@stepShardId, 
				@shardMapId, 
				null, 
				@stepMinValue, 
				@stepMaxValue, 
				@stepStatus)

			set @stepMappingId = null
			set @stepMinValue = null
			set @stepMaxValue = null
			set @stepStatus = null

			set @addStepIndex = @addStepIndex + 1
		end
	end

	set @result = 1
	goto Exit_Procedure;

Error_ShardMapNotFound:
	set @result = 102
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [__ShardManagement].[spUpdateShardingSchemaInfoGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [__ShardManagement].[spUpdateShardingSchemaInfoGlobal]
@input xml,
@result int output
as
begin
	declare @gsmVersionMajorClient int,
			@gsmVersionMinorClient int,
			@name nvarchar(128),
			@schemaInfo xml

	select
		@gsmVersionMajorClient = x.value('(GsmVersion/MajorVersion)[1]', 'int'), 
		@gsmVersionMinorClient = x.value('(GsmVersion/MinorVersion)[1]', 'int'),
		@name = x.value('(SchemaInfo/Name)[1]', 'nvarchar(128)'),
		@schemaInfo = x.query('SchemaInfo/Info/*')
	from 
		@input.nodes('/UpdateShardingSchemaInfoGlobal') as t(x)

	if (@gsmVersionMajorClient is null or @gsmVersionMinorClient is null or @name is null or @schemaInfo is null)
		goto Error_MissingParameters;

	if (@gsmVersionMajorClient <> __ShardManagement.fnGetStoreVersionMajorGlobal())
		goto Error_GSMVersionMismatch;

	update 
		__ShardManagement.ShardedDatabaseSchemaInfosGlobal 
	set 
		SchemaInfo = @schemaInfo
	where
		Name = @name

	if (@@rowcount = 0)
		goto Error_SchemaInfoNameDoesNotExist;

	set @result = 1
	goto Exit_Procedure;

Error_SchemaInfoNameDoesNotExist:
	set @result = 401
	goto Exit_Procedure;

Error_MissingParameters:
	set @result = 50
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Error_GSMVersionMismatch:
	set @result = 51
	exec __ShardManagement.spGetStoreVersionGlobalHelper
	goto Exit_Procedure;

Exit_Procedure:
end

GO
/****** Object:  StoredProcedure [dbo].[CheckForDeviceAndInsert]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CheckForDeviceAndInsert]  --'shaikabdulosman@gmail.com'
@DeviceId nvarchar(1024),
@DeviceType int
as
begin
declare @count int
if exists (select 1 from DeviceDetails where DeviceToken = @DeviceId)
Begin
set @count= (select Id from DeviceDetails where DeviceToken = @DeviceId )
select * from DeviceDetails where Id = @count
End
else
Begin
insert into DeviceDetails(DeviceToken,DeviceType,CreatedDate) values(@DeviceId,@DeviceType,GetUtcDate())
set @count = @@Identity
select * from DeviceDetails where Id = @count
End
end





GO
/****** Object:  StoredProcedure [dbo].[CheckForDeviceTokenANDInsert]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CheckForDeviceTokenANDInsert]  --'shaikabdulosman@gmail.com'
@DeviceId int
as
begin
declare @count int
if exists (select 1 from userslookup where DeviceId = @DeviceId AND (emailId is null OR emailId = '') )
Begin
set @count= 0
select @count as Id
End
else
Begin
insert into userslookup(DeviceId,CreatedDate) values(@DeviceId,GetUtcDate())
set @count = @@Identity
select @count as Id
End
end





GO
/****** Object:  StoredProcedure [dbo].[CheckForUserAndInsert]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CheckForUserAndInsert]  --'Elitecrest8@gmail.com',8
@emailId nvarchar(255),
@DeviceId int
as
begin
declare @emailIdcount int
declare @count int
set @emailIdcount = (select count(*) from userslookup where emailid = @emailId)
if (@emailIdcount > 0)
Begin
select @count= Id from userslookup where EmailId = @emailId
select @count as Id, 0 as AlreadyExists
End
else if (@emailIdcount = 0)
Begin
insert into userslookup(emailId, DeviceId, CreatedDate) values(@emailId, @DeviceId, GetUtcDAte())
set @count = @@Identity
select @count as Id, 1 as AlreadyExists
End
end




GO
/****** Object:  StoredProcedure [dbo].[spCheckForDevice]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spCheckForDevice] --2
@DeviceId int
as

begin
declare @Id int
declare @count int
select @count = count(*) from userslookup where DeviceId=@DeviceId and emailId is null
if(@count > 0)
begin
select @Id = Id from userslookup where DeviceId=@DeviceId and emailId is null
select @Id as Id, @count as userexists
end
else
begin
set @Id = 0
select @Id as Id, @count as userexists
end
end




GO
/****** Object:  StoredProcedure [dbo].[spCheckForUser]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spCheckForUser] --'mrkiran@gmail.com'
@emailId nvarchar(255)
as

begin
declare @Id int
declare @count int
select @count = count(*) from userslookup where emailid=@emailId
if(@count > 0)
begin
select @Id = Id from userslookup where emailid = @emailId
select @Id as Id, @count as userexists
end
else
begin
set @Id = 0
select @Id as Id, @count as userexists
end
end



GO
/****** Object:  StoredProcedure [dbo].[spCheckForUserAndInsert]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spCheckForUserAndInsert]  --'mahesh123@gmail.com'
@emailId nvarchar(255),
@DeviceId int
as
begin
declare @count int
if exists (select 1 from userslookup where EmailId = @emailId )
Begin
select @count= Id from userslookup where EmailId = @emailId
select @count as Id
End
else
Begin
insert into userslookup(emailId, DeviceId, CreatedDate) values(@emailId, @DeviceId, GetUtcDAte())
set @count = @@Identity
select @count as Id
End
end


GO
/****** Object:  UserDefinedFunction [__ShardManagement].[fnGetStoreVersionMajorGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create function [__ShardManagement].[fnGetStoreVersionMajorGlobal]()
returns int
as
begin
	return (select StoreVersionMajor from __ShardManagement.ShardMapManagerGlobal)
end

GO
/****** Object:  Table [__ShardManagement].[OperationsLogGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[OperationsLogGlobal](
	[OperationId] [uniqueidentifier] NOT NULL,
	[OperationCode] [int] NOT NULL,
	[Data] [xml] NOT NULL,
	[UndoStartState] [int] NOT NULL DEFAULT ((100)),
	[ShardVersionRemoves] [uniqueidentifier] NULL,
	[ShardVersionAdds] [uniqueidentifier] NULL,
 CONSTRAINT [pkOperationsLogGlobal_OperationId] PRIMARY KEY CLUSTERED 
(
	[OperationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [__ShardManagement].[ShardedDatabaseSchemaInfosGlobal]    Script Date: 21-12-2016 16:58:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[ShardedDatabaseSchemaInfosGlobal](
	[Name] [nvarchar](128) NOT NULL,
	[SchemaInfo] [xml] NOT NULL,
 CONSTRAINT [pkShardedDatabaseSchemaInfosGlobal_Name] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [__ShardManagement].[ShardMapManagerGlobal]    Script Date: 21-12-2016 16:58:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[ShardMapManagerGlobal](
	[StoreVersionMajor] [int] NOT NULL,
	[StoreVersionMinor] [int] NOT NULL,
 CONSTRAINT [pkShardMapManagerGlobal_StoreVersionMajor] PRIMARY KEY CLUSTERED 
(
	[StoreVersionMajor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [__ShardManagement].[ShardMappingsGlobal]    Script Date: 21-12-2016 16:58:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [__ShardManagement].[ShardMappingsGlobal](
	[MappingId] [uniqueidentifier] NOT NULL,
	[Readable] [bit] NOT NULL,
	[ShardId] [uniqueidentifier] NOT NULL,
	[ShardMapId] [uniqueidentifier] NOT NULL,
	[OperationId] [uniqueidentifier] NULL,
	[MinValue] [varbinary](128) NOT NULL,
	[MaxValue] [varbinary](128) NULL,
	[Status] [int] NOT NULL,
	[LockOwnerId] [uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
 CONSTRAINT [pkShardMappingsGlobal_ShardMapId_MinValue_Readable] PRIMARY KEY CLUSTERED 
(
	[ShardMapId] ASC,
	[MinValue] ASC,
	[Readable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [__ShardManagement].[ShardMapsGlobal]    Script Date: 21-12-2016 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[ShardMapsGlobal](
	[ShardMapId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ShardMapType] [int] NOT NULL,
	[KeyType] [int] NOT NULL,
 CONSTRAINT [pkShardMapsGlobal_ShardMapId] PRIMARY KEY CLUSTERED 
(
	[ShardMapId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [__ShardManagement].[ShardsGlobal]    Script Date: 21-12-2016 16:58:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [__ShardManagement].[ShardsGlobal](
	[ShardId] [uniqueidentifier] NOT NULL,
	[Readable] [bit] NOT NULL,
	[Version] [uniqueidentifier] NOT NULL,
	[ShardMapId] [uniqueidentifier] NOT NULL,
	[OperationId] [uniqueidentifier] NULL,
	[Protocol] [int] NOT NULL,
	[ServerName] [nvarchar](128) NOT NULL,
	[Port] [int] NOT NULL,
	[DatabaseName] [nvarchar](128) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [pkShardsGlobal_ShardId] PRIMARY KEY CLUSTERED 
(
	[ShardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[DeviceDetails]    Script Date: 21-12-2016 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeviceDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DeviceToken] [nvarchar](1024) NOT NULL,
	[DeviceType] [int] NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [dbo].[UsersLookUp]    Script Date: 21-12-2016 16:58:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersLookUp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[emailId] [nvarchar](255) NULL,
	[DeviceId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LoginType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
INSERT [__ShardManagement].[ShardedDatabaseSchemaInfosGlobal] ([Name], [SchemaInfo]) VALUES (N'UserIdShardMap', N'<Schema xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><ReferenceTableSet i:type="ArrayOfReferenceTableInfo"><ReferenceTableInfo><SchemaName>dbo</SchemaName><TableName>Sports</TableName></ReferenceTableInfo><ReferenceTableInfo><SchemaName>dbo</SchemaName><TableName>Libararies</TableName></ReferenceTableInfo></ReferenceTableSet><ShardedTableSet i:type="ArrayOfShardedTableInfo"><ShardedTableInfo><SchemaName>dbo</SchemaName><TableName>Users</TableName><KeyColumnName>UserId</KeyColumnName></ShardedTableInfo></ShardedTableSet></Schema>')
INSERT [__ShardManagement].[ShardMapManagerGlobal] ([StoreVersionMajor], [StoreVersionMinor]) VALUES (1, 2)
INSERT [__ShardManagement].[ShardMappingsGlobal] ([MappingId], [Readable], [ShardId], [ShardMapId], [OperationId], [MinValue], [MaxValue], [Status], [LockOwnerId]) VALUES (N'57518a61-da43-4b4e-9aae-5e2cc1555a43', 1, N'7ea4c450-ebc8-4c6a-addc-1b431f07398a', N'd6fcafac-814b-4547-ab64-1f986b044f8e', NULL, 0x80000000, 0x80001388, 1, N'00000000-0000-0000-0000-000000000000')
INSERT [__ShardManagement].[ShardMappingsGlobal] ([MappingId], [Readable], [ShardId], [ShardMapId], [OperationId], [MinValue], [MaxValue], [Status], [LockOwnerId]) VALUES (N'03b7dab5-26f7-41df-addc-25ad55d91daf', 1, N'c637e34b-7746-44f9-9400-c539eeb7907e', N'd6fcafac-814b-4547-ab64-1f986b044f8e', NULL, 0x80001388, 0x80002710, 1, N'00000000-0000-0000-0000-000000000000')
INSERT [__ShardManagement].[ShardMapsGlobal] ([ShardMapId], [Name], [ShardMapType], [KeyType]) VALUES (N'd6fcafac-814b-4547-ab64-1f986b044f8e', N'UserIdShardMap', 2, 1)
INSERT [__ShardManagement].[ShardsGlobal] ([ShardId], [Readable], [Version], [ShardMapId], [OperationId], [Protocol], [ServerName], [Port], [DatabaseName], [Status]) VALUES (N'7ea4c450-ebc8-4c6a-addc-1b431f07398a', 1, N'5d01064d-7909-4451-8850-67cf39f2b433', N'd6fcafac-814b-4547-ab64-1f986b044f8e', NULL, 0, N'graa3njllc.database.windows.net,1433', 0, N'ElasticScaleStarterKit_Shard0', 1)
INSERT [__ShardManagement].[ShardsGlobal] ([ShardId], [Readable], [Version], [ShardMapId], [OperationId], [Protocol], [ServerName], [Port], [DatabaseName], [Status]) VALUES (N'c637e34b-7746-44f9-9400-c539eeb7907e', 1, N'ea142c1a-a353-415a-812e-1debde3ad9e8', N'd6fcafac-814b-4547-ab64-1f986b044f8e', NULL, 0, N'graa3njllc.database.windows.net,1433', 0, N'ElasticScaleStarterKit_Shard1', 1)
SET IDENTITY_INSERT [dbo].[DeviceDetails] ON 

INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (1, N'352192073542002', 2, CAST(N'2016-12-19 11:13:14.083' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (2, N'359283050383958', 2, CAST(N'2016-12-19 11:26:08.373' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (3, N'D5EFDFB3-06C0-430B-B176-5E97204406A6', 1, CAST(N'2016-12-19 12:01:42.820' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (4, N'867867020003160', 2, CAST(N'2016-12-19 12:12:46.180' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (5, N'862738033243918', 2, CAST(N'2016-12-19 12:28:59.147' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (6, N'FDA3A2BF-9699-41BA-870C-2CC1095402B5', 1, CAST(N'2016-12-19 21:17:55.340' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (7, N'990007036310636', 2, CAST(N'2016-12-19 23:07:14.330' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (8, N'D02A4985-B360-4F1E-AB85-34A97BD53207', 1, CAST(N'2016-12-19 23:09:46.590' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (9, N'353881071900464', 2, CAST(N'2016-12-20 00:24:21.503' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (10, N'911464850038528', 2, CAST(N'2016-12-20 06:11:04.383' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (11, N'8141DF4E-07C2-4800-AF5C-8E4BF129D69A', 1, CAST(N'2016-12-20 07:18:32.590' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (12, N'7E34D5B9-6C98-47C8-993B-9FE0F88CFD86', 1, CAST(N'2016-12-20 07:50:40.673' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (13, N'354981050028714', 2, CAST(N'2016-12-20 09:59:15.377' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (14, N'715126813f915de1', 2, CAST(N'2016-12-20 12:36:43.520' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (15, N'7C08FC1F-E2BC-4192-9619-E9C227434B11', 1, CAST(N'2016-12-20 17:03:58.007' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (16, N'5653BBE2-6603-4053-BD2E-20B921B77525', 1, CAST(N'2016-12-20 17:56:38.170' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (17, N'F3922D89-8369-4AEB-A881-D6DC6F4F8A11', 1, CAST(N'2016-12-20 21:24:39.673' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (18, N'8D269CB7-A3C5-4541-8A6E-53EF14B78D81', 1, CAST(N'2016-12-21 00:13:08.673' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (19, N'24397576-3A77-4DE3-A399-CD47932ECE9B', 1, CAST(N'2016-12-21 01:19:58.427' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (20, N'98BAE7CD-1109-4D29-A553-DBD729CD2C3D', 1, CAST(N'2016-12-21 01:22:24.390' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (21, N'36CE381E-8DB7-4D3F-9016-84EE15AA91D4', 1, CAST(N'2016-12-21 01:23:45.343' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (22, N'4F5B8C4B-8550-44B3-8989-69593ECC2451', 1, CAST(N'2016-12-21 01:25:47.453' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (23, N'107E3BF3-3E33-4D4D-9A3C-05B62294131D', 1, CAST(N'2016-12-21 04:56:15.350' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (24, N'352829068006705', 2, CAST(N'2016-12-21 05:17:43.040' AS DateTime))
INSERT [dbo].[DeviceDetails] ([Id], [DeviceToken], [DeviceType], [CreatedDate]) VALUES (25, N'7C52EF6F-4201-45DC-BB75-EB8322F10BAA', 1, CAST(N'2016-12-21 07:54:09.670' AS DateTime))
SET IDENTITY_INSERT [dbo].[DeviceDetails] OFF
SET IDENTITY_INSERT [dbo].[UsersLookUp] ON 

INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (3, NULL, 17, CAST(N'2016-12-19 11:22:51.930' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (4, NULL, 2, CAST(N'2016-12-19 11:26:16.183' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (5, NULL, 3, CAST(N'2016-12-19 12:02:42.907' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (6, N'elitecrest8@gmail.com', 3, CAST(N'2016-12-19 12:04:56.740' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (10, N'tpidave@gmail.com', 6, CAST(N'2016-12-19 21:20:14.573' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (11, NULL, 7, CAST(N'2016-12-19 23:07:24.703' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (12, NULL, 8, CAST(N'2016-12-19 23:09:51.997' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (13, N'doisogo@hotmail.com', 9, CAST(N'2016-12-20 00:24:40.110' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (17, N'elitecrest7@gmail.com', 8, CAST(N'2016-12-20 06:24:27.870' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (18, N'Testgjjfc@gmail.com', 34, CAST(N'2016-12-20 06:36:20.293' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (19, NULL, 11, CAST(N'2016-12-20 07:18:48.413' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (20, N'elitecrest54@gmail.com', 10, CAST(N'2016-12-20 07:32:04.377' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (21, N'elitecrest14@gmail.com', 12, CAST(N'2016-12-20 07:57:07.850' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (22, NULL, 12, CAST(N'2016-12-20 08:49:33.013' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (23, NULL, 4, CAST(N'2016-12-20 08:50:39.147' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (24, N'Satyap@gmail.com', 3, CAST(N'2016-12-20 09:56:50.360' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (25, N'elitecrest9@gmail.com', 12, CAST(N'2016-12-20 10:01:16.483' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (26, N'kamalsai24@gmail.com', 13, CAST(N'2016-12-20 10:14:44.757' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (27, N'elitecrest16@gmail.com', 12, CAST(N'2016-12-20 10:34:56.053' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (28, N'theju254@gmail.com', 12, CAST(N'2016-12-20 11:58:24.997' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (29, N'elitecrest15@gmail.com', 4, CAST(N'2016-12-20 12:13:05.140' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (30, N'elitecrest20@gmail.com', 12, CAST(N'2016-12-20 12:25:13.393' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (31, N'elitecrest19@gmail.com', 12, CAST(N'2016-12-20 12:30:03.440' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (32, N'elitecrest18@gmail.com', 4, CAST(N'2016-12-20 12:38:23.837' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (33, N'puramamaheshbabu@gmail.com', 12, CAST(N'2016-12-20 12:39:12.977' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (34, N'elitecrest17@gmail.com', 4, CAST(N'2016-12-20 12:41:05.730' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (35, N'crestelite@gmail.com', 3, CAST(N'2016-12-20 12:41:40.653' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (36, N'chakri.crazy4@gmail.com', 12, CAST(N'2016-12-20 12:41:59.310' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (37, N'elitecrest1.2@gmail.com', 3, CAST(N'2016-12-20 12:45:19.393' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (38, N'elitecrest1.1@gmail.com', 4, CAST(N'2016-12-20 12:53:55.167' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (39, N'daileyr@campbell.edu', 15, CAST(N'2016-12-20 17:04:41.913' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (40, NULL, 16, CAST(N'2016-12-20 17:59:07.327' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (41, N'blair42379@yahoo.com', 18, CAST(N'2016-12-21 00:14:52.407' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (42, NULL, 19, CAST(N'2016-12-21 01:20:42.290' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (43, NULL, 23, CAST(N'2016-12-21 04:59:54.210' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (44, NULL, 24, CAST(N'2016-12-21 05:24:38.560' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (45, N'elitecrest2@gmail.com', 4, CAST(N'2016-12-21 08:09:13.310' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (46, N'elitecrest3@gmail.com', 4, CAST(N'2016-12-21 08:56:23.137' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (47, N'elitecrest10@gmail.com', 4, CAST(N'2016-12-21 08:58:15.020' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (48, N'elitecres20@gmail.com', 25, CAST(N'2016-12-21 09:18:38.217' AS DateTime), NULL)
INSERT [dbo].[UsersLookUp] ([Id], [emailId], [DeviceId], [CreatedDate], [LoginType]) VALUES (49, NULL, 25, CAST(N'2016-12-21 09:27:41.807' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[UsersLookUp] OFF
/****** Object:  Index [ucShardMappingsGlobal_MappingId]    Script Date: 21-12-2016 16:58:40 ******/
ALTER TABLE [__ShardManagement].[ShardMappingsGlobal] ADD  CONSTRAINT [ucShardMappingsGlobal_MappingId] UNIQUE NONCLUSTERED 
(
	[MappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ucShardMapsGlobal_Name]    Script Date: 21-12-2016 16:58:40 ******/
ALTER TABLE [__ShardManagement].[ShardMapsGlobal] ADD  CONSTRAINT [ucShardMapsGlobal_Name] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ucShardsGlobal_Location]    Script Date: 21-12-2016 16:58:40 ******/
ALTER TABLE [__ShardManagement].[ShardsGlobal] ADD  CONSTRAINT [ucShardsGlobal_Location] UNIQUE NONCLUSTERED 
(
	[ShardMapId] ASC,
	[Protocol] ASC,
	[ServerName] ASC,
	[DatabaseName] ASC,
	[Port] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
ALTER TABLE [__ShardManagement].[ShardMappingsGlobal]  WITH CHECK ADD  CONSTRAINT [fkShardMappingsGlobal_ShardId] FOREIGN KEY([ShardId])
REFERENCES [__ShardManagement].[ShardsGlobal] ([ShardId])
GO
ALTER TABLE [__ShardManagement].[ShardMappingsGlobal] CHECK CONSTRAINT [fkShardMappingsGlobal_ShardId]
GO
ALTER TABLE [__ShardManagement].[ShardMappingsGlobal]  WITH CHECK ADD  CONSTRAINT [fkShardMappingsGlobal_ShardMapId] FOREIGN KEY([ShardMapId])
REFERENCES [__ShardManagement].[ShardMapsGlobal] ([ShardMapId])
GO
ALTER TABLE [__ShardManagement].[ShardMappingsGlobal] CHECK CONSTRAINT [fkShardMappingsGlobal_ShardMapId]
GO
ALTER TABLE [__ShardManagement].[ShardsGlobal]  WITH CHECK ADD  CONSTRAINT [fkShardsGlobal_ShardMapId] FOREIGN KEY([ShardMapId])
REFERENCES [__ShardManagement].[ShardMapsGlobal] ([ShardMapId])
GO
ALTER TABLE [__ShardManagement].[ShardsGlobal] CHECK CONSTRAINT [fkShardsGlobal_ShardMapId]
GO
USE [master]
GO
ALTER DATABASE [ElasticScaleStarterKit_ShardMapManagerDb] SET  READ_WRITE 
GO
