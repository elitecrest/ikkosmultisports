-- Copyright (c) Microsoft. All rights reserved.
-- Licensed under the MIT license. See LICENSE file in the project root for full license information.

---- Reference table that contains the same data on all shards
--IF OBJECT_ID('Regions', 'U') IS NULL 
--BEGIN
--    CREATE TABLE [Regions] (
--        [RegionId] [int] NOT NULL,
--        [Name] [nvarchar](256) NOT NULL
--     CONSTRAINT [PK_Regions_RegionId] PRIMARY KEY CLUSTERED (
--        [RegionId] ASC
--     ) 
--    ) 

--	INSERT INTO [Regions] ([RegionId], [Name]) VALUES (0, 'North America')
--	INSERT INTO [Regions] ([RegionId], [Name]) VALUES (1, 'South America')
--	INSERT INTO [Regions] ([RegionId], [Name]) VALUES (2, 'Europe')
--	INSERT INTO [Regions] ([RegionId], [Name]) VALUES (3, 'Asia')
--	INSERT INTO [Regions] ([RegionId], [Name]) VALUES (4, 'Africa')
--	INSERT INTO [Regions] ([RegionId], [Name]) VALUES (5, 'Oceania')
--END
--GO

---- Reference table that contains the same data on all shards
--IF OBJECT_ID('Products', 'U') IS NULL 
--BEGIN
--    CREATE TABLE [Products] (
--        [ProductId] [int] NOT NULL,
--        [Name] [nvarchar](256) NOT NULL
--     CONSTRAINT [PK_Products_ProductId] PRIMARY KEY CLUSTERED (
--        [ProductId] ASC
--     ) 
--    ) 

--	INSERT INTO [Products] ([ProductId], [Name]) VALUES (0, 'Gizmos')
--	INSERT INTO [Products] ([ProductId], [Name]) VALUES (1, 'Widgets')
--END
--GO

---- Sharded table containing our sharding key (CustomerId)
--IF OBJECT_ID('Customers', 'U') IS NULL 
--    CREATE TABLE [Customers] (
--        [CustomerId] [int] NOT NULL, -- since we shard on this column, it cannot be an IDENTITY
--        [Name] [nvarchar](256) NOT NULL,
--        [RegionId] [int] NOT NULL
--     CONSTRAINT [PK_Customer_CustomerId] PRIMARY KEY CLUSTERED (
--        [CustomerID] ASC
--     ),
--     CONSTRAINT [FK_Customer_RegionId] FOREIGN KEY (
--        [RegionId] 
--     ) REFERENCES [Regions]([RegionId])
--    ) 
--GO

---- Sharded table that has a foreign key column containing our sharding key (CustomerId)
--IF OBJECT_ID('Orders', 'U') IS NULL 
--    CREATE TABLE [Orders](
--        [CustomerId] [int] NOT NULL, -- since we shard on this column, it cannot be an IDENTITY
--        [OrderId] [int] NOT NULL IDENTITY(1,1), 
--        [OrderDate] [datetime] NOT NULL,
--        [ProductId] [int] NOT NULL
--     CONSTRAINT [PK_Orders_CustomerId_OrderId] PRIMARY KEY CLUSTERED (
--        [CustomerID] ASC,
--        [OrderID] ASC
--     ),
--     CONSTRAINT [FK_Orders_CustomerId] FOREIGN KEY (
--        [CustomerId] 
--     ) REFERENCES [Customers]([CustomerId]),
--     CONSTRAINT [FK_Orders_ProductId] FOREIGN KEY (
--        [ProductId] 
--     ) REFERENCES [Products]([ProductId])
--    ) 
--GO




IF OBJECT_ID('Sports', 'U') IS NULL 
BEGIN
    CREATE TABLE [Sports] (
        [SportId] [int] NOT NULL,
        [SportName] [nvarchar](256) NOT NULL
     CONSTRAINT [PK_Sports_SportId] PRIMARY KEY CLUSTERED (
        [SportId] ASC
     ) 
    ) 

	INSERT INTO [Sports] ([SportId], [SportName]) VALUES (0, 'Swimming')
	INSERT INTO [Sports] ([SportId], [SportName]) VALUES (1, 'Golf')
	INSERT INTO [Sports] ([SportId], [SportName]) VALUES (2, 'Soccer')
	INSERT INTO [Sports] ([SportId], [SportName]) VALUES (3, 'Lacrosse')
	INSERT INTO [Sports] ([SportId], [SportName]) VALUES (4, 'Football')
	INSERT INTO [Sports] ([SportId], [SportName]) VALUES (5, 'Baseball')
END
GO

-- Reference table that contains the same data on all shards
IF OBJECT_ID('Libraries', 'U') IS NULL 
BEGIN
    CREATE TABLE [Libraries] (
        [LibraryId] [int] NOT NULL,
        [LibraryName] [nvarchar](256) NOT NULL,
		[SportId] [int] NOT NULL,

     CONSTRAINT [PK_Libraries_LibraryId] PRIMARY KEY CLUSTERED (
        [LibraryId] ASC
     ),
	CONSTRAINT [FK_Libraries_SportId] FOREIGN KEY (
        [SportId] 
     ) REFERENCES [Sports]([SportId])
    )  

	--INSERT INTO [Libraries] ([LibraryId], [LibraryName],[SportId]) VALUES (0, 'King Aquatics',0)
	--INSERT INTO [Libraries] ([LibraryId], [LibraryName],[SportId]) VALUES (1, 'TPI',1)
END
GO


-- Sharded table containing our sharding key (CustomerId)
IF OBJECT_ID('Users', 'U') IS NULL 
    CREATE TABLE [Users] (
        [UserId] [int] NOT NULL, -- since we shard on this column, it cannot be an IDENTITY
        [Name] [nvarchar](256) NOT NULL,
		
		[LibraryId] [int] NOT NULL

     CONSTRAINT [PK_Users_UserId] PRIMARY KEY CLUSTERED (
        [UserId] ASC
     ),
     CONSTRAINT [FK_Users_LibraryId] FOREIGN KEY (
        [LibraryId] 
     ) REFERENCES [Libraries]([LibraryId])
    ) 
GO



---- Sharded table containing our sharding key (CustomerId)
--IF OBJECT_ID('Customers', 'U') IS NULL 
--    CREATE TABLE [Customers] (
--        [CustomerId] [int] NOT NULL, -- since we shard on this column, it cannot be an IDENTITY
--        [Name] [nvarchar](256) NOT NULL,
--        [RegionId] [int] NOT NULL
--     CONSTRAINT [PK_Customer_CustomerId] PRIMARY KEY CLUSTERED (
--        [CustomerID] ASC
--     ),
--     CONSTRAINT [FK_Customer_RegionId] FOREIGN KEY (
--        [RegionId] 
--     ) REFERENCES [Regions]([RegionId])
--    ) 
--GO






---- Sharded table that has a foreign key column containing our sharding key (CustomerId)
--IF OBJECT_ID('Orders', 'U') IS NULL 
--    CREATE TABLE [Orders](
--        [CustomerId] [int] NOT NULL, -- since we shard on this column, it cannot be an IDENTITY
--        [OrderId] [int] NOT NULL IDENTITY(1,1), 
--        [OrderDate] [datetime] NOT NULL,
--        [ProductId] [int] NOT NULL
--     CONSTRAINT [PK_Orders_CustomerId_OrderId] PRIMARY KEY CLUSTERED (
--        [CustomerID] ASC,
--        [OrderID] ASC
--     ),
--     CONSTRAINT [FK_Orders_CustomerId] FOREIGN KEY (
--        [CustomerId] 
--     ) REFERENCES [Customers]([CustomerId]),
--     CONSTRAINT [FK_Orders_ProductId] FOREIGN KEY (
--        [ProductId] 
--     ) REFERENCES [Products]([ProductId])
--    ) 
--GO

