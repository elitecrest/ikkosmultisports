For creating SubAdmin table:

create table SubAdmin
(
SubAdminId int Identity(1,1) Primary key,
AdminId int not null,
EmailAddress varchar(100),
Password varchar(50),
CreatedDate DateTime,
UpdatedDate DateTime
)

CREATE PROCEDURE [dbo].[CreateSubAdmin] 
	-- Add the parameters for the stored procedure here
	@AdminId int,
	@EmailAddress varchar(100),
	@Password varchar(50)
	
AS
BEGIN
declare @SubAdminId int
declare @Emailcount int

set @Emailcount = (select count(*) from SubAdmin where EmailAddress = @EmailAddress and AdminId = @AdminId)
if(@Emailcount > 0)
Begin
 set @subadminId = 0
 select @subadminId as result
End
else
Begin
	insert into SubAdmin(AdminId,EmailAddress,[Password],CreatedDate)
	values(@AdminId, @EmailAddress, @Password, Getutcdate())
	set @SubAdminId = @@Identity

	select @subadminId as result
End
	
END

GO


CREATE PROCEDURE GetSubAdminsByAdmin 
	-- Add the parameters for the stored procedure here
	@AdminId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT S.SubAdminId, L.LibraryName as ChannelName, S.AdminId, S.EmailAddress, S.[Password], S.CreatedDate from SubAdmin S inner join Libraries L on L.LibraryId = S.AdminId
	where adminId = @AdminId
END
GO



CREATE PROCEDURE GetAllChannels 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT libraryId as ChannelId, libraryname as Channelname from libraries
END
GO


-- ==========================================================
-- Create Stored Procedure Template for SQL Azure Database
-- ==========================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE LoginSubAdmin 
	-- Add the parameters for the stored procedure here
	@EmailAddress varchar(100),
	@Password varchar(50),
	@ChannelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @adminexists int
	declare @subadminId int
	declare @emailCount int

	set @adminexists = (select count(*) from SubAdmin where EmailAddress = @EmailAddress AND [Password] = @Password AND AdminId = @ChannelId)

	if(@adminexists > 0)
	Begin
	set @subadminId = (select SubAdminId from SubAdmin where EmailAddress = @EmailAddress AND [Password] = @Password AND AdminId = @ChannelId)
	select @subadminId as SubAdminId, 1 as AdminExists
	End
	else if(@adminexists = 0)
	Begin
	set @emailCount = (select SubAdminId from SubAdmin where EmailAddress = @EmailAddress AND [Password] = @Password)
	if(@emailCount > 0)
	Begin
	select @emailCount as SubAdminId, -1 as AdminExists
	End
	set @emailCount = (select SubAdminId from SubAdmin where EmailAddress = @EmailAddress AND AdminId = @ChannelId)
	if(@emailCount > 0)
	Begin
	select @emailCount as SubAdminId, -2 as AdminExists
	End
	End
	else 
	Begin
	select 0 as SubAdminId, -3 as AdminExists
	End
END
GO



CREATE PROCEDURE GetChannelBySubAdmin 
	-- Add the parameters for the stored procedure here
	@EmailAddress varchar(100),
	@Password varchar(50),
	@ChannelId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



select L.LibraryId, L.LibraryName as ChannelName,L.EmailAddress,L.SportId, S.SportName, L.FirstName, L.LastName, L.[Password],
	L.PricingCatalogId, L.LibraryImageUrl as ProfilePicURL, L.[Description] 
	from Libraries L inner join Sports S on L.SportId = S.SportId
	left join LibraryIntrovideos LV on L.LibraryId = LV.LibraryId
	 where L.LibraryId = @ChannelId
END
GO


create table VideoCompare
(
VideoId int Identity(1,1) Primary key,
IdealVideoId int,
RecordVideoId varchar(100),
PackageId int,
UserId int,
VideoUrl varchar(512),
ThumbNailUrl varchar(512),
UserName varchar(512),
[Format] varchar(50),
Duration int,
CreatedDate DateTime,
UpdatedDate DateTime
)



CREATE PROCEDURE [dbo].[UploadVideoToCmp]
	-- Add the parameters for the stored procedure here
	@VideoId int,
	@IdealVideoId int,
	@RecordVideoId varchar(100),
	@PackageId int,
	@ThumbNailUrl varchar(512),
	@VideoUrl varchar(512),
	@UserId int,
	@UserName varchar(512),
	@Duration int,
	@Format varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	declare @Id int
insert into VideoCompare(IdealVideoId,RecordVideoId,PackageId,VideoUrl,ThumbNailUrl,CreatedDate,UserId, UserName, [Format],duration)
values(@IdealVideoId,@RecordVideoId,@PackageId,@VideoUrl, @ThumbNailUrl, getutcdate(), @UserId, @UserName, @Format, @Duration)
set @Id = @@Identity

select @Id as result
END


GO


CREATE PROCEDURE [dbo].[GetLibraryVideos] 
	-- Add the parameters for the stored procedure here
	@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select VC.VideoId, VC.IdealVideoId, VC. RecordVideoId, VC.PackageId, VC.VideoUrl, VC.ThumbNailUrl,
VC.UserId, VC.UserName, VC.[Format], VC.Duration, V.VideoURL as IdealVideoUrl, V.ThumbNailUrl as IdealThumbNailUrl, 
V.MovementName as IdealVideoName, V.ActorName as ActorName from VideoCompare VC inner join Videos V on V.ID = VC.IdealVideoId
where VC.UserId = @UserId
END

GO

*****************************************************************************************************************************

alter table libraries
add LoginType int


ALTER PROCEDURE [dbo].[GetAdminUserInfo] --'sai@gmail.com','123456',10000,0
	-- Add the parameters for the stored procedure here
	@EmailAddress Varchar(50),
	@Password varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @UserCount int
    -- Insert statements for procedure here
	--set @UserCount = (select count(*) from Parents where ChurchId = @ChurchId AND EmailAddress = @EmailAddress AND [Password] = @Password)

	
	select L.LibraryId, L.LibraryName as ChannelName,L.EmailAddress,L.SportId, S.SportName, L.FirstName, L.LastName, L.[Password],
	L.PricingCatalogId, L.LibraryImageUrl as ProfilePicURL, L.[Description], L.LoginType
	from Libraries L inner join Sports S on L.SportId = S.SportId
	left join LibraryIntrovideos LV on L.LibraryId = LV.LibraryId
	 where EmailAddress = @EmailAddress AND [Password] = @Password
	
END


GO


alter table Videos
add LoginType int


ALTER PROCEDURE [dbo].[CreateAdminUser] 
	-- Add the parameters for the stored procedure here
	@Id int,
	@FirstName varchar(100),
	@LastName varchar(100),
	@ChannelName varchar(100),
	@EmailAddress varchar(100),
	@Password varchar(512),
	@SportId int,
	@ProfilePicUrl varchar(512),
	@VideoUrl varchar(1024),
	@ThumbNailUrl varchar(1024),
	@description varchar(1024),
	@PricingCatalogId int,
	@AuthCode nvarchar(512),
	@LoginType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @userexists int
declare @count int
declare @videoId int
declare @pricingcatId int
declare @channelId int
    -- Insert statements for procedure here
	set @count = (select count(*) from Libraries where EmailAddress = @EmailAddress)
	set @ChannelId = (select top 1 libraryid from libraries order by libraryid desc)
	if(@count > 0)
	Begin
	set @Id = 0
	select @count as UserExists, @Id as UserId
	End
	else
	Begin
	insert into Libraries(LibraryName,FirstName, LastName, SportId, EmailAddress,[Password],CreatedDate,IsPackageAvailable,LibraryImageUrl,IsPaid,PricingCatalogId, AuthCode, [Description], LoginType)
	values(@ChannelName,@FirstName,@LastName,@SportId,@EmailAddress,@Password,GetUtcDate(),0,@ProfilePicUrl, 0,@PricingCatalogId,@AuthCode,@description,@LoginType)
	set @Id = @@Identity
	
	set @pricingcatId = (select PricingCatalogId from libraries where libraryid = @ChannelId)
	if(@PricingCatalogId > 0)
	Begin	
	update libraries
	set pricingcatalogId = @pricingcatId + 1
	where libraryId = @Id
	End
	else
	Begin
	update libraries
	set pricingcatalogId = 13
	where libraryId = @Id
	End
	insert into ChannelSports(ChannelId, SportId, IsDefault, CreatedDate)
	values(@Id,@SportId,1,getutcdate())
	if(LEN(@VideoUrl) > 0)
	Begin
	insert into videos(MovementName, ActorName, [Format], VideoURL, ThumbNailUrl,[Status], CreatedBy, CreateDate,Playcount,VideoType, VideoMode, SportId, LoginType)
	values('Channel Intro Video', 'Channel', 'mp4',@VideoUrl,@ThumbNailUrl,1,@Id,getutcdate(),1,2,1,@SportId, @LoginType)
	set @videoId = @@Identity

	insert into libraryintrovideos(VideoId,LibraryId, CreatedDate, IsIntroVideo)
	values(@videoId,@Id,GetUtcDate(),1)
	End
	-- Update the sport details

	update sports
	set IsGreyOut = 1
	where sportId = @SportId

	
	select @count as UserExists, @Id as UserId
	End
END
GO


ALTER PROCEDURE [dbo].[GetVideos] --25
	-- Add the parameters for the stored procedure here
	@UserId int,
	@SportId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select ID, MovementName, ActorName, [Format], VideoURL, ThumbNailURL,[Status], CreateDate, PlayCount, IntroVideoUrl, VideoMode, Logintype
	from videos where ((createdby = @UserId) and (VideoType = 0 or VideoType = 1) and SportId = @SportId) 
    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>
END

GO

ALTER PROCEDURE [dbo].[UpdateAdminVideo] 
	-- Add the parameters for the stored procedure here
	@MomentName varchar(50),
	@CreatedBy int,
	@VideoMode int,
	@ActorName varchar(50),
	@Id int,
	@SportId int,
	@LoginType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	update Videos
	set MovementName =@MomentName, ActorName = @ActorName,
	 [Format] = 'mp4', [Status] = 1,
	VideoMode = @VideoMode,
	 UpdatedBy = @CreatedBy, UpdateDate = GetUtcDate(), PlayCount = 30,
	 LoginType = @LoginType
	 where Id = @Id
	
	
	Select @Id as Result
	
	
END

GO



ALTER PROCEDURE [dbo].[CreateAdminVideo] 
	-- Add the parameters for the stored procedure here
	@VideoURL varchar(1024),
	@MomentName varchar(50),
	@CreatedBy int,
	@ThumbNailURL varchar(1024),
	@VideoMode int,
	@Id int,
	@SportId int,
	@LoginType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if(@Id > 0)
	Begin
	set @Id = 0
	select @Id as Result
	End
	else
	Begin
	insert into Videos(MovementName, ActorName, [Format], VideoURL, ThumbNailURL, [Status], CreatedBy, CreateDate, PlayCount, videotype, VideoMode, SportId, LoginType)
	values(@MomentName,'Athlete','mp4',@VideoURL,@ThumbNailURL,1,@CreatedBy,GetUtcDate(),30,1, @VideoMode, @SportId, @LoginType)
	set @Id = @@Identity
	Select @Id as Result
	End
	
END
GO


alter table package
add LoginType int


ALTER PROCEDURE [dbo].[UpdatePackage]
	-- Add the parameters for the stored procedure here
	@VideoURL varchar(1024),
	@CreatedBy int,
	@ThumbNailURL varchar(1024),
	@PackageName varchar(256),
	@Description nvarchar(max),
	@PricingCatalogId int,
	@VideoId int,
	@Id int,
	@SportId int,
	@LoginType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	if (@PricingCatalogId > 0)
	Begin
	update package
	set PackageName = @PackageName,
	[Description] = @Description,
	PricingCatalogId = 15,
	UpdateDate = getutcdate(),
	LoginType = @LoginType
	where PackageId = @Id
	End
	else
	Begin
	update package
	set PackageName = @PackageName,
	[Description] = @Description,
	PricingCatalogId = 14,
	IsPaid = 1,
	UpdateDate = getutcdate(),
	LoginType = @LoginType
	where PackageId = @Id
	End

	if(LEN(@VideoURL) > 0 AND @VideoId > 0)
	Begin
	update Videos
	set VideoURL = @VideoURL,
	ThumbnailURL= @ThumbNailURL,
	MovementName = @PackageName,
	updatedby = @CreatedBy,
	updatedate = getutcdate(),
	LoginType = @LoginType
	where ID = @VideoId
	End
	else if(LEN(@VideoURL) > 0 AND @VideoId = 0)
	Begin
	insert into videos(MovementName,ActorName,[Format],VideoURL,ThumbNailURL,[Status],CreatedBy,CreateDate,PlayCount,VideoType, SportId, LoginType)
values(@PackageName,'Package','mp4',@VideoURL,@ThumbNailURL,1,@CreatedBy,GetUtcDate(),30,3, @SportId, @LoginType)

set @videoId = @@Identity
delete from packageintrovideos where packageid = @Id and IsIntroVideo = 1
insert into packageintrovideos(VideoId,PackageId,CreatedDate,IsIntroVideo)
values(@videoId,@Id,GetUtcDate(),1)
	End
select @Id as result
END

GO


ALTER PROCEDURE [dbo].[CreatePackage]
	-- Add the parameters for the stored procedure here
	@VideoURL varchar(1024),
	@CreatedBy int,
	@ThumbNailURL varchar(1024),
	@PackageName varchar(256),
	@Description nvarchar(max),
	@PricingCatalogId int,
	@Id int,
	@SportId int,
	@LoginType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @packageId int
	declare @videoId int
    -- Insert statements for procedure here
	if(@PricingCatalogId = 0)
	Begin
	insert into package(PackageName,LibraryId,CreatedDate,[Description], PricingCatalogId,IsPaid, SportId, LoginType)
values(@PackageName,@CreatedBy,GetUtcDate(),@Description,14,1,@SportId, @LoginType)
set @packageId = @@Identity
End
else
Begin
insert into package(PackageName,LibraryId,CreatedDate,[Description], PricingCatalogId,IsPaid, SportId, LoginType)
values(@PackageName,@CreatedBy,GetUtcDate(),@Description,15,0,@SportId, @LoginType)
set @packageId = @@Identity
End

if(LEN(@VideoURL) > 0)
Begin
insert into videos(MovementName,ActorName,[Format],VideoURL,ThumbNailURL,[Status],CreatedBy,CreateDate,PlayCount,VideoType,SportId,VideoMode, LoginType)
values(@PackageName,'Package Intro Video','mp4',@VideoURL,@ThumbNailURL,1,@CreatedBy,GetUtcDate(),30,3,@SportId,1, @LoginType)

set @videoId = @@Identity

insert into packageintrovideos(VideoId,PackageId,CreatedDate,IsIntroVideo)
values(@videoId,@packageId,GetUtcDate(),1)
End
else
Begin
insert into packageintrovideos(VideoId,PackageId,CreatedDate,IsIntroVideo)
values(0,@packageId,GetUTCDATE(),1)
End
update libraries
set IsPackageAvailable = 1
where LibraryId = @CreatedBy

select @packageId as result
END

GO

create table PackageRatings
(
Id int Identity(1,1) Primary Key,
PackageId int,
Rating int,
CreatedDate DateTime,
CreatedBy int,
UpdatedBy int,
UpdatedDate DateTime
)


CREATE PROCEDURE [dbo].[PackagesRating] 
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@Rating int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
	declare @packId int
	declare @packcount int

	set @packcount = (select count(*) from packagerating where packageId = @PackageId and CreatedBy = @UserId)

	if(@packcount > 0)
	Begin
	Update packageratings
	set Rating = @Rating, UpdatedBy = @UserId,UpdatedDate=GetUtcDate()
	where PackageId = @PackageId and createdby = @UserId
	end
	else
	Begin
	insert into packageratings(PackageId,Rating,CreatedDate,CreatedBy)
	values(@PackageId,@Rating,GetUtcDate(),@UserId)
	set @Id = @@Identity
	End

	select * from packageratings where packageid = @PackageId
END
GO

alter table package
alter column Rating varchar(50)

ALTER PROCEDURE [dbo].[PackageRating] --7
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@Rating varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	Update Package
	set Rating = @Rating
	where PackageId = @PackageId
	

	select @Rating as result
END



GO


create table ChannelRatings
(
Id int Identity(1,1) Primary Key,
ChannelId int,
Rating int,
CreatedDate DateTime,
CreatedBy int,
UpdatedBy int,
UpdatedDate DateTime
)

CREATE PROCEDURE [dbo].[ChannelsRating] 
	-- Add the parameters for the stored procedure here
	@ChannelId int,
	@Rating int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
	declare @chanId int
	declare @chancount int

	set @chancount = (select count(*) from ChannelRatings where ChannelId = @ChannelId and CreatedBy = @UserId)

	if(@chancount > 0)
	Begin
	Update ChannelRatings
	set Rating = @Rating, UpdatedBy = @UserId,UpdatedDate=GetUtcDate()
	where ChannelId = @ChannelId and createdby = @UserId
	end
	else
	Begin
	insert into ChannelRatings(ChannelId,Rating,CreatedDate,CreatedBy)
	values(@ChannelId,@Rating,GetUtcDate(),@UserId)
	set @Id = @@Identity
	End

	select * from ChannelRatings where ChannelId = @ChannelId
END
GO

alter table Libraries
add Rating varchar(50)


CREATE PROCEDURE [dbo].[ChannelRating] --7
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@Rating varchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
    -- Insert statements for procedure here
	--set @videoId = (select top 1 VideoId from LibraryIntroVideos where LibraryId = @LibraryId and IsIntroVideo = 1 order by ID desc)
	Update Libraries
	set Rating = @Rating
	where LibraryId = @PackageId
	

	select @Rating as result
END



GO


CREATE PROCEDURE [dbo].[GetChannelsRating] 
	-- Add the parameters for the stored procedure here
	@ChannelId int  

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 

	select * from ChannelRatings where ChannelId = @ChannelId
END


GO


 

/****** Object:  StoredProcedure [dbo].[PackagesRating]    Script Date: 1/16/2017 6:09:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[PackagesRating] 
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@Rating int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
	declare @packId int
	declare @packcount int

	set @packcount = (select count(*) from packageratings where packageId = @PackageId and CreatedBy = @UserId)

	if(@packcount > 0)
	Begin
	Update packageratings
	set Rating = @Rating, UpdatedBy = @UserId,UpdatedDate=GetUtcDate()
	where PackageId = @PackageId and createdby = @UserId
	end
	else
	Begin
	insert into packageratings(PackageId,Rating,CreatedDate,CreatedBy)
	values(@PackageId,@Rating,GetUtcDate(),@UserId)
	set @Id = @@Identity
	End

	select * from packageratings where packageid = @PackageId
END

GO

 

/****** Object:  StoredProcedure [dbo].[PackagesRating]    Script Date: 1/16/2017 6:09:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[PackagesRating] 
	-- Add the parameters for the stored procedure here
	@PackageId int,
	@Rating int,
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
	declare @packId int
	declare @packcount int

	set @packcount = (select count(*) from packageratings where packageId = @PackageId and CreatedBy = @UserId)

	if(@packcount > 0)
	Begin
	Update packageratings
	set Rating = @Rating, UpdatedBy = @UserId,UpdatedDate=GetUtcDate()
	where PackageId = @PackageId and createdby = @UserId
	end
	else
	Begin
	insert into packageratings(PackageId,Rating,CreatedDate,CreatedBy)
	values(@PackageId,@Rating,GetUtcDate(),@UserId)
	set @Id = @@Identity
	End

	select * from packageratings where packageid = @PackageId
END

GO




